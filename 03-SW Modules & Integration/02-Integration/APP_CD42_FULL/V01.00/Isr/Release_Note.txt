=============================================================================
                                   FAURECIA
                        Proprietary - Copyright (C) 2012
-----------------------------------------------------------------------------
Release note for ISR driver software module

-----------------------------------------------------------------------------


CHANGES
-------
V01.00 : 10/09/2013
####################

This release contains: 
   Source code: Isr.c/Isr.h (V01.00),
   Sw LLD (EME-13ST002-12118.01),
   Module tests description (EME-13ST002-12119.01)




