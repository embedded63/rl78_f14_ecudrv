/*******************************************************************************
** Copyright (c) 2012 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -----------------------------------------------------------------------------
** File Name : main.h
** -----------------------------------------------------------------------------
**
** Description : Header for main.c
**
** -----------------------------------------------------------------------------
**
** Documentation reference :
**
********************************************************************************
** R E V I S I O N H I S T O R Y
********************************************************************************
** V01.00 27/09/2013
** - First version

********************************************************************************/
#ifndef __main_H
#define __main_H

#include "Std_Types.h"

/************************* Declaration of global types and constants ******************************/
/* Shared varible type between Boot Block and Application */
typedef struct
{
    VAR(uint8, TYPEDEF) EntryBB;
}Core_ECUStType;
/********************************* Declaration of global macros ************************************/
#define ECU_REPROG_CODE             ((uint8)0x5A)
#define NRM_ENTRY_CODE              ((uint8)0x00)

/****************************** Declaration of exported variables *********************************/
extern VAR(Core_ECUStType, AUTOMATIC) Core_ECUSt; /* ECU status during jump from Application to Boot*/

/* temperory global variables */
extern VAR(uint8, CDD_VAR) MTRCMD_ResetHes1_bl;
extern VAR(uint8, CDD_VAR) MTRCMD_ResetHes2_bl;

extern uint8   SYSMD_KeepAlive_bl;
extern uint8   MTRCMD_MtrRlyR3R_P_bl;
extern uint8   MTRCMD_MtrRlyCom_bl;
extern uint8   MTRCMD_MtrRlyR3L_P_bl;
extern uint8   MTRCMD_GlobalMtrRly_bl;
extern uint8   MTRCMD_HesFetEnable_bl;

extern uint8   PWMCMD_DutyCycle1_u8;
extern uint8   PWMCMD_DutyCycle2_u8;
extern uint8   PWMCMD_DutyCycle3_u8;
extern uint8   PWMCMD_DutyCycle4_u8;
extern uint8   PWMCMD_DutyCycle5_u8;
extern uint8   PWMCMD_DutyCycle6_u8;
extern uint8   PWMCMD_DutyCycle7_u8;

/****************************** Declaration of exported constants *********************************/

/***************************************************************************************************
**                                      FUNCTIONS                                                 **
***************************************************************************************************/

/**************************** Internal functions declarations *************************************/

/******************************** Function definitions ********************************************/

/**************************** Internal Function definitions ***************************************/
#endif /* __main_H */
