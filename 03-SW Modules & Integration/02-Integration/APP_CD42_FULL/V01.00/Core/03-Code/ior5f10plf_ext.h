/*-------------------------------------------------------------------------
 *      Declarations of extended SFR registers for RL78 microcontroller R5F10PLF.
 *
 *      This file can be used by both the RL78,
 *      Assembler, ARL78, and the C/C++ compiler, ICCRL78.
 *
 *      This header file is generated from the device file:
 *          DR5F10PLF.DVF
 *          Copyright(C) 2013 Renesas
 *          Format version V3.00, File version V1.00
 *-------------------------------------------------------------------------*/

#ifndef __IOR5F10PLF_EXT_H__
#define __IOR5F10PLF_EXT_H__

#if !defined(__ARL78__) && !defined(__ICCRL78__)
  #error "IOR5F10PLF_EXT.H file for use with RL78 Assembler or Compiler only"
#endif

#if defined(__ARL78__)
  #if __CORE__ != __RL78_2__
    #error "IOR5F10PLF_EXT.H file for use with ARL78 option --core rl78_2 only"
  #endif
#endif
#if defined(__ICCRL78__)
  #if __CORE__ != __RL78_2__
    #error "IOR5F10PLF_EXT.H file for use with ICCRL78 option --core rl78_2 only"
  #endif
#endif

#ifdef __IAR_SYSTEMS_ICC__

#pragma system_include

#pragma language=save
#pragma language=extended

/*----------------------------------------------
 * SFR bits (default names)
 *--------------------------------------------*/

#ifndef __RL78_BIT_STRUCTURE__
  #define __RL78_BIT_STRUCTURE__
  typedef struct
  {
    unsigned char no0:1;
    unsigned char no1:1;
    unsigned char no2:1;
    unsigned char no3:1;
    unsigned char no4:1;
    unsigned char no5:1;
    unsigned char no6:1;
    unsigned char no7:1;
  } __BITS8;
#endif

#ifndef __RL78_BIT_STRUCTURE2__
  #define __RL78_BIT_STRUCTURE2__
  typedef struct
  {
    unsigned short no0:1;
    unsigned short no1:1;
    unsigned short no2:1;
    unsigned short no3:1;
    unsigned short no4:1;
    unsigned short no5:1;
    unsigned short no6:1;
    unsigned short no7:1;
    unsigned short no8:1;
    unsigned short no9:1;
    unsigned short no10:1;
    unsigned short no11:1;
    unsigned short no12:1;
    unsigned short no13:1;
    unsigned short no14:1;
    unsigned short no15:1;
  } __BITS16;
#endif

/*----------------------------------------------
 *       Extended SFR declarations
 *--------------------------------------------*/

__near __no_init volatile union { unsigned char ADM2; __BITS8 ADM2_bit; } @ 0xF0010;
__near __no_bit_access __no_init volatile union { unsigned char ADUL; __BITS8 ADUL_bit; } @ 0xF0011;
__near __no_bit_access __no_init volatile union { unsigned char ADLL; __BITS8 ADLL_bit; } @ 0xF0012;
__near __no_bit_access __no_init volatile union { unsigned char ADTES; __BITS8 ADTES_bit; } @ 0xF0013;
__near __no_bit_access __no_init volatile union { unsigned char PIOR0; __BITS8 PIOR0_bit; } @ 0xF0016;
__near __no_bit_access __no_init volatile union { unsigned char PIOR1; __BITS8 PIOR1_bit; } @ 0xF0017;
__near __no_bit_access __no_init volatile union { unsigned char PIOR4; __BITS8 PIOR4_bit; } @ 0xF001A;
__near __no_bit_access __no_init volatile union { unsigned char PIOR5; __BITS8 PIOR5_bit; } @ 0xF001B;
__near __no_bit_access __no_init volatile union { unsigned char PIOR7; __BITS8 PIOR7_bit; } @ 0xF001D;
__near __no_init volatile union { unsigned char PITHL1; __BITS8 PITHL1_bit; } @ 0xF0021;
__near __no_init volatile union { unsigned char PITHL3; __BITS8 PITHL3_bit; } @ 0xF0023;
__near __no_init volatile union { unsigned char PITHL4; __BITS8 PITHL4_bit; } @ 0xF0024;
__near __no_init volatile union { unsigned char PITHL5; __BITS8 PITHL5_bit; } @ 0xF0025;
__near __no_init volatile union { unsigned char PITHL6; __BITS8 PITHL6_bit; } @ 0xF0026;
__near __no_init volatile union { unsigned char PITHL7; __BITS8 PITHL7_bit; } @ 0xF0027;
__near __no_init volatile union { unsigned char PITHL12; __BITS8 PITHL12_bit; } @ 0xF002C;
__near __no_init volatile union { unsigned char PU0; __BITS8 PU0_bit; } @ 0xF0030;
__near __no_init volatile union { unsigned char PU1; __BITS8 PU1_bit; } @ 0xF0031;
__near __no_init volatile union { unsigned char PU3; __BITS8 PU3_bit; } @ 0xF0033;
__near __no_init volatile union { unsigned char PU4; __BITS8 PU4_bit; } @ 0xF0034;
__near __no_init volatile union { unsigned char PU6; __BITS8 PU6_bit; } @ 0xF0036;
__near __no_init volatile union { unsigned char PU7; __BITS8 PU7_bit; } @ 0xF0037;
__near __no_init volatile union { unsigned char PU9; __BITS8 PU9_bit; } @ 0xF0039;
__near __no_init volatile union { unsigned char PU12; __BITS8 PU12_bit; } @ 0xF003C;
__near __no_init volatile union { unsigned char PU14; __BITS8 PU14_bit; } @ 0xF003E;
__near __no_init volatile union { unsigned char PIM1; __BITS8 PIM1_bit; } @ 0xF0041;
__near __no_init volatile union { unsigned char PIM3; __BITS8 PIM3_bit; } @ 0xF0043;
__near __no_init volatile union { unsigned char PIM6; __BITS8 PIM6_bit; } @ 0xF0046;
__near __no_init volatile union { unsigned char PIM7; __BITS8 PIM7_bit; } @ 0xF0047;
__near __no_init volatile union { unsigned char PIM12; __BITS8 PIM12_bit; } @ 0xF004C;
__near __no_init volatile union { unsigned char POM1; __BITS8 POM1_bit; } @ 0xF0051;
__near __no_init volatile union { unsigned char POM6; __BITS8 POM6_bit; } @ 0xF0056;
__near __no_init volatile union { unsigned char POM7; __BITS8 POM7_bit; } @ 0xF0057;
__near __no_init volatile union { unsigned char POM12; __BITS8 POM12_bit; } @ 0xF005C;
__near __no_init volatile union { unsigned char PMC9; __BITS8 PMC9_bit; } @ 0xF0069;
__near __no_init volatile union { unsigned char PMC12; __BITS8 PMC12_bit; } @ 0xF006C;
__near __no_init volatile union { unsigned char NFEN0; __BITS8 NFEN0_bit; } @ 0xF0070;
__near __no_init volatile union { unsigned char NFEN1; __BITS8 NFEN1_bit; } @ 0xF0071;
__near __no_init volatile union { unsigned char NFEN2; __BITS8 NFEN2_bit; } @ 0xF0072;
__near __no_init volatile union { unsigned char ISC; __BITS8 ISC_bit; } @ 0xF0073;
__near __no_bit_access __no_init volatile union { unsigned char TIS0; __BITS8 TIS0_bit; } @ 0xF0074;
__near __no_bit_access __no_init volatile union { unsigned char TIS1; __BITS8 TIS1_bit; } @ 0xF0075;
__near __no_bit_access __no_init volatile union { unsigned char ADPC; __BITS8 ADPC_bit; } @ 0xF0076;
__near __no_init volatile union { unsigned char PMS; __BITS8 PMS_bit; } @ 0xF0077;
__near __no_bit_access __no_init volatile union { unsigned char IAWCTL; __BITS8 IAWCTL_bit; } @ 0xF0078;
__near __no_bit_access __no_init volatile union { unsigned char INTFLG0; __BITS8 INTFLG0_bit; } @ 0xF0079;
__near __no_bit_access __no_init volatile union { unsigned char LCHSEL; __BITS8 LCHSEL_bit; } @ 0xF007B;
__near __no_bit_access __no_init volatile union { unsigned char INTMSK; __BITS8 INTMSK_bit; } @ 0xF007C;
__near __no_init volatile union { unsigned char DFLCTL; __BITS8 DFLCTL_bit; } @ 0xF0090;
__near __no_bit_access __no_init volatile union { unsigned char HIOTRM; __BITS8 HIOTRM_bit; } @ 0xF00A0;
__near __no_bit_access __no_init volatile union { unsigned char HOCODIV; __BITS8 HOCODIV_bit; } @ 0xF00A8;
__near __no_bit_access __no_init volatile union { unsigned char SPMCTRL; __BITS8 SPMCTRL_bit; } @ 0xF00D8;
__near __no_bit_access __no_init volatile union { unsigned short SPOFR; __BITS16 SPOFR_bit; } @ 0xF00DA;
__near __no_bit_access __no_init volatile union { unsigned short SPUFR; __BITS16 SPUFR_bit; } @ 0xF00DC;
__near __no_init volatile union { unsigned char PER0; __BITS8 PER0_bit; } @ 0xF00F0;
__near __no_bit_access __no_init volatile union { unsigned char OSMC; __BITS8 OSMC_bit; } @ 0xF00F3;
__near __no_bit_access __no_init volatile const union { unsigned char BCDADJ; __BITS8 BCDADJ_bit; } @ 0xF00FE;
__near __no_bit_access __no_init volatile const union { unsigned short SSR00; __BITS16 SSR00_bit; struct { union { unsigned char SSR00L; __BITS8 SSR00L_bit; }; }; } @ 0xF0100;
__near __no_bit_access __no_init volatile const union { unsigned short SSR01; __BITS16 SSR01_bit; struct { union { unsigned char SSR01L; __BITS8 SSR01L_bit; }; }; } @ 0xF0102;
__near __no_bit_access __no_init volatile union { unsigned short SIR00; __BITS16 SIR00_bit; struct { union { unsigned char SIR00L; __BITS8 SIR00L_bit; }; }; } @ 0xF0104;
__near __no_bit_access __no_init volatile union { unsigned short SIR01; __BITS16 SIR01_bit; struct { union { unsigned char SIR01L; __BITS8 SIR01L_bit; }; }; } @ 0xF0106;
__near __no_bit_access __no_init volatile union { unsigned short SMR00; __BITS16 SMR00_bit; } @ 0xF0108;
__near __no_bit_access __no_init volatile union { unsigned short SMR01; __BITS16 SMR01_bit; } @ 0xF010A;
__near __no_bit_access __no_init volatile union { unsigned short SCR00; __BITS16 SCR00_bit; } @ 0xF010C;
__near __no_bit_access __no_init volatile union { unsigned short SCR01; __BITS16 SCR01_bit; } @ 0xF010E;
__near __no_init volatile const union { unsigned short SE0; struct { union { unsigned char SE0L; __BITS8 SE0L_bit; }; }; } @ 0xF0110;
__near __no_init volatile union { unsigned short SS0; struct { union { unsigned char SS0L; __BITS8 SS0L_bit; }; }; } @ 0xF0112;
__near __no_init volatile union { unsigned short ST0; struct { union { unsigned char ST0L; __BITS8 ST0L_bit; }; }; } @ 0xF0114;
__near __no_bit_access __no_init volatile union { unsigned short SPS0; __BITS16 SPS0_bit; struct { union { unsigned char SPS0L; __BITS8 SPS0L_bit; }; }; } @ 0xF0116;
__near __no_bit_access __no_init volatile union { unsigned short SO0; __BITS16 SO0_bit; } @ 0xF0118;
__near __no_init volatile union { unsigned short SOE0; struct { union { unsigned char SOE0L; __BITS8 SOE0L_bit; }; }; } @ 0xF011A;
__near __no_bit_access __no_init volatile union { unsigned short SOL0; __BITS16 SOL0_bit; struct { union { unsigned char SOL0L; __BITS8 SOL0L_bit; }; }; } @ 0xF0120;
__near __no_bit_access __no_init volatile union { unsigned short SSE0; __BITS16 SSE0_bit; struct { union { unsigned char SSE0L; __BITS8 SSE0L_bit; }; }; } @ 0xF0122;
__near __no_bit_access __no_init volatile const union { unsigned short SSR10; __BITS16 SSR10_bit; struct { union { unsigned char SSR10L; __BITS8 SSR10L_bit; }; }; } @ 0xF0140;
__near __no_bit_access __no_init volatile const union { unsigned short SSR11; __BITS16 SSR11_bit; struct { union { unsigned char SSR11L; __BITS8 SSR11L_bit; }; }; } @ 0xF0142;
__near __no_bit_access __no_init volatile union { unsigned short SIR10; __BITS16 SIR10_bit; struct { union { unsigned char SIR10L; __BITS8 SIR10L_bit; }; }; } @ 0xF0144;
__near __no_bit_access __no_init volatile union { unsigned short SIR11; __BITS16 SIR11_bit; struct { union { unsigned char SIR11L; __BITS8 SIR11L_bit; }; }; } @ 0xF0146;
__near __no_bit_access __no_init volatile union { unsigned short SMR10; __BITS16 SMR10_bit; } @ 0xF0148;
__near __no_bit_access __no_init volatile union { unsigned short SMR11; __BITS16 SMR11_bit; } @ 0xF014A;
__near __no_bit_access __no_init volatile union { unsigned short SCR10; __BITS16 SCR10_bit; } @ 0xF014C;
__near __no_bit_access __no_init volatile union { unsigned short SCR11; __BITS16 SCR11_bit; } @ 0xF014E;
__near __no_init volatile const union { unsigned short SE1; struct { union { unsigned char SE1L; __BITS8 SE1L_bit; }; }; } @ 0xF0150;
__near __no_init volatile union { unsigned short SS1; struct { union { unsigned char SS1L; __BITS8 SS1L_bit; }; }; } @ 0xF0152;
__near __no_init volatile union { unsigned short ST1; struct { union { unsigned char ST1L; __BITS8 ST1L_bit; }; }; } @ 0xF0154;
__near __no_bit_access __no_init volatile union { unsigned short SPS1; __BITS16 SPS1_bit; struct { union { unsigned char SPS1L; __BITS8 SPS1L_bit; }; }; } @ 0xF0156;
__near __no_bit_access __no_init volatile union { unsigned short SO1; __BITS16 SO1_bit; } @ 0xF0158;
__near __no_init volatile union { unsigned short SOE1; struct { union { unsigned char SOE1L; __BITS8 SOE1L_bit; }; }; } @ 0xF015A;
__near __no_bit_access __no_init volatile union { unsigned short SOL1; __BITS16 SOL1_bit; struct { union { unsigned char SOL1L; __BITS8 SOL1L_bit; }; }; } @ 0xF0160;
__near __no_bit_access __no_init volatile union { unsigned short SSE1; __BITS16 SSE1_bit; struct { union { unsigned char SSE1L; __BITS8 SSE1L_bit; }; }; } @ 0xF0162;
__near __no_bit_access __no_init volatile const union { unsigned short TCR00; __BITS16 TCR00_bit; } @ 0xF0180;
__near __no_bit_access __no_init volatile const union { unsigned short TCR01; __BITS16 TCR01_bit; } @ 0xF0182;
__near __no_bit_access __no_init volatile const union { unsigned short TCR02; __BITS16 TCR02_bit; } @ 0xF0184;
__near __no_bit_access __no_init volatile const union { unsigned short TCR03; __BITS16 TCR03_bit; } @ 0xF0186;
__near __no_bit_access __no_init volatile const union { unsigned short TCR04; __BITS16 TCR04_bit; } @ 0xF0188;
__near __no_bit_access __no_init volatile const union { unsigned short TCR05; __BITS16 TCR05_bit; } @ 0xF018A;
__near __no_bit_access __no_init volatile const union { unsigned short TCR06; __BITS16 TCR06_bit; } @ 0xF018C;
__near __no_bit_access __no_init volatile const union { unsigned short TCR07; __BITS16 TCR07_bit; } @ 0xF018E;
__near __no_bit_access __no_init volatile union { unsigned short TMR00; __BITS16 TMR00_bit; } @ 0xF0190;
__near __no_bit_access __no_init volatile union { unsigned short TMR01; __BITS16 TMR01_bit; } @ 0xF0192;
__near __no_bit_access __no_init volatile union { unsigned short TMR02; __BITS16 TMR02_bit; } @ 0xF0194;
__near __no_bit_access __no_init volatile union { unsigned short TMR03; __BITS16 TMR03_bit; } @ 0xF0196;
__near __no_bit_access __no_init volatile union { unsigned short TMR04; __BITS16 TMR04_bit; } @ 0xF0198;
__near __no_bit_access __no_init volatile union { unsigned short TMR05; __BITS16 TMR05_bit; } @ 0xF019A;
__near __no_bit_access __no_init volatile union { unsigned short TMR06; __BITS16 TMR06_bit; } @ 0xF019C;
__near __no_bit_access __no_init volatile union { unsigned short TMR07; __BITS16 TMR07_bit; } @ 0xF019E;
__near __no_bit_access __no_init volatile const union { unsigned short TSR00; __BITS16 TSR00_bit; struct { union { unsigned char TSR00L; __BITS8 TSR00L_bit; }; }; } @ 0xF01A0;
__near __no_bit_access __no_init volatile const union { unsigned short TSR01; __BITS16 TSR01_bit; struct { union { unsigned char TSR01L; __BITS8 TSR01L_bit; }; }; } @ 0xF01A2;
__near __no_bit_access __no_init volatile const union { unsigned short TSR02; __BITS16 TSR02_bit; struct { union { unsigned char TSR02L; __BITS8 TSR02L_bit; }; }; } @ 0xF01A4;
__near __no_bit_access __no_init volatile const union { unsigned short TSR03; __BITS16 TSR03_bit; struct { union { unsigned char TSR03L; __BITS8 TSR03L_bit; }; }; } @ 0xF01A6;
__near __no_bit_access __no_init volatile const union { unsigned short TSR04; __BITS16 TSR04_bit; struct { union { unsigned char TSR04L; __BITS8 TSR04L_bit; }; }; } @ 0xF01A8;
__near __no_bit_access __no_init volatile const union { unsigned short TSR05; __BITS16 TSR05_bit; struct { union { unsigned char TSR05L; __BITS8 TSR05L_bit; }; }; } @ 0xF01AA;
__near __no_bit_access __no_init volatile const union { unsigned short TSR06; __BITS16 TSR06_bit; struct { union { unsigned char TSR06L; __BITS8 TSR06L_bit; }; }; } @ 0xF01AC;
__near __no_bit_access __no_init volatile const union { unsigned short TSR07; __BITS16 TSR07_bit; struct { union { unsigned char TSR07L; __BITS8 TSR07L_bit; }; }; } @ 0xF01AE;
__near __no_init volatile const union { unsigned short TE0; struct { union { unsigned char TE0L; __BITS8 TE0L_bit; }; }; } @ 0xF01B0;
__near __no_init volatile union { unsigned short TS0; struct { union { unsigned char TS0L; __BITS8 TS0L_bit; }; }; } @ 0xF01B2;
__near __no_init volatile union { unsigned short TT0; struct { union { unsigned char TT0L; __BITS8 TT0L_bit; }; }; } @ 0xF01B4;
__near __no_bit_access __no_init volatile union { unsigned short TPS0; __BITS16 TPS0_bit; } @ 0xF01B6;
__near __no_bit_access __no_init volatile union { unsigned short TO0; __BITS16 TO0_bit; struct { union { unsigned char TO0L; __BITS8 TO0L_bit; }; }; } @ 0xF01B8;
__near __no_init volatile union { unsigned short TOE0; struct { union { unsigned char TOE0L; __BITS8 TOE0L_bit; }; }; } @ 0xF01BA;
__near __no_bit_access __no_init volatile union { unsigned short TOL0; __BITS16 TOL0_bit; struct { union { unsigned char TOL0L; __BITS8 TOL0L_bit; }; }; } @ 0xF01BC;
__near __no_bit_access __no_init volatile union { unsigned short TOM0; __BITS16 TOM0_bit; struct { union { unsigned char TOM0L; __BITS8 TOM0L_bit; }; }; } @ 0xF01BE;
__near __no_bit_access __no_init volatile const union { unsigned short TCR10; __BITS16 TCR10_bit; } @ 0xF01C0;
__near __no_bit_access __no_init volatile const union { unsigned short TCR11; __BITS16 TCR11_bit; } @ 0xF01C2;
__near __no_bit_access __no_init volatile const union { unsigned short TCR12; __BITS16 TCR12_bit; } @ 0xF01C4;
__near __no_bit_access __no_init volatile const union { unsigned short TCR13; __BITS16 TCR13_bit; } @ 0xF01C6;
__near __no_bit_access __no_init volatile union { unsigned short TMR10; __BITS16 TMR10_bit; } @ 0xF01D0;
__near __no_bit_access __no_init volatile union { unsigned short TMR11; __BITS16 TMR11_bit; } @ 0xF01D2;
__near __no_bit_access __no_init volatile union { unsigned short TMR12; __BITS16 TMR12_bit; } @ 0xF01D4;
__near __no_bit_access __no_init volatile union { unsigned short TMR13; __BITS16 TMR13_bit; } @ 0xF01D6;
__near __no_bit_access __no_init volatile const union { unsigned short TSR10; __BITS16 TSR10_bit; struct { union { unsigned char TSR10L; __BITS8 TSR10L_bit; }; }; } @ 0xF01E0;
__near __no_bit_access __no_init volatile const union { unsigned short TSR11; __BITS16 TSR11_bit; struct { union { unsigned char TSR11L; __BITS8 TSR11L_bit; }; }; } @ 0xF01E2;
__near __no_bit_access __no_init volatile const union { unsigned short TSR12; __BITS16 TSR12_bit; struct { union { unsigned char TSR12L; __BITS8 TSR12L_bit; }; }; } @ 0xF01E4;
__near __no_bit_access __no_init volatile const union { unsigned short TSR13; __BITS16 TSR13_bit; struct { union { unsigned char TSR13L; __BITS8 TSR13L_bit; }; }; } @ 0xF01E6;
__near __no_init volatile const union { unsigned short TE1; struct { union { unsigned char TE1L; __BITS8 TE1L_bit; }; }; } @ 0xF01F0;
__near __no_init volatile union { unsigned short TS1; struct { union { unsigned char TS1L; __BITS8 TS1L_bit; }; }; } @ 0xF01F2;
__near __no_init volatile union { unsigned short TT1; struct { union { unsigned char TT1L; __BITS8 TT1L_bit; }; }; } @ 0xF01F4;
__near __no_bit_access __no_init volatile union { unsigned short TPS1; __BITS16 TPS1_bit; } @ 0xF01F6;
__near __no_bit_access __no_init volatile union { unsigned short TO1; __BITS16 TO1_bit; struct { union { unsigned char TO1L; __BITS8 TO1L_bit; }; }; } @ 0xF01F8;
__near __no_init volatile union { unsigned short TOE1; struct { union { unsigned char TOE1L; __BITS8 TOE1L_bit; }; }; } @ 0xF01FA;
__near __no_bit_access __no_init volatile union { unsigned short TOL1; __BITS16 TOL1_bit; struct { union { unsigned char TOL1L; __BITS8 TOL1L_bit; }; }; } @ 0xF01FC;
__near __no_bit_access __no_init volatile union { unsigned short TOM1; __BITS16 TOM1_bit; struct { union { unsigned char TOM1L; __BITS8 TOM1L_bit; }; }; } @ 0xF01FE;
__near __no_bit_access __no_init volatile const union { unsigned short ERADR; __BITS16 ERADR_bit; } @ 0xF0200;
__near __no_bit_access __no_init volatile union { unsigned char ECCIER; __BITS8 ECCIER_bit; } @ 0xF0202;
__near __no_bit_access __no_init volatile union { unsigned char ECCER; __BITS8 ECCER_bit; } @ 0xF0203;
__near __no_bit_access __no_init volatile union { unsigned char ECCTPR; __BITS8 ECCTPR_bit; } @ 0xF0204;
__near __no_bit_access __no_init volatile union { unsigned char ECCTMDR; __BITS8 ECCTMDR_bit; } @ 0xF0205;
__near __no_bit_access __no_init volatile union { unsigned short ECCDWRVR; __BITS16 ECCDWRVR_bit; } @ 0xF0206;
__near __no_init volatile union { unsigned char PSRSEL; __BITS8 PSRSEL_bit; } @ 0xF0220;
__near __no_init volatile union { unsigned char PSNZCNT0; __BITS8 PSNZCNT0_bit; } @ 0xF0222;
__near __no_init volatile union { unsigned char PSNZCNT1; __BITS8 PSNZCNT1_bit; } @ 0xF0223;
__near __no_init volatile union { unsigned char PSNZCNT2; __BITS8 PSNZCNT2_bit; } @ 0xF0224;
__near __no_init volatile union { unsigned char PSNZCNT3; __BITS8 PSNZCNT3_bit; } @ 0xF0225;
__near __no_init volatile union { unsigned char DAM2; __BITS8 DAM2_bit; } @ 0xF0227;
__near __no_bit_access __no_init volatile union { unsigned short PWMDLY0; __BITS16 PWMDLY0_bit; } @ 0xF0228;
__near __no_bit_access __no_init volatile union { unsigned short PWMDLY1; __BITS16 PWMDLY1_bit; } @ 0xF022A;
__near __no_bit_access __no_init volatile union { unsigned short PWMDLY2; __BITS16 PWMDLY2_bit; } @ 0xF022C;
__near __no_init volatile union { unsigned char IICCTL00; __BITS8 IICCTL00_bit; } @ 0xF0230;
__near __no_init volatile union { unsigned char IICCTL01; __BITS8 IICCTL01_bit; } @ 0xF0231;
__near __no_bit_access __no_init volatile union { unsigned char IICWL0; __BITS8 IICWL0_bit; } @ 0xF0232;
__near __no_bit_access __no_init volatile union { unsigned char IICWH0; __BITS8 IICWH0_bit; } @ 0xF0233;
__near __no_bit_access __no_init volatile union { unsigned char SVA0; __BITS8 SVA0_bit; } @ 0xF0234;
__near __no_bit_access __no_init volatile union { unsigned char TRJCR0; __BITS8 TRJCR0_bit; } @ 0xF0240;
__near __no_init volatile union { unsigned char TRJIOC0; __BITS8 TRJIOC0_bit; } @ 0xF0241;
__near __no_init volatile union { unsigned char TRJMR0; __BITS8 TRJMR0_bit; } @ 0xF0242;
__near __no_init volatile union { unsigned char TRJISR0; __BITS8 TRJISR0_bit; } @ 0xF0243;
__near __no_init volatile union { unsigned char TRDELC; __BITS8 TRDELC_bit; } @ 0xF0260;
__near __no_bit_access __no_init volatile union { unsigned char TRDSTR; __BITS8 TRDSTR_bit; } @ 0xF0263;
__near __no_init volatile union { unsigned char TRDMR; __BITS8 TRDMR_bit; } @ 0xF0264;
__near __no_init volatile union { unsigned char TRDPMR; __BITS8 TRDPMR_bit; } @ 0xF0265;
__near __no_init volatile union { unsigned char TRDFCR; __BITS8 TRDFCR_bit; } @ 0xF0266;
__near __no_init volatile union { unsigned char TRDOER1; __BITS8 TRDOER1_bit; } @ 0xF0267;
__near __no_init volatile union { unsigned char TRDOER2; __BITS8 TRDOER2_bit; } @ 0xF0268;
__near __no_init volatile union { unsigned char TRDOCR; __BITS8 TRDOCR_bit; } @ 0xF0269;
__near __no_init volatile union { unsigned char TRDDF0; __BITS8 TRDDF0_bit; } @ 0xF026A;
__near __no_init volatile union { unsigned char TRDDF1; __BITS8 TRDDF1_bit; } @ 0xF026B;
__near __no_init volatile union { unsigned char TRDCR0; __BITS8 TRDCR0_bit; } @ 0xF0270;
__near __no_init volatile union { unsigned char TRDIORA0; __BITS8 TRDIORA0_bit; } @ 0xF0271;
__near __no_init volatile union { unsigned char TRDIORC0; __BITS8 TRDIORC0_bit; } @ 0xF0272;
__near __no_init volatile union { unsigned char TRDSR0; __BITS8 TRDSR0_bit; } @ 0xF0273;
__near __no_init volatile union { unsigned char TRDIER0; __BITS8 TRDIER0_bit; } @ 0xF0274;
__near __no_init volatile union { unsigned char TRDPOCR0; __BITS8 TRDPOCR0_bit; } @ 0xF0275;
__near __no_bit_access __no_init volatile union { unsigned short TRD0; __BITS16 TRD0_bit; } @ 0xF0276;
__near __no_bit_access __no_init volatile union { unsigned short TRDGRA0; __BITS16 TRDGRA0_bit; } @ 0xF0278;
__near __no_bit_access __no_init volatile union { unsigned short TRDGRB0; __BITS16 TRDGRB0_bit; } @ 0xF027A;
__near __no_init volatile union { unsigned char TRDCR1; __BITS8 TRDCR1_bit; } @ 0xF0280;
__near __no_init volatile union { unsigned char TRDIORA1; __BITS8 TRDIORA1_bit; } @ 0xF0281;
__near __no_init volatile union { unsigned char TRDIORC1; __BITS8 TRDIORC1_bit; } @ 0xF0282;
__near __no_init volatile union { unsigned char TRDSR1; __BITS8 TRDSR1_bit; } @ 0xF0283;
__near __no_init volatile union { unsigned char TRDIER1; __BITS8 TRDIER1_bit; } @ 0xF0284;
__near __no_init volatile union { unsigned char TRDPOCR1; __BITS8 TRDPOCR1_bit; } @ 0xF0285;
__near __no_bit_access __no_init volatile union { unsigned short TRD1; __BITS16 TRD1_bit; } @ 0xF0286;
__near __no_bit_access __no_init volatile union { unsigned short TRDGRA1; __BITS16 TRDGRA1_bit; } @ 0xF0288;
__near __no_bit_access __no_init volatile union { unsigned short TRDGRB1; __BITS16 TRDGRB1_bit; } @ 0xF028A;
__near __no_init volatile union { unsigned char CMPCTL; __BITS8 CMPCTL_bit; } @ 0xF02A0;
__near __no_init volatile union { unsigned char CMPSEL; __BITS8 CMPSEL_bit; } @ 0xF02A1;
__near __no_init volatile const union { unsigned char CMPMON; __BITS8 CMPMON_bit; } @ 0xF02A2;
__near __no_init volatile union { unsigned char PER1; __BITS8 PER1_bit; } @ 0xF02C0;
__near __no_init volatile union { unsigned char PER2; __BITS8 PER2_bit; } @ 0xF02C1;
__near __no_init volatile union { unsigned char CANCKSEL; __BITS8 CANCKSEL_bit; } @ 0xF02C2;
__near __no_init volatile union { unsigned char LINCKSEL; __BITS8 LINCKSEL_bit; } @ 0xF02C3;
__near __no_init volatile union { unsigned char CKSEL; __BITS8 CKSEL_bit; } @ 0xF02C4;
__near __no_init volatile union { unsigned char PLLCTL; __BITS8 PLLCTL_bit; } @ 0xF02C5;
__near __no_init volatile const union { unsigned char PLLSTS; __BITS8 PLLSTS_bit; } @ 0xF02C6;
__near __no_bit_access __no_init volatile union { unsigned char MDIV; __BITS8 MDIV_bit; } @ 0xF02C7;
__near __no_init volatile union { unsigned char RTCCL; __BITS8 RTCCL_bit; } @ 0xF02C8;
__near __no_init volatile union { unsigned char POCRES; __BITS8 POCRES_bit; } @ 0xF02C9;
__near __no_init volatile union { unsigned char STPSTC; __BITS8 STPSTC_bit; } @ 0xF02CA;
__near __no_init volatile union { unsigned char HDTCCR0; __BITS8 HDTCCR0_bit; } @ 0xF02D0;
__near __no_init volatile union { unsigned char HDTCCT0; __BITS8 HDTCCT0_bit; } @ 0xF02D2;
__near __no_init volatile union { unsigned char HDTRLD0; __BITS8 HDTRLD0_bit; } @ 0xF02D3;
__near __no_bit_access __no_init volatile union { unsigned short HDTSAR0; __BITS16 HDTSAR0_bit; } @ 0xF02D4;
__near __no_bit_access __no_init volatile union { unsigned short HDTDAR0; __BITS16 HDTDAR0_bit; } @ 0xF02D6;
__near __no_init volatile union { unsigned char HDTCCR1; __BITS8 HDTCCR1_bit; } @ 0xF02D8;
__near __no_init volatile union { unsigned char HDTCCT1; __BITS8 HDTCCT1_bit; } @ 0xF02DA;
__near __no_init volatile union { unsigned char HDTRLD1; __BITS8 HDTRLD1_bit; } @ 0xF02DB;
__near __no_bit_access __no_init volatile union { unsigned short HDTSAR1; __BITS16 HDTSAR1_bit; } @ 0xF02DC;
__near __no_bit_access __no_init volatile union { unsigned short HDTDAR1; __BITS16 HDTDAR1_bit; } @ 0xF02DE;
__near __no_bit_access __no_init volatile union { unsigned char DTCBAR; __BITS8 DTCBAR_bit; } @ 0xF02E0;
__near __no_init volatile union { unsigned char SELHS0; __BITS8 SELHS0_bit; } @ 0xF02E1;
__near __no_init volatile union { unsigned char SELHS1; __BITS8 SELHS1_bit; } @ 0xF02E2;
__near __no_init volatile union { unsigned char DTCEN0; __BITS8 DTCEN0_bit; } @ 0xF02E8;
__near __no_init volatile union { unsigned char DTCEN1; __BITS8 DTCEN1_bit; } @ 0xF02E9;
__near __no_init volatile union { unsigned char DTCEN2; __BITS8 DTCEN2_bit; } @ 0xF02EA;
__near __no_init volatile union { unsigned char DTCEN3; __BITS8 DTCEN3_bit; } @ 0xF02EB;
__near __no_init volatile union { unsigned char DTCEN4; __BITS8 DTCEN4_bit; } @ 0xF02EC;
__near __no_init volatile union { unsigned char CRC0CTL; __BITS8 CRC0CTL_bit; } @ 0xF02F0;
__near __no_bit_access __no_init volatile union { unsigned short PGCRCL; __BITS16 PGCRCL_bit; } @ 0xF02F2;
__near __no_bit_access __no_init volatile union { unsigned char CRCMD; __BITS8 CRCMD_bit; } @ 0xF02F9;
__near __no_bit_access __no_init volatile union { unsigned short CRCD; __BITS16 CRCD_bit; } @ 0xF02FA;
__near __no_bit_access __no_init volatile union { unsigned short C0CFGL; __BITS16 C0CFGL_bit; struct { union { unsigned char C0CFGLL; __BITS8 C0CFGLL_bit; }; union { unsigned char C0CFGLH; __BITS8 C0CFGLH_bit; }; }; } @ 0xF0300;
__near __no_bit_access __no_init volatile union { unsigned short C0CFGH; __BITS16 C0CFGH_bit; struct { union { unsigned char C0CFGHL; __BITS8 C0CFGHL_bit; }; union { unsigned char C0CFGHH; __BITS8 C0CFGHH_bit; }; }; } @ 0xF0302;
__near __no_bit_access __no_init volatile union { unsigned short C0CTRL; __BITS16 C0CTRL_bit; struct { union { unsigned char C0CTRLL; __BITS8 C0CTRLL_bit; }; union { unsigned char C0CTRLH; __BITS8 C0CTRLH_bit; }; }; } @ 0xF0304;
__near __no_bit_access __no_init volatile union { unsigned short C0CTRH; __BITS16 C0CTRH_bit; struct { union { unsigned char C0CTRHL; __BITS8 C0CTRHL_bit; }; union { unsigned char C0CTRHH; __BITS8 C0CTRHH_bit; }; }; } @ 0xF0306;
__near __no_bit_access __no_init volatile const union { unsigned short C0STSL; __BITS16 C0STSL_bit; struct { union { const unsigned char C0STSLL; const __BITS8 C0STSLL_bit; }; union { const unsigned char C0STSLH; const __BITS8 C0STSLH_bit; }; }; } @ 0xF0308;
__near __no_bit_access __no_init volatile const union { unsigned short C0STSH; __BITS16 C0STSH_bit; struct { union { const unsigned char C0STSHL; const __BITS8 C0STSHL_bit; }; union { const unsigned char C0STSHH; const __BITS8 C0STSHH_bit; }; }; } @ 0xF030A;
__near __no_bit_access __no_init volatile union { unsigned short C0ERFLL; __BITS16 C0ERFLL_bit; struct { union { unsigned char C0ERFLLL; __BITS8 C0ERFLLL_bit; }; union { unsigned char C0ERFLLH; __BITS8 C0ERFLLH_bit; }; }; } @ 0xF030C;
__near __no_bit_access __no_init volatile const union { unsigned short C0ERFLH; __BITS16 C0ERFLH_bit; struct { union { const unsigned char C0ERFLHL; const __BITS8 C0ERFLHL_bit; }; union { const unsigned char C0ERFLHH; const __BITS8 C0ERFLHH_bit; }; }; } @ 0xF030E;
__near __no_bit_access __no_init volatile union { unsigned short GCFGL; __BITS16 GCFGL_bit; struct { union { unsigned char GCFGLL; __BITS8 GCFGLL_bit; }; union { unsigned char GCFGLH; __BITS8 GCFGLH_bit; }; }; } @ 0xF0322;
__near __no_bit_access __no_init volatile union { unsigned short GCFGH; __BITS16 GCFGH_bit; struct { union { unsigned char GCFGHL; __BITS8 GCFGHL_bit; }; union { unsigned char GCFGHH; __BITS8 GCFGHH_bit; }; }; } @ 0xF0324;
__near __no_bit_access __no_init volatile union { unsigned short GCTRL; __BITS16 GCTRL_bit; struct { union { unsigned char GCTRLL; __BITS8 GCTRLL_bit; }; union { unsigned char GCTRLH; __BITS8 GCTRLH_bit; }; }; } @ 0xF0326;
__near __no_bit_access __no_init volatile union { unsigned short GCTRH; __BITS16 GCTRH_bit; struct { union { unsigned char GCTRHL; __BITS8 GCTRHL_bit; }; union { unsigned char GCTRHH; __BITS8 GCTRHH_bit; }; }; } @ 0xF0328;
__near __no_bit_access __no_init volatile const union { unsigned short GSTS; __BITS16 GSTS_bit; struct { union { const unsigned char GSTSL; const __BITS8 GSTSL_bit; }; union { const unsigned char GSTSH; const __BITS8 GSTSH_bit; }; }; } @ 0xF032A;
__near __no_bit_access __no_init volatile union { unsigned char GERFLL; __BITS8 GERFLL_bit; } @ 0xF032C;
__near __no_bit_access __no_init volatile const union { unsigned short GTSC; __BITS16 GTSC_bit; } @ 0xF032E;
__near __no_bit_access __no_init volatile union { unsigned short GAFLCFG; __BITS16 GAFLCFG_bit; struct { union { unsigned char GAFLCFGL; __BITS8 GAFLCFGL_bit; }; union { unsigned char GAFLCFGH; __BITS8 GAFLCFGH_bit; }; }; } @ 0xF0330;
__near __no_bit_access __no_init volatile union { unsigned short RMNB; __BITS16 RMNB_bit; struct { union { unsigned char RMNBL; __BITS8 RMNBL_bit; }; }; } @ 0xF0332;
__near __no_bit_access __no_init volatile union { unsigned short RMND0; __BITS16 RMND0_bit; struct { union { unsigned char RMND0L; __BITS8 RMND0L_bit; }; union { unsigned char RMND0H; __BITS8 RMND0H_bit; }; }; } @ 0xF0334;
__near __no_bit_access __no_init volatile union { unsigned short RFCC0; __BITS16 RFCC0_bit; struct { union { unsigned char RFCC0L; __BITS8 RFCC0L_bit; }; union { unsigned char RFCC0H; __BITS8 RFCC0H_bit; }; }; } @ 0xF0338;
__near __no_bit_access __no_init volatile union { unsigned short RFCC1; __BITS16 RFCC1_bit; struct { union { unsigned char RFCC1L; __BITS8 RFCC1L_bit; }; union { unsigned char RFCC1H; __BITS8 RFCC1H_bit; }; }; } @ 0xF033A;
__near __no_bit_access __no_init volatile union { unsigned short RFSTS0; __BITS16 RFSTS0_bit; struct { union { unsigned char RFSTS0L; __BITS8 RFSTS0L_bit; }; union { const unsigned char RFSTS0H; const __BITS8 RFSTS0H_bit; }; }; } @ 0xF0340;
__near __no_bit_access __no_init volatile union { unsigned short RFSTS1; __BITS16 RFSTS1_bit; struct { union { unsigned char RFSTS1L; __BITS8 RFSTS1L_bit; }; union { const unsigned char RFSTS1H; const __BITS8 RFSTS1H_bit; }; }; } @ 0xF0342;
__near __no_bit_access __no_init volatile /* write only */ union { unsigned short RFPCTR0; __BITS16 RFPCTR0_bit; struct { union { /* write only */ unsigned char RFPCTR0L; /* write only */ __BITS8 RFPCTR0L_bit; }; union { /* write only */ unsigned char RFPCTR0H; /* write only */ __BITS8 RFPCTR0H_bit; }; }; } @ 0xF0348;
__near __no_bit_access __no_init volatile /* write only */ union { unsigned short RFPCTR1; __BITS16 RFPCTR1_bit; struct { union { /* write only */ unsigned char RFPCTR1L; /* write only */ __BITS8 RFPCTR1L_bit; }; union { /* write only */ unsigned char RFPCTR1H; /* write only */ __BITS8 RFPCTR1H_bit; }; }; } @ 0xF034A;
__near __no_bit_access __no_init volatile union { unsigned short CFCCL0; __BITS16 CFCCL0_bit; struct { union { unsigned char CFCCL0L; __BITS8 CFCCL0L_bit; }; union { unsigned char CFCCL0H; __BITS8 CFCCL0H_bit; }; }; } @ 0xF0350;
__near __no_bit_access __no_init volatile union { unsigned short CFCCH0; __BITS16 CFCCH0_bit; struct { union { unsigned char CFCCH0L; __BITS8 CFCCH0L_bit; }; union { unsigned char CFCCH0H; __BITS8 CFCCH0H_bit; }; }; } @ 0xF0352;
__near __no_bit_access __no_init volatile union { unsigned short CFSTS0; __BITS16 CFSTS0_bit; struct { union { unsigned char CFSTS0L; __BITS8 CFSTS0L_bit; }; union { const unsigned char CFSTS0H; const __BITS8 CFSTS0H_bit; }; }; } @ 0xF0358;
__near __no_bit_access __no_init volatile /* write only */ union { unsigned short CFPCTR0; __BITS16 CFPCTR0_bit; struct { union { unsigned char CFPCTR0L; __BITS8 CFPCTR0L_bit; }; }; } @ 0xF035C;
__near __no_bit_access __no_init volatile const union { unsigned char RFMSTS; __BITS8 RFMSTS_bit; } @ 0xF0360;
__near __no_bit_access __no_init volatile const union { unsigned char CFMSTS; __BITS8 CFMSTS_bit; } @ 0xF0361;
__near __no_bit_access __no_init volatile const union { unsigned char RFISTS; __BITS8 RFISTS_bit; } @ 0xF0362;
__near __no_bit_access __no_init volatile const union { unsigned char CFISTS; __BITS8 CFISTS_bit; } @ 0xF0363;
__near __no_bit_access __no_init volatile union { unsigned char TMC0; __BITS8 TMC0_bit; } @ 0xF0364;
__near __no_bit_access __no_init volatile union { unsigned char TMC1; __BITS8 TMC1_bit; } @ 0xF0365;
__near __no_bit_access __no_init volatile union { unsigned char TMC2; __BITS8 TMC2_bit; } @ 0xF0366;
__near __no_bit_access __no_init volatile union { unsigned char TMC3; __BITS8 TMC3_bit; } @ 0xF0367;
__near __no_bit_access __no_init volatile union { unsigned char TMSTS0; __BITS8 TMSTS0_bit; } @ 0xF036C;
__near __no_bit_access __no_init volatile union { unsigned char TMSTS1; __BITS8 TMSTS1_bit; } @ 0xF036D;
__near __no_bit_access __no_init volatile union { unsigned char TMSTS2; __BITS8 TMSTS2_bit; } @ 0xF036E;
__near __no_bit_access __no_init volatile union { unsigned char TMSTS3; __BITS8 TMSTS3_bit; } @ 0xF036F;
__near __no_bit_access __no_init volatile const union { unsigned short TMTRSTS; __BITS16 TMTRSTS_bit; struct { union { const unsigned char TMTRSTSL; const __BITS8 TMTRSTSL_bit; }; union { const unsigned char TMTRSTSH; const __BITS8 TMTRSTSH_bit; }; }; } @ 0xF0374;
__near __no_bit_access __no_init volatile const union { unsigned short TMTCSTS; __BITS16 TMTCSTS_bit; struct { union { const unsigned char TMTCSTSL; const __BITS8 TMTCSTSL_bit; }; union { const unsigned char TMTCSTSH; const __BITS8 TMTCSTSH_bit; }; }; } @ 0xF0376;
__near __no_bit_access __no_init volatile const union { unsigned short TMTASTS; __BITS16 TMTASTS_bit; struct { union { const unsigned char TMTASTSL; const __BITS8 TMTASTSL_bit; }; union { const unsigned char TMTASTSH; const __BITS8 TMTASTSH_bit; }; }; } @ 0xF0378;
__near __no_bit_access __no_init volatile union { unsigned short TMIEC; __BITS16 TMIEC_bit; struct { union { unsigned char TMIECL; __BITS8 TMIECL_bit; }; union { unsigned char TMIECH; __BITS8 TMIECH_bit; }; }; } @ 0xF037A;
__near __no_bit_access __no_init volatile union { unsigned short THLCC0; __BITS16 THLCC0_bit; struct { union { unsigned char THLCC0L; __BITS8 THLCC0L_bit; }; union { unsigned char THLCC0H; __BITS8 THLCC0H_bit; }; }; } @ 0xF037C;
__near __no_bit_access __no_init volatile union { unsigned short THLSTS0; __BITS16 THLSTS0_bit; struct { union { unsigned char THLSTS0L; __BITS8 THLSTS0L_bit; }; union { unsigned char THLSTS0H; __BITS8 THLSTS0H_bit; }; }; } @ 0xF0380;
__near __no_bit_access __no_init volatile /* write only */ union { unsigned short THLPCTR0; __BITS16 THLPCTR0_bit; struct { union { /* write only */ unsigned char THLPCTR0L; /* write only */ __BITS8 THLPCTR0L_bit; }; union { /* write only */ unsigned char THLPCTR0H; /* write only */ __BITS8 THLPCTR0H_bit; }; }; } @ 0xF0384;
__near __no_bit_access __no_init volatile const union { unsigned short GTINTSTS; __BITS16 GTINTSTS_bit; struct { union { const unsigned char GTINTSTSL; const __BITS8 GTINTSTSL_bit; }; union { const unsigned char GTINTSTSH; const __BITS8 GTINTSTSH_bit; }; }; } @ 0xF0388;
__near __no_bit_access __no_init volatile union { unsigned short GRWCR; __BITS16 GRWCR_bit; struct { union { unsigned char GRWCRL; __BITS8 GRWCRL_bit; }; union { unsigned char GRWCRH; __BITS8 GRWCRH_bit; }; }; } @ 0xF038A;
__near __no_bit_access __no_init volatile union { unsigned short GTSTCFG; __BITS16 GTSTCFG_bit; struct { union { unsigned char GTSTCFGL; __BITS8 GTSTCFGL_bit; }; union { unsigned char GTSTCFGH; __BITS8 GTSTCFGH_bit; }; }; } @ 0xF038C;
__near __no_bit_access __no_init volatile union { unsigned char GTSTCTRL; __BITS8 GTSTCTRL_bit; } @ 0xF038E;
__near __no_bit_access __no_init volatile /* write only */ union { unsigned short GLOCKK; __BITS16 GLOCKK_bit; } @ 0xF0394;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDL0; __BITS16 GAFLIDL0_bit; } @ 0xF03A0;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDL0; __BITS16 RMIDL0_bit; struct { union { unsigned char GAFLIDL0L; __BITS8 GAFLIDL0L_bit; }; union { unsigned char GAFLIDL0H; __BITS8 GAFLIDL0H_bit; }; }; } @ 0xF03A0;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDL0H; __BITS8 RMIDL0H_bit; } @ 0xF03A1;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDH0; __BITS16 GAFLIDH0_bit; } @ 0xF03A2;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDH0; __BITS16 RMIDH0_bit; struct { union { unsigned char GAFLIDH0L; __BITS8 GAFLIDH0L_bit; }; union { unsigned char GAFLIDH0H; __BITS8 GAFLIDH0H_bit; }; }; } @ 0xF03A2;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDH0H; __BITS8 RMIDH0H_bit; } @ 0xF03A3;
__near __no_bit_access __no_init volatile union { unsigned short GAFLML0; __BITS16 GAFLML0_bit; } @ 0xF03A4;
__near __no_bit_access __no_init volatile const union { unsigned short RMTS0; __BITS16 RMTS0_bit; struct { union { unsigned char GAFLML0L; __BITS8 GAFLML0L_bit; }; union { unsigned char GAFLML0H; __BITS8 GAFLML0H_bit; }; }; } @ 0xF03A4;
__near __no_bit_access __no_init volatile const union { unsigned char RMTS0H; __BITS8 RMTS0H_bit; } @ 0xF03A5;
__near __no_bit_access __no_init volatile union { unsigned short GAFLMH0; __BITS16 GAFLMH0_bit; } @ 0xF03A6;
__near __no_bit_access __no_init volatile const union { unsigned short RMPTR0; __BITS16 RMPTR0_bit; struct { union { unsigned char GAFLMH0L; __BITS8 GAFLMH0L_bit; }; union { unsigned char GAFLMH0H; __BITS8 GAFLMH0H_bit; }; }; } @ 0xF03A6;
__near __no_bit_access __no_init volatile const union { unsigned char RMPTR0H; __BITS8 RMPTR0H_bit; } @ 0xF03A7;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPL0; __BITS16 GAFLPL0_bit; } @ 0xF03A8;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF00; __BITS16 RMDF00_bit; struct { union { unsigned char GAFLPL0L; __BITS8 GAFLPL0L_bit; }; union { unsigned char GAFLPL0H; __BITS8 GAFLPL0H_bit; }; }; } @ 0xF03A8;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF00H; __BITS8 RMDF00H_bit; } @ 0xF03A9;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPH0; __BITS16 GAFLPH0_bit; } @ 0xF03AA;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF10; __BITS16 RMDF10_bit; struct { union { unsigned char GAFLPH0L; __BITS8 GAFLPH0L_bit; }; union { unsigned char GAFLPH0H; __BITS8 GAFLPH0H_bit; }; }; } @ 0xF03AA;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF10H; __BITS8 RMDF10H_bit; } @ 0xF03AB;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDL1; __BITS16 GAFLIDL1_bit; } @ 0xF03AC;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF20; __BITS16 RMDF20_bit; struct { union { unsigned char GAFLIDL1L; __BITS8 GAFLIDL1L_bit; }; union { unsigned char GAFLIDL1H; __BITS8 GAFLIDL1H_bit; }; }; } @ 0xF03AC;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF20H; __BITS8 RMDF20H_bit; } @ 0xF03AD;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDH1; __BITS16 GAFLIDH1_bit; } @ 0xF03AE;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF30; __BITS16 RMDF30_bit; struct { union { unsigned char GAFLIDH1L; __BITS8 GAFLIDH1L_bit; }; union { unsigned char GAFLIDH1H; __BITS8 GAFLIDH1H_bit; }; }; } @ 0xF03AE;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF30H; __BITS8 RMDF30H_bit; } @ 0xF03AF;
__near __no_bit_access __no_init volatile union { unsigned short GAFLML1; __BITS16 GAFLML1_bit; } @ 0xF03B0;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDL1; __BITS16 RMIDL1_bit; struct { union { unsigned char GAFLML1L; __BITS8 GAFLML1L_bit; }; union { unsigned char GAFLML1H; __BITS8 GAFLML1H_bit; }; }; } @ 0xF03B0;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDL1H; __BITS8 RMIDL1H_bit; } @ 0xF03B1;
__near __no_bit_access __no_init volatile union { unsigned short GAFLMH1; __BITS16 GAFLMH1_bit; } @ 0xF03B2;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDH1; __BITS16 RMIDH1_bit; struct { union { unsigned char GAFLMH1L; __BITS8 GAFLMH1L_bit; }; union { unsigned char GAFLMH1H; __BITS8 GAFLMH1H_bit; }; }; } @ 0xF03B2;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDH1H; __BITS8 RMIDH1H_bit; } @ 0xF03B3;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPL1; __BITS16 GAFLPL1_bit; } @ 0xF03B4;
__near __no_bit_access __no_init volatile const union { unsigned short RMTS1; __BITS16 RMTS1_bit; struct { union { unsigned char GAFLPL1L; __BITS8 GAFLPL1L_bit; }; union { unsigned char GAFLPL1H; __BITS8 GAFLPL1H_bit; }; }; } @ 0xF03B4;
__near __no_bit_access __no_init volatile const union { unsigned char RMTS1H; __BITS8 RMTS1H_bit; } @ 0xF03B5;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPH1; __BITS16 GAFLPH1_bit; } @ 0xF03B6;
__near __no_bit_access __no_init volatile const union { unsigned short RMPTR1; __BITS16 RMPTR1_bit; struct { union { unsigned char GAFLPH1L; __BITS8 GAFLPH1L_bit; }; union { unsigned char GAFLPH1H; __BITS8 GAFLPH1H_bit; }; }; } @ 0xF03B6;
__near __no_bit_access __no_init volatile const union { unsigned char RMPTR1H; __BITS8 RMPTR1H_bit; } @ 0xF03B7;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDL2; __BITS16 GAFLIDL2_bit; } @ 0xF03B8;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF01; __BITS16 RMDF01_bit; struct { union { unsigned char GAFLIDL2L; __BITS8 GAFLIDL2L_bit; }; union { unsigned char GAFLIDL2H; __BITS8 GAFLIDL2H_bit; }; }; } @ 0xF03B8;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF01H; __BITS8 RMDF01H_bit; } @ 0xF03B9;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDH2; __BITS16 GAFLIDH2_bit; } @ 0xF03BA;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF11; __BITS16 RMDF11_bit; struct { union { unsigned char GAFLIDH2L; __BITS8 GAFLIDH2L_bit; }; union { unsigned char GAFLIDH2H; __BITS8 GAFLIDH2H_bit; }; }; } @ 0xF03BA;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF11H; __BITS8 RMDF11H_bit; } @ 0xF03BB;
__near __no_bit_access __no_init volatile union { unsigned short GAFLML2; __BITS16 GAFLML2_bit; } @ 0xF03BC;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF21; __BITS16 RMDF21_bit; struct { union { unsigned char GAFLML2L; __BITS8 GAFLML2L_bit; }; union { unsigned char GAFLML2H; __BITS8 GAFLML2H_bit; }; }; } @ 0xF03BC;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF21H; __BITS8 RMDF21H_bit; } @ 0xF03BD;
__near __no_bit_access __no_init volatile union { unsigned short GAFLMH2; __BITS16 GAFLMH2_bit; } @ 0xF03BE;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF31; __BITS16 RMDF31_bit; struct { union { unsigned char GAFLMH2L; __BITS8 GAFLMH2L_bit; }; union { unsigned char GAFLMH2H; __BITS8 GAFLMH2H_bit; }; }; } @ 0xF03BE;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF31H; __BITS8 RMDF31H_bit; } @ 0xF03BF;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPL2; __BITS16 GAFLPL2_bit; } @ 0xF03C0;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDL2; __BITS16 RMIDL2_bit; struct { union { unsigned char GAFLPL2L; __BITS8 GAFLPL2L_bit; }; union { unsigned char GAFLPL2H; __BITS8 GAFLPL2H_bit; }; }; } @ 0xF03C0;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDL2H; __BITS8 RMIDL2H_bit; } @ 0xF03C1;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPH2; __BITS16 GAFLPH2_bit; } @ 0xF03C2;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDH2; __BITS16 RMIDH2_bit; struct { union { unsigned char GAFLPH2L; __BITS8 GAFLPH2L_bit; }; union { unsigned char GAFLPH2H; __BITS8 GAFLPH2H_bit; }; }; } @ 0xF03C2;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDH2H; __BITS8 RMIDH2H_bit; } @ 0xF03C3;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDL3; __BITS16 GAFLIDL3_bit; } @ 0xF03C4;
__near __no_bit_access __no_init volatile const union { unsigned short RMTS2; __BITS16 RMTS2_bit; struct { union { unsigned char GAFLIDL3L; __BITS8 GAFLIDL3L_bit; }; union { unsigned char GAFLIDL3H; __BITS8 GAFLIDL3H_bit; }; }; } @ 0xF03C4;
__near __no_bit_access __no_init volatile const union { unsigned char RMTS2H; __BITS8 RMTS2H_bit; } @ 0xF03C5;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDH3; __BITS16 GAFLIDH3_bit; } @ 0xF03C6;
__near __no_bit_access __no_init volatile const union { unsigned short RMPTR2; __BITS16 RMPTR2_bit; struct { union { unsigned char GAFLIDH3L; __BITS8 GAFLIDH3L_bit; }; union { unsigned char GAFLIDH3H; __BITS8 GAFLIDH3H_bit; }; }; } @ 0xF03C6;
__near __no_bit_access __no_init volatile const union { unsigned char RMPTR2H; __BITS8 RMPTR2H_bit; } @ 0xF03C7;
__near __no_bit_access __no_init volatile union { unsigned short GAFLML3; __BITS16 GAFLML3_bit; } @ 0xF03C8;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF02; __BITS16 RMDF02_bit; struct { union { unsigned char GAFLML3L; __BITS8 GAFLML3L_bit; }; union { unsigned char GAFLML3H; __BITS8 GAFLML3H_bit; }; }; } @ 0xF03C8;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF02H; __BITS8 RMDF02H_bit; } @ 0xF03C9;
__near __no_bit_access __no_init volatile union { unsigned short GAFLMH3; __BITS16 GAFLMH3_bit; } @ 0xF03CA;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF12; __BITS16 RMDF12_bit; struct { union { unsigned char GAFLMH3L; __BITS8 GAFLMH3L_bit; }; union { unsigned char GAFLMH3H; __BITS8 GAFLMH3H_bit; }; }; } @ 0xF03CA;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF12H; __BITS8 RMDF12H_bit; } @ 0xF03CB;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPL3; __BITS16 GAFLPL3_bit; } @ 0xF03CC;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF22; __BITS16 RMDF22_bit; struct { union { unsigned char GAFLPL3L; __BITS8 GAFLPL3L_bit; }; union { unsigned char GAFLPL3H; __BITS8 GAFLPL3H_bit; }; }; } @ 0xF03CC;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF22H; __BITS8 RMDF22H_bit; } @ 0xF03CD;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPH3; __BITS16 GAFLPH3_bit; } @ 0xF03CE;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF32; __BITS16 RMDF32_bit; struct { union { unsigned char GAFLPH3L; __BITS8 GAFLPH3L_bit; }; union { unsigned char GAFLPH3H; __BITS8 GAFLPH3H_bit; }; }; } @ 0xF03CE;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF32H; __BITS8 RMDF32H_bit; } @ 0xF03CF;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDL4; __BITS16 GAFLIDL4_bit; } @ 0xF03D0;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDL3; __BITS16 RMIDL3_bit; struct { union { unsigned char GAFLIDL4L; __BITS8 GAFLIDL4L_bit; }; union { unsigned char GAFLIDL4H; __BITS8 GAFLIDL4H_bit; }; }; } @ 0xF03D0;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDL3H; __BITS8 RMIDL3H_bit; } @ 0xF03D1;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDH4; __BITS16 GAFLIDH4_bit; } @ 0xF03D2;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDH3; __BITS16 RMIDH3_bit; struct { union { unsigned char GAFLIDH4L; __BITS8 GAFLIDH4L_bit; }; union { unsigned char GAFLIDH4H; __BITS8 GAFLIDH4H_bit; }; }; } @ 0xF03D2;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDH3H; __BITS8 RMIDH3H_bit; } @ 0xF03D3;
__near __no_bit_access __no_init volatile union { unsigned short GAFLML4; __BITS16 GAFLML4_bit; } @ 0xF03D4;
__near __no_bit_access __no_init volatile const union { unsigned short RMTS3; __BITS16 RMTS3_bit; struct { union { unsigned char GAFLML4L; __BITS8 GAFLML4L_bit; }; union { unsigned char GAFLML4H; __BITS8 GAFLML4H_bit; }; }; } @ 0xF03D4;
__near __no_bit_access __no_init volatile const union { unsigned char RMTS3H; __BITS8 RMTS3H_bit; } @ 0xF03D5;
__near __no_bit_access __no_init volatile union { unsigned short GAFLMH4; __BITS16 GAFLMH4_bit; } @ 0xF03D6;
__near __no_bit_access __no_init volatile const union { unsigned short RMPTR3; __BITS16 RMPTR3_bit; struct { union { unsigned char GAFLMH4L; __BITS8 GAFLMH4L_bit; }; union { unsigned char GAFLMH4H; __BITS8 GAFLMH4H_bit; }; }; } @ 0xF03D6;
__near __no_bit_access __no_init volatile const union { unsigned char RMPTR3H; __BITS8 RMPTR3H_bit; } @ 0xF03D7;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPL4; __BITS16 GAFLPL4_bit; } @ 0xF03D8;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF03; __BITS16 RMDF03_bit; struct { union { unsigned char GAFLPL4L; __BITS8 GAFLPL4L_bit; }; union { unsigned char GAFLPL4H; __BITS8 GAFLPL4H_bit; }; }; } @ 0xF03D8;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF03H; __BITS8 RMDF03H_bit; } @ 0xF03D9;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPH4; __BITS16 GAFLPH4_bit; } @ 0xF03DA;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF13; __BITS16 RMDF13_bit; struct { union { unsigned char GAFLPH4L; __BITS8 GAFLPH4L_bit; }; union { unsigned char GAFLPH4H; __BITS8 GAFLPH4H_bit; }; }; } @ 0xF03DA;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF13H; __BITS8 RMDF13H_bit; } @ 0xF03DB;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDL5; __BITS16 GAFLIDL5_bit; } @ 0xF03DC;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF23; __BITS16 RMDF23_bit; struct { union { unsigned char GAFLIDL5L; __BITS8 GAFLIDL5L_bit; }; union { unsigned char GAFLIDL5H; __BITS8 GAFLIDL5H_bit; }; }; } @ 0xF03DC;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF23H; __BITS8 RMDF23H_bit; } @ 0xF03DD;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDH5; __BITS16 GAFLIDH5_bit; } @ 0xF03DE;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF33; __BITS16 RMDF33_bit; struct { union { unsigned char GAFLIDH5L; __BITS8 GAFLIDH5L_bit; }; union { unsigned char GAFLIDH5H; __BITS8 GAFLIDH5H_bit; }; }; } @ 0xF03DE;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF33H; __BITS8 RMDF33H_bit; } @ 0xF03DF;
__near __no_bit_access __no_init volatile union { unsigned short GAFLML5; __BITS16 GAFLML5_bit; } @ 0xF03E0;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDL4; __BITS16 RMIDL4_bit; struct { union { unsigned char GAFLML5L; __BITS8 GAFLML5L_bit; }; union { unsigned char GAFLML5H; __BITS8 GAFLML5H_bit; }; }; } @ 0xF03E0;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDL4H; __BITS8 RMIDL4H_bit; } @ 0xF03E1;
__near __no_bit_access __no_init volatile union { unsigned short GAFLMH5; __BITS16 GAFLMH5_bit; } @ 0xF03E2;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDH4; __BITS16 RMIDH4_bit; struct { union { unsigned char GAFLMH5L; __BITS8 GAFLMH5L_bit; }; union { unsigned char GAFLMH5H; __BITS8 GAFLMH5H_bit; }; }; } @ 0xF03E2;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDH4H; __BITS8 RMIDH4H_bit; } @ 0xF03E3;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPL5; __BITS16 GAFLPL5_bit; } @ 0xF03E4;
__near __no_bit_access __no_init volatile const union { unsigned short RMTS4; __BITS16 RMTS4_bit; struct { union { unsigned char GAFLPL5L; __BITS8 GAFLPL5L_bit; }; union { unsigned char GAFLPL5H; __BITS8 GAFLPL5H_bit; }; }; } @ 0xF03E4;
__near __no_bit_access __no_init volatile const union { unsigned char RMTS4H; __BITS8 RMTS4H_bit; } @ 0xF03E5;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPH5; __BITS16 GAFLPH5_bit; } @ 0xF03E6;
__near __no_bit_access __no_init volatile const union { unsigned short RMPTR4; __BITS16 RMPTR4_bit; struct { union { unsigned char GAFLPH5L; __BITS8 GAFLPH5L_bit; }; union { unsigned char GAFLPH5H; __BITS8 GAFLPH5H_bit; }; }; } @ 0xF03E6;
__near __no_bit_access __no_init volatile const union { unsigned char RMPTR4H; __BITS8 RMPTR4H_bit; } @ 0xF03E7;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDL6; __BITS16 GAFLIDL6_bit; } @ 0xF03E8;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF04; __BITS16 RMDF04_bit; struct { union { unsigned char GAFLIDL6L; __BITS8 GAFLIDL6L_bit; }; union { unsigned char GAFLIDL6H; __BITS8 GAFLIDL6H_bit; }; }; } @ 0xF03E8;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF04H; __BITS8 RMDF04H_bit; } @ 0xF03E9;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDH6; __BITS16 GAFLIDH6_bit; } @ 0xF03EA;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF14; __BITS16 RMDF14_bit; struct { union { unsigned char GAFLIDH6L; __BITS8 GAFLIDH6L_bit; }; union { unsigned char GAFLIDH6H; __BITS8 GAFLIDH6H_bit; }; }; } @ 0xF03EA;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF14H; __BITS8 RMDF14H_bit; } @ 0xF03EB;
__near __no_bit_access __no_init volatile union { unsigned short GAFLML6; __BITS16 GAFLML6_bit; } @ 0xF03EC;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF24; __BITS16 RMDF24_bit; struct { union { unsigned char GAFLML6L; __BITS8 GAFLML6L_bit; }; union { unsigned char GAFLML6H; __BITS8 GAFLML6H_bit; }; }; } @ 0xF03EC;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF24H; __BITS8 RMDF24H_bit; } @ 0xF03ED;
__near __no_bit_access __no_init volatile union { unsigned short GAFLMH6; __BITS16 GAFLMH6_bit; } @ 0xF03EE;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF34; __BITS16 RMDF34_bit; struct { union { unsigned char GAFLMH6L; __BITS8 GAFLMH6L_bit; }; union { unsigned char GAFLMH6H; __BITS8 GAFLMH6H_bit; }; }; } @ 0xF03EE;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF34H; __BITS8 RMDF34H_bit; } @ 0xF03EF;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPL6; __BITS16 GAFLPL6_bit; } @ 0xF03F0;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDL5; __BITS16 RMIDL5_bit; struct { union { unsigned char GAFLPL6L; __BITS8 GAFLPL6L_bit; }; union { unsigned char GAFLPL6H; __BITS8 GAFLPL6H_bit; }; }; } @ 0xF03F0;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDL5H; __BITS8 RMIDL5H_bit; } @ 0xF03F1;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPH6; __BITS16 GAFLPH6_bit; } @ 0xF03F2;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDH5; __BITS16 RMIDH5_bit; struct { union { unsigned char GAFLPH6L; __BITS8 GAFLPH6L_bit; }; union { unsigned char GAFLPH6H; __BITS8 GAFLPH6H_bit; }; }; } @ 0xF03F2;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDH5H; __BITS8 RMIDH5H_bit; } @ 0xF03F3;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDL7; __BITS16 GAFLIDL7_bit; } @ 0xF03F4;
__near __no_bit_access __no_init volatile const union { unsigned short RMTS5; __BITS16 RMTS5_bit; struct { union { unsigned char GAFLIDL7L; __BITS8 GAFLIDL7L_bit; }; union { unsigned char GAFLIDL7H; __BITS8 GAFLIDL7H_bit; }; }; } @ 0xF03F4;
__near __no_bit_access __no_init volatile const union { unsigned char RMTS5H; __BITS8 RMTS5H_bit; } @ 0xF03F5;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDH7; __BITS16 GAFLIDH7_bit; } @ 0xF03F6;
__near __no_bit_access __no_init volatile const union { unsigned short RMPTR5; __BITS16 RMPTR5_bit; struct { union { unsigned char GAFLIDH7L; __BITS8 GAFLIDH7L_bit; }; union { unsigned char GAFLIDH7H; __BITS8 GAFLIDH7H_bit; }; }; } @ 0xF03F6;
__near __no_bit_access __no_init volatile const union { unsigned char RMPTR5H; __BITS8 RMPTR5H_bit; } @ 0xF03F7;
__near __no_bit_access __no_init volatile union { unsigned short GAFLML7; __BITS16 GAFLML7_bit; } @ 0xF03F8;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF05; __BITS16 RMDF05_bit; struct { union { unsigned char GAFLML7L; __BITS8 GAFLML7L_bit; }; union { unsigned char GAFLML7H; __BITS8 GAFLML7H_bit; }; }; } @ 0xF03F8;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF05H; __BITS8 RMDF05H_bit; } @ 0xF03F9;
__near __no_bit_access __no_init volatile union { unsigned short GAFLMH7; __BITS16 GAFLMH7_bit; } @ 0xF03FA;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF15; __BITS16 RMDF15_bit; struct { union { unsigned char GAFLMH7L; __BITS8 GAFLMH7L_bit; }; union { unsigned char GAFLMH7H; __BITS8 GAFLMH7H_bit; }; }; } @ 0xF03FA;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF15H; __BITS8 RMDF15H_bit; } @ 0xF03FB;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPL7; __BITS16 GAFLPL7_bit; } @ 0xF03FC;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF25; __BITS16 RMDF25_bit; struct { union { unsigned char GAFLPL7L; __BITS8 GAFLPL7L_bit; }; union { unsigned char GAFLPL7H; __BITS8 GAFLPL7H_bit; }; }; } @ 0xF03FC;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF25H; __BITS8 RMDF25H_bit; } @ 0xF03FD;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPH7; __BITS16 GAFLPH7_bit; } @ 0xF03FE;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF35; __BITS16 RMDF35_bit; struct { union { unsigned char GAFLPH7L; __BITS8 GAFLPH7L_bit; }; union { unsigned char GAFLPH7H; __BITS8 GAFLPH7H_bit; }; }; } @ 0xF03FE;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF35H; __BITS8 RMDF35H_bit; } @ 0xF03FF;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDL8; __BITS16 GAFLIDL8_bit; } @ 0xF0400;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDL6; __BITS16 RMIDL6_bit; struct { union { unsigned char GAFLIDL8L; __BITS8 GAFLIDL8L_bit; }; union { unsigned char GAFLIDL8H; __BITS8 GAFLIDL8H_bit; }; }; } @ 0xF0400;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDL6H; __BITS8 RMIDL6H_bit; } @ 0xF0401;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDH8; __BITS16 GAFLIDH8_bit; } @ 0xF0402;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDH6; __BITS16 RMIDH6_bit; struct { union { unsigned char GAFLIDH8L; __BITS8 GAFLIDH8L_bit; }; union { unsigned char GAFLIDH8H; __BITS8 GAFLIDH8H_bit; }; }; } @ 0xF0402;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDH6H; __BITS8 RMIDH6H_bit; } @ 0xF0403;
__near __no_bit_access __no_init volatile union { unsigned short GAFLML8; __BITS16 GAFLML8_bit; } @ 0xF0404;
__near __no_bit_access __no_init volatile const union { unsigned short RMTS6; __BITS16 RMTS6_bit; struct { union { unsigned char GAFLML8L; __BITS8 GAFLML8L_bit; }; union { unsigned char GAFLML8H; __BITS8 GAFLML8H_bit; }; }; } @ 0xF0404;
__near __no_bit_access __no_init volatile const union { unsigned char RMTS6H; __BITS8 RMTS6H_bit; } @ 0xF0405;
__near __no_bit_access __no_init volatile union { unsigned short GAFLMH8; __BITS16 GAFLMH8_bit; } @ 0xF0406;
__near __no_bit_access __no_init volatile const union { unsigned short RMPTR6; __BITS16 RMPTR6_bit; struct { union { unsigned char GAFLMH8L; __BITS8 GAFLMH8L_bit; }; union { unsigned char GAFLMH8H; __BITS8 GAFLMH8H_bit; }; }; } @ 0xF0406;
__near __no_bit_access __no_init volatile const union { unsigned char RMPTR6H; __BITS8 RMPTR6H_bit; } @ 0xF0407;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPL8; __BITS16 GAFLPL8_bit; } @ 0xF0408;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF06; __BITS16 RMDF06_bit; struct { union { unsigned char GAFLPL8L; __BITS8 GAFLPL8L_bit; }; union { unsigned char GAFLPL8H; __BITS8 GAFLPL8H_bit; }; }; } @ 0xF0408;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF06H; __BITS8 RMDF06H_bit; } @ 0xF0409;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPH8; __BITS16 GAFLPH8_bit; } @ 0xF040A;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF16; __BITS16 RMDF16_bit; struct { union { unsigned char GAFLPH8L; __BITS8 GAFLPH8L_bit; }; union { unsigned char GAFLPH8H; __BITS8 GAFLPH8H_bit; }; }; } @ 0xF040A;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF16H; __BITS8 RMDF16H_bit; } @ 0xF040B;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDL9; __BITS16 GAFLIDL9_bit; } @ 0xF040C;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF26; __BITS16 RMDF26_bit; struct { union { unsigned char GAFLIDL9L; __BITS8 GAFLIDL9L_bit; }; union { unsigned char GAFLIDL9H; __BITS8 GAFLIDL9H_bit; }; }; } @ 0xF040C;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF26H; __BITS8 RMDF26H_bit; } @ 0xF040D;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDH9; __BITS16 GAFLIDH9_bit; } @ 0xF040E;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF36; __BITS16 RMDF36_bit; struct { union { unsigned char GAFLIDH9L; __BITS8 GAFLIDH9L_bit; }; union { unsigned char GAFLIDH9H; __BITS8 GAFLIDH9H_bit; }; }; } @ 0xF040E;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF36H; __BITS8 RMDF36H_bit; } @ 0xF040F;
__near __no_bit_access __no_init volatile union { unsigned short GAFLML9; __BITS16 GAFLML9_bit; } @ 0xF0410;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDL7; __BITS16 RMIDL7_bit; struct { union { unsigned char GAFLML9L; __BITS8 GAFLML9L_bit; }; union { unsigned char GAFLML9H; __BITS8 GAFLML9H_bit; }; }; } @ 0xF0410;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDL7H; __BITS8 RMIDL7H_bit; } @ 0xF0411;
__near __no_bit_access __no_init volatile union { unsigned short GAFLMH9; __BITS16 GAFLMH9_bit; } @ 0xF0412;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDH7; __BITS16 RMIDH7_bit; struct { union { unsigned char GAFLMH9L; __BITS8 GAFLMH9L_bit; }; union { unsigned char GAFLMH9H; __BITS8 GAFLMH9H_bit; }; }; } @ 0xF0412;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDH7H; __BITS8 RMIDH7H_bit; } @ 0xF0413;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPL9; __BITS16 GAFLPL9_bit; } @ 0xF0414;
__near __no_bit_access __no_init volatile const union { unsigned short RMTS7; __BITS16 RMTS7_bit; struct { union { unsigned char GAFLPL9L; __BITS8 GAFLPL9L_bit; }; union { unsigned char GAFLPL9H; __BITS8 GAFLPL9H_bit; }; }; } @ 0xF0414;
__near __no_bit_access __no_init volatile const union { unsigned char RMTS7H; __BITS8 RMTS7H_bit; } @ 0xF0415;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPH9; __BITS16 GAFLPH9_bit; } @ 0xF0416;
__near __no_bit_access __no_init volatile const union { unsigned short RMPTR7; __BITS16 RMPTR7_bit; struct { union { unsigned char GAFLPH9L; __BITS8 GAFLPH9L_bit; }; union { unsigned char GAFLPH9H; __BITS8 GAFLPH9H_bit; }; }; } @ 0xF0416;
__near __no_bit_access __no_init volatile const union { unsigned char RMPTR7H; __BITS8 RMPTR7H_bit; } @ 0xF0417;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDL10; __BITS16 GAFLIDL10_bit; } @ 0xF0418;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF07; __BITS16 RMDF07_bit; struct { union { unsigned char GAFLIDL10L; __BITS8 GAFLIDL10L_bit; }; union { unsigned char GAFLIDL10H; __BITS8 GAFLIDL10H_bit; }; }; } @ 0xF0418;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF07H; __BITS8 RMDF07H_bit; } @ 0xF0419;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDH10; __BITS16 GAFLIDH10_bit; } @ 0xF041A;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF17; __BITS16 RMDF17_bit; struct { union { unsigned char GAFLIDH10L; __BITS8 GAFLIDH10L_bit; }; union { unsigned char GAFLIDH10H; __BITS8 GAFLIDH10H_bit; }; }; } @ 0xF041A;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF17H; __BITS8 RMDF17H_bit; } @ 0xF041B;
__near __no_bit_access __no_init volatile union { unsigned short GAFLML10; __BITS16 GAFLML10_bit; } @ 0xF041C;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF27; __BITS16 RMDF27_bit; struct { union { unsigned char GAFLML10L; __BITS8 GAFLML10L_bit; }; union { unsigned char GAFLML10H; __BITS8 GAFLML10H_bit; }; }; } @ 0xF041C;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF27H; __BITS8 RMDF27H_bit; } @ 0xF041D;
__near __no_bit_access __no_init volatile union { unsigned short GAFLMH10; __BITS16 GAFLMH10_bit; } @ 0xF041E;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF37; __BITS16 RMDF37_bit; struct { union { unsigned char GAFLMH10L; __BITS8 GAFLMH10L_bit; }; union { unsigned char GAFLMH10H; __BITS8 GAFLMH10H_bit; }; }; } @ 0xF041E;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF37H; __BITS8 RMDF37H_bit; } @ 0xF041F;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPL10; __BITS16 GAFLPL10_bit; } @ 0xF0420;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDL8; __BITS16 RMIDL8_bit; struct { union { unsigned char GAFLPL10L; __BITS8 GAFLPL10L_bit; }; union { unsigned char GAFLPL10H; __BITS8 GAFLPL10H_bit; }; }; } @ 0xF0420;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDL8H; __BITS8 RMIDL8H_bit; } @ 0xF0421;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPH10; __BITS16 GAFLPH10_bit; } @ 0xF0422;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDH8; __BITS16 RMIDH8_bit; struct { union { unsigned char GAFLPH10L; __BITS8 GAFLPH10L_bit; }; union { unsigned char GAFLPH10H; __BITS8 GAFLPH10H_bit; }; }; } @ 0xF0422;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDH8H; __BITS8 RMIDH8H_bit; } @ 0xF0423;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDL11; __BITS16 GAFLIDL11_bit; } @ 0xF0424;
__near __no_bit_access __no_init volatile const union { unsigned short RMTS8; __BITS16 RMTS8_bit; struct { union { unsigned char GAFLIDL11L; __BITS8 GAFLIDL11L_bit; }; union { unsigned char GAFLIDL11H; __BITS8 GAFLIDL11H_bit; }; }; } @ 0xF0424;
__near __no_bit_access __no_init volatile const union { unsigned char RMTS8H; __BITS8 RMTS8H_bit; } @ 0xF0425;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDH11; __BITS16 GAFLIDH11_bit; } @ 0xF0426;
__near __no_bit_access __no_init volatile const union { unsigned short RMPTR8; __BITS16 RMPTR8_bit; struct { union { unsigned char GAFLIDH11L; __BITS8 GAFLIDH11L_bit; }; union { unsigned char GAFLIDH11H; __BITS8 GAFLIDH11H_bit; }; }; } @ 0xF0426;
__near __no_bit_access __no_init volatile const union { unsigned char RMPTR8H; __BITS8 RMPTR8H_bit; } @ 0xF0427;
__near __no_bit_access __no_init volatile union { unsigned short GAFLML11; __BITS16 GAFLML11_bit; } @ 0xF0428;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF08; __BITS16 RMDF08_bit; struct { union { unsigned char GAFLML11L; __BITS8 GAFLML11L_bit; }; union { unsigned char GAFLML11H; __BITS8 GAFLML11H_bit; }; }; } @ 0xF0428;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF08H; __BITS8 RMDF08H_bit; } @ 0xF0429;
__near __no_bit_access __no_init volatile union { unsigned short GAFLMH11; __BITS16 GAFLMH11_bit; } @ 0xF042A;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF18; __BITS16 RMDF18_bit; struct { union { unsigned char GAFLMH11L; __BITS8 GAFLMH11L_bit; }; union { unsigned char GAFLMH11H; __BITS8 GAFLMH11H_bit; }; }; } @ 0xF042A;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF18H; __BITS8 RMDF18H_bit; } @ 0xF042B;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPL11; __BITS16 GAFLPL11_bit; } @ 0xF042C;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF28; __BITS16 RMDF28_bit; struct { union { unsigned char GAFLPL11L; __BITS8 GAFLPL11L_bit; }; union { unsigned char GAFLPL11H; __BITS8 GAFLPL11H_bit; }; }; } @ 0xF042C;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF28H; __BITS8 RMDF28H_bit; } @ 0xF042D;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPH11; __BITS16 GAFLPH11_bit; } @ 0xF042E;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF38; __BITS16 RMDF38_bit; struct { union { unsigned char GAFLPH11L; __BITS8 GAFLPH11L_bit; }; union { unsigned char GAFLPH11H; __BITS8 GAFLPH11H_bit; }; }; } @ 0xF042E;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF38H; __BITS8 RMDF38H_bit; } @ 0xF042F;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDL12; __BITS16 GAFLIDL12_bit; } @ 0xF0430;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDL9; __BITS16 RMIDL9_bit; struct { union { unsigned char GAFLIDL12L; __BITS8 GAFLIDL12L_bit; }; union { unsigned char GAFLIDL12H; __BITS8 GAFLIDL12H_bit; }; }; } @ 0xF0430;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDL9H; __BITS8 RMIDL9H_bit; } @ 0xF0431;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDH12; __BITS16 GAFLIDH12_bit; } @ 0xF0432;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDH9; __BITS16 RMIDH9_bit; struct { union { unsigned char GAFLIDH12L; __BITS8 GAFLIDH12L_bit; }; union { unsigned char GAFLIDH12H; __BITS8 GAFLIDH12H_bit; }; }; } @ 0xF0432;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDH9H; __BITS8 RMIDH9H_bit; } @ 0xF0433;
__near __no_bit_access __no_init volatile union { unsigned short GAFLML12; __BITS16 GAFLML12_bit; } @ 0xF0434;
__near __no_bit_access __no_init volatile const union { unsigned short RMTS9; __BITS16 RMTS9_bit; struct { union { unsigned char GAFLML12L; __BITS8 GAFLML12L_bit; }; union { unsigned char GAFLML12H; __BITS8 GAFLML12H_bit; }; }; } @ 0xF0434;
__near __no_bit_access __no_init volatile const union { unsigned char RMTS9H; __BITS8 RMTS9H_bit; } @ 0xF0435;
__near __no_bit_access __no_init volatile union { unsigned short GAFLMH12; __BITS16 GAFLMH12_bit; } @ 0xF0436;
__near __no_bit_access __no_init volatile const union { unsigned short RMPTR9; __BITS16 RMPTR9_bit; struct { union { unsigned char GAFLMH12L; __BITS8 GAFLMH12L_bit; }; union { unsigned char GAFLMH12H; __BITS8 GAFLMH12H_bit; }; }; } @ 0xF0436;
__near __no_bit_access __no_init volatile const union { unsigned char RMPTR9H; __BITS8 RMPTR9H_bit; } @ 0xF0437;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPL12; __BITS16 GAFLPL12_bit; } @ 0xF0438;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF09; __BITS16 RMDF09_bit; struct { union { unsigned char GAFLPL12L; __BITS8 GAFLPL12L_bit; }; union { unsigned char GAFLPL12H; __BITS8 GAFLPL12H_bit; }; }; } @ 0xF0438;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF09H; __BITS8 RMDF09H_bit; } @ 0xF0439;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPH12; __BITS16 GAFLPH12_bit; } @ 0xF043A;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF19; __BITS16 RMDF19_bit; struct { union { unsigned char GAFLPH12L; __BITS8 GAFLPH12L_bit; }; union { unsigned char GAFLPH12H; __BITS8 GAFLPH12H_bit; }; }; } @ 0xF043A;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF19H; __BITS8 RMDF19H_bit; } @ 0xF043B;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDL13; __BITS16 GAFLIDL13_bit; } @ 0xF043C;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF29; __BITS16 RMDF29_bit; struct { union { unsigned char GAFLIDL13L; __BITS8 GAFLIDL13L_bit; }; union { unsigned char GAFLIDL13H; __BITS8 GAFLIDL13H_bit; }; }; } @ 0xF043C;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF29H; __BITS8 RMDF29H_bit; } @ 0xF043D;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDH13; __BITS16 GAFLIDH13_bit; } @ 0xF043E;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF39; __BITS16 RMDF39_bit; struct { union { unsigned char GAFLIDH13L; __BITS8 GAFLIDH13L_bit; }; union { unsigned char GAFLIDH13H; __BITS8 GAFLIDH13H_bit; }; }; } @ 0xF043E;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF39H; __BITS8 RMDF39H_bit; } @ 0xF043F;
__near __no_bit_access __no_init volatile union { unsigned short GAFLML13; __BITS16 GAFLML13_bit; } @ 0xF0440;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDL10; __BITS16 RMIDL10_bit; struct { union { unsigned char GAFLML13L; __BITS8 GAFLML13L_bit; }; union { unsigned char GAFLML13H; __BITS8 GAFLML13H_bit; }; }; } @ 0xF0440;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDL10H; __BITS8 RMIDL10H_bit; } @ 0xF0441;
__near __no_bit_access __no_init volatile union { unsigned short GAFLMH13; __BITS16 GAFLMH13_bit; } @ 0xF0442;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDH10; __BITS16 RMIDH10_bit; struct { union { unsigned char GAFLMH13L; __BITS8 GAFLMH13L_bit; }; union { unsigned char GAFLMH13H; __BITS8 GAFLMH13H_bit; }; }; } @ 0xF0442;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDH10H; __BITS8 RMIDH10H_bit; } @ 0xF0443;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPL13; __BITS16 GAFLPL13_bit; } @ 0xF0444;
__near __no_bit_access __no_init volatile const union { unsigned short RMTS10; __BITS16 RMTS10_bit; struct { union { unsigned char GAFLPL13L; __BITS8 GAFLPL13L_bit; }; union { unsigned char GAFLPL13H; __BITS8 GAFLPL13H_bit; }; }; } @ 0xF0444;
__near __no_bit_access __no_init volatile const union { unsigned char RMTS10H; __BITS8 RMTS10H_bit; } @ 0xF0445;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPH13; __BITS16 GAFLPH13_bit; } @ 0xF0446;
__near __no_bit_access __no_init volatile const union { unsigned short RMPTR10; __BITS16 RMPTR10_bit; struct { union { unsigned char GAFLPH13L; __BITS8 GAFLPH13L_bit; }; union { unsigned char GAFLPH13H; __BITS8 GAFLPH13H_bit; }; }; } @ 0xF0446;
__near __no_bit_access __no_init volatile const union { unsigned char RMPTR10H; __BITS8 RMPTR10H_bit; } @ 0xF0447;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDL14; __BITS16 GAFLIDL14_bit; } @ 0xF0448;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF010; __BITS16 RMDF010_bit; struct { union { unsigned char GAFLIDL14L; __BITS8 GAFLIDL14L_bit; }; union { unsigned char GAFLIDL14H; __BITS8 GAFLIDL14H_bit; }; }; } @ 0xF0448;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF010H; __BITS8 RMDF010H_bit; } @ 0xF0449;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDH14; __BITS16 GAFLIDH14_bit; } @ 0xF044A;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF110; __BITS16 RMDF110_bit; struct { union { unsigned char GAFLIDH14L; __BITS8 GAFLIDH14L_bit; }; union { unsigned char GAFLIDH14H; __BITS8 GAFLIDH14H_bit; }; }; } @ 0xF044A;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF110H; __BITS8 RMDF110H_bit; } @ 0xF044B;
__near __no_bit_access __no_init volatile union { unsigned short GAFLML14; __BITS16 GAFLML14_bit; } @ 0xF044C;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF210; __BITS16 RMDF210_bit; struct { union { unsigned char GAFLML14L; __BITS8 GAFLML14L_bit; }; union { unsigned char GAFLML14H; __BITS8 GAFLML14H_bit; }; }; } @ 0xF044C;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF210H; __BITS8 RMDF210H_bit; } @ 0xF044D;
__near __no_bit_access __no_init volatile union { unsigned short GAFLMH14; __BITS16 GAFLMH14_bit; } @ 0xF044E;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF310; __BITS16 RMDF310_bit; struct { union { unsigned char GAFLMH14L; __BITS8 GAFLMH14L_bit; }; union { unsigned char GAFLMH14H; __BITS8 GAFLMH14H_bit; }; }; } @ 0xF044E;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF310H; __BITS8 RMDF310H_bit; } @ 0xF044F;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPL14; __BITS16 GAFLPL14_bit; } @ 0xF0450;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDL11; __BITS16 RMIDL11_bit; struct { union { unsigned char GAFLPL14L; __BITS8 GAFLPL14L_bit; }; union { unsigned char GAFLPL14H; __BITS8 GAFLPL14H_bit; }; }; } @ 0xF0450;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDL11H; __BITS8 RMIDL11H_bit; } @ 0xF0451;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPH14; __BITS16 GAFLPH14_bit; } @ 0xF0452;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDH11; __BITS16 RMIDH11_bit; struct { union { unsigned char GAFLPH14L; __BITS8 GAFLPH14L_bit; }; union { unsigned char GAFLPH14H; __BITS8 GAFLPH14H_bit; }; }; } @ 0xF0452;
__near __no_bit_access __no_init volatile const union { unsigned char RMIDH11H; __BITS8 RMIDH11H_bit; } @ 0xF0453;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDL15; __BITS16 GAFLIDL15_bit; } @ 0xF0454;
__near __no_bit_access __no_init volatile const union { unsigned short RMTS11; __BITS16 RMTS11_bit; struct { union { unsigned char GAFLIDL15L; __BITS8 GAFLIDL15L_bit; }; union { unsigned char GAFLIDL15H; __BITS8 GAFLIDL15H_bit; }; }; } @ 0xF0454;
__near __no_bit_access __no_init volatile const union { unsigned char RMTS11H; __BITS8 RMTS11H_bit; } @ 0xF0455;
__near __no_bit_access __no_init volatile union { unsigned short GAFLIDH15; __BITS16 GAFLIDH15_bit; } @ 0xF0456;
__near __no_bit_access __no_init volatile const union { unsigned short RMPTR11; __BITS16 RMPTR11_bit; struct { union { unsigned char GAFLIDH15L; __BITS8 GAFLIDH15L_bit; }; union { unsigned char GAFLIDH15H; __BITS8 GAFLIDH15H_bit; }; }; } @ 0xF0456;
__near __no_bit_access __no_init volatile const union { unsigned char RMPTR11H; __BITS8 RMPTR11H_bit; } @ 0xF0457;
__near __no_bit_access __no_init volatile union { unsigned short GAFLML15; __BITS16 GAFLML15_bit; } @ 0xF0458;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF011; __BITS16 RMDF011_bit; struct { union { unsigned char GAFLML15L; __BITS8 GAFLML15L_bit; }; union { unsigned char GAFLML15H; __BITS8 GAFLML15H_bit; }; }; } @ 0xF0458;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF011H; __BITS8 RMDF011H_bit; } @ 0xF0459;
__near __no_bit_access __no_init volatile union { unsigned short GAFLMH15; __BITS16 GAFLMH15_bit; } @ 0xF045A;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF111; __BITS16 RMDF111_bit; struct { union { unsigned char GAFLMH15L; __BITS8 GAFLMH15L_bit; }; union { unsigned char GAFLMH15H; __BITS8 GAFLMH15H_bit; }; }; } @ 0xF045A;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF111H; __BITS8 RMDF111H_bit; } @ 0xF045B;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPL15; __BITS16 GAFLPL15_bit; } @ 0xF045C;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF211; __BITS16 RMDF211_bit; struct { union { unsigned char GAFLPL15L; __BITS8 GAFLPL15L_bit; }; union { unsigned char GAFLPL15H; __BITS8 GAFLPL15H_bit; }; }; } @ 0xF045C;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF211H; __BITS8 RMDF211H_bit; } @ 0xF045D;
__near __no_bit_access __no_init volatile union { unsigned short GAFLPH15; __BITS16 GAFLPH15_bit; } @ 0xF045E;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF311; __BITS16 RMDF311_bit; struct { union { unsigned char GAFLPH15L; __BITS8 GAFLPH15L_bit; }; union { unsigned char GAFLPH15H; __BITS8 GAFLPH15H_bit; }; }; } @ 0xF045E;
__near __no_bit_access __no_init volatile const union { unsigned char RMDF311H; __BITS8 RMDF311H_bit; } @ 0xF045F;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDL12; __BITS16 RMIDL12_bit; struct { union { const unsigned char RMIDL12L; const __BITS8 RMIDL12L_bit; }; union { const unsigned char RMIDL12H; const __BITS8 RMIDL12H_bit; }; }; } @ 0xF0460;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDH12; __BITS16 RMIDH12_bit; struct { union { const unsigned char RMIDH12L; const __BITS8 RMIDH12L_bit; }; union { const unsigned char RMIDH12H; const __BITS8 RMIDH12H_bit; }; }; } @ 0xF0462;
__near __no_bit_access __no_init volatile const union { unsigned short RMTS12; __BITS16 RMTS12_bit; struct { union { const unsigned char RMTS12L; const __BITS8 RMTS12L_bit; }; union { const unsigned char RMTS12H; const __BITS8 RMTS12H_bit; }; }; } @ 0xF0464;
__near __no_bit_access __no_init volatile const union { unsigned short RMPTR12; __BITS16 RMPTR12_bit; struct { union { const unsigned char RMPTR12L; const __BITS8 RMPTR12L_bit; }; union { const unsigned char RMPTR12H; const __BITS8 RMPTR12H_bit; }; }; } @ 0xF0466;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF012; __BITS16 RMDF012_bit; struct { union { const unsigned char RMDF012L; const __BITS8 RMDF012L_bit; }; union { const unsigned char RMDF012H; const __BITS8 RMDF012H_bit; }; }; } @ 0xF0468;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF112; __BITS16 RMDF112_bit; struct { union { const unsigned char RMDF112L; const __BITS8 RMDF112L_bit; }; union { const unsigned char RMDF112H; const __BITS8 RMDF112H_bit; }; }; } @ 0xF046A;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF212; __BITS16 RMDF212_bit; struct { union { const unsigned char RMDF212L; const __BITS8 RMDF212L_bit; }; union { const unsigned char RMDF212H; const __BITS8 RMDF212H_bit; }; }; } @ 0xF046C;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF312; __BITS16 RMDF312_bit; struct { union { const unsigned char RMDF312L; const __BITS8 RMDF312L_bit; }; union { const unsigned char RMDF312H; const __BITS8 RMDF312H_bit; }; }; } @ 0xF046E;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDL13; __BITS16 RMIDL13_bit; struct { union { const unsigned char RMIDL13L; const __BITS8 RMIDL13L_bit; }; union { const unsigned char RMIDL13H; const __BITS8 RMIDL13H_bit; }; }; } @ 0xF0470;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDH13; __BITS16 RMIDH13_bit; struct { union { const unsigned char RMIDH13L; const __BITS8 RMIDH13L_bit; }; union { const unsigned char RMIDH13H; const __BITS8 RMIDH13H_bit; }; }; } @ 0xF0472;
__near __no_bit_access __no_init volatile const union { unsigned short RMTS13; __BITS16 RMTS13_bit; struct { union { const unsigned char RMTS13L; const __BITS8 RMTS13L_bit; }; union { const unsigned char RMTS13H; const __BITS8 RMTS13H_bit; }; }; } @ 0xF0474;
__near __no_bit_access __no_init volatile const union { unsigned short RMPTR13; __BITS16 RMPTR13_bit; struct { union { const unsigned char RMPTR13L; const __BITS8 RMPTR13L_bit; }; union { const unsigned char RMPTR13H; const __BITS8 RMPTR13H_bit; }; }; } @ 0xF0476;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF013; __BITS16 RMDF013_bit; struct { union { const unsigned char RMDF013L; const __BITS8 RMDF013L_bit; }; union { const unsigned char RMDF013H; const __BITS8 RMDF013H_bit; }; }; } @ 0xF0478;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF113; __BITS16 RMDF113_bit; struct { union { const unsigned char RMDF113L; const __BITS8 RMDF113L_bit; }; union { const unsigned char RMDF113H; const __BITS8 RMDF113H_bit; }; }; } @ 0xF047A;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF213; __BITS16 RMDF213_bit; struct { union { const unsigned char RMDF213L; const __BITS8 RMDF213L_bit; }; union { const unsigned char RMDF213H; const __BITS8 RMDF213H_bit; }; }; } @ 0xF047C;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF313; __BITS16 RMDF313_bit; struct { union { const unsigned char RMDF313L; const __BITS8 RMDF313L_bit; }; union { const unsigned char RMDF313H; const __BITS8 RMDF313H_bit; }; }; } @ 0xF047E;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDL14; __BITS16 RMIDL14_bit; struct { union { const unsigned char RMIDL14L; const __BITS8 RMIDL14L_bit; }; union { const unsigned char RMIDL14H; const __BITS8 RMIDL14H_bit; }; }; } @ 0xF0480;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDH14; __BITS16 RMIDH14_bit; struct { union { const unsigned char RMIDH14L; const __BITS8 RMIDH14L_bit; }; union { const unsigned char RMIDH14H; const __BITS8 RMIDH14H_bit; }; }; } @ 0xF0482;
__near __no_bit_access __no_init volatile const union { unsigned short RMTS14; __BITS16 RMTS14_bit; struct { union { const unsigned char RMTS14L; const __BITS8 RMTS14L_bit; }; union { const unsigned char RMTS14H; const __BITS8 RMTS14H_bit; }; }; } @ 0xF0484;
__near __no_bit_access __no_init volatile const union { unsigned short RMPTR14; __BITS16 RMPTR14_bit; struct { union { const unsigned char RMPTR14L; const __BITS8 RMPTR14L_bit; }; union { const unsigned char RMPTR14H; const __BITS8 RMPTR14H_bit; }; }; } @ 0xF0486;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF014; __BITS16 RMDF014_bit; struct { union { const unsigned char RMDF014L; const __BITS8 RMDF014L_bit; }; union { const unsigned char RMDF014H; const __BITS8 RMDF014H_bit; }; }; } @ 0xF0488;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF114; __BITS16 RMDF114_bit; struct { union { const unsigned char RMDF114L; const __BITS8 RMDF114L_bit; }; union { const unsigned char RMDF114H; const __BITS8 RMDF114H_bit; }; }; } @ 0xF048A;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF214; __BITS16 RMDF214_bit; struct { union { const unsigned char RMDF214L; const __BITS8 RMDF214L_bit; }; union { const unsigned char RMDF214H; const __BITS8 RMDF214H_bit; }; }; } @ 0xF048C;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF314; __BITS16 RMDF314_bit; struct { union { const unsigned char RMDF314L; const __BITS8 RMDF314L_bit; }; union { const unsigned char RMDF314H; const __BITS8 RMDF314H_bit; }; }; } @ 0xF048E;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDL15; __BITS16 RMIDL15_bit; struct { union { const unsigned char RMIDL15L; const __BITS8 RMIDL15L_bit; }; union { const unsigned char RMIDL15H; const __BITS8 RMIDL15H_bit; }; }; } @ 0xF0490;
__near __no_bit_access __no_init volatile const union { unsigned short RMIDH15; __BITS16 RMIDH15_bit; struct { union { const unsigned char RMIDH15L; const __BITS8 RMIDH15L_bit; }; union { const unsigned char RMIDH15H; const __BITS8 RMIDH15H_bit; }; }; } @ 0xF0492;
__near __no_bit_access __no_init volatile const union { unsigned short RMTS15; __BITS16 RMTS15_bit; struct { union { const unsigned char RMTS15L; const __BITS8 RMTS15L_bit; }; union { const unsigned char RMTS15H; const __BITS8 RMTS15H_bit; }; }; } @ 0xF0494;
__near __no_bit_access __no_init volatile const union { unsigned short RMPTR15; __BITS16 RMPTR15_bit; struct { union { const unsigned char RMPTR15L; const __BITS8 RMPTR15L_bit; }; union { const unsigned char RMPTR15H; const __BITS8 RMPTR15H_bit; }; }; } @ 0xF0496;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF015; __BITS16 RMDF015_bit; struct { union { const unsigned char RMDF015L; const __BITS8 RMDF015L_bit; }; union { const unsigned char RMDF015H; const __BITS8 RMDF015H_bit; }; }; } @ 0xF0498;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF115; __BITS16 RMDF115_bit; struct { union { const unsigned char RMDF115L; const __BITS8 RMDF115L_bit; }; union { const unsigned char RMDF115H; const __BITS8 RMDF115H_bit; }; }; } @ 0xF049A;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF215; __BITS16 RMDF215_bit; struct { union { const unsigned char RMDF215L; const __BITS8 RMDF215L_bit; }; union { const unsigned char RMDF215H; const __BITS8 RMDF215H_bit; }; }; } @ 0xF049C;
__near __no_bit_access __no_init volatile const union { unsigned short RMDF315; __BITS16 RMDF315_bit; struct { union { const unsigned char RMDF315L; const __BITS8 RMDF315L_bit; }; union { const unsigned char RMDF315H; const __BITS8 RMDF315H_bit; }; }; } @ 0xF049E;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC0; __BITS16 RPGACC0_bit; struct { union { unsigned char RPGACC0L; __BITS8 RPGACC0L_bit; }; union { unsigned char RPGACC0H; __BITS8 RPGACC0H_bit; }; }; } @ 0xF0580;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC1; __BITS16 RPGACC1_bit; struct { union { unsigned char RPGACC1L; __BITS8 RPGACC1L_bit; }; union { unsigned char RPGACC1H; __BITS8 RPGACC1H_bit; }; }; } @ 0xF0582;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC2; __BITS16 RPGACC2_bit; struct { union { unsigned char RPGACC2L; __BITS8 RPGACC2L_bit; }; union { unsigned char RPGACC2H; __BITS8 RPGACC2H_bit; }; }; } @ 0xF0584;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC3; __BITS16 RPGACC3_bit; struct { union { unsigned char RPGACC3L; __BITS8 RPGACC3L_bit; }; union { unsigned char RPGACC3H; __BITS8 RPGACC3H_bit; }; }; } @ 0xF0586;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC4; __BITS16 RPGACC4_bit; struct { union { unsigned char RPGACC4L; __BITS8 RPGACC4L_bit; }; union { unsigned char RPGACC4H; __BITS8 RPGACC4H_bit; }; }; } @ 0xF0588;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC5; __BITS16 RPGACC5_bit; struct { union { unsigned char RPGACC5L; __BITS8 RPGACC5L_bit; }; union { unsigned char RPGACC5H; __BITS8 RPGACC5H_bit; }; }; } @ 0xF058A;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC6; __BITS16 RPGACC6_bit; struct { union { unsigned char RPGACC6L; __BITS8 RPGACC6L_bit; }; union { unsigned char RPGACC6H; __BITS8 RPGACC6H_bit; }; }; } @ 0xF058C;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC7; __BITS16 RPGACC7_bit; struct { union { unsigned char RPGACC7L; __BITS8 RPGACC7L_bit; }; union { unsigned char RPGACC7H; __BITS8 RPGACC7H_bit; }; }; } @ 0xF058E;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC8; __BITS16 RPGACC8_bit; struct { union { unsigned char RPGACC8L; __BITS8 RPGACC8L_bit; }; union { unsigned char RPGACC8H; __BITS8 RPGACC8H_bit; }; }; } @ 0xF0590;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC9; __BITS16 RPGACC9_bit; struct { union { unsigned char RPGACC9L; __BITS8 RPGACC9L_bit; }; union { unsigned char RPGACC9H; __BITS8 RPGACC9H_bit; }; }; } @ 0xF0592;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC10; __BITS16 RPGACC10_bit; struct { union { unsigned char RPGACC10L; __BITS8 RPGACC10L_bit; }; union { unsigned char RPGACC10H; __BITS8 RPGACC10H_bit; }; }; } @ 0xF0594;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC11; __BITS16 RPGACC11_bit; struct { union { unsigned char RPGACC11L; __BITS8 RPGACC11L_bit; }; union { unsigned char RPGACC11H; __BITS8 RPGACC11H_bit; }; }; } @ 0xF0596;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC12; __BITS16 RPGACC12_bit; struct { union { unsigned char RPGACC12L; __BITS8 RPGACC12L_bit; }; union { unsigned char RPGACC12H; __BITS8 RPGACC12H_bit; }; }; } @ 0xF0598;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC13; __BITS16 RPGACC13_bit; struct { union { unsigned char RPGACC13L; __BITS8 RPGACC13L_bit; }; union { unsigned char RPGACC13H; __BITS8 RPGACC13H_bit; }; }; } @ 0xF059A;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC14; __BITS16 RPGACC14_bit; struct { union { unsigned char RPGACC14L; __BITS8 RPGACC14L_bit; }; union { unsigned char RPGACC14H; __BITS8 RPGACC14H_bit; }; }; } @ 0xF059C;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC15; __BITS16 RPGACC15_bit; struct { union { unsigned char RPGACC15L; __BITS8 RPGACC15L_bit; }; union { unsigned char RPGACC15H; __BITS8 RPGACC15H_bit; }; }; } @ 0xF059E;
__near __no_bit_access __no_init volatile const union { unsigned short RFIDL0; __BITS16 RFIDL0_bit; } @ 0xF05A0;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC16; __BITS16 RPGACC16_bit; struct { union { const unsigned char RFIDL0L; const __BITS8 RFIDL0L_bit; }; union { const unsigned char RFIDL0H; const __BITS8 RFIDL0H_bit; }; }; } @ 0xF05A0;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC16H; __BITS8 RPGACC16H_bit; } @ 0xF05A1;
__near __no_bit_access __no_init volatile const union { unsigned short RFIDH0; __BITS16 RFIDH0_bit; } @ 0xF05A2;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC17; __BITS16 RPGACC17_bit; struct { union { const unsigned char RFIDH0L; const __BITS8 RFIDH0L_bit; }; union { const unsigned char RFIDH0H; const __BITS8 RFIDH0H_bit; }; }; } @ 0xF05A2;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC17H; __BITS8 RPGACC17H_bit; } @ 0xF05A3;
__near __no_bit_access __no_init volatile const union { unsigned short RFTS0; __BITS16 RFTS0_bit; } @ 0xF05A4;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC18; __BITS16 RPGACC18_bit; struct { union { const unsigned char RFTS0L; const __BITS8 RFTS0L_bit; }; union { const unsigned char RFTS0H; const __BITS8 RFTS0H_bit; }; }; } @ 0xF05A4;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC18H; __BITS8 RPGACC18H_bit; } @ 0xF05A5;
__near __no_bit_access __no_init volatile const union { unsigned short RFPTR0; __BITS16 RFPTR0_bit; } @ 0xF05A6;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC19; __BITS16 RPGACC19_bit; struct { union { const unsigned char RFPTR0L; const __BITS8 RFPTR0L_bit; }; union { const unsigned char RFPTR0H; const __BITS8 RFPTR0H_bit; }; }; } @ 0xF05A6;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC19H; __BITS8 RPGACC19H_bit; } @ 0xF05A7;
__near __no_bit_access __no_init volatile const union { unsigned short RFDF00; __BITS16 RFDF00_bit; } @ 0xF05A8;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC20; __BITS16 RPGACC20_bit; struct { union { const unsigned char RFDF00L; const __BITS8 RFDF00L_bit; }; union { const unsigned char RFDF00H; const __BITS8 RFDF00H_bit; }; }; } @ 0xF05A8;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC20H; __BITS8 RPGACC20H_bit; } @ 0xF05A9;
__near __no_bit_access __no_init volatile const union { unsigned short RFDF10; __BITS16 RFDF10_bit; } @ 0xF05AA;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC21; __BITS16 RPGACC21_bit; struct { union { const unsigned char RFDF10L; const __BITS8 RFDF10L_bit; }; union { const unsigned char RFDF10H; const __BITS8 RFDF10H_bit; }; }; } @ 0xF05AA;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC21H; __BITS8 RPGACC21H_bit; } @ 0xF05AB;
__near __no_bit_access __no_init volatile const union { unsigned short RFDF20; __BITS16 RFDF20_bit; } @ 0xF05AC;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC22; __BITS16 RPGACC22_bit; struct { union { const unsigned char RFDF20L; const __BITS8 RFDF20L_bit; }; union { const unsigned char RFDF20H; const __BITS8 RFDF20H_bit; }; }; } @ 0xF05AC;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC22H; __BITS8 RPGACC22H_bit; } @ 0xF05AD;
__near __no_bit_access __no_init volatile const union { unsigned short RFDF30; __BITS16 RFDF30_bit; } @ 0xF05AE;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC23; __BITS16 RPGACC23_bit; struct { union { const unsigned char RFDF30L; const __BITS8 RFDF30L_bit; }; union { const unsigned char RFDF30H; const __BITS8 RFDF30H_bit; }; }; } @ 0xF05AE;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC23H; __BITS8 RPGACC23H_bit; } @ 0xF05AF;
__near __no_bit_access __no_init volatile const union { unsigned short RFIDL1; __BITS16 RFIDL1_bit; } @ 0xF05B0;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC24; __BITS16 RPGACC24_bit; struct { union { const unsigned char RFIDL1L; const __BITS8 RFIDL1L_bit; }; union { const unsigned char RFIDL1H; const __BITS8 RFIDL1H_bit; }; }; } @ 0xF05B0;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC24H; __BITS8 RPGACC24H_bit; } @ 0xF05B1;
__near __no_bit_access __no_init volatile const union { unsigned short RFIDH1; __BITS16 RFIDH1_bit; } @ 0xF05B2;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC25; __BITS16 RPGACC25_bit; struct { union { const unsigned char RFIDH1L; const __BITS8 RFIDH1L_bit; }; union { const unsigned char RFIDH1H; const __BITS8 RFIDH1H_bit; }; }; } @ 0xF05B2;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC25H; __BITS8 RPGACC25H_bit; } @ 0xF05B3;
__near __no_bit_access __no_init volatile const union { unsigned short RFTS1; __BITS16 RFTS1_bit; } @ 0xF05B4;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC26; __BITS16 RPGACC26_bit; struct { union { const unsigned char RFTS1L; const __BITS8 RFTS1L_bit; }; union { const unsigned char RFTS1H; const __BITS8 RFTS1H_bit; }; }; } @ 0xF05B4;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC26H; __BITS8 RPGACC26H_bit; } @ 0xF05B5;
__near __no_bit_access __no_init volatile const union { unsigned short RFPTR1; __BITS16 RFPTR1_bit; } @ 0xF05B6;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC27; __BITS16 RPGACC27_bit; struct { union { const unsigned char RFPTR1L; const __BITS8 RFPTR1L_bit; }; union { const unsigned char RFPTR1H; const __BITS8 RFPTR1H_bit; }; }; } @ 0xF05B6;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC27H; __BITS8 RPGACC27H_bit; } @ 0xF05B7;
__near __no_bit_access __no_init volatile const union { unsigned short RFDF01; __BITS16 RFDF01_bit; } @ 0xF05B8;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC28; __BITS16 RPGACC28_bit; struct { union { const unsigned char RFDF01L; const __BITS8 RFDF01L_bit; }; union { const unsigned char RFDF01H; const __BITS8 RFDF01H_bit; }; }; } @ 0xF05B8;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC28H; __BITS8 RPGACC28H_bit; } @ 0xF05B9;
__near __no_bit_access __no_init volatile const union { unsigned short RFDF11; __BITS16 RFDF11_bit; } @ 0xF05BA;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC29; __BITS16 RPGACC29_bit; struct { union { const unsigned char RFDF11L; const __BITS8 RFDF11L_bit; }; union { const unsigned char RFDF11H; const __BITS8 RFDF11H_bit; }; }; } @ 0xF05BA;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC29H; __BITS8 RPGACC29H_bit; } @ 0xF05BB;
__near __no_bit_access __no_init volatile const union { unsigned short RFDF21; __BITS16 RFDF21_bit; } @ 0xF05BC;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC30; __BITS16 RPGACC30_bit; struct { union { const unsigned char RFDF21L; const __BITS8 RFDF21L_bit; }; union { const unsigned char RFDF21H; const __BITS8 RFDF21H_bit; }; }; } @ 0xF05BC;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC30H; __BITS8 RPGACC30H_bit; } @ 0xF05BD;
__near __no_bit_access __no_init volatile const union { unsigned short RFDF31; __BITS16 RFDF31_bit; } @ 0xF05BE;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC31; __BITS16 RPGACC31_bit; struct { union { const unsigned char RFDF31L; const __BITS8 RFDF31L_bit; }; union { const unsigned char RFDF31H; const __BITS8 RFDF31H_bit; }; }; } @ 0xF05BE;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC31H; __BITS8 RPGACC31H_bit; } @ 0xF05BF;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC32; __BITS16 RPGACC32_bit; struct { union { unsigned char RPGACC32L; __BITS8 RPGACC32L_bit; }; union { unsigned char RPGACC32H; __BITS8 RPGACC32H_bit; }; }; } @ 0xF05C0;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC33; __BITS16 RPGACC33_bit; struct { union { unsigned char RPGACC33L; __BITS8 RPGACC33L_bit; }; union { unsigned char RPGACC33H; __BITS8 RPGACC33H_bit; }; }; } @ 0xF05C2;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC34; __BITS16 RPGACC34_bit; struct { union { unsigned char RPGACC34L; __BITS8 RPGACC34L_bit; }; union { unsigned char RPGACC34H; __BITS8 RPGACC34H_bit; }; }; } @ 0xF05C4;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC35; __BITS16 RPGACC35_bit; struct { union { unsigned char RPGACC35L; __BITS8 RPGACC35L_bit; }; union { unsigned char RPGACC35H; __BITS8 RPGACC35H_bit; }; }; } @ 0xF05C6;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC36; __BITS16 RPGACC36_bit; struct { union { unsigned char RPGACC36L; __BITS8 RPGACC36L_bit; }; union { unsigned char RPGACC36H; __BITS8 RPGACC36H_bit; }; }; } @ 0xF05C8;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC37; __BITS16 RPGACC37_bit; struct { union { unsigned char RPGACC37L; __BITS8 RPGACC37L_bit; }; union { unsigned char RPGACC37H; __BITS8 RPGACC37H_bit; }; }; } @ 0xF05CA;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC38; __BITS16 RPGACC38_bit; struct { union { unsigned char RPGACC38L; __BITS8 RPGACC38L_bit; }; union { unsigned char RPGACC38H; __BITS8 RPGACC38H_bit; }; }; } @ 0xF05CC;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC39; __BITS16 RPGACC39_bit; struct { union { unsigned char RPGACC39L; __BITS8 RPGACC39L_bit; }; union { unsigned char RPGACC39H; __BITS8 RPGACC39H_bit; }; }; } @ 0xF05CE;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC40; __BITS16 RPGACC40_bit; struct { union { unsigned char RPGACC40L; __BITS8 RPGACC40L_bit; }; union { unsigned char RPGACC40H; __BITS8 RPGACC40H_bit; }; }; } @ 0xF05D0;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC41; __BITS16 RPGACC41_bit; struct { union { unsigned char RPGACC41L; __BITS8 RPGACC41L_bit; }; union { unsigned char RPGACC41H; __BITS8 RPGACC41H_bit; }; }; } @ 0xF05D2;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC42; __BITS16 RPGACC42_bit; struct { union { unsigned char RPGACC42L; __BITS8 RPGACC42L_bit; }; union { unsigned char RPGACC42H; __BITS8 RPGACC42H_bit; }; }; } @ 0xF05D4;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC43; __BITS16 RPGACC43_bit; struct { union { unsigned char RPGACC43L; __BITS8 RPGACC43L_bit; }; union { unsigned char RPGACC43H; __BITS8 RPGACC43H_bit; }; }; } @ 0xF05D6;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC44; __BITS16 RPGACC44_bit; struct { union { unsigned char RPGACC44L; __BITS8 RPGACC44L_bit; }; union { unsigned char RPGACC44H; __BITS8 RPGACC44H_bit; }; }; } @ 0xF05D8;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC45; __BITS16 RPGACC45_bit; struct { union { unsigned char RPGACC45L; __BITS8 RPGACC45L_bit; }; union { unsigned char RPGACC45H; __BITS8 RPGACC45H_bit; }; }; } @ 0xF05DA;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC46; __BITS16 RPGACC46_bit; struct { union { unsigned char RPGACC46L; __BITS8 RPGACC46L_bit; }; union { unsigned char RPGACC46H; __BITS8 RPGACC46H_bit; }; }; } @ 0xF05DC;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC47; __BITS16 RPGACC47_bit; struct { union { unsigned char RPGACC47L; __BITS8 RPGACC47L_bit; }; union { unsigned char RPGACC47H; __BITS8 RPGACC47H_bit; }; }; } @ 0xF05DE;
__near __no_bit_access __no_init volatile union { unsigned short CFIDL0; __BITS16 CFIDL0_bit; } @ 0xF05E0;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC48; __BITS16 RPGACC48_bit; struct { union { unsigned char CFIDL0L; __BITS8 CFIDL0L_bit; }; union { unsigned char CFIDL0H; __BITS8 CFIDL0H_bit; }; }; } @ 0xF05E0;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC48H; __BITS8 RPGACC48H_bit; } @ 0xF05E1;
__near __no_bit_access __no_init volatile union { unsigned short CFIDH0; __BITS16 CFIDH0_bit; } @ 0xF05E2;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC49; __BITS16 RPGACC49_bit; struct { union { unsigned char CFIDH0L; __BITS8 CFIDH0L_bit; }; union { unsigned char CFIDH0H; __BITS8 CFIDH0H_bit; }; }; } @ 0xF05E2;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC49H; __BITS8 RPGACC49H_bit; } @ 0xF05E3;
__near __no_bit_access __no_init volatile const union { unsigned short CFTS0; __BITS16 CFTS0_bit; } @ 0xF05E4;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC50; __BITS16 RPGACC50_bit; struct { union { const unsigned char CFTS0L; const __BITS8 CFTS0L_bit; }; union { const unsigned char CFTS0H; const __BITS8 CFTS0H_bit; }; }; } @ 0xF05E4;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC50H; __BITS8 RPGACC50H_bit; } @ 0xF05E5;
__near __no_bit_access __no_init volatile union { unsigned short CFPTR0; __BITS16 CFPTR0_bit; } @ 0xF05E6;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC51; __BITS16 RPGACC51_bit; struct { union { unsigned char CFPTR0L; __BITS8 CFPTR0L_bit; }; union { unsigned char CFPTR0H; __BITS8 CFPTR0H_bit; }; }; } @ 0xF05E6;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC51H; __BITS8 RPGACC51H_bit; } @ 0xF05E7;
__near __no_bit_access __no_init volatile union { unsigned short CFDF00; __BITS16 CFDF00_bit; } @ 0xF05E8;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC52; __BITS16 RPGACC52_bit; struct { union { unsigned char CFDF00L; __BITS8 CFDF00L_bit; }; union { unsigned char CFDF00H; __BITS8 CFDF00H_bit; }; }; } @ 0xF05E8;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC52H; __BITS8 RPGACC52H_bit; } @ 0xF05E9;
__near __no_bit_access __no_init volatile union { unsigned short CFDF10; __BITS16 CFDF10_bit; } @ 0xF05EA;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC53; __BITS16 RPGACC53_bit; struct { union { unsigned char CFDF10L; __BITS8 CFDF10L_bit; }; union { unsigned char CFDF10H; __BITS8 CFDF10H_bit; }; }; } @ 0xF05EA;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC53H; __BITS8 RPGACC53H_bit; } @ 0xF05EB;
__near __no_bit_access __no_init volatile union { unsigned short CFDF20; __BITS16 CFDF20_bit; } @ 0xF05EC;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC54; __BITS16 RPGACC54_bit; struct { union { unsigned char CFDF20L; __BITS8 CFDF20L_bit; }; union { unsigned char CFDF20H; __BITS8 CFDF20H_bit; }; }; } @ 0xF05EC;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC54H; __BITS8 RPGACC54H_bit; } @ 0xF05ED;
__near __no_bit_access __no_init volatile union { unsigned short CFDF30; __BITS16 CFDF30_bit; } @ 0xF05EE;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC55; __BITS16 RPGACC55_bit; struct { union { unsigned char CFDF30L; __BITS8 CFDF30L_bit; }; union { unsigned char CFDF30H; __BITS8 CFDF30H_bit; }; }; } @ 0xF05EE;
__near __no_bit_access __no_init volatile union { unsigned char RPGACC55H; __BITS8 RPGACC55H_bit; } @ 0xF05EF;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC56; __BITS16 RPGACC56_bit; struct { union { unsigned char RPGACC56L; __BITS8 RPGACC56L_bit; }; union { unsigned char RPGACC56H; __BITS8 RPGACC56H_bit; }; }; } @ 0xF05F0;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC57; __BITS16 RPGACC57_bit; struct { union { unsigned char RPGACC57L; __BITS8 RPGACC57L_bit; }; union { unsigned char RPGACC57H; __BITS8 RPGACC57H_bit; }; }; } @ 0xF05F2;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC58; __BITS16 RPGACC58_bit; struct { union { unsigned char RPGACC58L; __BITS8 RPGACC58L_bit; }; union { unsigned char RPGACC58H; __BITS8 RPGACC58H_bit; }; }; } @ 0xF05F4;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC59; __BITS16 RPGACC59_bit; struct { union { unsigned char RPGACC59L; __BITS8 RPGACC59L_bit; }; union { unsigned char RPGACC59H; __BITS8 RPGACC59H_bit; }; }; } @ 0xF05F6;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC60; __BITS16 RPGACC60_bit; struct { union { unsigned char RPGACC60L; __BITS8 RPGACC60L_bit; }; union { unsigned char RPGACC60H; __BITS8 RPGACC60H_bit; }; }; } @ 0xF05F8;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC61; __BITS16 RPGACC61_bit; struct { union { unsigned char RPGACC61L; __BITS8 RPGACC61L_bit; }; union { unsigned char RPGACC61H; __BITS8 RPGACC61H_bit; }; }; } @ 0xF05FA;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC62; __BITS16 RPGACC62_bit; struct { union { unsigned char RPGACC62L; __BITS8 RPGACC62L_bit; }; union { unsigned char RPGACC62H; __BITS8 RPGACC62H_bit; }; }; } @ 0xF05FC;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC63; __BITS16 RPGACC63_bit; struct { union { unsigned char RPGACC63L; __BITS8 RPGACC63L_bit; }; union { unsigned char RPGACC63H; __BITS8 RPGACC63H_bit; }; }; } @ 0xF05FE;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC64; __BITS16 RPGACC64_bit; } @ 0xF0600;
__near __no_bit_access __no_init volatile union { unsigned short TMIDL0; __BITS16 TMIDL0_bit; struct { union { unsigned char RPGACC64L; __BITS8 RPGACC64L_bit; }; union { unsigned char RPGACC64H; __BITS8 RPGACC64H_bit; }; }; } @ 0xF0600;
__near __no_bit_access __no_init volatile union { unsigned char TMIDL0H; __BITS8 TMIDL0H_bit; } @ 0xF0601;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC65; __BITS16 RPGACC65_bit; } @ 0xF0602;
__near __no_bit_access __no_init volatile union { unsigned short TMIDH0; __BITS16 TMIDH0_bit; struct { union { unsigned char RPGACC65L; __BITS8 RPGACC65L_bit; }; union { unsigned char RPGACC65H; __BITS8 RPGACC65H_bit; }; }; } @ 0xF0602;
__near __no_bit_access __no_init volatile union { unsigned char TMIDH0H; __BITS8 TMIDH0H_bit; } @ 0xF0603;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC66; __BITS16 RPGACC66_bit; struct { union { unsigned char RPGACC66L; __BITS8 RPGACC66L_bit; }; union { unsigned char RPGACC66H; __BITS8 RPGACC66H_bit; }; }; } @ 0xF0604;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC67; __BITS16 RPGACC67_bit; } @ 0xF0606;
__near __no_bit_access __no_init volatile union { unsigned short TMPTR0; __BITS16 TMPTR0_bit; struct { union { unsigned char RPGACC67L; __BITS8 RPGACC67L_bit; }; union { unsigned char RPGACC67H; __BITS8 RPGACC67H_bit; }; }; } @ 0xF0606;
__near __no_bit_access __no_init volatile union { unsigned char TMPTR0H; __BITS8 TMPTR0H_bit; } @ 0xF0607;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC68; __BITS16 RPGACC68_bit; } @ 0xF0608;
__near __no_bit_access __no_init volatile union { unsigned short TMDF00; __BITS16 TMDF00_bit; struct { union { unsigned char RPGACC68L; __BITS8 RPGACC68L_bit; }; union { unsigned char RPGACC68H; __BITS8 RPGACC68H_bit; }; }; } @ 0xF0608;
__near __no_bit_access __no_init volatile union { unsigned char TMDF00H; __BITS8 TMDF00H_bit; } @ 0xF0609;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC69; __BITS16 RPGACC69_bit; } @ 0xF060A;
__near __no_bit_access __no_init volatile union { unsigned short TMDF10; __BITS16 TMDF10_bit; struct { union { unsigned char RPGACC69L; __BITS8 RPGACC69L_bit; }; union { unsigned char RPGACC69H; __BITS8 RPGACC69H_bit; }; }; } @ 0xF060A;
__near __no_bit_access __no_init volatile union { unsigned char TMDF10H; __BITS8 TMDF10H_bit; } @ 0xF060B;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC70; __BITS16 RPGACC70_bit; } @ 0xF060C;
__near __no_bit_access __no_init volatile union { unsigned short TMDF20; __BITS16 TMDF20_bit; struct { union { unsigned char RPGACC70L; __BITS8 RPGACC70L_bit; }; union { unsigned char RPGACC70H; __BITS8 RPGACC70H_bit; }; }; } @ 0xF060C;
__near __no_bit_access __no_init volatile union { unsigned char TMDF20H; __BITS8 TMDF20H_bit; } @ 0xF060D;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC71; __BITS16 RPGACC71_bit; } @ 0xF060E;
__near __no_bit_access __no_init volatile union { unsigned short TMDF30; __BITS16 TMDF30_bit; struct { union { unsigned char RPGACC71L; __BITS8 RPGACC71L_bit; }; union { unsigned char RPGACC71H; __BITS8 RPGACC71H_bit; }; }; } @ 0xF060E;
__near __no_bit_access __no_init volatile union { unsigned char TMDF30H; __BITS8 TMDF30H_bit; } @ 0xF060F;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC72; __BITS16 RPGACC72_bit; } @ 0xF0610;
__near __no_bit_access __no_init volatile union { unsigned short TMIDL1; __BITS16 TMIDL1_bit; struct { union { unsigned char RPGACC72L; __BITS8 RPGACC72L_bit; }; union { unsigned char RPGACC72H; __BITS8 RPGACC72H_bit; }; }; } @ 0xF0610;
__near __no_bit_access __no_init volatile union { unsigned char TMIDL1H; __BITS8 TMIDL1H_bit; } @ 0xF0611;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC73; __BITS16 RPGACC73_bit; } @ 0xF0612;
__near __no_bit_access __no_init volatile union { unsigned short TMIDH1; __BITS16 TMIDH1_bit; struct { union { unsigned char RPGACC73L; __BITS8 RPGACC73L_bit; }; union { unsigned char RPGACC73H; __BITS8 RPGACC73H_bit; }; }; } @ 0xF0612;
__near __no_bit_access __no_init volatile union { unsigned char TMIDH1H; __BITS8 TMIDH1H_bit; } @ 0xF0613;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC74; __BITS16 RPGACC74_bit; struct { union { unsigned char RPGACC74L; __BITS8 RPGACC74L_bit; }; union { unsigned char RPGACC74H; __BITS8 RPGACC74H_bit; }; }; } @ 0xF0614;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC75; __BITS16 RPGACC75_bit; } @ 0xF0616;
__near __no_bit_access __no_init volatile union { unsigned short TMPTR1; __BITS16 TMPTR1_bit; struct { union { unsigned char RPGACC75L; __BITS8 RPGACC75L_bit; }; union { unsigned char RPGACC75H; __BITS8 RPGACC75H_bit; }; }; } @ 0xF0616;
__near __no_bit_access __no_init volatile union { unsigned char TMPTR1H; __BITS8 TMPTR1H_bit; } @ 0xF0617;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC76; __BITS16 RPGACC76_bit; } @ 0xF0618;
__near __no_bit_access __no_init volatile union { unsigned short TMDF01; __BITS16 TMDF01_bit; struct { union { unsigned char RPGACC76L; __BITS8 RPGACC76L_bit; }; union { unsigned char RPGACC76H; __BITS8 RPGACC76H_bit; }; }; } @ 0xF0618;
__near __no_bit_access __no_init volatile union { unsigned char TMDF01H; __BITS8 TMDF01H_bit; } @ 0xF0619;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC77; __BITS16 RPGACC77_bit; } @ 0xF061A;
__near __no_bit_access __no_init volatile union { unsigned short TMDF11; __BITS16 TMDF11_bit; struct { union { unsigned char RPGACC77L; __BITS8 RPGACC77L_bit; }; union { unsigned char RPGACC77H; __BITS8 RPGACC77H_bit; }; }; } @ 0xF061A;
__near __no_bit_access __no_init volatile union { unsigned char TMDF11H; __BITS8 TMDF11H_bit; } @ 0xF061B;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC78; __BITS16 RPGACC78_bit; } @ 0xF061C;
__near __no_bit_access __no_init volatile union { unsigned short TMDF21; __BITS16 TMDF21_bit; struct { union { unsigned char RPGACC78L; __BITS8 RPGACC78L_bit; }; union { unsigned char RPGACC78H; __BITS8 RPGACC78H_bit; }; }; } @ 0xF061C;
__near __no_bit_access __no_init volatile union { unsigned char TMDF21H; __BITS8 TMDF21H_bit; } @ 0xF061D;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC79; __BITS16 RPGACC79_bit; } @ 0xF061E;
__near __no_bit_access __no_init volatile union { unsigned short TMDF31; __BITS16 TMDF31_bit; struct { union { unsigned char RPGACC79L; __BITS8 RPGACC79L_bit; }; union { unsigned char RPGACC79H; __BITS8 RPGACC79H_bit; }; }; } @ 0xF061E;
__near __no_bit_access __no_init volatile union { unsigned char TMDF31H; __BITS8 TMDF31H_bit; } @ 0xF061F;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC80; __BITS16 RPGACC80_bit; } @ 0xF0620;
__near __no_bit_access __no_init volatile union { unsigned short TMIDL2; __BITS16 TMIDL2_bit; struct { union { unsigned char RPGACC80L; __BITS8 RPGACC80L_bit; }; union { unsigned char RPGACC80H; __BITS8 RPGACC80H_bit; }; }; } @ 0xF0620;
__near __no_bit_access __no_init volatile union { unsigned char TMIDL2H; __BITS8 TMIDL2H_bit; } @ 0xF0621;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC81; __BITS16 RPGACC81_bit; } @ 0xF0622;
__near __no_bit_access __no_init volatile union { unsigned short TMIDH2; __BITS16 TMIDH2_bit; struct { union { unsigned char RPGACC81L; __BITS8 RPGACC81L_bit; }; union { unsigned char RPGACC81H; __BITS8 RPGACC81H_bit; }; }; } @ 0xF0622;
__near __no_bit_access __no_init volatile union { unsigned char TMIDH2H; __BITS8 TMIDH2H_bit; } @ 0xF0623;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC82; __BITS16 RPGACC82_bit; struct { union { unsigned char RPGACC82L; __BITS8 RPGACC82L_bit; }; union { unsigned char RPGACC82H; __BITS8 RPGACC82H_bit; }; }; } @ 0xF0624;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC83; __BITS16 RPGACC83_bit; } @ 0xF0626;
__near __no_bit_access __no_init volatile union { unsigned short TMPTR2; __BITS16 TMPTR2_bit; struct { union { unsigned char RPGACC83L; __BITS8 RPGACC83L_bit; }; union { unsigned char RPGACC83H; __BITS8 RPGACC83H_bit; }; }; } @ 0xF0626;
__near __no_bit_access __no_init volatile union { unsigned char TMPTR2H; __BITS8 TMPTR2H_bit; } @ 0xF0627;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC84; __BITS16 RPGACC84_bit; } @ 0xF0628;
__near __no_bit_access __no_init volatile union { unsigned short TMDF02; __BITS16 TMDF02_bit; struct { union { unsigned char RPGACC84L; __BITS8 RPGACC84L_bit; }; union { unsigned char RPGACC84H; __BITS8 RPGACC84H_bit; }; }; } @ 0xF0628;
__near __no_bit_access __no_init volatile union { unsigned char TMDF02H; __BITS8 TMDF02H_bit; } @ 0xF0629;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC85; __BITS16 RPGACC85_bit; } @ 0xF062A;
__near __no_bit_access __no_init volatile union { unsigned short TMDF12; __BITS16 TMDF12_bit; struct { union { unsigned char RPGACC85L; __BITS8 RPGACC85L_bit; }; union { unsigned char RPGACC85H; __BITS8 RPGACC85H_bit; }; }; } @ 0xF062A;
__near __no_bit_access __no_init volatile union { unsigned char TMDF12H; __BITS8 TMDF12H_bit; } @ 0xF062B;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC86; __BITS16 RPGACC86_bit; } @ 0xF062C;
__near __no_bit_access __no_init volatile union { unsigned short TMDF22; __BITS16 TMDF22_bit; struct { union { unsigned char RPGACC86L; __BITS8 RPGACC86L_bit; }; union { unsigned char RPGACC86H; __BITS8 RPGACC86H_bit; }; }; } @ 0xF062C;
__near __no_bit_access __no_init volatile union { unsigned char TMDF22H; __BITS8 TMDF22H_bit; } @ 0xF062D;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC87; __BITS16 RPGACC87_bit; } @ 0xF062E;
__near __no_bit_access __no_init volatile union { unsigned short TMDF32; __BITS16 TMDF32_bit; struct { union { unsigned char RPGACC87L; __BITS8 RPGACC87L_bit; }; union { unsigned char RPGACC87H; __BITS8 RPGACC87H_bit; }; }; } @ 0xF062E;
__near __no_bit_access __no_init volatile union { unsigned char TMDF32H; __BITS8 TMDF32H_bit; } @ 0xF062F;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC88; __BITS16 RPGACC88_bit; } @ 0xF0630;
__near __no_bit_access __no_init volatile union { unsigned short TMIDL3; __BITS16 TMIDL3_bit; struct { union { unsigned char RPGACC88L; __BITS8 RPGACC88L_bit; }; union { unsigned char RPGACC88H; __BITS8 RPGACC88H_bit; }; }; } @ 0xF0630;
__near __no_bit_access __no_init volatile union { unsigned char TMIDL3H; __BITS8 TMIDL3H_bit; } @ 0xF0631;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC89; __BITS16 RPGACC89_bit; } @ 0xF0632;
__near __no_bit_access __no_init volatile union { unsigned short TMIDH3; __BITS16 TMIDH3_bit; struct { union { unsigned char RPGACC89L; __BITS8 RPGACC89L_bit; }; union { unsigned char RPGACC89H; __BITS8 RPGACC89H_bit; }; }; } @ 0xF0632;
__near __no_bit_access __no_init volatile union { unsigned char TMIDH3H; __BITS8 TMIDH3H_bit; } @ 0xF0633;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC90; __BITS16 RPGACC90_bit; struct { union { unsigned char RPGACC90L; __BITS8 RPGACC90L_bit; }; union { unsigned char RPGACC90H; __BITS8 RPGACC90H_bit; }; }; } @ 0xF0634;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC91; __BITS16 RPGACC91_bit; } @ 0xF0636;
__near __no_bit_access __no_init volatile union { unsigned short TMPTR3; __BITS16 TMPTR3_bit; struct { union { unsigned char RPGACC91L; __BITS8 RPGACC91L_bit; }; union { unsigned char RPGACC91H; __BITS8 RPGACC91H_bit; }; }; } @ 0xF0636;
__near __no_bit_access __no_init volatile union { unsigned char TMPTR3H; __BITS8 TMPTR3H_bit; } @ 0xF0637;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC92; __BITS16 RPGACC92_bit; } @ 0xF0638;
__near __no_bit_access __no_init volatile union { unsigned short TMDF03; __BITS16 TMDF03_bit; struct { union { unsigned char RPGACC92L; __BITS8 RPGACC92L_bit; }; union { unsigned char RPGACC92H; __BITS8 RPGACC92H_bit; }; }; } @ 0xF0638;
__near __no_bit_access __no_init volatile union { unsigned char TMDF03H; __BITS8 TMDF03H_bit; } @ 0xF0639;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC93; __BITS16 RPGACC93_bit; } @ 0xF063A;
__near __no_bit_access __no_init volatile union { unsigned short TMDF13; __BITS16 TMDF13_bit; struct { union { unsigned char RPGACC93L; __BITS8 RPGACC93L_bit; }; union { unsigned char RPGACC93H; __BITS8 RPGACC93H_bit; }; }; } @ 0xF063A;
__near __no_bit_access __no_init volatile union { unsigned char TMDF13H; __BITS8 TMDF13H_bit; } @ 0xF063B;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC94; __BITS16 RPGACC94_bit; } @ 0xF063C;
__near __no_bit_access __no_init volatile union { unsigned short TMDF23; __BITS16 TMDF23_bit; struct { union { unsigned char RPGACC94L; __BITS8 RPGACC94L_bit; }; union { unsigned char RPGACC94H; __BITS8 RPGACC94H_bit; }; }; } @ 0xF063C;
__near __no_bit_access __no_init volatile union { unsigned char TMDF23H; __BITS8 TMDF23H_bit; } @ 0xF063D;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC95; __BITS16 RPGACC95_bit; } @ 0xF063E;
__near __no_bit_access __no_init volatile union { unsigned short TMDF33; __BITS16 TMDF33_bit; struct { union { unsigned char RPGACC95L; __BITS8 RPGACC95L_bit; }; union { unsigned char RPGACC95H; __BITS8 RPGACC95H_bit; }; }; } @ 0xF063E;
__near __no_bit_access __no_init volatile union { unsigned char TMDF33H; __BITS8 TMDF33H_bit; } @ 0xF063F;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC96; __BITS16 RPGACC96_bit; struct { union { unsigned char RPGACC96L; __BITS8 RPGACC96L_bit; }; union { unsigned char RPGACC96H; __BITS8 RPGACC96H_bit; }; }; } @ 0xF0640;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC97; __BITS16 RPGACC97_bit; struct { union { unsigned char RPGACC97L; __BITS8 RPGACC97L_bit; }; union { unsigned char RPGACC97H; __BITS8 RPGACC97H_bit; }; }; } @ 0xF0642;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC98; __BITS16 RPGACC98_bit; struct { union { unsigned char RPGACC98L; __BITS8 RPGACC98L_bit; }; union { unsigned char RPGACC98H; __BITS8 RPGACC98H_bit; }; }; } @ 0xF0644;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC99; __BITS16 RPGACC99_bit; struct { union { unsigned char RPGACC99L; __BITS8 RPGACC99L_bit; }; union { unsigned char RPGACC99H; __BITS8 RPGACC99H_bit; }; }; } @ 0xF0646;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC100; __BITS16 RPGACC100_bit; struct { union { unsigned char RPGACC100L; __BITS8 RPGACC100L_bit; }; union { unsigned char RPGACC100H; __BITS8 RPGACC100H_bit; }; }; } @ 0xF0648;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC101; __BITS16 RPGACC101_bit; struct { union { unsigned char RPGACC101L; __BITS8 RPGACC101L_bit; }; union { unsigned char RPGACC101H; __BITS8 RPGACC101H_bit; }; }; } @ 0xF064A;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC102; __BITS16 RPGACC102_bit; struct { union { unsigned char RPGACC102L; __BITS8 RPGACC102L_bit; }; union { unsigned char RPGACC102H; __BITS8 RPGACC102H_bit; }; }; } @ 0xF064C;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC103; __BITS16 RPGACC103_bit; struct { union { unsigned char RPGACC103L; __BITS8 RPGACC103L_bit; }; union { unsigned char RPGACC103H; __BITS8 RPGACC103H_bit; }; }; } @ 0xF064E;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC104; __BITS16 RPGACC104_bit; struct { union { unsigned char RPGACC104L; __BITS8 RPGACC104L_bit; }; union { unsigned char RPGACC104H; __BITS8 RPGACC104H_bit; }; }; } @ 0xF0650;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC105; __BITS16 RPGACC105_bit; struct { union { unsigned char RPGACC105L; __BITS8 RPGACC105L_bit; }; union { unsigned char RPGACC105H; __BITS8 RPGACC105H_bit; }; }; } @ 0xF0652;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC106; __BITS16 RPGACC106_bit; struct { union { unsigned char RPGACC106L; __BITS8 RPGACC106L_bit; }; union { unsigned char RPGACC106H; __BITS8 RPGACC106H_bit; }; }; } @ 0xF0654;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC107; __BITS16 RPGACC107_bit; struct { union { unsigned char RPGACC107L; __BITS8 RPGACC107L_bit; }; union { unsigned char RPGACC107H; __BITS8 RPGACC107H_bit; }; }; } @ 0xF0656;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC108; __BITS16 RPGACC108_bit; struct { union { unsigned char RPGACC108L; __BITS8 RPGACC108L_bit; }; union { unsigned char RPGACC108H; __BITS8 RPGACC108H_bit; }; }; } @ 0xF0658;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC109; __BITS16 RPGACC109_bit; struct { union { unsigned char RPGACC109L; __BITS8 RPGACC109L_bit; }; union { unsigned char RPGACC109H; __BITS8 RPGACC109H_bit; }; }; } @ 0xF065A;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC110; __BITS16 RPGACC110_bit; struct { union { unsigned char RPGACC110L; __BITS8 RPGACC110L_bit; }; union { unsigned char RPGACC110H; __BITS8 RPGACC110H_bit; }; }; } @ 0xF065C;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC111; __BITS16 RPGACC111_bit; struct { union { unsigned char RPGACC111L; __BITS8 RPGACC111L_bit; }; union { unsigned char RPGACC111H; __BITS8 RPGACC111H_bit; }; }; } @ 0xF065E;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC112; __BITS16 RPGACC112_bit; struct { union { unsigned char RPGACC112L; __BITS8 RPGACC112L_bit; }; union { unsigned char RPGACC112H; __BITS8 RPGACC112H_bit; }; }; } @ 0xF0660;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC113; __BITS16 RPGACC113_bit; struct { union { unsigned char RPGACC113L; __BITS8 RPGACC113L_bit; }; union { unsigned char RPGACC113H; __BITS8 RPGACC113H_bit; }; }; } @ 0xF0662;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC114; __BITS16 RPGACC114_bit; struct { union { unsigned char RPGACC114L; __BITS8 RPGACC114L_bit; }; union { unsigned char RPGACC114H; __BITS8 RPGACC114H_bit; }; }; } @ 0xF0664;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC115; __BITS16 RPGACC115_bit; struct { union { unsigned char RPGACC115L; __BITS8 RPGACC115L_bit; }; union { unsigned char RPGACC115H; __BITS8 RPGACC115H_bit; }; }; } @ 0xF0666;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC116; __BITS16 RPGACC116_bit; struct { union { unsigned char RPGACC116L; __BITS8 RPGACC116L_bit; }; union { unsigned char RPGACC116H; __BITS8 RPGACC116H_bit; }; }; } @ 0xF0668;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC117; __BITS16 RPGACC117_bit; struct { union { unsigned char RPGACC117L; __BITS8 RPGACC117L_bit; }; union { unsigned char RPGACC117H; __BITS8 RPGACC117H_bit; }; }; } @ 0xF066A;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC118; __BITS16 RPGACC118_bit; struct { union { unsigned char RPGACC118L; __BITS8 RPGACC118L_bit; }; union { unsigned char RPGACC118H; __BITS8 RPGACC118H_bit; }; }; } @ 0xF066C;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC119; __BITS16 RPGACC119_bit; struct { union { unsigned char RPGACC119L; __BITS8 RPGACC119L_bit; }; union { unsigned char RPGACC119H; __BITS8 RPGACC119H_bit; }; }; } @ 0xF066E;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC120; __BITS16 RPGACC120_bit; struct { union { unsigned char RPGACC120L; __BITS8 RPGACC120L_bit; }; union { unsigned char RPGACC120H; __BITS8 RPGACC120H_bit; }; }; } @ 0xF0670;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC121; __BITS16 RPGACC121_bit; struct { union { unsigned char RPGACC121L; __BITS8 RPGACC121L_bit; }; union { unsigned char RPGACC121H; __BITS8 RPGACC121H_bit; }; }; } @ 0xF0672;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC122; __BITS16 RPGACC122_bit; struct { union { unsigned char RPGACC122L; __BITS8 RPGACC122L_bit; }; union { unsigned char RPGACC122H; __BITS8 RPGACC122H_bit; }; }; } @ 0xF0674;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC123; __BITS16 RPGACC123_bit; struct { union { unsigned char RPGACC123L; __BITS8 RPGACC123L_bit; }; union { unsigned char RPGACC123H; __BITS8 RPGACC123H_bit; }; }; } @ 0xF0676;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC124; __BITS16 RPGACC124_bit; struct { union { unsigned char RPGACC124L; __BITS8 RPGACC124L_bit; }; union { unsigned char RPGACC124H; __BITS8 RPGACC124H_bit; }; }; } @ 0xF0678;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC125; __BITS16 RPGACC125_bit; struct { union { unsigned char RPGACC125L; __BITS8 RPGACC125L_bit; }; union { unsigned char RPGACC125H; __BITS8 RPGACC125H_bit; }; }; } @ 0xF067A;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC126; __BITS16 RPGACC126_bit; struct { union { unsigned char RPGACC126L; __BITS8 RPGACC126L_bit; }; union { unsigned char RPGACC126H; __BITS8 RPGACC126H_bit; }; }; } @ 0xF067C;
__near __no_bit_access __no_init volatile union { unsigned short RPGACC127; __BITS16 RPGACC127_bit; struct { union { unsigned char RPGACC127L; __BITS8 RPGACC127L_bit; }; union { unsigned char RPGACC127H; __BITS8 RPGACC127H_bit; }; }; } @ 0xF067E;
__near __no_bit_access __no_init volatile const union { unsigned short THLACC0; __BITS16 THLACC0_bit; struct { union { const unsigned char THLACC0L; const __BITS8 THLACC0L_bit; }; union { const unsigned char THLACC0H; const __BITS8 THLACC0H_bit; }; }; } @ 0xF0680;
__near __no_bit_access __no_init volatile union { unsigned char LWBR0; __BITS8 LWBR0_bit; } @ 0xF06C1;
__near __no_bit_access __no_init volatile union { unsigned char LWBR1; __BITS8 LWBR1_bit; } @ 0xF06C1;
__near __no_bit_access __no_init volatile union { unsigned char LWBR2; __BITS8 LWBR2_bit; } @ 0xF06C1;
__near __no_bit_access __no_init volatile union { unsigned short LBRP0; __BITS16 LBRP0_bit; } @ 0xF06C2;
__near __no_bit_access __no_init volatile union { unsigned short LBRP1; __BITS16 LBRP1_bit; } @ 0xF06C2;
__near __no_bit_access __no_init volatile union { unsigned short LBRP2; __BITS16 LBRP2_bit; struct { union { unsigned char LBRP00; __BITS8 LBRP00_bit; }; union { unsigned char LBRP01; __BITS8 LBRP01_bit; }; }; } @ 0xF06C2;
__near __no_bit_access __no_init volatile union { unsigned char LBRP11; __BITS8 LBRP11_bit; } @ 0xF06C3;
__near __no_bit_access __no_init volatile union { unsigned char LBRP21; __BITS8 LBRP21_bit; } @ 0xF06C3;
__near __no_bit_access __no_init volatile union { unsigned char LSTC0; __BITS8 LSTC0_bit; } @ 0xF06C4;
__near __no_bit_access __no_init volatile union { unsigned char LSTC1; __BITS8 LSTC1_bit; } @ 0xF06C4;
__near __no_bit_access __no_init volatile union { unsigned char LSTC2; __BITS8 LSTC2_bit; } @ 0xF06C4;
__near __no_bit_access __no_init volatile union { unsigned char LUSC0; __BITS8 LUSC0_bit; } @ 0xF06C5;
__near __no_bit_access __no_init volatile union { unsigned char LUSC1; __BITS8 LUSC1_bit; } @ 0xF06C5;
__near __no_bit_access __no_init volatile union { unsigned char LUSC2; __BITS8 LUSC2_bit; } @ 0xF06C5;
__near __no_bit_access __no_init volatile union { unsigned char LMD0; __BITS8 LMD0_bit; } @ 0xF06C8;
__near __no_bit_access __no_init volatile union { unsigned char LMD1; __BITS8 LMD1_bit; } @ 0xF06C8;
__near __no_bit_access __no_init volatile union { unsigned char LMD2; __BITS8 LMD2_bit; } @ 0xF06C8;
__near __no_bit_access __no_init volatile union { unsigned char LBFC0; __BITS8 LBFC0_bit; } @ 0xF06C9;
__near __no_bit_access __no_init volatile union { unsigned char LBFC1; __BITS8 LBFC1_bit; } @ 0xF06C9;
__near __no_bit_access __no_init volatile union { unsigned char LBFC2; __BITS8 LBFC2_bit; } @ 0xF06C9;
__near __no_bit_access __no_init volatile union { unsigned char LSC0; __BITS8 LSC0_bit; } @ 0xF06CA;
__near __no_bit_access __no_init volatile union { unsigned char LSC1; __BITS8 LSC1_bit; } @ 0xF06CA;
__near __no_bit_access __no_init volatile union { unsigned char LSC2; __BITS8 LSC2_bit; } @ 0xF06CA;
__near __no_bit_access __no_init volatile union { unsigned char LWUP0; __BITS8 LWUP0_bit; } @ 0xF06CB;
__near __no_bit_access __no_init volatile union { unsigned char LWUP1; __BITS8 LWUP1_bit; } @ 0xF06CB;
__near __no_bit_access __no_init volatile union { unsigned char LWUP2; __BITS8 LWUP2_bit; } @ 0xF06CB;
__near __no_bit_access __no_init volatile union { unsigned char LIE0; __BITS8 LIE0_bit; } @ 0xF06CC;
__near __no_bit_access __no_init volatile union { unsigned char LIE1; __BITS8 LIE1_bit; } @ 0xF06CC;
__near __no_bit_access __no_init volatile union { unsigned char LIE2; __BITS8 LIE2_bit; } @ 0xF06CC;
__near __no_bit_access __no_init volatile union { unsigned char LEDE0; __BITS8 LEDE0_bit; } @ 0xF06CD;
__near __no_bit_access __no_init volatile union { unsigned char LEDE1; __BITS8 LEDE1_bit; } @ 0xF06CD;
__near __no_bit_access __no_init volatile union { unsigned char LEDE2; __BITS8 LEDE2_bit; } @ 0xF06CD;
__near __no_bit_access __no_init volatile union { unsigned char LCUC0; __BITS8 LCUC0_bit; } @ 0xF06CE;
__near __no_bit_access __no_init volatile union { unsigned char LCUC1; __BITS8 LCUC1_bit; } @ 0xF06CE;
__near __no_bit_access __no_init volatile union { unsigned char LCUC2; __BITS8 LCUC2_bit; } @ 0xF06CE;
__near __no_bit_access __no_init volatile union { unsigned char LTRC0; __BITS8 LTRC0_bit; } @ 0xF06D0;
__near __no_bit_access __no_init volatile union { unsigned char LTRC1; __BITS8 LTRC1_bit; } @ 0xF06D0;
__near __no_bit_access __no_init volatile union { unsigned char LTRC2; __BITS8 LTRC2_bit; } @ 0xF06D0;
__near __no_bit_access __no_init volatile const union { unsigned char LMST0; __BITS8 LMST0_bit; } @ 0xF06D1;
__near __no_bit_access __no_init volatile const union { unsigned char LMST1; __BITS8 LMST1_bit; } @ 0xF06D1;
__near __no_bit_access __no_init volatile const union { unsigned char LMST2; __BITS8 LMST2_bit; } @ 0xF06D1;
__near __no_bit_access __no_init volatile union { unsigned char LST0; __BITS8 LST0_bit; } @ 0xF06D2;
__near __no_bit_access __no_init volatile union { unsigned char LST1; __BITS8 LST1_bit; } @ 0xF06D2;
__near __no_bit_access __no_init volatile union { unsigned char LST2; __BITS8 LST2_bit; } @ 0xF06D2;
__near __no_bit_access __no_init volatile union { unsigned char LEST0; __BITS8 LEST0_bit; } @ 0xF06D3;
__near __no_bit_access __no_init volatile union { unsigned char LEST1; __BITS8 LEST1_bit; } @ 0xF06D3;
__near __no_bit_access __no_init volatile union { unsigned char LEST2; __BITS8 LEST2_bit; } @ 0xF06D3;
__near __no_bit_access __no_init volatile union { unsigned char LDFC0; __BITS8 LDFC0_bit; } @ 0xF06D4;
__near __no_bit_access __no_init volatile union { unsigned char LDFC1; __BITS8 LDFC1_bit; } @ 0xF06D4;
__near __no_bit_access __no_init volatile union { unsigned char LDFC2; __BITS8 LDFC2_bit; } @ 0xF06D4;
__near __no_bit_access __no_init volatile union { unsigned char LIDB0; __BITS8 LIDB0_bit; } @ 0xF06D5;
__near __no_bit_access __no_init volatile union { unsigned char LIDB1; __BITS8 LIDB1_bit; } @ 0xF06D5;
__near __no_bit_access __no_init volatile union { unsigned char LIDB2; __BITS8 LIDB2_bit; } @ 0xF06D5;
__near __no_bit_access __no_init volatile union { unsigned char LCBR0; __BITS8 LCBR0_bit; } @ 0xF06D6;
__near __no_bit_access __no_init volatile union { unsigned char LCBR1; __BITS8 LCBR1_bit; } @ 0xF06D6;
__near __no_bit_access __no_init volatile union { unsigned char LCBR2; __BITS8 LCBR2_bit; } @ 0xF06D6;
__near __no_bit_access __no_init volatile union { unsigned char LUDB00; __BITS8 LUDB00_bit; } @ 0xF06D7;
__near __no_bit_access __no_init volatile union { unsigned char LUDB10; __BITS8 LUDB10_bit; } @ 0xF06D7;
__near __no_bit_access __no_init volatile union { unsigned char LUDB20; __BITS8 LUDB20_bit; } @ 0xF06D7;
__near __no_bit_access __no_init volatile union { unsigned char LDB01; __BITS8 LDB01_bit; } @ 0xF06D8;
__near __no_bit_access __no_init volatile union { unsigned char LDB11; __BITS8 LDB11_bit; } @ 0xF06D8;
__near __no_bit_access __no_init volatile union { unsigned char LDB21; __BITS8 LDB21_bit; } @ 0xF06D8;
__near __no_bit_access __no_init volatile union { unsigned char LDB02; __BITS8 LDB02_bit; } @ 0xF06D9;
__near __no_bit_access __no_init volatile union { unsigned char LDB12; __BITS8 LDB12_bit; } @ 0xF06D9;
__near __no_bit_access __no_init volatile union { unsigned char LDB22; __BITS8 LDB22_bit; } @ 0xF06D9;
__near __no_bit_access __no_init volatile union { unsigned char LDB03; __BITS8 LDB03_bit; } @ 0xF06DA;
__near __no_bit_access __no_init volatile union { unsigned char LDB13; __BITS8 LDB13_bit; } @ 0xF06DA;
__near __no_bit_access __no_init volatile union { unsigned char LDB23; __BITS8 LDB23_bit; } @ 0xF06DA;
__near __no_bit_access __no_init volatile union { unsigned char LDB04; __BITS8 LDB04_bit; } @ 0xF06DB;
__near __no_bit_access __no_init volatile union { unsigned char LDB14; __BITS8 LDB14_bit; } @ 0xF06DB;
__near __no_bit_access __no_init volatile union { unsigned char LDB24; __BITS8 LDB24_bit; } @ 0xF06DB;
__near __no_bit_access __no_init volatile union { unsigned char LDB05; __BITS8 LDB05_bit; } @ 0xF06DC;
__near __no_bit_access __no_init volatile union { unsigned char LDB15; __BITS8 LDB15_bit; } @ 0xF06DC;
__near __no_bit_access __no_init volatile union { unsigned char LDB25; __BITS8 LDB25_bit; } @ 0xF06DC;
__near __no_bit_access __no_init volatile union { unsigned char LDB06; __BITS8 LDB06_bit; } @ 0xF06DD;
__near __no_bit_access __no_init volatile union { unsigned char LDB16; __BITS8 LDB16_bit; } @ 0xF06DD;
__near __no_bit_access __no_init volatile union { unsigned char LDB26; __BITS8 LDB26_bit; } @ 0xF06DD;
__near __no_bit_access __no_init volatile union { unsigned char LDB07; __BITS8 LDB07_bit; } @ 0xF06DE;
__near __no_bit_access __no_init volatile union { unsigned char LDB17; __BITS8 LDB17_bit; } @ 0xF06DE;
__near __no_bit_access __no_init volatile union { unsigned char LDB27; __BITS8 LDB27_bit; } @ 0xF06DE;
__near __no_bit_access __no_init volatile union { unsigned char LDB08; __BITS8 LDB08_bit; } @ 0xF06DF;
__near __no_bit_access __no_init volatile union { unsigned char LDB18; __BITS8 LDB18_bit; } @ 0xF06DF;
__near __no_bit_access __no_init volatile union { unsigned char LDB28; __BITS8 LDB28_bit; } @ 0xF06DF;
__near __no_bit_access __no_init volatile union { unsigned char LUOER0; __BITS8 LUOER0_bit; } @ 0xF06E0;
__near __no_bit_access __no_init volatile union { unsigned char LUOER1; __BITS8 LUOER1_bit; } @ 0xF06E0;
__near __no_bit_access __no_init volatile union { unsigned char LUOER2; __BITS8 LUOER2_bit; } @ 0xF06E0;
__near __no_bit_access __no_init volatile union { unsigned char LUOR01; __BITS8 LUOR01_bit; } @ 0xF06E1;
__near __no_bit_access __no_init volatile union { unsigned char LUOR11; __BITS8 LUOR11_bit; } @ 0xF06E1;
__near __no_bit_access __no_init volatile union { unsigned char LUOR21; __BITS8 LUOR21_bit; } @ 0xF06E1;
__near __no_bit_access __no_init volatile union { unsigned short LUTDR0; __BITS16 LUTDR0_bit; } @ 0xF06E4;
__near __no_bit_access __no_init volatile union { unsigned short LUTDR1; __BITS16 LUTDR1_bit; } @ 0xF06E4;
__near __no_bit_access __no_init volatile union { unsigned short LUTDR2; __BITS16 LUTDR2_bit; struct { union { unsigned char LUTDR0L; __BITS8 LUTDR0L_bit; }; union { unsigned char LUTDR0H; __BITS8 LUTDR0H_bit; }; }; } @ 0xF06E4;
__near __no_bit_access __no_init volatile union { unsigned char LUTDR1H; __BITS8 LUTDR1H_bit; } @ 0xF06E5;
__near __no_bit_access __no_init volatile union { unsigned char LUTDR2H; __BITS8 LUTDR2H_bit; } @ 0xF06E5;
__near __no_bit_access __no_init volatile const union { unsigned short LURDR0; __BITS16 LURDR0_bit; } @ 0xF06E6;
__near __no_bit_access __no_init volatile const union { unsigned short LURDR1; __BITS16 LURDR1_bit; } @ 0xF06E6;
__near __no_bit_access __no_init volatile const union { unsigned short LURDR2; __BITS16 LURDR2_bit; struct { union { const unsigned char LURDR0L; const __BITS8 LURDR0L_bit; }; union { const unsigned char LURDR0H; const __BITS8 LURDR0H_bit; }; }; } @ 0xF06E6;
__near __no_bit_access __no_init volatile const union { unsigned char LURDR1H; __BITS8 LURDR1H_bit; } @ 0xF06E7;
__near __no_bit_access __no_init volatile const union { unsigned char LURDR2H; __BITS8 LURDR2H_bit; } @ 0xF06E7;
__near __no_bit_access __no_init volatile union { unsigned short LUWTDR0; __BITS16 LUWTDR0_bit; } @ 0xF06E8;
__near __no_bit_access __no_init volatile union { unsigned short LUWTDR1; __BITS16 LUWTDR1_bit; } @ 0xF06E8;
__near __no_bit_access __no_init volatile union { unsigned short LUWTDR2; __BITS16 LUWTDR2_bit; struct { union { unsigned char LUWTDR0L; __BITS8 LUWTDR0L_bit; }; union { unsigned char LUWTDR0H; __BITS8 LUWTDR0H_bit; }; }; } @ 0xF06E8;
__near __no_bit_access __no_init volatile union { unsigned char LUWTDR1H; __BITS8 LUWTDR1H_bit; } @ 0xF06E9;
__near __no_bit_access __no_init volatile union { unsigned char LUWTDR2H; __BITS8 LUWTDR2H_bit; } @ 0xF06E9;
__near __no_bit_access __no_init volatile union { unsigned short TRJ0; __BITS16 TRJ0_bit; } @ 0xF06F0;
__near __no_init volatile union { unsigned char ELSELR00; __BITS8 ELSELR00_bit; } @ 0xF0780;
__near __no_init volatile union { unsigned char ELSELR01; __BITS8 ELSELR01_bit; } @ 0xF0781;
__near __no_init volatile union { unsigned char ELSELR02; __BITS8 ELSELR02_bit; } @ 0xF0782;
__near __no_init volatile union { unsigned char ELSELR03; __BITS8 ELSELR03_bit; } @ 0xF0783;
__near __no_init volatile union { unsigned char ELSELR04; __BITS8 ELSELR04_bit; } @ 0xF0784;
__near __no_init volatile union { unsigned char ELSELR05; __BITS8 ELSELR05_bit; } @ 0xF0785;
__near __no_init volatile union { unsigned char ELSELR06; __BITS8 ELSELR06_bit; } @ 0xF0786;
__near __no_init volatile union { unsigned char ELSELR07; __BITS8 ELSELR07_bit; } @ 0xF0787;
__near __no_init volatile union { unsigned char ELSELR08; __BITS8 ELSELR08_bit; } @ 0xF0788;
__near __no_init volatile union { unsigned char ELSELR09; __BITS8 ELSELR09_bit; } @ 0xF0789;
__near __no_init volatile union { unsigned char ELSELR10; __BITS8 ELSELR10_bit; } @ 0xF078A;
__near __no_init volatile union { unsigned char ELSELR11; __BITS8 ELSELR11_bit; } @ 0xF078B;
__near __no_init volatile union { unsigned char ELSELR12; __BITS8 ELSELR12_bit; } @ 0xF078C;
__near __no_init volatile union { unsigned char ELSELR13; __BITS8 ELSELR13_bit; } @ 0xF078D;
__near __no_init volatile union { unsigned char ELSELR14; __BITS8 ELSELR14_bit; } @ 0xF078E;
__near __no_init volatile union { unsigned char ELSELR15; __BITS8 ELSELR15_bit; } @ 0xF078F;
__near __no_init volatile union { unsigned char ELSELR16; __BITS8 ELSELR16_bit; } @ 0xF0790;
__near __no_init volatile union { unsigned char ELSELR17; __BITS8 ELSELR17_bit; } @ 0xF0791;
__near __no_init volatile union { unsigned char ELSELR18; __BITS8 ELSELR18_bit; } @ 0xF0792;
__near __no_init volatile union { unsigned char ELSELR19; __BITS8 ELSELR19_bit; } @ 0xF0793;

/*----------------------------------------------
 *       Extended SFR bit declarations
 *--------------------------------------------*/

#define ADTYP             ADM2_bit.no0
#define AWC               ADM2_bit.no2
#define ADRCK             ADM2_bit.no3

#define DFLEN             DFLCTL_bit.no0

#define TAU0EN            PER0_bit.no0
#define TAU1EN            PER0_bit.no1
#define SAU0EN            PER0_bit.no2
#define SAU1EN            PER0_bit.no3
#define IICA0EN           PER0_bit.no4
#define ADCEN             PER0_bit.no5
#define RTCEN             PER0_bit.no7

#define ANO0EN            DAM2_bit.no0

#define SPT0              IICCTL00_bit.no0
#define STT0              IICCTL00_bit.no1
#define ACKE0             IICCTL00_bit.no2
#define WTIM0             IICCTL00_bit.no3
#define SPIE0             IICCTL00_bit.no4
#define WREL0             IICCTL00_bit.no5
#define LREL0             IICCTL00_bit.no6
#define IICE0             IICCTL00_bit.no7

#define PRS0              IICCTL01_bit.no0
#define DFC0              IICCTL01_bit.no2
#define SMC0              IICCTL01_bit.no3
#define DAD0              IICCTL01_bit.no4
#define CLD0              IICCTL01_bit.no5
#define WUP0              IICCTL01_bit.no7

#define TRDSYNC           TRDMR_bit.no0
#define TRDBFC0           TRDMR_bit.no4
#define TRDBFD0           TRDMR_bit.no5
#define TRDBFC1           TRDMR_bit.no6
#define TRDBFD1           TRDMR_bit.no7

#define TRDPWMB0          TRDPMR_bit.no0
#define TRDPWMC0          TRDPMR_bit.no1
#define TRDPWMD0          TRDPMR_bit.no2
#define TRDPWMB1          TRDPMR_bit.no4
#define TRDPWMC1          TRDPMR_bit.no5
#define TRDPWMD1          TRDPMR_bit.no6

#define TRDSHUTS          TRDOER2_bit.no0
#define TRDPTO            TRDOER2_bit.no7

#define COE               CMPCTL_bit.no1
#define HCMPON            CMPCTL_bit.no7

#define CPOE              CMPSEL_bit.no6

#define TRJ0EN            PER1_bit.no0
#define DTCEN             PER1_bit.no3
#define TRD0EN            PER1_bit.no4
#define CMPEN             PER1_bit.no5
#define DACEN             PER1_bit.no7

#define CAN0EN            PER2_bit.no0
#define LIN0EN            PER2_bit.no2

#define CAN0MCKE          CANCKSEL_bit.no0

#define LIN0MCK           LINCKSEL_bit.no0
#define LIN0MCKE          LINCKSEL_bit.no4

#define SELLOSC           CKSEL_bit.no0
#define TRD_CKSEL         CKSEL_bit.no2

#define PLLON             PLLCTL_bit.no0
#define PLLMUL            PLLCTL_bit.no1
#define SELPLL            PLLCTL_bit.no2
#define PLLDIV0           PLLCTL_bit.no4
#define PLLDIV1           PLLCTL_bit.no5
#define LCKSEL0           PLLCTL_bit.no6
#define LCKSEL1           PLLCTL_bit.no7

#define SELPLLS           PLLSTS_bit.no3
#define LOCK              PLLSTS_bit.no7

#define POCRES0           POCRES_bit.no0
#define CLKRF             POCRES_bit.no4

#define STPSEL            STPSTC_bit.no0
#define STPLV             STPSTC_bit.no4
#define STPOEN            STPSTC_bit.no7

#define HMODE0            HDTCCR0_bit.no0
#define HRPTSEL0          HDTCCR0_bit.no1
#define HSAMOD0           HDTCCR0_bit.no2
#define HDAMOD0           HDTCCR0_bit.no3
#define HCHNE0            HDTCCR0_bit.no4
#define HRPTINT0          HDTCCR0_bit.no5
#define HSZ0              HDTCCR0_bit.no6

#define HMODE1            HDTCCR1_bit.no0
#define HRPTSEL1          HDTCCR1_bit.no1
#define HSAMOD1           HDTCCR1_bit.no2
#define HDAMOD1           HDTCCR1_bit.no3
#define HCHNE1            HDTCCR1_bit.no4
#define HRPTINT1          HDTCCR1_bit.no5
#define HSZ1              HDTCCR1_bit.no6

#define DTCEN00           DTCEN0_bit.no0
#define DTCEN01           DTCEN0_bit.no1
#define DTCEN02           DTCEN0_bit.no2
#define DTCEN03           DTCEN0_bit.no3
#define DTCEN04           DTCEN0_bit.no4
#define DTCEN05           DTCEN0_bit.no5
#define DTCEN06           DTCEN0_bit.no6

#define DTCEN10           DTCEN1_bit.no0
#define DTCEN11           DTCEN1_bit.no1
#define DTCEN12           DTCEN1_bit.no2
#define DTCEN13           DTCEN1_bit.no3
#define DTCEN14           DTCEN1_bit.no4
#define DTCEN15           DTCEN1_bit.no5
#define DTCEN16           DTCEN1_bit.no6
#define DTCEN17           DTCEN1_bit.no7

#define DTCEN20           DTCEN2_bit.no0
#define DTCEN21           DTCEN2_bit.no1
#define DTCEN22           DTCEN2_bit.no2
#define DTCEN23           DTCEN2_bit.no3
#define DTCEN24           DTCEN2_bit.no4
#define DTCEN25           DTCEN2_bit.no5
#define DTCEN27           DTCEN2_bit.no7

#define DTCEN30           DTCEN3_bit.no0
#define DTCEN31           DTCEN3_bit.no1
#define DTCEN32           DTCEN3_bit.no2
#define DTCEN33           DTCEN3_bit.no3
#define DTCEN34           DTCEN3_bit.no4
#define DTCEN35           DTCEN3_bit.no5
#define DTCEN36           DTCEN3_bit.no6
#define DTCEN37           DTCEN3_bit.no7

#define DTCEN40           DTCEN4_bit.no0
#define DTCEN41           DTCEN4_bit.no1
#define DTCEN42           DTCEN4_bit.no2
#define DTCEN43           DTCEN4_bit.no3
#define DTCEN44           DTCEN4_bit.no4
#define DTCEN45           DTCEN4_bit.no5
#define DTCEN46           DTCEN4_bit.no6
#define DTCEN47           DTCEN4_bit.no7

#define CRC0EN            CRC0CTL_bit.no7

#pragma language=restore

#endif /* __IAR_SYSTEMS_ICC__ */

#ifdef __IAR_SYSTEMS_ASM__

/*----------------------------------------------
 *       Extended SFR declarations
 *--------------------------------------------*/

ADM2       DEFINE  0xF0010
ADUL       DEFINE  0xF0011
ADLL       DEFINE  0xF0012
ADTES      DEFINE  0xF0013
PIOR0      DEFINE  0xF0016
PIOR1      DEFINE  0xF0017
PIOR4      DEFINE  0xF001A
PIOR5      DEFINE  0xF001B
PIOR7      DEFINE  0xF001D
PITHL1     DEFINE  0xF0021
PITHL3     DEFINE  0xF0023
PITHL4     DEFINE  0xF0024
PITHL5     DEFINE  0xF0025
PITHL6     DEFINE  0xF0026
PITHL7     DEFINE  0xF0027
PITHL12    DEFINE  0xF002C
PU0        DEFINE  0xF0030
PU1        DEFINE  0xF0031
PU3        DEFINE  0xF0033
PU4        DEFINE  0xF0034
PU6        DEFINE  0xF0036
PU7        DEFINE  0xF0037
PU9        DEFINE  0xF0039
PU12       DEFINE  0xF003C
PU14       DEFINE  0xF003E
PIM1       DEFINE  0xF0041
PIM3       DEFINE  0xF0043
PIM6       DEFINE  0xF0046
PIM7       DEFINE  0xF0047
PIM12      DEFINE  0xF004C
POM1       DEFINE  0xF0051
POM6       DEFINE  0xF0056
POM7       DEFINE  0xF0057
POM12      DEFINE  0xF005C
PMC9       DEFINE  0xF0069
PMC12      DEFINE  0xF006C
NFEN0      DEFINE  0xF0070
NFEN1      DEFINE  0xF0071
NFEN2      DEFINE  0xF0072
ISC        DEFINE  0xF0073
TIS0       DEFINE  0xF0074
TIS1       DEFINE  0xF0075
ADPC       DEFINE  0xF0076
PMS        DEFINE  0xF0077
IAWCTL     DEFINE  0xF0078
INTFLG0    DEFINE  0xF0079
LCHSEL     DEFINE  0xF007B
INTMSK     DEFINE  0xF007C
DFLCTL     DEFINE  0xF0090
HIOTRM     DEFINE  0xF00A0
HOCODIV    DEFINE  0xF00A8
SPMCTRL    DEFINE  0xF00D8
SPOFR      DEFINE  0xF00DA
SPUFR      DEFINE  0xF00DC
PER0       DEFINE  0xF00F0
OSMC       DEFINE  0xF00F3
BCDADJ     DEFINE  0xF00FE
SSR00      DEFINE  0xF0100
SSR00L     DEFINE  0xF0100
SSR01      DEFINE  0xF0102
SSR01L     DEFINE  0xF0102
SIR00      DEFINE  0xF0104
SIR00L     DEFINE  0xF0104
SIR01      DEFINE  0xF0106
SIR01L     DEFINE  0xF0106
SMR00      DEFINE  0xF0108
SMR01      DEFINE  0xF010A
SCR00      DEFINE  0xF010C
SCR01      DEFINE  0xF010E
SE0        DEFINE  0xF0110
SE0L       DEFINE  0xF0110
SS0        DEFINE  0xF0112
SS0L       DEFINE  0xF0112
ST0        DEFINE  0xF0114
ST0L       DEFINE  0xF0114
SPS0       DEFINE  0xF0116
SPS0L      DEFINE  0xF0116
SO0        DEFINE  0xF0118
SOE0       DEFINE  0xF011A
SOE0L      DEFINE  0xF011A
SOL0       DEFINE  0xF0120
SOL0L      DEFINE  0xF0120
SSE0       DEFINE  0xF0122
SSE0L      DEFINE  0xF0122
SSR10      DEFINE  0xF0140
SSR10L     DEFINE  0xF0140
SSR11      DEFINE  0xF0142
SSR11L     DEFINE  0xF0142
SIR10      DEFINE  0xF0144
SIR10L     DEFINE  0xF0144
SIR11      DEFINE  0xF0146
SIR11L     DEFINE  0xF0146
SMR10      DEFINE  0xF0148
SMR11      DEFINE  0xF014A
SCR10      DEFINE  0xF014C
SCR11      DEFINE  0xF014E
SE1        DEFINE  0xF0150
SE1L       DEFINE  0xF0150
SS1        DEFINE  0xF0152
SS1L       DEFINE  0xF0152
ST1        DEFINE  0xF0154
ST1L       DEFINE  0xF0154
SPS1       DEFINE  0xF0156
SPS1L      DEFINE  0xF0156
SO1        DEFINE  0xF0158
SOE1       DEFINE  0xF015A
SOE1L      DEFINE  0xF015A
SOL1       DEFINE  0xF0160
SOL1L      DEFINE  0xF0160
SSE1       DEFINE  0xF0162
SSE1L      DEFINE  0xF0162
TCR00      DEFINE  0xF0180
TCR01      DEFINE  0xF0182
TCR02      DEFINE  0xF0184
TCR03      DEFINE  0xF0186
TCR04      DEFINE  0xF0188
TCR05      DEFINE  0xF018A
TCR06      DEFINE  0xF018C
TCR07      DEFINE  0xF018E
TMR00      DEFINE  0xF0190
TMR01      DEFINE  0xF0192
TMR02      DEFINE  0xF0194
TMR03      DEFINE  0xF0196
TMR04      DEFINE  0xF0198
TMR05      DEFINE  0xF019A
TMR06      DEFINE  0xF019C
TMR07      DEFINE  0xF019E
TSR00      DEFINE  0xF01A0
TSR00L     DEFINE  0xF01A0
TSR01      DEFINE  0xF01A2
TSR01L     DEFINE  0xF01A2
TSR02      DEFINE  0xF01A4
TSR02L     DEFINE  0xF01A4
TSR03      DEFINE  0xF01A6
TSR03L     DEFINE  0xF01A6
TSR04      DEFINE  0xF01A8
TSR04L     DEFINE  0xF01A8
TSR05      DEFINE  0xF01AA
TSR05L     DEFINE  0xF01AA
TSR06      DEFINE  0xF01AC
TSR06L     DEFINE  0xF01AC
TSR07      DEFINE  0xF01AE
TSR07L     DEFINE  0xF01AE
TE0        DEFINE  0xF01B0
TE0L       DEFINE  0xF01B0
TS0        DEFINE  0xF01B2
TS0L       DEFINE  0xF01B2
TT0        DEFINE  0xF01B4
TT0L       DEFINE  0xF01B4
TPS0       DEFINE  0xF01B6
TO0        DEFINE  0xF01B8
TO0L       DEFINE  0xF01B8
TOE0       DEFINE  0xF01BA
TOE0L      DEFINE  0xF01BA
TOL0       DEFINE  0xF01BC
TOL0L      DEFINE  0xF01BC
TOM0       DEFINE  0xF01BE
TOM0L      DEFINE  0xF01BE
TCR10      DEFINE  0xF01C0
TCR11      DEFINE  0xF01C2
TCR12      DEFINE  0xF01C4
TCR13      DEFINE  0xF01C6
TMR10      DEFINE  0xF01D0
TMR11      DEFINE  0xF01D2
TMR12      DEFINE  0xF01D4
TMR13      DEFINE  0xF01D6
TSR10      DEFINE  0xF01E0
TSR10L     DEFINE  0xF01E0
TSR11      DEFINE  0xF01E2
TSR11L     DEFINE  0xF01E2
TSR12      DEFINE  0xF01E4
TSR12L     DEFINE  0xF01E4
TSR13      DEFINE  0xF01E6
TSR13L     DEFINE  0xF01E6
TE1        DEFINE  0xF01F0
TE1L       DEFINE  0xF01F0
TS1        DEFINE  0xF01F2
TS1L       DEFINE  0xF01F2
TT1        DEFINE  0xF01F4
TT1L       DEFINE  0xF01F4
TPS1       DEFINE  0xF01F6
TO1        DEFINE  0xF01F8
TO1L       DEFINE  0xF01F8
TOE1       DEFINE  0xF01FA
TOE1L      DEFINE  0xF01FA
TOL1       DEFINE  0xF01FC
TOL1L      DEFINE  0xF01FC
TOM1       DEFINE  0xF01FE
TOM1L      DEFINE  0xF01FE
ERADR      DEFINE  0xF0200
ECCIER     DEFINE  0xF0202
ECCER      DEFINE  0xF0203
ECCTPR     DEFINE  0xF0204
ECCTMDR    DEFINE  0xF0205
ECCDWRVR   DEFINE  0xF0206
PSRSEL     DEFINE  0xF0220
PSNZCNT0   DEFINE  0xF0222
PSNZCNT1   DEFINE  0xF0223
PSNZCNT2   DEFINE  0xF0224
PSNZCNT3   DEFINE  0xF0225
DAM2       DEFINE  0xF0227
PWMDLY0    DEFINE  0xF0228
PWMDLY1    DEFINE  0xF022A
PWMDLY2    DEFINE  0xF022C
IICCTL00   DEFINE  0xF0230
IICCTL01   DEFINE  0xF0231
IICWL0     DEFINE  0xF0232
IICWH0     DEFINE  0xF0233
SVA0       DEFINE  0xF0234
TRJCR0     DEFINE  0xF0240
TRJIOC0    DEFINE  0xF0241
TRJMR0     DEFINE  0xF0242
TRJISR0    DEFINE  0xF0243
TRDELC     DEFINE  0xF0260
TRDSTR     DEFINE  0xF0263
TRDMR      DEFINE  0xF0264
TRDPMR     DEFINE  0xF0265
TRDFCR     DEFINE  0xF0266
TRDOER1    DEFINE  0xF0267
TRDOER2    DEFINE  0xF0268
TRDOCR     DEFINE  0xF0269
TRDDF0     DEFINE  0xF026A
TRDDF1     DEFINE  0xF026B
TRDCR0     DEFINE  0xF0270
TRDIORA0   DEFINE  0xF0271
TRDIORC0   DEFINE  0xF0272
TRDSR0     DEFINE  0xF0273
TRDIER0    DEFINE  0xF0274
TRDPOCR0   DEFINE  0xF0275
TRD0       DEFINE  0xF0276
TRDGRA0    DEFINE  0xF0278
TRDGRB0    DEFINE  0xF027A
TRDCR1     DEFINE  0xF0280
TRDIORA1   DEFINE  0xF0281
TRDIORC1   DEFINE  0xF0282
TRDSR1     DEFINE  0xF0283
TRDIER1    DEFINE  0xF0284
TRDPOCR1   DEFINE  0xF0285
TRD1       DEFINE  0xF0286
TRDGRA1    DEFINE  0xF0288
TRDGRB1    DEFINE  0xF028A
CMPCTL     DEFINE  0xF02A0
CMPSEL     DEFINE  0xF02A1
CMPMON     DEFINE  0xF02A2
PER1       DEFINE  0xF02C0
PER2       DEFINE  0xF02C1
CANCKSEL   DEFINE  0xF02C2
LINCKSEL   DEFINE  0xF02C3
CKSEL      DEFINE  0xF02C4
PLLCTL     DEFINE  0xF02C5
PLLSTS     DEFINE  0xF02C6
MDIV       DEFINE  0xF02C7
RTCCL      DEFINE  0xF02C8
POCRES     DEFINE  0xF02C9
STPSTC     DEFINE  0xF02CA
HDTCCR0    DEFINE  0xF02D0
HDTCCT0    DEFINE  0xF02D2
HDTRLD0    DEFINE  0xF02D3
HDTSAR0    DEFINE  0xF02D4
HDTDAR0    DEFINE  0xF02D6
HDTCCR1    DEFINE  0xF02D8
HDTCCT1    DEFINE  0xF02DA
HDTRLD1    DEFINE  0xF02DB
HDTSAR1    DEFINE  0xF02DC
HDTDAR1    DEFINE  0xF02DE
DTCBAR     DEFINE  0xF02E0
SELHS0     DEFINE  0xF02E1
SELHS1     DEFINE  0xF02E2
DTCEN0     DEFINE  0xF02E8
DTCEN1     DEFINE  0xF02E9
DTCEN2     DEFINE  0xF02EA
DTCEN3     DEFINE  0xF02EB
DTCEN4     DEFINE  0xF02EC
CRC0CTL    DEFINE  0xF02F0
PGCRCL     DEFINE  0xF02F2
CRCMD      DEFINE  0xF02F9
CRCD       DEFINE  0xF02FA
C0CFGL     DEFINE  0xF0300
C0CFGLL    DEFINE  0xF0300
C0CFGLH    DEFINE  0xF0301
C0CFGH     DEFINE  0xF0302
C0CFGHL    DEFINE  0xF0302
C0CFGHH    DEFINE  0xF0303
C0CTRL     DEFINE  0xF0304
C0CTRLL    DEFINE  0xF0304
C0CTRLH    DEFINE  0xF0305
C0CTRH     DEFINE  0xF0306
C0CTRHL    DEFINE  0xF0306
C0CTRHH    DEFINE  0xF0307
C0STSL     DEFINE  0xF0308
C0STSLL    DEFINE  0xF0308
C0STSLH    DEFINE  0xF0309
C0STSH     DEFINE  0xF030A
C0STSHL    DEFINE  0xF030A
C0STSHH    DEFINE  0xF030B
C0ERFLL    DEFINE  0xF030C
C0ERFLLL   DEFINE  0xF030C
C0ERFLLH   DEFINE  0xF030D
C0ERFLH    DEFINE  0xF030E
C0ERFLHL   DEFINE  0xF030E
C0ERFLHH   DEFINE  0xF030F
GCFGL      DEFINE  0xF0322
GCFGLL     DEFINE  0xF0322
GCFGLH     DEFINE  0xF0323
GCFGH      DEFINE  0xF0324
GCFGHL     DEFINE  0xF0324
GCFGHH     DEFINE  0xF0325
GCTRL      DEFINE  0xF0326
GCTRLL     DEFINE  0xF0326
GCTRLH     DEFINE  0xF0327
GCTRH      DEFINE  0xF0328
GCTRHL     DEFINE  0xF0328
GCTRHH     DEFINE  0xF0329
GSTS       DEFINE  0xF032A
GSTSL      DEFINE  0xF032A
GSTSH      DEFINE  0xF032B
GERFLL     DEFINE  0xF032C
GTSC       DEFINE  0xF032E
GAFLCFG    DEFINE  0xF0330
GAFLCFGL   DEFINE  0xF0330
GAFLCFGH   DEFINE  0xF0331
RMNB       DEFINE  0xF0332
RMNBL      DEFINE  0xF0332
RMND0      DEFINE  0xF0334
RMND0L     DEFINE  0xF0334
RMND0H     DEFINE  0xF0335
RFCC0      DEFINE  0xF0338
RFCC0L     DEFINE  0xF0338
RFCC0H     DEFINE  0xF0339
RFCC1      DEFINE  0xF033A
RFCC1L     DEFINE  0xF033A
RFCC1H     DEFINE  0xF033B
RFSTS0     DEFINE  0xF0340
RFSTS0L    DEFINE  0xF0340
RFSTS0H    DEFINE  0xF0341
RFSTS1     DEFINE  0xF0342
RFSTS1L    DEFINE  0xF0342
RFSTS1H    DEFINE  0xF0343
RFPCTR0    DEFINE  0xF0348
RFPCTR0L   DEFINE  0xF0348
RFPCTR0H   DEFINE  0xF0349
RFPCTR1    DEFINE  0xF034A
RFPCTR1L   DEFINE  0xF034A
RFPCTR1H   DEFINE  0xF034B
CFCCL0     DEFINE  0xF0350
CFCCL0L    DEFINE  0xF0350
CFCCL0H    DEFINE  0xF0351
CFCCH0     DEFINE  0xF0352
CFCCH0L    DEFINE  0xF0352
CFCCH0H    DEFINE  0xF0353
CFSTS0     DEFINE  0xF0358
CFSTS0L    DEFINE  0xF0358
CFSTS0H    DEFINE  0xF0359
CFPCTR0    DEFINE  0xF035C
CFPCTR0L   DEFINE  0xF035C
RFMSTS     DEFINE  0xF0360
CFMSTS     DEFINE  0xF0361
RFISTS     DEFINE  0xF0362
CFISTS     DEFINE  0xF0363
TMC0       DEFINE  0xF0364
TMC1       DEFINE  0xF0365
TMC2       DEFINE  0xF0366
TMC3       DEFINE  0xF0367
TMSTS0     DEFINE  0xF036C
TMSTS1     DEFINE  0xF036D
TMSTS2     DEFINE  0xF036E
TMSTS3     DEFINE  0xF036F
TMTRSTS    DEFINE  0xF0374
TMTRSTSL   DEFINE  0xF0374
TMTRSTSH   DEFINE  0xF0375
TMTCSTS    DEFINE  0xF0376
TMTCSTSL   DEFINE  0xF0376
TMTCSTSH   DEFINE  0xF0377
TMTASTS    DEFINE  0xF0378
TMTASTSL   DEFINE  0xF0378
TMTASTSH   DEFINE  0xF0379
TMIEC      DEFINE  0xF037A
TMIECL     DEFINE  0xF037A
TMIECH     DEFINE  0xF037B
THLCC0     DEFINE  0xF037C
THLCC0L    DEFINE  0xF037C
THLCC0H    DEFINE  0xF037D
THLSTS0    DEFINE  0xF0380
THLSTS0L   DEFINE  0xF0380
THLSTS0H   DEFINE  0xF0381
THLPCTR0   DEFINE  0xF0384
THLPCTR0L  DEFINE  0xF0384
THLPCTR0H  DEFINE  0xF0385
GTINTSTS   DEFINE  0xF0388
GTINTSTSL  DEFINE  0xF0388
GTINTSTSH  DEFINE  0xF0389
GRWCR      DEFINE  0xF038A
GRWCRL     DEFINE  0xF038A
GRWCRH     DEFINE  0xF038B
GTSTCFG    DEFINE  0xF038C
GTSTCFGL   DEFINE  0xF038C
GTSTCFGH   DEFINE  0xF038D
GTSTCTRL   DEFINE  0xF038E
GLOCKK     DEFINE  0xF0394
GAFLIDL0   DEFINE  0xF03A0
RMIDL0     DEFINE  0xF03A0
GAFLIDL0L  DEFINE  0xF03A0
GAFLIDL0H  DEFINE  0xF03A1
RMIDL0L    DEFINE  0xF03A0
RMIDL0H    DEFINE  0xF03A1
GAFLIDH0   DEFINE  0xF03A2
RMIDH0     DEFINE  0xF03A2
GAFLIDH0L  DEFINE  0xF03A2
GAFLIDH0H  DEFINE  0xF03A3
RMIDH0L    DEFINE  0xF03A2
RMIDH0H    DEFINE  0xF03A3
GAFLML0    DEFINE  0xF03A4
RMTS0      DEFINE  0xF03A4
GAFLML0L   DEFINE  0xF03A4
GAFLML0H   DEFINE  0xF03A5
RMTS0L     DEFINE  0xF03A4
RMTS0H     DEFINE  0xF03A5
GAFLMH0    DEFINE  0xF03A6
RMPTR0     DEFINE  0xF03A6
GAFLMH0L   DEFINE  0xF03A6
GAFLMH0H   DEFINE  0xF03A7
RMPTR0L    DEFINE  0xF03A6
RMPTR0H    DEFINE  0xF03A7
GAFLPL0    DEFINE  0xF03A8
RMDF00     DEFINE  0xF03A8
GAFLPL0L   DEFINE  0xF03A8
GAFLPL0H   DEFINE  0xF03A9
RMDF00L    DEFINE  0xF03A8
RMDF00H    DEFINE  0xF03A9
GAFLPH0    DEFINE  0xF03AA
RMDF10     DEFINE  0xF03AA
GAFLPH0L   DEFINE  0xF03AA
GAFLPH0H   DEFINE  0xF03AB
RMDF10L    DEFINE  0xF03AA
RMDF10H    DEFINE  0xF03AB
GAFLIDL1   DEFINE  0xF03AC
RMDF20     DEFINE  0xF03AC
GAFLIDL1L  DEFINE  0xF03AC
GAFLIDL1H  DEFINE  0xF03AD
RMDF20L    DEFINE  0xF03AC
RMDF20H    DEFINE  0xF03AD
GAFLIDH1   DEFINE  0xF03AE
RMDF30     DEFINE  0xF03AE
GAFLIDH1L  DEFINE  0xF03AE
GAFLIDH1H  DEFINE  0xF03AF
RMDF30L    DEFINE  0xF03AE
RMDF30H    DEFINE  0xF03AF
GAFLML1    DEFINE  0xF03B0
RMIDL1     DEFINE  0xF03B0
GAFLML1L   DEFINE  0xF03B0
GAFLML1H   DEFINE  0xF03B1
RMIDL1L    DEFINE  0xF03B0
RMIDL1H    DEFINE  0xF03B1
GAFLMH1    DEFINE  0xF03B2
RMIDH1     DEFINE  0xF03B2
GAFLMH1L   DEFINE  0xF03B2
GAFLMH1H   DEFINE  0xF03B3
RMIDH1L    DEFINE  0xF03B2
RMIDH1H    DEFINE  0xF03B3
GAFLPL1    DEFINE  0xF03B4
RMTS1      DEFINE  0xF03B4
GAFLPL1L   DEFINE  0xF03B4
GAFLPL1H   DEFINE  0xF03B5
RMTS1L     DEFINE  0xF03B4
RMTS1H     DEFINE  0xF03B5
GAFLPH1    DEFINE  0xF03B6
RMPTR1     DEFINE  0xF03B6
GAFLPH1L   DEFINE  0xF03B6
GAFLPH1H   DEFINE  0xF03B7
RMPTR1L    DEFINE  0xF03B6
RMPTR1H    DEFINE  0xF03B7
GAFLIDL2   DEFINE  0xF03B8
RMDF01     DEFINE  0xF03B8
GAFLIDL2L  DEFINE  0xF03B8
GAFLIDL2H  DEFINE  0xF03B9
RMDF01L    DEFINE  0xF03B8
RMDF01H    DEFINE  0xF03B9
GAFLIDH2   DEFINE  0xF03BA
RMDF11     DEFINE  0xF03BA
GAFLIDH2L  DEFINE  0xF03BA
GAFLIDH2H  DEFINE  0xF03BB
RMDF11L    DEFINE  0xF03BA
RMDF11H    DEFINE  0xF03BB
GAFLML2    DEFINE  0xF03BC
RMDF21     DEFINE  0xF03BC
GAFLML2L   DEFINE  0xF03BC
GAFLML2H   DEFINE  0xF03BD
RMDF21L    DEFINE  0xF03BC
RMDF21H    DEFINE  0xF03BD
GAFLMH2    DEFINE  0xF03BE
RMDF31     DEFINE  0xF03BE
GAFLMH2L   DEFINE  0xF03BE
GAFLMH2H   DEFINE  0xF03BF
RMDF31L    DEFINE  0xF03BE
RMDF31H    DEFINE  0xF03BF
GAFLPL2    DEFINE  0xF03C0
RMIDL2     DEFINE  0xF03C0
GAFLPL2L   DEFINE  0xF03C0
GAFLPL2H   DEFINE  0xF03C1
RMIDL2L    DEFINE  0xF03C0
RMIDL2H    DEFINE  0xF03C1
GAFLPH2    DEFINE  0xF03C2
RMIDH2     DEFINE  0xF03C2
GAFLPH2L   DEFINE  0xF03C2
GAFLPH2H   DEFINE  0xF03C3
RMIDH2L    DEFINE  0xF03C2
RMIDH2H    DEFINE  0xF03C3
GAFLIDL3   DEFINE  0xF03C4
RMTS2      DEFINE  0xF03C4
GAFLIDL3L  DEFINE  0xF03C4
GAFLIDL3H  DEFINE  0xF03C5
RMTS2L     DEFINE  0xF03C4
RMTS2H     DEFINE  0xF03C5
GAFLIDH3   DEFINE  0xF03C6
RMPTR2     DEFINE  0xF03C6
GAFLIDH3L  DEFINE  0xF03C6
GAFLIDH3H  DEFINE  0xF03C7
RMPTR2L    DEFINE  0xF03C6
RMPTR2H    DEFINE  0xF03C7
GAFLML3    DEFINE  0xF03C8
RMDF02     DEFINE  0xF03C8
GAFLML3L   DEFINE  0xF03C8
GAFLML3H   DEFINE  0xF03C9
RMDF02L    DEFINE  0xF03C8
RMDF02H    DEFINE  0xF03C9
GAFLMH3    DEFINE  0xF03CA
RMDF12     DEFINE  0xF03CA
GAFLMH3L   DEFINE  0xF03CA
GAFLMH3H   DEFINE  0xF03CB
RMDF12L    DEFINE  0xF03CA
RMDF12H    DEFINE  0xF03CB
GAFLPL3    DEFINE  0xF03CC
RMDF22     DEFINE  0xF03CC
GAFLPL3L   DEFINE  0xF03CC
GAFLPL3H   DEFINE  0xF03CD
RMDF22L    DEFINE  0xF03CC
RMDF22H    DEFINE  0xF03CD
GAFLPH3    DEFINE  0xF03CE
RMDF32     DEFINE  0xF03CE
GAFLPH3L   DEFINE  0xF03CE
GAFLPH3H   DEFINE  0xF03CF
RMDF32L    DEFINE  0xF03CE
RMDF32H    DEFINE  0xF03CF
GAFLIDL4   DEFINE  0xF03D0
RMIDL3     DEFINE  0xF03D0
GAFLIDL4L  DEFINE  0xF03D0
GAFLIDL4H  DEFINE  0xF03D1
RMIDL3L    DEFINE  0xF03D0
RMIDL3H    DEFINE  0xF03D1
GAFLIDH4   DEFINE  0xF03D2
RMIDH3     DEFINE  0xF03D2
GAFLIDH4L  DEFINE  0xF03D2
GAFLIDH4H  DEFINE  0xF03D3
RMIDH3L    DEFINE  0xF03D2
RMIDH3H    DEFINE  0xF03D3
GAFLML4    DEFINE  0xF03D4
RMTS3      DEFINE  0xF03D4
GAFLML4L   DEFINE  0xF03D4
GAFLML4H   DEFINE  0xF03D5
RMTS3L     DEFINE  0xF03D4
RMTS3H     DEFINE  0xF03D5
GAFLMH4    DEFINE  0xF03D6
RMPTR3     DEFINE  0xF03D6
GAFLMH4L   DEFINE  0xF03D6
GAFLMH4H   DEFINE  0xF03D7
RMPTR3L    DEFINE  0xF03D6
RMPTR3H    DEFINE  0xF03D7
GAFLPL4    DEFINE  0xF03D8
RMDF03     DEFINE  0xF03D8
GAFLPL4L   DEFINE  0xF03D8
GAFLPL4H   DEFINE  0xF03D9
RMDF03L    DEFINE  0xF03D8
RMDF03H    DEFINE  0xF03D9
GAFLPH4    DEFINE  0xF03DA
RMDF13     DEFINE  0xF03DA
GAFLPH4L   DEFINE  0xF03DA
GAFLPH4H   DEFINE  0xF03DB
RMDF13L    DEFINE  0xF03DA
RMDF13H    DEFINE  0xF03DB
GAFLIDL5   DEFINE  0xF03DC
RMDF23     DEFINE  0xF03DC
GAFLIDL5L  DEFINE  0xF03DC
GAFLIDL5H  DEFINE  0xF03DD
RMDF23L    DEFINE  0xF03DC
RMDF23H    DEFINE  0xF03DD
GAFLIDH5   DEFINE  0xF03DE
RMDF33     DEFINE  0xF03DE
GAFLIDH5L  DEFINE  0xF03DE
GAFLIDH5H  DEFINE  0xF03DF
RMDF33L    DEFINE  0xF03DE
RMDF33H    DEFINE  0xF03DF
GAFLML5    DEFINE  0xF03E0
RMIDL4     DEFINE  0xF03E0
GAFLML5L   DEFINE  0xF03E0
GAFLML5H   DEFINE  0xF03E1
RMIDL4L    DEFINE  0xF03E0
RMIDL4H    DEFINE  0xF03E1
GAFLMH5    DEFINE  0xF03E2
RMIDH4     DEFINE  0xF03E2
GAFLMH5L   DEFINE  0xF03E2
GAFLMH5H   DEFINE  0xF03E3
RMIDH4L    DEFINE  0xF03E2
RMIDH4H    DEFINE  0xF03E3
GAFLPL5    DEFINE  0xF03E4
RMTS4      DEFINE  0xF03E4
GAFLPL5L   DEFINE  0xF03E4
GAFLPL5H   DEFINE  0xF03E5
RMTS4L     DEFINE  0xF03E4
RMTS4H     DEFINE  0xF03E5
GAFLPH5    DEFINE  0xF03E6
RMPTR4     DEFINE  0xF03E6
GAFLPH5L   DEFINE  0xF03E6
GAFLPH5H   DEFINE  0xF03E7
RMPTR4L    DEFINE  0xF03E6
RMPTR4H    DEFINE  0xF03E7
GAFLIDL6   DEFINE  0xF03E8
RMDF04     DEFINE  0xF03E8
GAFLIDL6L  DEFINE  0xF03E8
GAFLIDL6H  DEFINE  0xF03E9
RMDF04L    DEFINE  0xF03E8
RMDF04H    DEFINE  0xF03E9
GAFLIDH6   DEFINE  0xF03EA
RMDF14     DEFINE  0xF03EA
GAFLIDH6L  DEFINE  0xF03EA
GAFLIDH6H  DEFINE  0xF03EB
RMDF14L    DEFINE  0xF03EA
RMDF14H    DEFINE  0xF03EB
GAFLML6    DEFINE  0xF03EC
RMDF24     DEFINE  0xF03EC
GAFLML6L   DEFINE  0xF03EC
GAFLML6H   DEFINE  0xF03ED
RMDF24L    DEFINE  0xF03EC
RMDF24H    DEFINE  0xF03ED
GAFLMH6    DEFINE  0xF03EE
RMDF34     DEFINE  0xF03EE
GAFLMH6L   DEFINE  0xF03EE
GAFLMH6H   DEFINE  0xF03EF
RMDF34L    DEFINE  0xF03EE
RMDF34H    DEFINE  0xF03EF
GAFLPL6    DEFINE  0xF03F0
RMIDL5     DEFINE  0xF03F0
GAFLPL6L   DEFINE  0xF03F0
GAFLPL6H   DEFINE  0xF03F1
RMIDL5L    DEFINE  0xF03F0
RMIDL5H    DEFINE  0xF03F1
GAFLPH6    DEFINE  0xF03F2
RMIDH5     DEFINE  0xF03F2
GAFLPH6L   DEFINE  0xF03F2
GAFLPH6H   DEFINE  0xF03F3
RMIDH5L    DEFINE  0xF03F2
RMIDH5H    DEFINE  0xF03F3
GAFLIDL7   DEFINE  0xF03F4
RMTS5      DEFINE  0xF03F4
GAFLIDL7L  DEFINE  0xF03F4
GAFLIDL7H  DEFINE  0xF03F5
RMTS5L     DEFINE  0xF03F4
RMTS5H     DEFINE  0xF03F5
GAFLIDH7   DEFINE  0xF03F6
RMPTR5     DEFINE  0xF03F6
GAFLIDH7L  DEFINE  0xF03F6
GAFLIDH7H  DEFINE  0xF03F7
RMPTR5L    DEFINE  0xF03F6
RMPTR5H    DEFINE  0xF03F7
GAFLML7    DEFINE  0xF03F8
RMDF05     DEFINE  0xF03F8
GAFLML7L   DEFINE  0xF03F8
GAFLML7H   DEFINE  0xF03F9
RMDF05L    DEFINE  0xF03F8
RMDF05H    DEFINE  0xF03F9
GAFLMH7    DEFINE  0xF03FA
RMDF15     DEFINE  0xF03FA
GAFLMH7L   DEFINE  0xF03FA
GAFLMH7H   DEFINE  0xF03FB
RMDF15L    DEFINE  0xF03FA
RMDF15H    DEFINE  0xF03FB
GAFLPL7    DEFINE  0xF03FC
RMDF25     DEFINE  0xF03FC
GAFLPL7L   DEFINE  0xF03FC
GAFLPL7H   DEFINE  0xF03FD
RMDF25L    DEFINE  0xF03FC
RMDF25H    DEFINE  0xF03FD
GAFLPH7    DEFINE  0xF03FE
RMDF35     DEFINE  0xF03FE
GAFLPH7L   DEFINE  0xF03FE
GAFLPH7H   DEFINE  0xF03FF
RMDF35L    DEFINE  0xF03FE
RMDF35H    DEFINE  0xF03FF
GAFLIDL8   DEFINE  0xF0400
RMIDL6     DEFINE  0xF0400
GAFLIDL8L  DEFINE  0xF0400
GAFLIDL8H  DEFINE  0xF0401
RMIDL6L    DEFINE  0xF0400
RMIDL6H    DEFINE  0xF0401
GAFLIDH8   DEFINE  0xF0402
RMIDH6     DEFINE  0xF0402
GAFLIDH8L  DEFINE  0xF0402
GAFLIDH8H  DEFINE  0xF0403
RMIDH6L    DEFINE  0xF0402
RMIDH6H    DEFINE  0xF0403
GAFLML8    DEFINE  0xF0404
RMTS6      DEFINE  0xF0404
GAFLML8L   DEFINE  0xF0404
GAFLML8H   DEFINE  0xF0405
RMTS6L     DEFINE  0xF0404
RMTS6H     DEFINE  0xF0405
GAFLMH8    DEFINE  0xF0406
RMPTR6     DEFINE  0xF0406
GAFLMH8L   DEFINE  0xF0406
GAFLMH8H   DEFINE  0xF0407
RMPTR6L    DEFINE  0xF0406
RMPTR6H    DEFINE  0xF0407
GAFLPL8    DEFINE  0xF0408
RMDF06     DEFINE  0xF0408
GAFLPL8L   DEFINE  0xF0408
GAFLPL8H   DEFINE  0xF0409
RMDF06L    DEFINE  0xF0408
RMDF06H    DEFINE  0xF0409
GAFLPH8    DEFINE  0xF040A
RMDF16     DEFINE  0xF040A
GAFLPH8L   DEFINE  0xF040A
GAFLPH8H   DEFINE  0xF040B
RMDF16L    DEFINE  0xF040A
RMDF16H    DEFINE  0xF040B
GAFLIDL9   DEFINE  0xF040C
RMDF26     DEFINE  0xF040C
GAFLIDL9L  DEFINE  0xF040C
GAFLIDL9H  DEFINE  0xF040D
RMDF26L    DEFINE  0xF040C
RMDF26H    DEFINE  0xF040D
GAFLIDH9   DEFINE  0xF040E
RMDF36     DEFINE  0xF040E
GAFLIDH9L  DEFINE  0xF040E
GAFLIDH9H  DEFINE  0xF040F
RMDF36L    DEFINE  0xF040E
RMDF36H    DEFINE  0xF040F
GAFLML9    DEFINE  0xF0410
RMIDL7     DEFINE  0xF0410
GAFLML9L   DEFINE  0xF0410
GAFLML9H   DEFINE  0xF0411
RMIDL7L    DEFINE  0xF0410
RMIDL7H    DEFINE  0xF0411
GAFLMH9    DEFINE  0xF0412
RMIDH7     DEFINE  0xF0412
GAFLMH9L   DEFINE  0xF0412
GAFLMH9H   DEFINE  0xF0413
RMIDH7L    DEFINE  0xF0412
RMIDH7H    DEFINE  0xF0413
GAFLPL9    DEFINE  0xF0414
RMTS7      DEFINE  0xF0414
GAFLPL9L   DEFINE  0xF0414
GAFLPL9H   DEFINE  0xF0415
RMTS7L     DEFINE  0xF0414
RMTS7H     DEFINE  0xF0415
GAFLPH9    DEFINE  0xF0416
RMPTR7     DEFINE  0xF0416
GAFLPH9L   DEFINE  0xF0416
GAFLPH9H   DEFINE  0xF0417
RMPTR7L    DEFINE  0xF0416
RMPTR7H    DEFINE  0xF0417
GAFLIDL10  DEFINE  0xF0418
RMDF07     DEFINE  0xF0418
GAFLIDL10L DEFINE  0xF0418
GAFLIDL10H DEFINE  0xF0419
RMDF07L    DEFINE  0xF0418
RMDF07H    DEFINE  0xF0419
GAFLIDH10  DEFINE  0xF041A
RMDF17     DEFINE  0xF041A
GAFLIDH10L DEFINE  0xF041A
GAFLIDH10H DEFINE  0xF041B
RMDF17L    DEFINE  0xF041A
RMDF17H    DEFINE  0xF041B
GAFLML10   DEFINE  0xF041C
RMDF27     DEFINE  0xF041C
GAFLML10L  DEFINE  0xF041C
GAFLML10H  DEFINE  0xF041D
RMDF27L    DEFINE  0xF041C
RMDF27H    DEFINE  0xF041D
GAFLMH10   DEFINE  0xF041E
RMDF37     DEFINE  0xF041E
GAFLMH10L  DEFINE  0xF041E
GAFLMH10H  DEFINE  0xF041F
RMDF37L    DEFINE  0xF041E
RMDF37H    DEFINE  0xF041F
GAFLPL10   DEFINE  0xF0420
RMIDL8     DEFINE  0xF0420
GAFLPL10L  DEFINE  0xF0420
GAFLPL10H  DEFINE  0xF0421
RMIDL8L    DEFINE  0xF0420
RMIDL8H    DEFINE  0xF0421
GAFLPH10   DEFINE  0xF0422
RMIDH8     DEFINE  0xF0422
GAFLPH10L  DEFINE  0xF0422
GAFLPH10H  DEFINE  0xF0423
RMIDH8L    DEFINE  0xF0422
RMIDH8H    DEFINE  0xF0423
GAFLIDL11  DEFINE  0xF0424
RMTS8      DEFINE  0xF0424
GAFLIDL11L DEFINE  0xF0424
GAFLIDL11H DEFINE  0xF0425
RMTS8L     DEFINE  0xF0424
RMTS8H     DEFINE  0xF0425
GAFLIDH11  DEFINE  0xF0426
RMPTR8     DEFINE  0xF0426
GAFLIDH11L DEFINE  0xF0426
GAFLIDH11H DEFINE  0xF0427
RMPTR8L    DEFINE  0xF0426
RMPTR8H    DEFINE  0xF0427
GAFLML11   DEFINE  0xF0428
RMDF08     DEFINE  0xF0428
GAFLML11L  DEFINE  0xF0428
GAFLML11H  DEFINE  0xF0429
RMDF08L    DEFINE  0xF0428
RMDF08H    DEFINE  0xF0429
GAFLMH11   DEFINE  0xF042A
RMDF18     DEFINE  0xF042A
GAFLMH11L  DEFINE  0xF042A
GAFLMH11H  DEFINE  0xF042B
RMDF18L    DEFINE  0xF042A
RMDF18H    DEFINE  0xF042B
GAFLPL11   DEFINE  0xF042C
RMDF28     DEFINE  0xF042C
GAFLPL11L  DEFINE  0xF042C
GAFLPL11H  DEFINE  0xF042D
RMDF28L    DEFINE  0xF042C
RMDF28H    DEFINE  0xF042D
GAFLPH11   DEFINE  0xF042E
RMDF38     DEFINE  0xF042E
GAFLPH11L  DEFINE  0xF042E
GAFLPH11H  DEFINE  0xF042F
RMDF38L    DEFINE  0xF042E
RMDF38H    DEFINE  0xF042F
GAFLIDL12  DEFINE  0xF0430
RMIDL9     DEFINE  0xF0430
GAFLIDL12L DEFINE  0xF0430
GAFLIDL12H DEFINE  0xF0431
RMIDL9L    DEFINE  0xF0430
RMIDL9H    DEFINE  0xF0431
GAFLIDH12  DEFINE  0xF0432
RMIDH9     DEFINE  0xF0432
GAFLIDH12L DEFINE  0xF0432
GAFLIDH12H DEFINE  0xF0433
RMIDH9L    DEFINE  0xF0432
RMIDH9H    DEFINE  0xF0433
GAFLML12   DEFINE  0xF0434
RMTS9      DEFINE  0xF0434
GAFLML12L  DEFINE  0xF0434
GAFLML12H  DEFINE  0xF0435
RMTS9L     DEFINE  0xF0434
RMTS9H     DEFINE  0xF0435
GAFLMH12   DEFINE  0xF0436
RMPTR9     DEFINE  0xF0436
GAFLMH12L  DEFINE  0xF0436
GAFLMH12H  DEFINE  0xF0437
RMPTR9L    DEFINE  0xF0436
RMPTR9H    DEFINE  0xF0437
GAFLPL12   DEFINE  0xF0438
RMDF09     DEFINE  0xF0438
GAFLPL12L  DEFINE  0xF0438
GAFLPL12H  DEFINE  0xF0439
RMDF09L    DEFINE  0xF0438
RMDF09H    DEFINE  0xF0439
GAFLPH12   DEFINE  0xF043A
RMDF19     DEFINE  0xF043A
GAFLPH12L  DEFINE  0xF043A
GAFLPH12H  DEFINE  0xF043B
RMDF19L    DEFINE  0xF043A
RMDF19H    DEFINE  0xF043B
GAFLIDL13  DEFINE  0xF043C
RMDF29     DEFINE  0xF043C
GAFLIDL13L DEFINE  0xF043C
GAFLIDL13H DEFINE  0xF043D
RMDF29L    DEFINE  0xF043C
RMDF29H    DEFINE  0xF043D
GAFLIDH13  DEFINE  0xF043E
RMDF39     DEFINE  0xF043E
GAFLIDH13L DEFINE  0xF043E
GAFLIDH13H DEFINE  0xF043F
RMDF39L    DEFINE  0xF043E
RMDF39H    DEFINE  0xF043F
GAFLML13   DEFINE  0xF0440
RMIDL10    DEFINE  0xF0440
GAFLML13L  DEFINE  0xF0440
GAFLML13H  DEFINE  0xF0441
RMIDL10L   DEFINE  0xF0440
RMIDL10H   DEFINE  0xF0441
GAFLMH13   DEFINE  0xF0442
RMIDH10    DEFINE  0xF0442
GAFLMH13L  DEFINE  0xF0442
GAFLMH13H  DEFINE  0xF0443
RMIDH10L   DEFINE  0xF0442
RMIDH10H   DEFINE  0xF0443
GAFLPL13   DEFINE  0xF0444
RMTS10     DEFINE  0xF0444
GAFLPL13L  DEFINE  0xF0444
GAFLPL13H  DEFINE  0xF0445
RMTS10L    DEFINE  0xF0444
RMTS10H    DEFINE  0xF0445
GAFLPH13   DEFINE  0xF0446
RMPTR10    DEFINE  0xF0446
GAFLPH13L  DEFINE  0xF0446
GAFLPH13H  DEFINE  0xF0447
RMPTR10L   DEFINE  0xF0446
RMPTR10H   DEFINE  0xF0447
GAFLIDL14  DEFINE  0xF0448
RMDF010    DEFINE  0xF0448
GAFLIDL14L DEFINE  0xF0448
GAFLIDL14H DEFINE  0xF0449
RMDF010L   DEFINE  0xF0448
RMDF010H   DEFINE  0xF0449
GAFLIDH14  DEFINE  0xF044A
RMDF110    DEFINE  0xF044A
GAFLIDH14L DEFINE  0xF044A
GAFLIDH14H DEFINE  0xF044B
RMDF110L   DEFINE  0xF044A
RMDF110H   DEFINE  0xF044B
GAFLML14   DEFINE  0xF044C
RMDF210    DEFINE  0xF044C
GAFLML14L  DEFINE  0xF044C
GAFLML14H  DEFINE  0xF044D
RMDF210L   DEFINE  0xF044C
RMDF210H   DEFINE  0xF044D
GAFLMH14   DEFINE  0xF044E
RMDF310    DEFINE  0xF044E
GAFLMH14L  DEFINE  0xF044E
GAFLMH14H  DEFINE  0xF044F
RMDF310L   DEFINE  0xF044E
RMDF310H   DEFINE  0xF044F
GAFLPL14   DEFINE  0xF0450
RMIDL11    DEFINE  0xF0450
GAFLPL14L  DEFINE  0xF0450
GAFLPL14H  DEFINE  0xF0451
RMIDL11L   DEFINE  0xF0450
RMIDL11H   DEFINE  0xF0451
GAFLPH14   DEFINE  0xF0452
RMIDH11    DEFINE  0xF0452
GAFLPH14L  DEFINE  0xF0452
GAFLPH14H  DEFINE  0xF0453
RMIDH11L   DEFINE  0xF0452
RMIDH11H   DEFINE  0xF0453
GAFLIDL15  DEFINE  0xF0454
RMTS11     DEFINE  0xF0454
GAFLIDL15L DEFINE  0xF0454
GAFLIDL15H DEFINE  0xF0455
RMTS11L    DEFINE  0xF0454
RMTS11H    DEFINE  0xF0455
GAFLIDH15  DEFINE  0xF0456
RMPTR11    DEFINE  0xF0456
GAFLIDH15L DEFINE  0xF0456
GAFLIDH15H DEFINE  0xF0457
RMPTR11L   DEFINE  0xF0456
RMPTR11H   DEFINE  0xF0457
GAFLML15   DEFINE  0xF0458
RMDF011    DEFINE  0xF0458
GAFLML15L  DEFINE  0xF0458
GAFLML15H  DEFINE  0xF0459
RMDF011L   DEFINE  0xF0458
RMDF011H   DEFINE  0xF0459
GAFLMH15   DEFINE  0xF045A
RMDF111    DEFINE  0xF045A
GAFLMH15L  DEFINE  0xF045A
GAFLMH15H  DEFINE  0xF045B
RMDF111L   DEFINE  0xF045A
RMDF111H   DEFINE  0xF045B
GAFLPL15   DEFINE  0xF045C
RMDF211    DEFINE  0xF045C
GAFLPL15L  DEFINE  0xF045C
GAFLPL15H  DEFINE  0xF045D
RMDF211L   DEFINE  0xF045C
RMDF211H   DEFINE  0xF045D
GAFLPH15   DEFINE  0xF045E
RMDF311    DEFINE  0xF045E
GAFLPH15L  DEFINE  0xF045E
GAFLPH15H  DEFINE  0xF045F
RMDF311L   DEFINE  0xF045E
RMDF311H   DEFINE  0xF045F
RMIDL12    DEFINE  0xF0460
RMIDL12L   DEFINE  0xF0460
RMIDL12H   DEFINE  0xF0461
RMIDH12    DEFINE  0xF0462
RMIDH12L   DEFINE  0xF0462
RMIDH12H   DEFINE  0xF0463
RMTS12     DEFINE  0xF0464
RMTS12L    DEFINE  0xF0464
RMTS12H    DEFINE  0xF0465
RMPTR12    DEFINE  0xF0466
RMPTR12L   DEFINE  0xF0466
RMPTR12H   DEFINE  0xF0467
RMDF012    DEFINE  0xF0468
RMDF012L   DEFINE  0xF0468
RMDF012H   DEFINE  0xF0469
RMDF112    DEFINE  0xF046A
RMDF112L   DEFINE  0xF046A
RMDF112H   DEFINE  0xF046B
RMDF212    DEFINE  0xF046C
RMDF212L   DEFINE  0xF046C
RMDF212H   DEFINE  0xF046D
RMDF312    DEFINE  0xF046E
RMDF312L   DEFINE  0xF046E
RMDF312H   DEFINE  0xF046F
RMIDL13    DEFINE  0xF0470
RMIDL13L   DEFINE  0xF0470
RMIDL13H   DEFINE  0xF0471
RMIDH13    DEFINE  0xF0472
RMIDH13L   DEFINE  0xF0472
RMIDH13H   DEFINE  0xF0473
RMTS13     DEFINE  0xF0474
RMTS13L    DEFINE  0xF0474
RMTS13H    DEFINE  0xF0475
RMPTR13    DEFINE  0xF0476
RMPTR13L   DEFINE  0xF0476
RMPTR13H   DEFINE  0xF0477
RMDF013    DEFINE  0xF0478
RMDF013L   DEFINE  0xF0478
RMDF013H   DEFINE  0xF0479
RMDF113    DEFINE  0xF047A
RMDF113L   DEFINE  0xF047A
RMDF113H   DEFINE  0xF047B
RMDF213    DEFINE  0xF047C
RMDF213L   DEFINE  0xF047C
RMDF213H   DEFINE  0xF047D
RMDF313    DEFINE  0xF047E
RMDF313L   DEFINE  0xF047E
RMDF313H   DEFINE  0xF047F
RMIDL14    DEFINE  0xF0480
RMIDL14L   DEFINE  0xF0480
RMIDL14H   DEFINE  0xF0481
RMIDH14    DEFINE  0xF0482
RMIDH14L   DEFINE  0xF0482
RMIDH14H   DEFINE  0xF0483
RMTS14     DEFINE  0xF0484
RMTS14L    DEFINE  0xF0484
RMTS14H    DEFINE  0xF0485
RMPTR14    DEFINE  0xF0486
RMPTR14L   DEFINE  0xF0486
RMPTR14H   DEFINE  0xF0487
RMDF014    DEFINE  0xF0488
RMDF014L   DEFINE  0xF0488
RMDF014H   DEFINE  0xF0489
RMDF114    DEFINE  0xF048A
RMDF114L   DEFINE  0xF048A
RMDF114H   DEFINE  0xF048B
RMDF214    DEFINE  0xF048C
RMDF214L   DEFINE  0xF048C
RMDF214H   DEFINE  0xF048D
RMDF314    DEFINE  0xF048E
RMDF314L   DEFINE  0xF048E
RMDF314H   DEFINE  0xF048F
RMIDL15    DEFINE  0xF0490
RMIDL15L   DEFINE  0xF0490
RMIDL15H   DEFINE  0xF0491
RMIDH15    DEFINE  0xF0492
RMIDH15L   DEFINE  0xF0492
RMIDH15H   DEFINE  0xF0493
RMTS15     DEFINE  0xF0494
RMTS15L    DEFINE  0xF0494
RMTS15H    DEFINE  0xF0495
RMPTR15    DEFINE  0xF0496
RMPTR15L   DEFINE  0xF0496
RMPTR15H   DEFINE  0xF0497
RMDF015    DEFINE  0xF0498
RMDF015L   DEFINE  0xF0498
RMDF015H   DEFINE  0xF0499
RMDF115    DEFINE  0xF049A
RMDF115L   DEFINE  0xF049A
RMDF115H   DEFINE  0xF049B
RMDF215    DEFINE  0xF049C
RMDF215L   DEFINE  0xF049C
RMDF215H   DEFINE  0xF049D
RMDF315    DEFINE  0xF049E
RMDF315L   DEFINE  0xF049E
RMDF315H   DEFINE  0xF049F
RPGACC0    DEFINE  0xF0580
RPGACC0L   DEFINE  0xF0580
RPGACC0H   DEFINE  0xF0581
RPGACC1    DEFINE  0xF0582
RPGACC1L   DEFINE  0xF0582
RPGACC1H   DEFINE  0xF0583
RPGACC2    DEFINE  0xF0584
RPGACC2L   DEFINE  0xF0584
RPGACC2H   DEFINE  0xF0585
RPGACC3    DEFINE  0xF0586
RPGACC3L   DEFINE  0xF0586
RPGACC3H   DEFINE  0xF0587
RPGACC4    DEFINE  0xF0588
RPGACC4L   DEFINE  0xF0588
RPGACC4H   DEFINE  0xF0589
RPGACC5    DEFINE  0xF058A
RPGACC5L   DEFINE  0xF058A
RPGACC5H   DEFINE  0xF058B
RPGACC6    DEFINE  0xF058C
RPGACC6L   DEFINE  0xF058C
RPGACC6H   DEFINE  0xF058D
RPGACC7    DEFINE  0xF058E
RPGACC7L   DEFINE  0xF058E
RPGACC7H   DEFINE  0xF058F
RPGACC8    DEFINE  0xF0590
RPGACC8L   DEFINE  0xF0590
RPGACC8H   DEFINE  0xF0591
RPGACC9    DEFINE  0xF0592
RPGACC9L   DEFINE  0xF0592
RPGACC9H   DEFINE  0xF0593
RPGACC10   DEFINE  0xF0594
RPGACC10L  DEFINE  0xF0594
RPGACC10H  DEFINE  0xF0595
RPGACC11   DEFINE  0xF0596
RPGACC11L  DEFINE  0xF0596
RPGACC11H  DEFINE  0xF0597
RPGACC12   DEFINE  0xF0598
RPGACC12L  DEFINE  0xF0598
RPGACC12H  DEFINE  0xF0599
RPGACC13   DEFINE  0xF059A
RPGACC13L  DEFINE  0xF059A
RPGACC13H  DEFINE  0xF059B
RPGACC14   DEFINE  0xF059C
RPGACC14L  DEFINE  0xF059C
RPGACC14H  DEFINE  0xF059D
RPGACC15   DEFINE  0xF059E
RPGACC15L  DEFINE  0xF059E
RPGACC15H  DEFINE  0xF059F
RFIDL0     DEFINE  0xF05A0
RPGACC16   DEFINE  0xF05A0
RFIDL0L    DEFINE  0xF05A0
RFIDL0H    DEFINE  0xF05A1
RPGACC16L  DEFINE  0xF05A0
RPGACC16H  DEFINE  0xF05A1
RFIDH0     DEFINE  0xF05A2
RPGACC17   DEFINE  0xF05A2
RFIDH0L    DEFINE  0xF05A2
RFIDH0H    DEFINE  0xF05A3
RPGACC17L  DEFINE  0xF05A2
RPGACC17H  DEFINE  0xF05A3
RFTS0      DEFINE  0xF05A4
RPGACC18   DEFINE  0xF05A4
RFTS0L     DEFINE  0xF05A4
RFTS0H     DEFINE  0xF05A5
RPGACC18L  DEFINE  0xF05A4
RPGACC18H  DEFINE  0xF05A5
RFPTR0     DEFINE  0xF05A6
RPGACC19   DEFINE  0xF05A6
RFPTR0L    DEFINE  0xF05A6
RFPTR0H    DEFINE  0xF05A7
RPGACC19L  DEFINE  0xF05A6
RPGACC19H  DEFINE  0xF05A7
RFDF00     DEFINE  0xF05A8
RPGACC20   DEFINE  0xF05A8
RFDF00L    DEFINE  0xF05A8
RFDF00H    DEFINE  0xF05A9
RPGACC20L  DEFINE  0xF05A8
RPGACC20H  DEFINE  0xF05A9
RFDF10     DEFINE  0xF05AA
RPGACC21   DEFINE  0xF05AA
RFDF10L    DEFINE  0xF05AA
RFDF10H    DEFINE  0xF05AB
RPGACC21L  DEFINE  0xF05AA
RPGACC21H  DEFINE  0xF05AB
RFDF20     DEFINE  0xF05AC
RPGACC22   DEFINE  0xF05AC
RFDF20L    DEFINE  0xF05AC
RFDF20H    DEFINE  0xF05AD
RPGACC22L  DEFINE  0xF05AC
RPGACC22H  DEFINE  0xF05AD
RFDF30     DEFINE  0xF05AE
RPGACC23   DEFINE  0xF05AE
RFDF30L    DEFINE  0xF05AE
RFDF30H    DEFINE  0xF05AF
RPGACC23L  DEFINE  0xF05AE
RPGACC23H  DEFINE  0xF05AF
RFIDL1     DEFINE  0xF05B0
RPGACC24   DEFINE  0xF05B0
RFIDL1L    DEFINE  0xF05B0
RFIDL1H    DEFINE  0xF05B1
RPGACC24L  DEFINE  0xF05B0
RPGACC24H  DEFINE  0xF05B1
RFIDH1     DEFINE  0xF05B2
RPGACC25   DEFINE  0xF05B2
RFIDH1L    DEFINE  0xF05B2
RFIDH1H    DEFINE  0xF05B3
RPGACC25L  DEFINE  0xF05B2
RPGACC25H  DEFINE  0xF05B3
RFTS1      DEFINE  0xF05B4
RPGACC26   DEFINE  0xF05B4
RFTS1L     DEFINE  0xF05B4
RFTS1H     DEFINE  0xF05B5
RPGACC26L  DEFINE  0xF05B4
RPGACC26H  DEFINE  0xF05B5
RFPTR1     DEFINE  0xF05B6
RPGACC27   DEFINE  0xF05B6
RFPTR1L    DEFINE  0xF05B6
RFPTR1H    DEFINE  0xF05B7
RPGACC27L  DEFINE  0xF05B6
RPGACC27H  DEFINE  0xF05B7
RFDF01     DEFINE  0xF05B8
RPGACC28   DEFINE  0xF05B8
RFDF01L    DEFINE  0xF05B8
RFDF01H    DEFINE  0xF05B9
RPGACC28L  DEFINE  0xF05B8
RPGACC28H  DEFINE  0xF05B9
RFDF11     DEFINE  0xF05BA
RPGACC29   DEFINE  0xF05BA
RFDF11L    DEFINE  0xF05BA
RFDF11H    DEFINE  0xF05BB
RPGACC29L  DEFINE  0xF05BA
RPGACC29H  DEFINE  0xF05BB
RFDF21     DEFINE  0xF05BC
RPGACC30   DEFINE  0xF05BC
RFDF21L    DEFINE  0xF05BC
RFDF21H    DEFINE  0xF05BD
RPGACC30L  DEFINE  0xF05BC
RPGACC30H  DEFINE  0xF05BD
RFDF31     DEFINE  0xF05BE
RPGACC31   DEFINE  0xF05BE
RFDF31L    DEFINE  0xF05BE
RFDF31H    DEFINE  0xF05BF
RPGACC31L  DEFINE  0xF05BE
RPGACC31H  DEFINE  0xF05BF
RPGACC32   DEFINE  0xF05C0
RPGACC32L  DEFINE  0xF05C0
RPGACC32H  DEFINE  0xF05C1
RPGACC33   DEFINE  0xF05C2
RPGACC33L  DEFINE  0xF05C2
RPGACC33H  DEFINE  0xF05C3
RPGACC34   DEFINE  0xF05C4
RPGACC34L  DEFINE  0xF05C4
RPGACC34H  DEFINE  0xF05C5
RPGACC35   DEFINE  0xF05C6
RPGACC35L  DEFINE  0xF05C6
RPGACC35H  DEFINE  0xF05C7
RPGACC36   DEFINE  0xF05C8
RPGACC36L  DEFINE  0xF05C8
RPGACC36H  DEFINE  0xF05C9
RPGACC37   DEFINE  0xF05CA
RPGACC37L  DEFINE  0xF05CA
RPGACC37H  DEFINE  0xF05CB
RPGACC38   DEFINE  0xF05CC
RPGACC38L  DEFINE  0xF05CC
RPGACC38H  DEFINE  0xF05CD
RPGACC39   DEFINE  0xF05CE
RPGACC39L  DEFINE  0xF05CE
RPGACC39H  DEFINE  0xF05CF
RPGACC40   DEFINE  0xF05D0
RPGACC40L  DEFINE  0xF05D0
RPGACC40H  DEFINE  0xF05D1
RPGACC41   DEFINE  0xF05D2
RPGACC41L  DEFINE  0xF05D2
RPGACC41H  DEFINE  0xF05D3
RPGACC42   DEFINE  0xF05D4
RPGACC42L  DEFINE  0xF05D4
RPGACC42H  DEFINE  0xF05D5
RPGACC43   DEFINE  0xF05D6
RPGACC43L  DEFINE  0xF05D6
RPGACC43H  DEFINE  0xF05D7
RPGACC44   DEFINE  0xF05D8
RPGACC44L  DEFINE  0xF05D8
RPGACC44H  DEFINE  0xF05D9
RPGACC45   DEFINE  0xF05DA
RPGACC45L  DEFINE  0xF05DA
RPGACC45H  DEFINE  0xF05DB
RPGACC46   DEFINE  0xF05DC
RPGACC46L  DEFINE  0xF05DC
RPGACC46H  DEFINE  0xF05DD
RPGACC47   DEFINE  0xF05DE
RPGACC47L  DEFINE  0xF05DE
RPGACC47H  DEFINE  0xF05DF
CFIDL0     DEFINE  0xF05E0
RPGACC48   DEFINE  0xF05E0
CFIDL0L    DEFINE  0xF05E0
CFIDL0H    DEFINE  0xF05E1
RPGACC48L  DEFINE  0xF05E0
RPGACC48H  DEFINE  0xF05E1
CFIDH0     DEFINE  0xF05E2
RPGACC49   DEFINE  0xF05E2
CFIDH0L    DEFINE  0xF05E2
CFIDH0H    DEFINE  0xF05E3
RPGACC49L  DEFINE  0xF05E2
RPGACC49H  DEFINE  0xF05E3
CFTS0      DEFINE  0xF05E4
RPGACC50   DEFINE  0xF05E4
CFTS0L     DEFINE  0xF05E4
CFTS0H     DEFINE  0xF05E5
RPGACC50L  DEFINE  0xF05E4
RPGACC50H  DEFINE  0xF05E5
CFPTR0     DEFINE  0xF05E6
RPGACC51   DEFINE  0xF05E6
CFPTR0L    DEFINE  0xF05E6
CFPTR0H    DEFINE  0xF05E7
RPGACC51L  DEFINE  0xF05E6
RPGACC51H  DEFINE  0xF05E7
CFDF00     DEFINE  0xF05E8
RPGACC52   DEFINE  0xF05E8
CFDF00L    DEFINE  0xF05E8
CFDF00H    DEFINE  0xF05E9
RPGACC52L  DEFINE  0xF05E8
RPGACC52H  DEFINE  0xF05E9
CFDF10     DEFINE  0xF05EA
RPGACC53   DEFINE  0xF05EA
CFDF10L    DEFINE  0xF05EA
CFDF10H    DEFINE  0xF05EB
RPGACC53L  DEFINE  0xF05EA
RPGACC53H  DEFINE  0xF05EB
CFDF20     DEFINE  0xF05EC
RPGACC54   DEFINE  0xF05EC
CFDF20L    DEFINE  0xF05EC
CFDF20H    DEFINE  0xF05ED
RPGACC54L  DEFINE  0xF05EC
RPGACC54H  DEFINE  0xF05ED
CFDF30     DEFINE  0xF05EE
RPGACC55   DEFINE  0xF05EE
CFDF30L    DEFINE  0xF05EE
CFDF30H    DEFINE  0xF05EF
RPGACC55L  DEFINE  0xF05EE
RPGACC55H  DEFINE  0xF05EF
RPGACC56   DEFINE  0xF05F0
RPGACC56L  DEFINE  0xF05F0
RPGACC56H  DEFINE  0xF05F1
RPGACC57   DEFINE  0xF05F2
RPGACC57L  DEFINE  0xF05F2
RPGACC57H  DEFINE  0xF05F3
RPGACC58   DEFINE  0xF05F4
RPGACC58L  DEFINE  0xF05F4
RPGACC58H  DEFINE  0xF05F5
RPGACC59   DEFINE  0xF05F6
RPGACC59L  DEFINE  0xF05F6
RPGACC59H  DEFINE  0xF05F7
RPGACC60   DEFINE  0xF05F8
RPGACC60L  DEFINE  0xF05F8
RPGACC60H  DEFINE  0xF05F9
RPGACC61   DEFINE  0xF05FA
RPGACC61L  DEFINE  0xF05FA
RPGACC61H  DEFINE  0xF05FB
RPGACC62   DEFINE  0xF05FC
RPGACC62L  DEFINE  0xF05FC
RPGACC62H  DEFINE  0xF05FD
RPGACC63   DEFINE  0xF05FE
RPGACC63L  DEFINE  0xF05FE
RPGACC63H  DEFINE  0xF05FF
RPGACC64   DEFINE  0xF0600
TMIDL0     DEFINE  0xF0600
RPGACC64L  DEFINE  0xF0600
RPGACC64H  DEFINE  0xF0601
TMIDL0L    DEFINE  0xF0600
TMIDL0H    DEFINE  0xF0601
RPGACC65   DEFINE  0xF0602
TMIDH0     DEFINE  0xF0602
RPGACC65L  DEFINE  0xF0602
RPGACC65H  DEFINE  0xF0603
TMIDH0L    DEFINE  0xF0602
TMIDH0H    DEFINE  0xF0603
RPGACC66   DEFINE  0xF0604
RPGACC66L  DEFINE  0xF0604
RPGACC66H  DEFINE  0xF0605
RPGACC67   DEFINE  0xF0606
TMPTR0     DEFINE  0xF0606
RPGACC67L  DEFINE  0xF0606
RPGACC67H  DEFINE  0xF0607
TMPTR0L    DEFINE  0xF0606
TMPTR0H    DEFINE  0xF0607
RPGACC68   DEFINE  0xF0608
TMDF00     DEFINE  0xF0608
RPGACC68L  DEFINE  0xF0608
RPGACC68H  DEFINE  0xF0609
TMDF00L    DEFINE  0xF0608
TMDF00H    DEFINE  0xF0609
RPGACC69   DEFINE  0xF060A
TMDF10     DEFINE  0xF060A
RPGACC69L  DEFINE  0xF060A
RPGACC69H  DEFINE  0xF060B
TMDF10L    DEFINE  0xF060A
TMDF10H    DEFINE  0xF060B
RPGACC70   DEFINE  0xF060C
TMDF20     DEFINE  0xF060C
RPGACC70L  DEFINE  0xF060C
RPGACC70H  DEFINE  0xF060D
TMDF20L    DEFINE  0xF060C
TMDF20H    DEFINE  0xF060D
RPGACC71   DEFINE  0xF060E
TMDF30     DEFINE  0xF060E
RPGACC71L  DEFINE  0xF060E
RPGACC71H  DEFINE  0xF060F
TMDF30L    DEFINE  0xF060E
TMDF30H    DEFINE  0xF060F
RPGACC72   DEFINE  0xF0610
TMIDL1     DEFINE  0xF0610
RPGACC72L  DEFINE  0xF0610
RPGACC72H  DEFINE  0xF0611
TMIDL1L    DEFINE  0xF0610
TMIDL1H    DEFINE  0xF0611
RPGACC73   DEFINE  0xF0612
TMIDH1     DEFINE  0xF0612
RPGACC73L  DEFINE  0xF0612
RPGACC73H  DEFINE  0xF0613
TMIDH1L    DEFINE  0xF0612
TMIDH1H    DEFINE  0xF0613
RPGACC74   DEFINE  0xF0614
RPGACC74L  DEFINE  0xF0614
RPGACC74H  DEFINE  0xF0615
RPGACC75   DEFINE  0xF0616
TMPTR1     DEFINE  0xF0616
RPGACC75L  DEFINE  0xF0616
RPGACC75H  DEFINE  0xF0617
TMPTR1L    DEFINE  0xF0616
TMPTR1H    DEFINE  0xF0617
RPGACC76   DEFINE  0xF0618
TMDF01     DEFINE  0xF0618
RPGACC76L  DEFINE  0xF0618
RPGACC76H  DEFINE  0xF0619
TMDF01L    DEFINE  0xF0618
TMDF01H    DEFINE  0xF0619
RPGACC77   DEFINE  0xF061A
TMDF11     DEFINE  0xF061A
RPGACC77L  DEFINE  0xF061A
RPGACC77H  DEFINE  0xF061B
TMDF11L    DEFINE  0xF061A
TMDF11H    DEFINE  0xF061B
RPGACC78   DEFINE  0xF061C
TMDF21     DEFINE  0xF061C
RPGACC78L  DEFINE  0xF061C
RPGACC78H  DEFINE  0xF061D
TMDF21L    DEFINE  0xF061C
TMDF21H    DEFINE  0xF061D
RPGACC79   DEFINE  0xF061E
TMDF31     DEFINE  0xF061E
RPGACC79L  DEFINE  0xF061E
RPGACC79H  DEFINE  0xF061F
TMDF31L    DEFINE  0xF061E
TMDF31H    DEFINE  0xF061F
RPGACC80   DEFINE  0xF0620
TMIDL2     DEFINE  0xF0620
RPGACC80L  DEFINE  0xF0620
RPGACC80H  DEFINE  0xF0621
TMIDL2L    DEFINE  0xF0620
TMIDL2H    DEFINE  0xF0621
RPGACC81   DEFINE  0xF0622
TMIDH2     DEFINE  0xF0622
RPGACC81L  DEFINE  0xF0622
RPGACC81H  DEFINE  0xF0623
TMIDH2L    DEFINE  0xF0622
TMIDH2H    DEFINE  0xF0623
RPGACC82   DEFINE  0xF0624
RPGACC82L  DEFINE  0xF0624
RPGACC82H  DEFINE  0xF0625
RPGACC83   DEFINE  0xF0626
TMPTR2     DEFINE  0xF0626
RPGACC83L  DEFINE  0xF0626
RPGACC83H  DEFINE  0xF0627
TMPTR2L    DEFINE  0xF0626
TMPTR2H    DEFINE  0xF0627
RPGACC84   DEFINE  0xF0628
TMDF02     DEFINE  0xF0628
RPGACC84L  DEFINE  0xF0628
RPGACC84H  DEFINE  0xF0629
TMDF02L    DEFINE  0xF0628
TMDF02H    DEFINE  0xF0629
RPGACC85   DEFINE  0xF062A
TMDF12     DEFINE  0xF062A
RPGACC85L  DEFINE  0xF062A
RPGACC85H  DEFINE  0xF062B
TMDF12L    DEFINE  0xF062A
TMDF12H    DEFINE  0xF062B
RPGACC86   DEFINE  0xF062C
TMDF22     DEFINE  0xF062C
RPGACC86L  DEFINE  0xF062C
RPGACC86H  DEFINE  0xF062D
TMDF22L    DEFINE  0xF062C
TMDF22H    DEFINE  0xF062D
RPGACC87   DEFINE  0xF062E
TMDF32     DEFINE  0xF062E
RPGACC87L  DEFINE  0xF062E
RPGACC87H  DEFINE  0xF062F
TMDF32L    DEFINE  0xF062E
TMDF32H    DEFINE  0xF062F
RPGACC88   DEFINE  0xF0630
TMIDL3     DEFINE  0xF0630
RPGACC88L  DEFINE  0xF0630
RPGACC88H  DEFINE  0xF0631
TMIDL3L    DEFINE  0xF0630
TMIDL3H    DEFINE  0xF0631
RPGACC89   DEFINE  0xF0632
TMIDH3     DEFINE  0xF0632
RPGACC89L  DEFINE  0xF0632
RPGACC89H  DEFINE  0xF0633
TMIDH3L    DEFINE  0xF0632
TMIDH3H    DEFINE  0xF0633
RPGACC90   DEFINE  0xF0634
RPGACC90L  DEFINE  0xF0634
RPGACC90H  DEFINE  0xF0635
RPGACC91   DEFINE  0xF0636
TMPTR3     DEFINE  0xF0636
RPGACC91L  DEFINE  0xF0636
RPGACC91H  DEFINE  0xF0637
TMPTR3L    DEFINE  0xF0636
TMPTR3H    DEFINE  0xF0637
RPGACC92   DEFINE  0xF0638
TMDF03     DEFINE  0xF0638
RPGACC92L  DEFINE  0xF0638
RPGACC92H  DEFINE  0xF0639
TMDF03L    DEFINE  0xF0638
TMDF03H    DEFINE  0xF0639
RPGACC93   DEFINE  0xF063A
TMDF13     DEFINE  0xF063A
RPGACC93L  DEFINE  0xF063A
RPGACC93H  DEFINE  0xF063B
TMDF13L    DEFINE  0xF063A
TMDF13H    DEFINE  0xF063B
RPGACC94   DEFINE  0xF063C
TMDF23     DEFINE  0xF063C
RPGACC94L  DEFINE  0xF063C
RPGACC94H  DEFINE  0xF063D
TMDF23L    DEFINE  0xF063C
TMDF23H    DEFINE  0xF063D
RPGACC95   DEFINE  0xF063E
TMDF33     DEFINE  0xF063E
RPGACC95L  DEFINE  0xF063E
RPGACC95H  DEFINE  0xF063F
TMDF33L    DEFINE  0xF063E
TMDF33H    DEFINE  0xF063F
RPGACC96   DEFINE  0xF0640
RPGACC96L  DEFINE  0xF0640
RPGACC96H  DEFINE  0xF0641
RPGACC97   DEFINE  0xF0642
RPGACC97L  DEFINE  0xF0642
RPGACC97H  DEFINE  0xF0643
RPGACC98   DEFINE  0xF0644
RPGACC98L  DEFINE  0xF0644
RPGACC98H  DEFINE  0xF0645
RPGACC99   DEFINE  0xF0646
RPGACC99L  DEFINE  0xF0646
RPGACC99H  DEFINE  0xF0647
RPGACC100  DEFINE  0xF0648
RPGACC100L DEFINE  0xF0648
RPGACC100H DEFINE  0xF0649
RPGACC101  DEFINE  0xF064A
RPGACC101L DEFINE  0xF064A
RPGACC101H DEFINE  0xF064B
RPGACC102  DEFINE  0xF064C
RPGACC102L DEFINE  0xF064C
RPGACC102H DEFINE  0xF064D
RPGACC103  DEFINE  0xF064E
RPGACC103L DEFINE  0xF064E
RPGACC103H DEFINE  0xF064F
RPGACC104  DEFINE  0xF0650
RPGACC104L DEFINE  0xF0650
RPGACC104H DEFINE  0xF0651
RPGACC105  DEFINE  0xF0652
RPGACC105L DEFINE  0xF0652
RPGACC105H DEFINE  0xF0653
RPGACC106  DEFINE  0xF0654
RPGACC106L DEFINE  0xF0654
RPGACC106H DEFINE  0xF0655
RPGACC107  DEFINE  0xF0656
RPGACC107L DEFINE  0xF0656
RPGACC107H DEFINE  0xF0657
RPGACC108  DEFINE  0xF0658
RPGACC108L DEFINE  0xF0658
RPGACC108H DEFINE  0xF0659
RPGACC109  DEFINE  0xF065A
RPGACC109L DEFINE  0xF065A
RPGACC109H DEFINE  0xF065B
RPGACC110  DEFINE  0xF065C
RPGACC110L DEFINE  0xF065C
RPGACC110H DEFINE  0xF065D
RPGACC111  DEFINE  0xF065E
RPGACC111L DEFINE  0xF065E
RPGACC111H DEFINE  0xF065F
RPGACC112  DEFINE  0xF0660
RPGACC112L DEFINE  0xF0660
RPGACC112H DEFINE  0xF0661
RPGACC113  DEFINE  0xF0662
RPGACC113L DEFINE  0xF0662
RPGACC113H DEFINE  0xF0663
RPGACC114  DEFINE  0xF0664
RPGACC114L DEFINE  0xF0664
RPGACC114H DEFINE  0xF0665
RPGACC115  DEFINE  0xF0666
RPGACC115L DEFINE  0xF0666
RPGACC115H DEFINE  0xF0667
RPGACC116  DEFINE  0xF0668
RPGACC116L DEFINE  0xF0668
RPGACC116H DEFINE  0xF0669
RPGACC117  DEFINE  0xF066A
RPGACC117L DEFINE  0xF066A
RPGACC117H DEFINE  0xF066B
RPGACC118  DEFINE  0xF066C
RPGACC118L DEFINE  0xF066C
RPGACC118H DEFINE  0xF066D
RPGACC119  DEFINE  0xF066E
RPGACC119L DEFINE  0xF066E
RPGACC119H DEFINE  0xF066F
RPGACC120  DEFINE  0xF0670
RPGACC120L DEFINE  0xF0670
RPGACC120H DEFINE  0xF0671
RPGACC121  DEFINE  0xF0672
RPGACC121L DEFINE  0xF0672
RPGACC121H DEFINE  0xF0673
RPGACC122  DEFINE  0xF0674
RPGACC122L DEFINE  0xF0674
RPGACC122H DEFINE  0xF0675
RPGACC123  DEFINE  0xF0676
RPGACC123L DEFINE  0xF0676
RPGACC123H DEFINE  0xF0677
RPGACC124  DEFINE  0xF0678
RPGACC124L DEFINE  0xF0678
RPGACC124H DEFINE  0xF0679
RPGACC125  DEFINE  0xF067A
RPGACC125L DEFINE  0xF067A
RPGACC125H DEFINE  0xF067B
RPGACC126  DEFINE  0xF067C
RPGACC126L DEFINE  0xF067C
RPGACC126H DEFINE  0xF067D
RPGACC127  DEFINE  0xF067E
RPGACC127L DEFINE  0xF067E
RPGACC127H DEFINE  0xF067F
THLACC0    DEFINE  0xF0680
THLACC0L   DEFINE  0xF0680
THLACC0H   DEFINE  0xF0681
LWBR0      DEFINE  0xF06C1
LWBR1      DEFINE  0xF06C1
LWBR2      DEFINE  0xF06C1
LBRP0      DEFINE  0xF06C2
LBRP1      DEFINE  0xF06C2
LBRP2      DEFINE  0xF06C2
LBRP00     DEFINE  0xF06C2
LBRP01     DEFINE  0xF06C3
LBRP20     DEFINE  0xF06C2
LBRP11     DEFINE  0xF06C3
LBRP21     DEFINE  0xF06C3
LSTC0      DEFINE  0xF06C4
LSTC1      DEFINE  0xF06C4
LSTC2      DEFINE  0xF06C4
LUSC0      DEFINE  0xF06C5
LUSC1      DEFINE  0xF06C5
LUSC2      DEFINE  0xF06C5
LMD0       DEFINE  0xF06C8
LMD1       DEFINE  0xF06C8
LMD2       DEFINE  0xF06C8
LBFC0      DEFINE  0xF06C9
LBFC1      DEFINE  0xF06C9
LBFC2      DEFINE  0xF06C9
LSC0       DEFINE  0xF06CA
LSC1       DEFINE  0xF06CA
LSC2       DEFINE  0xF06CA
LWUP0      DEFINE  0xF06CB
LWUP1      DEFINE  0xF06CB
LWUP2      DEFINE  0xF06CB
LIE0       DEFINE  0xF06CC
LIE1       DEFINE  0xF06CC
LIE2       DEFINE  0xF06CC
LEDE0      DEFINE  0xF06CD
LEDE1      DEFINE  0xF06CD
LEDE2      DEFINE  0xF06CD
LCUC0      DEFINE  0xF06CE
LCUC1      DEFINE  0xF06CE
LCUC2      DEFINE  0xF06CE
LTRC0      DEFINE  0xF06D0
LTRC1      DEFINE  0xF06D0
LTRC2      DEFINE  0xF06D0
LMST0      DEFINE  0xF06D1
LMST1      DEFINE  0xF06D1
LMST2      DEFINE  0xF06D1
LST0       DEFINE  0xF06D2
LST1       DEFINE  0xF06D2
LST2       DEFINE  0xF06D2
LEST0      DEFINE  0xF06D3
LEST1      DEFINE  0xF06D3
LEST2      DEFINE  0xF06D3
LDFC0      DEFINE  0xF06D4
LDFC1      DEFINE  0xF06D4
LDFC2      DEFINE  0xF06D4
LIDB0      DEFINE  0xF06D5
LIDB1      DEFINE  0xF06D5
LIDB2      DEFINE  0xF06D5
LCBR0      DEFINE  0xF06D6
LCBR1      DEFINE  0xF06D6
LCBR2      DEFINE  0xF06D6
LUDB00     DEFINE  0xF06D7
LUDB10     DEFINE  0xF06D7
LUDB20     DEFINE  0xF06D7
LDB01      DEFINE  0xF06D8
LDB11      DEFINE  0xF06D8
LDB21      DEFINE  0xF06D8
LDB02      DEFINE  0xF06D9
LDB12      DEFINE  0xF06D9
LDB22      DEFINE  0xF06D9
LDB03      DEFINE  0xF06DA
LDB13      DEFINE  0xF06DA
LDB23      DEFINE  0xF06DA
LDB04      DEFINE  0xF06DB
LDB14      DEFINE  0xF06DB
LDB24      DEFINE  0xF06DB
LDB05      DEFINE  0xF06DC
LDB15      DEFINE  0xF06DC
LDB25      DEFINE  0xF06DC
LDB06      DEFINE  0xF06DD
LDB16      DEFINE  0xF06DD
LDB26      DEFINE  0xF06DD
LDB07      DEFINE  0xF06DE
LDB17      DEFINE  0xF06DE
LDB27      DEFINE  0xF06DE
LDB08      DEFINE  0xF06DF
LDB18      DEFINE  0xF06DF
LDB28      DEFINE  0xF06DF
LUOER0     DEFINE  0xF06E0
LUOER1     DEFINE  0xF06E0
LUOER2     DEFINE  0xF06E0
LUOR01     DEFINE  0xF06E1
LUOR11     DEFINE  0xF06E1
LUOR21     DEFINE  0xF06E1
LUTDR0     DEFINE  0xF06E4
LUTDR1     DEFINE  0xF06E4
LUTDR2     DEFINE  0xF06E4
LUTDR0L    DEFINE  0xF06E4
LUTDR0H    DEFINE  0xF06E5
LUTDR2L    DEFINE  0xF06E4
LUTDR1H    DEFINE  0xF06E5
LUTDR2H    DEFINE  0xF06E5
LURDR0     DEFINE  0xF06E6
LURDR1     DEFINE  0xF06E6
LURDR2     DEFINE  0xF06E6
LURDR0L    DEFINE  0xF06E6
LURDR0H    DEFINE  0xF06E7
LURDR2L    DEFINE  0xF06E6
LURDR1H    DEFINE  0xF06E7
LURDR2H    DEFINE  0xF06E7
LUWTDR0    DEFINE  0xF06E8
LUWTDR1    DEFINE  0xF06E8
LUWTDR2    DEFINE  0xF06E8
LUWTDR0L   DEFINE  0xF06E8
LUWTDR0H   DEFINE  0xF06E9
LUWTDR2L   DEFINE  0xF06E8
LUWTDR1H   DEFINE  0xF06E9
LUWTDR2H   DEFINE  0xF06E9
TRJ0       DEFINE  0xF06F0
ELSELR00   DEFINE  0xF0780
ELSELR01   DEFINE  0xF0781
ELSELR02   DEFINE  0xF0782
ELSELR03   DEFINE  0xF0783
ELSELR04   DEFINE  0xF0784
ELSELR05   DEFINE  0xF0785
ELSELR06   DEFINE  0xF0786
ELSELR07   DEFINE  0xF0787
ELSELR08   DEFINE  0xF0788
ELSELR09   DEFINE  0xF0789
ELSELR10   DEFINE  0xF078A
ELSELR11   DEFINE  0xF078B
ELSELR12   DEFINE  0xF078C
ELSELR13   DEFINE  0xF078D
ELSELR14   DEFINE  0xF078E
ELSELR15   DEFINE  0xF078F
ELSELR16   DEFINE  0xF0790
ELSELR17   DEFINE  0xF0791
ELSELR18   DEFINE  0xF0792
ELSELR19   DEFINE  0xF0793

#endif /* __IAR_SYSTEMS_ASM__ */

#endif /* __IOR5F10PLF_EXT_H__ */
