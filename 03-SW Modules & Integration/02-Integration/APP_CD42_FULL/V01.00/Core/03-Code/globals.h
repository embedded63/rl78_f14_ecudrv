/*******************************************************************************
** Copyright (c) 2012 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -----------------------------------------------------------------------------
** File Name : globals.h
** -----------------------------------------------------------------------------
**
** Documentation reference : 
**
********************************************************************************
** R E V I S I O N H I S T O R Y
********************************************************************************
** V01.00 05/08/2013
** - Baseline Created with RTSCU

*******************************************************************************/

#ifndef GLOBALS_H
#define GLOBALS_H

#include "Std_Types.h"


#define ACTUATOR_TEST    1u
#define PARAMETER_CONFIG 1u
#define CLA_RECALL       1u

#define CONVERT_SEC_TO_20MS_COUNT(x)  ((x/2u)*100u)


/* MISRA Rule violation 1.1 & 6.4: Bit-fields are declared unsigned short
instead of unsigned int.*/
extern volatile __saddr struct {
  VAR(uint16, GLOBAL_VAR) extP0_flag:1;
  VAR(uint16, RTSCU_VAR)  TM04_flag:1;
  VAR(uint16, GLOBAL_VAR) WDTI_flag:1;
  VAR(uint16, RAINT_VAR)  INTAD_flag:1;
  VAR(uint16, SPIIT_VAR)  INTCSIS0_flag:1;
  /* flags for future usage */
  VAR(uint16, GLOBAL_VAR) LIN_RX_FLAG:1;
  VAR(uint16, GLOBAL_VAR) flag06:1;
  VAR(uint16, GLOBAL_VAR) flag07:1;
  VAR(uint16, GLOBAL_VAR) flag08:1;
  VAR(uint16, GLOBAL_VAR) flag09:1;
  VAR(uint16, GLOBAL_VAR) flag10:1;
  VAR(uint16, GLOBAL_VAR) flag11:1;
  VAR(uint16, GLOBAL_VAR) flag12:1;
  VAR(uint16, GLOBAL_VAR) flag13:1;
  VAR(uint16, GLOBAL_VAR) flag14:1;
  VAR(uint16, GLOBAL_VAR) flag15:1;
};

#endif /* GLOBALS_H */

