/*-------------------------------------------------------------------------
 *      Declarations of extended SFR registers for RL78 microcontroller R5F109GE.
 *
 *      This file can be used by both the RL78,
 *      Assembler, ARL78, and the C/C++ compiler, ICCRL78.
 *
 *      This header file is generated from the device file:
 *          DR5F109GE.DVF
 *          Copyright(C) 2011 Renesas
 *          Format version V3.00, File version V1.00
 *-------------------------------------------------------------------------*/

#ifndef __IOR5F109GE_EXT_H__
#define __IOR5F109GE_EXT_H__

#if !defined(__ARL78__) && !defined(__ICCRL78__)
  #error "IOR5F109GE_EXT.H file for use with RL78 Assembler or Compiler only"
#endif

#if defined(__ARL78__)
  #if __CORE__ != __RL78_1__
    #error "IOR5F109GE_EXT.H file for use with ARL78 option --core rl78_1 only"
  #endif
#endif
#if defined(__ICCRL78__)
  #if __CORE__ != __RL78_1__
    #error "IOR5F109GE_EXT.H file for use with ICCRL78 option --core rl78_1 only"
  #endif
#endif

#ifdef __IAR_SYSTEMS_ICC__

#pragma system_include

#pragma language=save
#pragma language=extended

/*----------------------------------------------
 * SFR bits (default names)
 *--------------------------------------------*/

#ifndef __RL78_BIT_STRUCTURE__
  #define __RL78_BIT_STRUCTURE__
  typedef struct
  {
    unsigned char no0:1;
    unsigned char no1:1;
    unsigned char no2:1;
    unsigned char no3:1;
    unsigned char no4:1;
    unsigned char no5:1;
    unsigned char no6:1;
    unsigned char no7:1;
  } __BITS8;
#endif

#ifndef __RL78_BIT_STRUCTURE2__
  #define __RL78_BIT_STRUCTURE2__
  typedef struct
  {
    unsigned char no0:1;
    unsigned char no1:1;
    unsigned char no2:1;
    unsigned char no3:1;
    unsigned char no4:1;
    unsigned char no5:1;
    unsigned char no6:1;
    unsigned char no7:1;
    unsigned char no8:1;
    unsigned char no9:1;
    unsigned char no10:1;
    unsigned char no11:1;
    unsigned char no12:1;
    unsigned char no13:1;
    unsigned char no14:1;
    unsigned char no15:1;
  } __BITS16;
#endif

/*----------------------------------------------
 *       Extended SFR declarations
 *--------------------------------------------*/

__near __no_init volatile union { unsigned char ADM2; __BITS8 ADM2_bit; } @ 0xF0010;
__near __no_bit_access __no_init volatile union { unsigned char ADUL; __BITS8 ADUL_bit; } @ 0xF0011;
__near __no_bit_access __no_init volatile union { unsigned char ADLL; __BITS8 ADLL_bit; } @ 0xF0012;
__near __no_bit_access __no_init volatile union { unsigned char ADTES; __BITS8 ADTES_bit; } @ 0xF0013;
__near __no_init volatile union { unsigned char PU0; __BITS8 PU0_bit; } @ 0xF0030;
__near __no_init volatile union { unsigned char PU1; __BITS8 PU1_bit; } @ 0xF0031;
__near __no_init volatile union { unsigned char PU3; __BITS8 PU3_bit; } @ 0xF0033;
__near __no_init volatile union { unsigned char PU4; __BITS8 PU4_bit; } @ 0xF0034;
__near __no_init volatile union { unsigned char PU5; __BITS8 PU5_bit; } @ 0xF0035;
__near __no_init volatile union { unsigned char PU6; __BITS8 PU6_bit; } @ 0xF0036;
__near __no_init volatile union { unsigned char PU7; __BITS8 PU7_bit; } @ 0xF0037;
__near __no_init volatile union { unsigned char PU12; __BITS8 PU12_bit; } @ 0xF003C;
__near __no_init volatile union { unsigned char PU14; __BITS8 PU14_bit; } @ 0xF003E;
__near __no_init volatile union { unsigned char PIM0; __BITS8 PIM0_bit; } @ 0xF0040;
__near __no_init volatile union { unsigned char PIM1; __BITS8 PIM1_bit; } @ 0xF0041;
__near __no_init volatile union { unsigned char POM0; __BITS8 POM0_bit; } @ 0xF0050;
__near __no_init volatile union { unsigned char POM1; __BITS8 POM1_bit; } @ 0xF0051;
__near __no_init volatile union { unsigned char POM5; __BITS8 POM5_bit; } @ 0xF0055;
__near __no_init volatile union { unsigned char POM7; __BITS8 POM7_bit; } @ 0xF0057;
__near __no_init volatile union { unsigned char PMC0; __BITS8 PMC0_bit; } @ 0xF006A;
__near __no_init volatile union { unsigned char PMC14; __BITS8 PMC14_bit; } @ 0xF006E;
__near __no_init volatile union { unsigned char PMC12; __BITS8 PMC12_bit; } @ 0xF006C;
__near __no_init volatile union { unsigned char PMC14; __BITS8 PMC14_bit; } @ 0xF006E;
__near __no_init volatile union { unsigned char NFEN0; __BITS8 NFEN0_bit; } @ 0xF0070;
__near __no_init volatile union { unsigned char NFEN1; __BITS8 NFEN1_bit; } @ 0xF0071;
__near __no_init volatile union { unsigned char ISC; __BITS8 ISC_bit; } @ 0xF0073;
__near __no_bit_access __no_init volatile union { unsigned char TIS0; __BITS8 TIS0_bit; } @ 0xF0074;
__near __no_bit_access __no_init volatile union { unsigned char ADPC; __BITS8 ADPC_bit; } @ 0xF0076;
__near __no_bit_access __no_init volatile union { unsigned char PIOR; __BITS8 PIOR_bit; } @ 0xF0077;
__near __no_bit_access __no_init volatile union { unsigned char IAWCTL; __BITS8 IAWCTL_bit; } @ 0xF0078;
__near __no_init volatile union { unsigned char DFLCTL; __BITS8 DFLCTL_bit; } @ 0xF0090;
__near __no_bit_access __no_init volatile union { unsigned char HIOTRM; __BITS8 HIOTRM_bit; } @ 0xF00A0;
__near __no_bit_access __no_init volatile union { unsigned char HOCODIV; __BITS8 HOCODIV_bit; } @ 0xF00A8;
__near __no_bit_access __no_init volatile const union { unsigned char TEMPCAL0; __BITS8 TEMPCAL0_bit; } @ 0xF00AC;
__near __no_bit_access __no_init volatile const union { unsigned char TEMPCAL1; __BITS8 TEMPCAL1_bit; } @ 0xF00AD;
__near __no_bit_access __no_init volatile const union { unsigned char TEMPCAL2; __BITS8 TEMPCAL2_bit; } @ 0xF00AE;
__near __no_bit_access __no_init volatile const union { unsigned char TEMPCAL3; __BITS8 TEMPCAL3_bit; } @ 0xF00AF;
__near __no_bit_access __no_init volatile union { unsigned short MDCL; __BITS16 MDCL_bit; } @ 0xF00E0;
__near __no_bit_access __no_init volatile union { unsigned short MDCH; __BITS16 MDCH_bit; } @ 0xF00E2;
__near __no_init volatile union { unsigned char MDUC; __BITS8 MDUC_bit; } @ 0xF00E8;
__near __no_init volatile union { unsigned char PER0; __BITS8 PER0_bit; } @ 0xF00F0;
__near __no_bit_access __no_init volatile union { unsigned char OSMC; __BITS8 OSMC_bit; } @ 0xF00F3;
__near __no_init volatile union { unsigned char RPECTL; __BITS8 RPECTL_bit; } @ 0xF00F5;
__near __no_bit_access __no_init volatile const union { unsigned char BCDADJ; __BITS8 BCDADJ_bit; } @ 0xF00FE;
__near __no_bit_access __no_init volatile const union { unsigned short SSR00; __BITS16 SSR00_bit; struct { union { unsigned char SSR00L; __BITS8 SSR00L_bit; }; }; } @ 0xF0100;
__near __no_bit_access __no_init volatile const union { unsigned short SSR01; __BITS16 SSR01_bit; struct { union { unsigned char SSR01L; __BITS8 SSR01L_bit; }; }; } @ 0xF0102;
__near __no_bit_access __no_init volatile const union { unsigned short SSR02; __BITS16 SSR02_bit; struct { union { unsigned char SSR02L; __BITS8 SSR02L_bit; }; }; } @ 0xF0104;
__near __no_bit_access __no_init volatile const union { unsigned short SSR03; __BITS16 SSR03_bit; struct { union { unsigned char SSR03L; __BITS8 SSR03L_bit; }; }; } @ 0xF0106;
__near __no_bit_access __no_init volatile union { unsigned short SIR00; __BITS16 SIR00_bit; struct { union { unsigned char SIR00L; __BITS8 SIR00L_bit; }; }; } @ 0xF0108;
__near __no_bit_access __no_init volatile union { unsigned short SIR01; __BITS16 SIR01_bit; struct { union { unsigned char SIR01L; __BITS8 SIR01L_bit; }; }; } @ 0xF010A;
__near __no_bit_access __no_init volatile union { unsigned short SIR02; __BITS16 SIR02_bit; struct { union { unsigned char SIR02L; __BITS8 SIR02L_bit; }; }; } @ 0xF010C;
__near __no_bit_access __no_init volatile union { unsigned short SIR03; __BITS16 SIR03_bit; struct { union { unsigned char SIR03L; __BITS8 SIR03L_bit; }; }; } @ 0xF010E;
__near __no_bit_access __no_init volatile union { unsigned short SMR00; __BITS16 SMR00_bit; } @ 0xF0110;
__near __no_bit_access __no_init volatile union { unsigned short SMR01; __BITS16 SMR01_bit; } @ 0xF0112;
__near __no_bit_access __no_init volatile union { unsigned short SMR02; __BITS16 SMR02_bit; } @ 0xF0114;
__near __no_bit_access __no_init volatile union { unsigned short SMR03; __BITS16 SMR03_bit; } @ 0xF0116;
__near __no_bit_access __no_init volatile union { unsigned short SCR00; __BITS16 SCR00_bit; } @ 0xF0118;
__near __no_bit_access __no_init volatile union { unsigned short SCR01; __BITS16 SCR01_bit; } @ 0xF011A;
__near __no_bit_access __no_init volatile union { unsigned short SCR02; __BITS16 SCR02_bit; } @ 0xF011C;
__near __no_bit_access __no_init volatile union { unsigned short SCR03; __BITS16 SCR03_bit; } @ 0xF011E;
__near __no_init volatile const union { unsigned short SE0; struct { union { unsigned char SE0L; __BITS8 SE0L_bit; }; }; } @ 0xF0120;
__near __no_init volatile union { unsigned short SS0; struct { union { unsigned char SS0L; __BITS8 SS0L_bit; }; }; } @ 0xF0122;
__near __no_init volatile union { unsigned short ST0; struct { union { unsigned char ST0L; __BITS8 ST0L_bit; }; }; } @ 0xF0124;
__near __no_bit_access __no_init volatile union { unsigned short SPS0; __BITS16 SPS0_bit; struct { union { unsigned char SPS0L; __BITS8 SPS0L_bit; }; }; } @ 0xF0126;
__near __no_bit_access __no_init volatile union { unsigned short SO0; __BITS16 SO0_bit; } @ 0xF0128;
__near __no_init volatile union { unsigned short SOE0; struct { union { unsigned char SOE0L; __BITS8 SOE0L_bit; }; }; } @ 0xF012A;
__near __no_bit_access __no_init volatile union { unsigned short SOL0; __BITS16 SOL0_bit; struct { union { unsigned char SOL0L; __BITS8 SOL0L_bit; }; }; } @ 0xF0134;
__near __no_bit_access __no_init volatile union { unsigned short SSC0; __BITS16 SSC0_bit; struct { union { unsigned char SSC0L; __BITS8 SSC0L_bit; }; }; } @ 0xF0138;
__near __no_bit_access __no_init volatile const union { unsigned short SSR10; __BITS16 SSR10_bit; struct { union { unsigned char SSR10L; __BITS8 SSR10L_bit; }; }; } @ 0xF0140;
__near __no_bit_access __no_init volatile const union { unsigned short SSR11; __BITS16 SSR11_bit; struct { union { unsigned char SSR11L; __BITS8 SSR11L_bit; }; }; } @ 0xF0142;
__near __no_bit_access __no_init volatile union { unsigned short SIR10; __BITS16 SIR10_bit; struct { union { unsigned char SIR10L; __BITS8 SIR10L_bit; }; }; } @ 0xF0148;
__near __no_bit_access __no_init volatile union { unsigned short SIR11; __BITS16 SIR11_bit; struct { union { unsigned char SIR11L; __BITS8 SIR11L_bit; }; }; } @ 0xF014A;
__near __no_bit_access __no_init volatile union { unsigned short SMR10; __BITS16 SMR10_bit; } @ 0xF0150;
__near __no_bit_access __no_init volatile union { unsigned short SMR11; __BITS16 SMR11_bit; } @ 0xF0152;
__near __no_bit_access __no_init volatile union { unsigned short SCR10; __BITS16 SCR10_bit; } @ 0xF0158;
__near __no_bit_access __no_init volatile union { unsigned short SCR11; __BITS16 SCR11_bit; } @ 0xF015A;
__near __no_init volatile const union { unsigned short SE1; struct { union { unsigned char SE1L; __BITS8 SE1L_bit; }; }; } @ 0xF0160;
__near __no_init volatile union { unsigned short SS1; struct { union { unsigned char SS1L; __BITS8 SS1L_bit; }; }; } @ 0xF0162;
__near __no_init volatile union { unsigned short ST1; struct { union { unsigned char ST1L; __BITS8 ST1L_bit; }; }; } @ 0xF0164;
__near __no_bit_access __no_init volatile union { unsigned short SPS1; __BITS16 SPS1_bit; struct { union { unsigned char SPS1L; __BITS8 SPS1L_bit; }; }; } @ 0xF0166;
__near __no_bit_access __no_init volatile union { unsigned short SO1; __BITS16 SO1_bit; } @ 0xF0168;
__near __no_init volatile union { unsigned short SOE1; struct { union { unsigned char SOE1L; __BITS8 SOE1L_bit; }; }; } @ 0xF016A;
__near __no_bit_access __no_init volatile union { unsigned short SOL1; __BITS16 SOL1_bit; struct { union { unsigned char SOL1L; __BITS8 SOL1L_bit; }; }; } @ 0xF0174;
__near __no_bit_access __no_init volatile const union { unsigned short TCR00; __BITS16 TCR00_bit; } @ 0xF0180;
__near __no_bit_access __no_init volatile const union { unsigned short TCR01; __BITS16 TCR01_bit; } @ 0xF0182;
__near __no_bit_access __no_init volatile const union { unsigned short TCR02; __BITS16 TCR02_bit; } @ 0xF0184;
__near __no_bit_access __no_init volatile const union { unsigned short TCR03; __BITS16 TCR03_bit; } @ 0xF0186;
__near __no_bit_access __no_init volatile const union { unsigned short TCR04; __BITS16 TCR04_bit; } @ 0xF0188;
__near __no_bit_access __no_init volatile const union { unsigned short TCR05; __BITS16 TCR05_bit; } @ 0xF018A;
__near __no_bit_access __no_init volatile const union { unsigned short TCR06; __BITS16 TCR06_bit; } @ 0xF018C;
__near __no_bit_access __no_init volatile const union { unsigned short TCR07; __BITS16 TCR07_bit; } @ 0xF018E;
__near __no_bit_access __no_init volatile union { unsigned short TMR00; __BITS16 TMR00_bit; } @ 0xF0190;
__near __no_bit_access __no_init volatile union { unsigned short TMR01; __BITS16 TMR01_bit; } @ 0xF0192;
__near __no_bit_access __no_init volatile union { unsigned short TMR02; __BITS16 TMR02_bit; } @ 0xF0194;
__near __no_bit_access __no_init volatile union { unsigned short TMR03; __BITS16 TMR03_bit; } @ 0xF0196;
__near __no_bit_access __no_init volatile union { unsigned short TMR04; __BITS16 TMR04_bit; } @ 0xF0198;
__near __no_bit_access __no_init volatile union { unsigned short TMR05; __BITS16 TMR05_bit; } @ 0xF019A;
__near __no_bit_access __no_init volatile union { unsigned short TMR06; __BITS16 TMR06_bit; } @ 0xF019C;
__near __no_bit_access __no_init volatile union { unsigned short TMR07; __BITS16 TMR07_bit; } @ 0xF019E;
__near __no_bit_access __no_init volatile const union { unsigned short TSR00; __BITS16 TSR00_bit; struct { union { unsigned char TSR00L; __BITS8 TSR00L_bit; }; }; } @ 0xF01A0;
__near __no_bit_access __no_init volatile const union { unsigned short TSR01; __BITS16 TSR01_bit; struct { union { unsigned char TSR01L; __BITS8 TSR01L_bit; }; }; } @ 0xF01A2;
__near __no_bit_access __no_init volatile const union { unsigned short TSR02; __BITS16 TSR02_bit; struct { union { unsigned char TSR02L; __BITS8 TSR02L_bit; }; }; } @ 0xF01A4;
__near __no_bit_access __no_init volatile const union { unsigned short TSR03; __BITS16 TSR03_bit; struct { union { unsigned char TSR03L; __BITS8 TSR03L_bit; }; }; } @ 0xF01A6;
__near __no_bit_access __no_init volatile const union { unsigned short TSR04; __BITS16 TSR04_bit; struct { union { unsigned char TSR04L; __BITS8 TSR04L_bit; }; }; } @ 0xF01A8;
__near __no_bit_access __no_init volatile const union { unsigned short TSR05; __BITS16 TSR05_bit; struct { union { unsigned char TSR05L; __BITS8 TSR05L_bit; }; }; } @ 0xF01AA;
__near __no_bit_access __no_init volatile const union { unsigned short TSR06; __BITS16 TSR06_bit; struct { union { unsigned char TSR06L; __BITS8 TSR06L_bit; }; }; } @ 0xF01AC;
__near __no_bit_access __no_init volatile const union { unsigned short TSR07; __BITS16 TSR07_bit; struct { union { unsigned char TSR07L; __BITS8 TSR07L_bit; }; }; } @ 0xF01AE;
__near __no_init volatile const union { unsigned short TE0; struct { union { unsigned char TE0L; __BITS8 TE0L_bit; }; }; } @ 0xF01B0;
__near __no_init volatile union { unsigned short TS0; struct { union { unsigned char TS0L; __BITS8 TS0L_bit; }; }; } @ 0xF01B2;
__near __no_init volatile union { unsigned short TT0; struct { union { unsigned char TT0L; __BITS8 TT0L_bit; }; }; } @ 0xF01B4;
__near __no_bit_access __no_init volatile union { unsigned short TPS0; __BITS16 TPS0_bit; } @ 0xF01B6;
__near __no_bit_access __no_init volatile union { unsigned short TO0; __BITS16 TO0_bit; struct { union { unsigned char TO0L; __BITS8 TO0L_bit; }; }; } @ 0xF01B8;
__near __no_init volatile union { unsigned short TOE0; struct { union { unsigned char TOE0L; __BITS8 TOE0L_bit; }; }; } @ 0xF01BA;
__near __no_bit_access __no_init volatile union { unsigned short TOL0; __BITS16 TOL0_bit; struct { union { unsigned char TOL0L; __BITS8 TOL0L_bit; }; }; } @ 0xF01BC;
__near __no_bit_access __no_init volatile union { unsigned short TOM0; __BITS16 TOM0_bit; struct { union { unsigned char TOM0L; __BITS8 TOM0L_bit; }; }; } @ 0xF01BE;
__near __no_init volatile union { unsigned char IICCTL00; __BITS8 IICCTL00_bit; } @ 0xF0230;
__near __no_init volatile union { unsigned char IICCTL01; __BITS8 IICCTL01_bit; } @ 0xF0231;
__near __no_bit_access __no_init volatile union { unsigned char IICWL0; __BITS8 IICWL0_bit; } @ 0xF0232;
__near __no_bit_access __no_init volatile union { unsigned char IICWH0; __BITS8 IICWH0_bit; } @ 0xF0233;
__near __no_bit_access __no_init volatile union { unsigned char SVA0; __BITS8 SVA0_bit; } @ 0xF0234;
__near __no_init volatile union { unsigned char CRC0CTL; __BITS8 CRC0CTL_bit; } @ 0xF02F0;
__near __no_bit_access __no_init volatile union { unsigned short PGCRCL; __BITS16 PGCRCL_bit; } @ 0xF02F2;
__near __no_bit_access __no_init volatile union { unsigned short CRCD; __BITS16 CRCD_bit; } @ 0xF02FA;
__near __no_init volatile union { unsigned char PERX; __BITS8 PERX_bit; } @ 0xF0500;
__near __no_init volatile union { unsigned char PCKSEL; __BITS8 PCKSEL_bit; } @ 0xF0501;
__near __no_init volatile union { unsigned char PMX0; __BITS8 PMX0_bit; } @ 0xF0504;
__near __no_init volatile union { unsigned char PMX1; __BITS8 PMX1_bit; } @ 0xF0505;
__near __no_init volatile union { unsigned char PMX2; __BITS8 PMX2_bit; } @ 0xF0506;
__near __no_init volatile union { unsigned char NFENX; __BITS8 NFENX_bit; } @ 0xF050A;
__near __no_init volatile union { unsigned char UF0CTL0; __BITS8 UF0CTL0_bit; } @ 0xF0520;
__near __no_init volatile union { unsigned char UF0OPT0; __BITS8 UF0OPT0_bit; } @ 0xF0521;
__near __no_bit_access __no_init volatile union { unsigned short UF0CTL1; __BITS16 UF0CTL1_bit; } @ 0xF0522;
__near __no_init volatile union { unsigned char UF0OPT1; __BITS8 UF0OPT1_bit; } @ 0xF0524;
__near __no_init volatile union { unsigned char UF0OPT2; __BITS8 UF0OPT2_bit; } @ 0xF0525;
__near __no_bit_access __no_init volatile const union { unsigned short UF0STR; __BITS16 UF0STR_bit; } @ 0xF0526;
__near __no_bit_access __no_init volatile union { unsigned short UF0STC; __BITS16 UF0STC_bit; } @ 0xF0528;
__near __no_bit_access __no_init volatile /* write only */ union { unsigned short UF0WTX; __BITS16 UF0WTX_bit; struct { union { unsigned char UF0WTXB; __BITS8 UF0WTXB_bit; }; }; } @ 0xF052A;
__near __no_bit_access __no_init volatile union { unsigned char UF0ID; __BITS8 UF0ID_bit; } @ 0xF052E;
__near __no_bit_access __no_init volatile union { unsigned char UF0BUF0; __BITS8 UF0BUF0_bit; } @ 0xF052F;
__near __no_bit_access __no_init volatile union { unsigned char UF0BUF1; __BITS8 UF0BUF1_bit; } @ 0xF0530;
__near __no_bit_access __no_init volatile union { unsigned char UF0BUF2; __BITS8 UF0BUF2_bit; } @ 0xF0531;
__near __no_bit_access __no_init volatile union { unsigned char UF0BUF3; __BITS8 UF0BUF3_bit; } @ 0xF0532;
__near __no_bit_access __no_init volatile union { unsigned char UF0BUF4; __BITS8 UF0BUF4_bit; } @ 0xF0533;
__near __no_bit_access __no_init volatile union { unsigned char UF0BUF5; __BITS8 UF0BUF5_bit; } @ 0xF0534;
__near __no_bit_access __no_init volatile union { unsigned char UF0BUF6; __BITS8 UF0BUF6_bit; } @ 0xF0535;
__near __no_bit_access __no_init volatile union { unsigned char UF0BUF7; __BITS8 UF0BUF7_bit; } @ 0xF0536;
__near __no_bit_access __no_init volatile union { unsigned char UF0BUF8; __BITS8 UF0BUF8_bit; } @ 0xF0537;
__near __no_bit_access __no_init volatile union { unsigned short UF0BUCTL; __BITS16 UF0BUCTL_bit; } @ 0xF0538;
__near __no_bit_access __no_init volatile union { unsigned short SDRS0; __BITS16 SDRS0_bit; struct { union { unsigned char SDRS0L; __BITS8 SDRS0L_bit; }; }; } @ 0xF0540;
__near __no_bit_access __no_init volatile union { unsigned short SDRS1; __BITS16 SDRS1_bit; struct { union { unsigned char SDRS1L; __BITS8 SDRS1L_bit; }; }; } @ 0xF0542;
__near __no_bit_access __no_init volatile union { unsigned short UF0TX; __BITS16 UF0TX_bit; struct { union { unsigned char UF0TXB; __BITS8 UF0TXB_bit; }; }; } @ 0xF0548;
__near __no_bit_access __no_init volatile union { unsigned short UF0RX; __BITS16 UF0RX_bit; struct { union { unsigned char UF0RXB; __BITS8 UF0RXB_bit; }; }; } @ 0xF054A;
__near __no_bit_access __no_init volatile const union { unsigned short SSRS0; __BITS16 SSRS0_bit; struct { union { unsigned char SSRS0L; __BITS8 SSRS0L_bit; }; }; } @ 0xF0550;
__near __no_bit_access __no_init volatile const union { unsigned short SSRS1; __BITS16 SSRS1_bit; struct { union { unsigned char SSRS1L; __BITS8 SSRS1L_bit; }; }; } @ 0xF0552;
__near __no_bit_access __no_init volatile union { unsigned short SIRS0; __BITS16 SIRS0_bit; struct { union { unsigned char SIRS0L; __BITS8 SIRS0L_bit; }; }; } @ 0xF0554;
__near __no_bit_access __no_init volatile union { unsigned short SIRS1; __BITS16 SIRS1_bit; struct { union { unsigned char SIRS1L; __BITS8 SIRS1L_bit; }; }; } @ 0xF0556;
__near __no_bit_access __no_init volatile union { unsigned short SMRS0; __BITS16 SMRS0_bit; } @ 0xF0558;
__near __no_bit_access __no_init volatile union { unsigned short SMRS1; __BITS16 SMRS1_bit; } @ 0xF055A;
__near __no_bit_access __no_init volatile union { unsigned short SCRS0; __BITS16 SCRS0_bit; } @ 0xF055C;
__near __no_bit_access __no_init volatile union { unsigned short SCRS1; __BITS16 SCRS1_bit; } @ 0xF055E;
__near __no_init volatile const union { unsigned short SES; struct { union { unsigned char SESL; __BITS8 SESL_bit; }; }; } @ 0xF0560;
__near __no_init volatile union { unsigned short SSS; struct { union { unsigned char SSSL; __BITS8 SSSL_bit; }; }; } @ 0xF0562;
__near __no_init volatile union { unsigned short STS; struct { union { unsigned char STSL; __BITS8 STSL_bit; }; }; } @ 0xF0564;
__near __no_bit_access __no_init volatile union { unsigned short SPSS; __BITS16 SPSS_bit; struct { union { unsigned char SPSSL; __BITS8 SPSSL_bit; }; }; } @ 0xF0566;
__near __no_bit_access __no_init volatile union { unsigned short SOS; __BITS16 SOS_bit; } @ 0xF0568;
__near __no_init volatile union { unsigned short SOES; struct { union { unsigned char SOESL; __BITS8 SOESL_bit; }; }; } @ 0xF056A;
__near __no_bit_access __no_init volatile union { unsigned short SOLS; __BITS16 SOLS_bit; struct { union { unsigned char SOLSL; __BITS8 SOLSL_bit; }; }; } @ 0xF0570;
__near __no_init volatile union { unsigned char WUTMCTL; __BITS8 WUTMCTL_bit; } @ 0xF0580;
__near __no_bit_access __no_init volatile union { unsigned short WUTMCMP; __BITS16 WUTMCMP_bit; } @ 0xF0582;

/*----------------------------------------------
 *       Extended SFR bit declarations
 *--------------------------------------------*/

#define ADTYP             ADM2_bit.no0
#define AWC               ADM2_bit.no2
#define ADRCK             ADM2_bit.no3

#define DFLEN             DFLCTL_bit.no0

#define DIVST             MDUC_bit.no0
#define MACSF             MDUC_bit.no1
#define MACOF             MDUC_bit.no2
#define MDSM              MDUC_bit.no3
#define MACMODE           MDUC_bit.no6
#define DIVMODE           MDUC_bit.no7

#define TAU0EN            PER0_bit.no0
#define SAU0EN            PER0_bit.no2
#define SAU1EN            PER0_bit.no3
#define IICA0EN           PER0_bit.no4
#define ADCEN             PER0_bit.no5
#define RTCEN             PER0_bit.no7

#define RPEF              RPECTL_bit.no0
#define RPERDIS           RPECTL_bit.no7

#define SPT0              IICCTL00_bit.no0
#define STT0              IICCTL00_bit.no1
#define ACKE0             IICCTL00_bit.no2
#define WTIM0             IICCTL00_bit.no3
#define SPIE0             IICCTL00_bit.no4
#define WREL0             IICCTL00_bit.no5
#define LREL0             IICCTL00_bit.no6
#define IICE0             IICCTL00_bit.no7

#define PRS0              IICCTL01_bit.no0
#define DFC0              IICCTL01_bit.no2
#define SMC0              IICCTL01_bit.no3
#define DAD0              IICCTL01_bit.no4
#define CLD0              IICCTL01_bit.no5
#define WUP0              IICCTL01_bit.no7

#define CRC0EN            CRC0CTL_bit.no7

#define WUTEN             PERX_bit.no0
#define SAUSEN            PERX_bit.no1
#define UF0EN             PERX_bit.no2

#define WUTMCKE           PCKSEL_bit.no2

#define UF0RXE            UF0CTL0_bit.no5
#define UF0TXE            UF0CTL0_bit.no6

#define UF0BTT            UF0OPT0_bit.no5
#define UF0BRT            UF0OPT0_bit.no6
#define UF0BRF            UF0OPT0_bit.no7

#define UF0EBC            UF0OPT1_bit.no5

#define UF0ITS            UF0OPT2_bit.no0

#define WUTMCE            WUTMCTL_bit.no7

#pragma language=restore

#endif /* __IAR_SYSTEMS_ICC__ */

#ifdef __IAR_SYSTEMS_ASM__

/*----------------------------------------------
 *       Extended SFR declarations
 *--------------------------------------------*/

ADM2       DEFINE  0xF0010
ADUL       DEFINE  0xF0011
ADLL       DEFINE  0xF0012
ADTES      DEFINE  0xF0013
PU0        DEFINE  0xF0030
PU1        DEFINE  0xF0031
PU3        DEFINE  0xF0033
PU4        DEFINE  0xF0034
PU5        DEFINE  0xF0035
PU7        DEFINE  0xF0037
PU12       DEFINE  0xF003C
PU14       DEFINE  0xF003E
PIM0       DEFINE  0xF0040
PIM1       DEFINE  0xF0041
POM0       DEFINE  0xF0050
POM1       DEFINE  0xF0051
POM5       DEFINE  0xF0055
POM7       DEFINE  0xF0057
PMC12      DEFINE  0xF006C
PMC14      DEFINE  0xF006E
NFEN0      DEFINE  0xF0070
NFEN1      DEFINE  0xF0071
ISC        DEFINE  0xF0073
TIS0       DEFINE  0xF0074
ADPC       DEFINE  0xF0076
PIOR       DEFINE  0xF0077
IAWCTL     DEFINE  0xF0078
DFLCTL     DEFINE  0xF0090
HIOTRM     DEFINE  0xF00A0
HOCODIV    DEFINE  0xF00A8
TEMPCAL0   DEFINE  0xF00AC
TEMPCAL1   DEFINE  0xF00AD
TEMPCAL2   DEFINE  0xF00AE
TEMPCAL3   DEFINE  0xF00AF
MDCL       DEFINE  0xF00E0
MDCH       DEFINE  0xF00E2
MDUC       DEFINE  0xF00E8
PER0       DEFINE  0xF00F0
OSMC       DEFINE  0xF00F3
RPECTL     DEFINE  0xF00F5
BCDADJ     DEFINE  0xF00FE
SSR00      DEFINE  0xF0100
SSR00L     DEFINE  0xF0100
SSR01      DEFINE  0xF0102
SSR01L     DEFINE  0xF0102
SSR02      DEFINE  0xF0104
SSR02L     DEFINE  0xF0104
SSR03      DEFINE  0xF0106
SSR03L     DEFINE  0xF0106
SIR00      DEFINE  0xF0108
SIR00L     DEFINE  0xF0108
SIR01      DEFINE  0xF010A
SIR01L     DEFINE  0xF010A
SIR02      DEFINE  0xF010C
SIR02L     DEFINE  0xF010C
SIR03      DEFINE  0xF010E
SIR03L     DEFINE  0xF010E
SMR00      DEFINE  0xF0110
SMR01      DEFINE  0xF0112
SMR02      DEFINE  0xF0114
SMR03      DEFINE  0xF0116
SCR00      DEFINE  0xF0118
SCR01      DEFINE  0xF011A
SCR02      DEFINE  0xF011C
SCR03      DEFINE  0xF011E
SE0        DEFINE  0xF0120
SE0L       DEFINE  0xF0120
SS0        DEFINE  0xF0122
SS0L       DEFINE  0xF0122
ST0        DEFINE  0xF0124
ST0L       DEFINE  0xF0124
SPS0       DEFINE  0xF0126
SPS0L      DEFINE  0xF0126
SO0        DEFINE  0xF0128
SOE0       DEFINE  0xF012A
SOE0L      DEFINE  0xF012A
SOL0       DEFINE  0xF0134
SOL0L      DEFINE  0xF0134
SSC0       DEFINE  0xF0138
SSC0L      DEFINE  0xF0138
SSR10      DEFINE  0xF0140
SSR10L     DEFINE  0xF0140
SSR11      DEFINE  0xF0142
SSR11L     DEFINE  0xF0142
SIR10      DEFINE  0xF0148
SIR10L     DEFINE  0xF0148
SIR11      DEFINE  0xF014A
SIR11L     DEFINE  0xF014A
SMR10      DEFINE  0xF0150
SMR11      DEFINE  0xF0152
SCR10      DEFINE  0xF0158
SCR11      DEFINE  0xF015A
SE1        DEFINE  0xF0160
SE1L       DEFINE  0xF0160
SS1        DEFINE  0xF0162
SS1L       DEFINE  0xF0162
ST1        DEFINE  0xF0164
ST1L       DEFINE  0xF0164
SPS1       DEFINE  0xF0166
SPS1L      DEFINE  0xF0166
SO1        DEFINE  0xF0168
SOE1       DEFINE  0xF016A
SOE1L      DEFINE  0xF016A
SOL1       DEFINE  0xF0174
SOL1L      DEFINE  0xF0174
TCR00      DEFINE  0xF0180
TCR01      DEFINE  0xF0182
TCR02      DEFINE  0xF0184
TCR03      DEFINE  0xF0186
TCR04      DEFINE  0xF0188
TCR05      DEFINE  0xF018A
TCR06      DEFINE  0xF018C
TCR07      DEFINE  0xF018E
TMR00      DEFINE  0xF0190
TMR01      DEFINE  0xF0192
TMR02      DEFINE  0xF0194
TMR03      DEFINE  0xF0196
TMR04      DEFINE  0xF0198
TMR05      DEFINE  0xF019A
TMR06      DEFINE  0xF019C
TMR07      DEFINE  0xF019E
TSR00      DEFINE  0xF01A0
TSR00L     DEFINE  0xF01A0
TSR01      DEFINE  0xF01A2
TSR01L     DEFINE  0xF01A2
TSR02      DEFINE  0xF01A4
TSR02L     DEFINE  0xF01A4
TSR03      DEFINE  0xF01A6
TSR03L     DEFINE  0xF01A6
TSR04      DEFINE  0xF01A8
TSR04L     DEFINE  0xF01A8
TSR05      DEFINE  0xF01AA
TSR05L     DEFINE  0xF01AA
TSR06      DEFINE  0xF01AC
TSR06L     DEFINE  0xF01AC
TSR07      DEFINE  0xF01AE
TSR07L     DEFINE  0xF01AE
TE0        DEFINE  0xF01B0
TE0L       DEFINE  0xF01B0
TS0        DEFINE  0xF01B2
TS0L       DEFINE  0xF01B2
TT0        DEFINE  0xF01B4
TT0L       DEFINE  0xF01B4
TPS0       DEFINE  0xF01B6
TO0        DEFINE  0xF01B8
TO0L       DEFINE  0xF01B8
TOE0       DEFINE  0xF01BA
TOE0L      DEFINE  0xF01BA
TOL0       DEFINE  0xF01BC
TOL0L      DEFINE  0xF01BC
TOM0       DEFINE  0xF01BE
TOM0L      DEFINE  0xF01BE
IICCTL00   DEFINE  0xF0230
IICCTL01   DEFINE  0xF0231
IICWL0     DEFINE  0xF0232
IICWH0     DEFINE  0xF0233
SVA0       DEFINE  0xF0234
CRC0CTL    DEFINE  0xF02F0
PGCRCL     DEFINE  0xF02F2
CRCD       DEFINE  0xF02FA
PERX       DEFINE  0xF0500
PCKSEL     DEFINE  0xF0501
PMX0       DEFINE  0xF0504
PMX1       DEFINE  0xF0505
PMX2       DEFINE  0xF0506
NFENX      DEFINE  0xF050A
UF0CTL0    DEFINE  0xF0520
UF0OPT0    DEFINE  0xF0521
UF0CTL1    DEFINE  0xF0522
UF0OPT1    DEFINE  0xF0524
UF0OPT2    DEFINE  0xF0525
UF0STR     DEFINE  0xF0526
UF0STC     DEFINE  0xF0528
UF0WTX     DEFINE  0xF052A
UF0WTXB    DEFINE  0xF052A
UF0ID      DEFINE  0xF052E
UF0BUF0    DEFINE  0xF052F
UF0BUF1    DEFINE  0xF0530
UF0BUF2    DEFINE  0xF0531
UF0BUF3    DEFINE  0xF0532
UF0BUF4    DEFINE  0xF0533
UF0BUF5    DEFINE  0xF0534
UF0BUF6    DEFINE  0xF0535
UF0BUF7    DEFINE  0xF0536
UF0BUF8    DEFINE  0xF0537
UF0BUCTL   DEFINE  0xF0538
SDRS0      DEFINE  0xF0540
SDRS0L     DEFINE  0xF0540
SDRS1      DEFINE  0xF0542
SDRS1L     DEFINE  0xF0542
UF0TX      DEFINE  0xF0548
UF0TXB     DEFINE  0xF0548
UF0RX      DEFINE  0xF054A
UF0RXB     DEFINE  0xF054A
SSRS0      DEFINE  0xF0550
SSRS0L     DEFINE  0xF0550
SSRS1      DEFINE  0xF0552
SSRS1L     DEFINE  0xF0552
SIRS0      DEFINE  0xF0554
SIRS0L     DEFINE  0xF0554
SIRS1      DEFINE  0xF0556
SIRS1L     DEFINE  0xF0556
SMRS0      DEFINE  0xF0558
SMRS1      DEFINE  0xF055A
SCRS0      DEFINE  0xF055C
SCRS1      DEFINE  0xF055E
SES        DEFINE  0xF0560
SESL       DEFINE  0xF0560
SSS        DEFINE  0xF0562
SSSL       DEFINE  0xF0562
STS        DEFINE  0xF0564
STSL       DEFINE  0xF0564
SPSS       DEFINE  0xF0566
SPSSL      DEFINE  0xF0566
SOS        DEFINE  0xF0568
SOES       DEFINE  0xF056A
SOESL      DEFINE  0xF056A
SOLS       DEFINE  0xF0570
SOLSL      DEFINE  0xF0570
WUTMCTL    DEFINE  0xF0580
WUTMCMP    DEFINE  0xF0582

#endif /* __IAR_SYSTEMS_ASM__ */

#endif /* __IOR5F109GE_EXT_H__ */
