/*******************************************************************************
** Copyright (c) 2012 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -----------------------------------------------------------------------------
** File Name : low_level_initialization.h 
** -----------------------------------------------------------------------------
**
** Description : Header file for low_level_initialization.c
**
** -----------------------------------------------------------------------------
**
** Documentation reference : EME-11ST004-12028 (SW LLD STARU)
**
********************************************************************************
** R E V I S I O N H I S T O R Y
********************************************************************************
** V01.00 05/08/2013
** - Baseline Created
********************************************************************************/
#ifndef __low_level_initialization_H
#define __low_level_initialization_H

#include "Std_Types.h"

/* --------------------------------------------------------------------------*/
/* Option-bytes and security ID                                              */
/* --------------------------------------------------------------------------*/
#define OPT_BYTES_SIZE     4u
#define SECU_ID_SIZE       10u


/* --------------------------------------------------------------------------*/
/* Variable Initialization                                                   */
/* --------------------------------------------------------------------------*/
#define ENABLE_VAR_INIT    TRUE
#define DISABLE_VAR_INIT   FALSE

/* --------------------------------------------------------------------------*/
/* Clock Configuration                                                       */
/* --------------------------------------------------------------------------*/
#define _00_CGC_HISYS_PORT          (0x00U) /* X1, X2 as I/O port */
#define _00_CGC_SUB_PORT            (0x00U) /* XT1, XT2 as I/O port */
#define _00_CGC_SYSOSC_DEFAULT      (0x00U)
#define _00_CGC_SUBMODE_DEFAULT     (0x00U)
#define _10_CGC_RTC_CLK_FIL         (0x10U) /* use fIL clcok */
#define _40_CGC_HISYS_OSC           (0x40U) /* X1, X2 as crystal/ceramic resonator connection */
#define _01_CGC_SYSOSC_OVER10M      (0x01U) /* fX > 10MHz */

/* --------------------------------------------------------------------------*/
/* I/O Port Configuration                                                    */
/* --------------------------------------------------------------------------*/
#define LOW                             0u
#define HIGH                            1u
#define OUTPUT                          0u
#define INPUT                           1u 

#define IRQ_ENABLE                      0u
#define IRQ_DISABLE                     1u

#endif /* __low_level_initialization_H */

