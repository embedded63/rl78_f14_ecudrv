/*******************************************************************************
** Copyright (c) 2012 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -----------------------------------------------------------------------------
** File Name : main.c
** -----------------------------------------------------------------------------
**
** Description : main for C programming language
**
** -----------------------------------------------------------------------------
**
** Documentation reference :
**
********************************************************************************
** R E V I S I O N H I S T O R Y
********************************************************************************
** V01.00 05/08/2013
** - Baseline Created.

********************************************************************************/

#include "main.h"
#include "Pwm_Drv.h"
#include "Adc_Drv.h"
#include "Dio_Hal.h"
#include "Cdd_Drv.h"
#include "Gpt_Drv.h"
#include "SchM.h"
#include "SchM_StkMon.h"
#include "Pwm_Hal.h"
/*============================================================================*/
/* Variables                                                                  */
/*============================================================================*/
VAR(uint8, CDD_VAR) MTRCMD_ResetHes1_bl;
VAR(uint8, CDD_VAR) MTRCMD_ResetHes2_bl;

uint8   SYSMD_KeepAlive_bl;
uint8   MTRCMD_MtrRlyR3R_P_bl;
uint8   MTRCMD_MtrRlyCom_bl;
uint8   MTRCMD_MtrRlyR3L_P_bl;
uint8   MTRCMD_GlobalMtrRly_bl;
uint8   MTRCMD_HesFetEnable_bl;

uint8   PWMCMD_DutyCycle1_u8 =0u;
uint8   PWMCMD_DutyCycle2_u8 =0u;
uint8   PWMCMD_DutyCycle3_u8 =0u;
uint8   PWMCMD_DutyCycle4_u8 =0u;
uint8   PWMCMD_DutyCycle5_u8 = 0u;
uint8   PWMCMD_DutyCycle6_u8 = 0u;
uint8   PWMCMD_DutyCycle7_u8 = 0u;
/* COMMON MEMORY BETWEEN APPLICATION AND BOOT */
__root __no_init VAR(Core_ECUStType, AUTOMATIC) Core_ECUSt @ 0xFF30A;

/*============================================================================*/
/* Prototypes                                                                 */
/*============================================================================*/
#define LED01   P7_bit.no4
#define LED02   P7_bit.no5
#define LEDON   FALSE
#define LEDOFF  (!FALSE)
/*============================================================================*/
/* Functions                                                                  */
/*============================================================================*/
void  main(void)
{
   __enable_interrupt();
   Dio_InitHal();
   Gpt_Init();
   Adc_Init();
   Pwm_Init();
   Pwm_HalInit();
#if defined (FULL_VERSION)
   Cdd_Init();
#endif
   SchM_Init();
   SchM_StkMonInit();
   SchM_Main();
}