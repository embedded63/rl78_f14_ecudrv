/*******************************************************************************
** Copyright (c) 2012 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -----------------------------------------------------------------------------
** File Name : low_level_initialization.c
** -----------------------------------------------------------------------------
**
** Description : Before main initializations
**
** -----------------------------------------------------------------------------
**
** Documentation reference :
**
********************************************************************************
** R E V I S I O N H I S T O R Y
********************************************************************************
** V01.00  27/09/2013
** - First Version
**
********************************************************************************/

/*  Includes                                                                   */
#include "low_level_initialization.h"

/*===========================================================================*/
/* Constants                                                                 */
/*===========================================================================*/
#if defined (FULL_VERSION)
/* High-speed system clock pin operation mode (EXCLK, OSCSEL) */
#define CORE_CGC_HISYS_PORT          (0x00U) /* X1, X2 as I/O port */
/* Subsystem clock pin operation mode (EXCLKS, OSCSELS) */
#define CORE_CGC_SUB_PORT            (0x00U) /* XT1, XT2 as I/O port */
/* XT1 oscillator oscillation mode selection (AMPHS1, AMPHS0) */
#define CORE_CGC_SUBMODE_DEFAULT     (0x00U)
/* Control of X1 high-speed system clock oscillation frequency (AMPH) */
#define CORE_CGC_SYSOSC_DEFAULT      (0x00U)

/* FMP clock selection division register (MDIV) */
#define CORE_CGC_FMP_DIV_1           (0x01U) /* fMP/2^1 */

/* Operation speed mode control register (OSMC) */
#define CORE_CGC_RTC_CLK_OTHER       (0x00U) /* Other than fIL */

/* RTC clock selection register (RTCCL) */
#define CORE_CGC_RTC_FIH             (0x80U) /* RTC uses Internal high speed clock (fIH) */
/* Operation selection of RTC macro (RTCCL6,RTCCKS1 - RTCCKS0)  */
#define CORE_CGC_RTC_DIV122          (0x42U) /* RTC uses high-speed clock / 122 */

#else

#define EXTERNAL_OSCILLATOR_8MHZ 1u

#endif

#define WDGTT_ENABLE_BIT 1u
#define WDGTT_DISABLE_BIT 0u

#define WDGTT_WINDOW_PERIOD 0X02u
#define WDGTT_TIME_UNIT 0x05u  /* 213/fIL (474.90 ms) */

/* Watchdog configuration for MBM Plux */

#define WDGTT_PARAMETERS                                 \
/* intervalIntrrupt:1 */  ((WDGTT_DISABLE_BIT << 7u)|     \
/* windowPeriod:2     */   (WDGTT_WINDOW_PERIOD << 5u)|   \
/* counterOperation:1 */   (WDGTT_ENABLE_BIT << 4u)|      \
/* watchdogTime:3     */   (WDGTT_TIME_UNIT << 1u)|       \
/* haultEnable:1      */   (WDGTT_DISABLE_BIT))

#ifdef __DEBUG_BUILD_C
/* Option Byte Definition                                                    */
/* watchdog enabled, LVD off, 32MHz clock, OCD interface enabled             */
__root __far const uint8 OptionByte[OPT_BYTES_SIZE] @ 0x00C0u = {
  WDGTT_PARAMETERS, 0xFFu, 0xE8u, 0x85u,
};

/* Security Byte Definition                                                  */
__root __far const uint8 SecuIDCode[SECU_ID_SIZE]   @ 0x00C4u = {
  0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u, 0x00u
};
#endif

/*===========================================================================*/
/* Prototypes                                                                */
/*===========================================================================*/
void WDT_Restart(void);
#pragma diag_suppress=Pm011
int __low_level_init (void);
#pragma diag_default=Pm011

/*===========================================================================*/
/* Functions                                                                 */
/*===========================================================================*/
#pragma diag_suppress=Pm011
int __low_level_init (void)
{
#if defined (FULL_VERSION)
    /* Set fMX */
    CMC = CORE_CGC_HISYS_PORT | CORE_CGC_SUB_PORT | CORE_CGC_SYSOSC_DEFAULT | CORE_CGC_SUBMODE_DEFAULT;
    MSTOP = 1U;
    /* Set fMAIN */
    MCM0 = 0U;
    /* Set fMP to clock through mode */
    SELPLL = 0U;
    /* Set fSUB */
    XTSTOP = 1U;
    OSMC = CORE_CGC_RTC_CLK_OTHER;
    /* Set fCLK */
    CSS = 0U;
    MDIV = CORE_CGC_FMP_DIV_1;
    /* Set fIH */
    HIOSTOP = 0U;
    /* Set RTC clock source */
    RTCCL = CORE_CGC_RTC_FIH | CORE_CGC_RTC_DIV122;
    /* Set Timer RD clock source to fCLK, fMP */
    TRD_CKSEL = 0U;
#else

    /* initializes the clock generator */
    /* Set fMX */
    CMC = 0x41u; /*x1 mode fx>10Mhz*/
    CSC = 0x40u; /*start x1 operating */
    while(OSTC != 0xF0u)
    {
    }
    /* Set fMAIN */
    MCM0 = 1U;
    /* Set fCLK */
    CSS = 0U;
    /* end initializes the clock generator */
#endif

    return (int)(1U);
}


void WDT_Restart(void) {
  WDTE = 0xACU;                        /* restart watchdog timer              */
}
