/*******************************************************************************
** Copyright (c) 2012 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -----------------------------------------------------------------------------
** File Name : globals.c
** -----------------------------------------------------------------------------
**
** Description : Flags used in interrup context
**
** -----------------------------------------------------------------------------
**
** Documentation reference : EME-11ST004-12028 (SW LLD STARU)
**
********************************************************************************
** R E V I S I O N H I S T O R Y
********************************************************************************
** V01.00 D.Lahiri 26/04/2012
** - Baseline Created with RTSCU
** -----------------------------------------------------------------------------
** V01.02 D.Lahiri 14/05/2012
** - Added init and wake up functions
** -----------------------------------------------------------------------------
** V02.00 D.Chanda 11/07/2012
** - Code approved for Release
** -----------------------------------------------------------------------------
** V02.01 D.Lahiri 26/07/2012
** - File header format changed
** -----------------------------------------------------------------------------
** V03.00 D.Chanda 27/07/2012
** - Code approved for Release
** -----------------------------------------------------------------------------
** V03.01 K.Uppala 31/08/2012
** - PR0022920 MISRA COMPLIANCE issue fixed.
** -----------------------------------------------------------------------------
** V03.02 K.Uppala 18/10/2012
** - CR0023668 Internal Clock for Timer Array Unit Module is changed to 4Mhz   
** -----------------------------------------------------------------------------
** V03.03 K.Uppala 25/04/2013
** - CR0025269 Updated the STARU module as per the new specification changes.
*******************************************************************************/
#include "Std_Types.h"
/* MISRA Rule violation 1.1 & 6.4: Bit-fields are declared unsigned short
instead of unsigned int.*/
volatile __saddr struct {
  VAR(uint16, GLOBAL_VAR) extP0_flag:1;
  VAR(uint16, RTSCU_VAR)  TM04_flag:1;
  VAR(uint16, GLOBAL_VAR) WDTI_flag:1;
  VAR(uint16, RAINT_VAR)  INTAD_flag:1;
  VAR(uint16, SPIIT_VAR)  INTCSIS0_flag:1;
  /* flags for future usage */
  VAR(uint16, LINHAL_VAR) LIN_RX_FLAG:1;
  VAR(uint16, GLOBAL_VAR) flag06:1;
  VAR(uint16, GLOBAL_VAR) flag07:1;
  VAR(uint16, GLOBAL_VAR) flag08:1;
  VAR(uint16, GLOBAL_VAR) flag09:1;
  VAR(uint16, GLOBAL_VAR) flag10:1;
  VAR(uint16, GLOBAL_VAR) flag11:1;
  VAR(uint16, GLOBAL_VAR) flag12:1;
  VAR(uint16, GLOBAL_VAR) flag13:1;
  VAR(uint16, GLOBAL_VAR) flag14:1;
  VAR(uint16, GLOBAL_VAR) flag15:1;
};

