
;************************************************************************
;
;	Copyright (c) 2011 FAURECIA
;
; This software is the property of Faurecia.
; It can not be used or duplicated without Faurecia authorization.
;
;
;************************************************************************
;************************************************************************
;*************************************************************************
;** R E V I S I O N  H I S T O R Y
;*************************************************************************
;**
;** V01.00 01/08/2013
;** - First release for Branching Interrupts
;**
;*************************************************************************
#ifdef FULL_VERSION
#include <ior5f10plf.h>
#else
#include <ior5f109be.h>
#endif

 EXTERN Default_isr
 
#ifdef FULL_VERSION
 EXTERN Adc_Isr
 EXTERN Cdd_IsrHesR3L
 EXTERN Cdd_IsrHesR3R
 EXTERN Gpt_Isr1ms
 EXTERN Pwm_INTTM00
 EXTERN Pwm_INTTM01
 EXTERN Pwm_INTTM02
 EXTERN Pwm_INTTM03
 EXTERN Pwm_INTTM04
 EXTERN Pwm_INTTM05
 EXTERN Pwm_INTTM06
 EXTERN Pwm_INTTM07
 EXTERN SchM_StkMonIsr
#endif

#ifdef FULL_VERSION
;************************************************************************
;
; include isr's here
;
;************************************************************************
INTWDTI_APPISR     EQU   Default_isr   		;INTWDTI_vect   
INTLVI_APPISR      EQU   Default_isr   		;INTLVI_vect    
INTP0_APPISR       EQU   Cdd_IsrHesR3R 		;INTP0_vect     
INTP1_APPISR       EQU   Default_isr		;INTP1_vect     
INTP2_APPISR       EQU   Default_isr   		;INTP2_vect     
INTP3_APPISR       EQU   Default_isr   		;INTP3_vect     
INTSPM_APPISR      EQU   SchM_StkMonIsr		;INTSPM_vect     
INTP5_APPISR       EQU   Default_isr   		;INTP5_vect     
INTCLM_APPISR      EQU   Default_isr   		;INTCLM_vect    
INTST0_APPISR      EQU   Default_isr   		;INTST0_vect    
INTSR0_APPISR      EQU   Default_isr   		;INTSR0_vect    
INTTRD0_APPISR     EQU   Default_isr   		;INTTRD0_vect   
INTTRD1_APPISR     EQU   Default_isr   		;INTTRD1_vect   
INTTRJ0_APPISR     EQU   Gpt_Isr1ms   		;INTTRJ0_vect   
INTRAM_APPISR      EQU   Default_isr   		;INTRAM_vect    
INTLIN0TRM_APPISR  EQU   Default_isr   		;INTLIN0TRM_vect
INTLIN0RVC_APPISR  EQU   Default_isr   		;INTLIN0RVC_vect
INTLIN0_APPISR     EQU   Default_isr   		;INTLIN0_vect   
INTIICA0_APPISR    EQU   Default_isr   		;INTIICA0_vect  
INTP8_APPISR       EQU   Default_isr   		;INTP8_vect     
INTTM00_APPISR     EQU   Pwm_INTTM00   		;INTTM00_vect   
INTTM01_APPISR     EQU   Pwm_INTTM01   		;INTTM01_vect   
INTTM02_APPISR     EQU   Pwm_INTTM02   		;INTTM02_vect   
INTTM03_APPISR     EQU   Pwm_INTTM03   		;INTTM03_vect   
INTAD_APPISR       EQU   Adc_Isr   		    ;INTAD_vect     
INTP6_APPISR       EQU   Cdd_IsrHesR3L      ;INTP6_vect     
INTP7_APPISR       EQU   Default_isr   		;INTP7_vect     
INTP9_APPISR       EQU   Default_isr   		;INTP9_vect     
INTP10_APPISR      EQU   Default_isr   		;INTP10_vect    
INTST1_APPISR      EQU   Default_isr   		;INTST1_vect    
INTSR1_APPISR      EQU   Default_isr   		;INTSR1_vect    
INTTM04_APPISR     EQU   Pwm_INTTM04   		;INTTM04_vect   
INTTM05_APPISR     EQU   Pwm_INTTM05   		;INTTM05_vect   
INTTM06_APPISR     EQU   Pwm_INTTM06            ;INTTM06_vect   
INTTM07_APPISR     EQU   Pwm_INTTM07   		;INTTM07_vect   
INTLIN0WUP_APPISR  EQU   Default_isr   		;INTLIN0WUP_vect
INTKR_APPISR       EQU   Default_isr   		;INTKR_vect     
INTCAN0ERR_APPISR  EQU   Default_isr   		;INTCAN0ERR_vect
INTCAN0WUP_APPISR  EQU   Default_isr   		;INTCAN0WUP_vect
INTCAN0CFR_APPISR  EQU   Default_isr   		;INTCAN0CFR_vect
INTCAN0TRM_APPISR  EQU   Default_isr   		;INTCAN0TRM_vect
INTCANGRFR_APPISR  EQU   Default_isr   		;INTCANGRFR_vect
INTCANGERR_APPISR  EQU   Default_isr   		;INTCANGERR_vect
INTTM10_APPISR     EQU   Default_isr   		;INTTM10_vect   
INTTM11_APPISR     EQU   Default_isr   		;INTTM11_vect   
INTTM12_APPISR     EQU   Default_isr   		;INTTM12_vect   
INTTM13_APPISR     EQU   Default_isr   		;INTTM13_vect   
INTFL_APPISR       EQU   Default_isr   		;INTFL_vect     
BRK_I_APPISR       EQU   Default_isr   		;BRK_I_vect     

;************************************************************************
;
; Branch Vector Table
;
;************************************************************************

		           RSEG  BRINTVEC
INTWDTI_ISR   		BR		   N:INTWDTI_APPISR   
INTLVI_ISR    		BR		   N:INTLVI_APPISR    
INTP0_ISR     		BR		   N:INTP0_APPISR     
INTP1_ISR     		BR		   N:INTP1_APPISR     
INTP2_ISR     		BR		   N:INTP2_APPISR     
INTP3_ISR     		BR		   N:INTP3_APPISR     
INTSPM_ISR     		BR		   N:INTSPM_APPISR     
INTP5_ISR     		BR		   N:INTP5_APPISR     
INTCLM_ISR    		BR		   N:INTCLM_APPISR    
INTST0_ISR    		BR		   N:INTST0_APPISR    
INTSR0_ISR    		BR		   N:INTSR0_APPISR    
INTTRD0_ISR   		BR		   N:INTTRD0_APPISR   
INTTRD1_ISR   		BR		   N:INTTRD1_APPISR   
INTTRJ0_ISR   		BR		   N:INTTRJ0_APPISR   
INTRAM_ISR    		BR		   N:INTRAM_APPISR    
INTLIN0TRM_ISR		BR		   N:INTLIN0TRM_APPISR
INTLIN0RVC_ISR		BR		   N:INTLIN0RVC_APPISR
INTLIN0_ISR   		BR		   N:INTLIN0_APPISR   
INTIICA0_ISR  		BR		   N:INTIICA0_APPISR  
INTP8_ISR     		BR		   N:INTP8_APPISR     
INTTM00_ISR   		BR		   N:INTTM00_APPISR   
INTTM01_ISR   		BR		   N:INTTM01_APPISR   
INTTM02_ISR   		BR		   N:INTTM02_APPISR   
INTTM03_ISR   		BR		   N:INTTM03_APPISR   
INTAD_ISR     		BR		   N:INTAD_APPISR     
INTP6_ISR     		BR		   N:INTP6_APPISR     
INTP7_ISR     		BR		   N:INTP7_APPISR     
INTP9_ISR     		BR		   N:INTP9_APPISR     
INTP10_ISR    		BR		   N:INTP10_APPISR    
INTST1_ISR    		BR		   N:INTST1_APPISR    
INTSR1_ISR    		BR		   N:INTSR1_APPISR    
INTTM04_ISR   		BR		   N:INTTM04_APPISR   
INTTM05_ISR   		BR		   N:INTTM05_APPISR   
INTTM06_ISR   		BR		   N:INTTM06_APPISR   
INTTM07_ISR   		BR		   N:INTTM07_APPISR   
INTLIN0WUP_ISR		BR		   N:INTLIN0WUP_APPISR
INTKR_ISR     		BR		   N:INTKR_APPISR     
INTCAN0ERR_ISR		BR		   N:INTCAN0ERR_APPISR
INTCAN0WUP_ISR		BR		   N:INTCAN0WUP_APPISR
INTCAN0CFR_ISR		BR		   N:INTCAN0CFR_APPISR
INTCAN0TRM_ISR		BR		   N:INTCAN0TRM_APPISR
INTCANGRFR_ISR		BR		   N:INTCANGRFR_APPISR
INTCANGERR_ISR		BR		   N:INTCANGERR_APPISR
INTTM10_ISR   		BR		   N:INTTM10_APPISR   
INTTM11_ISR   		BR		   N:INTTM11_APPISR   
INTTM12_ISR   		BR		   N:INTTM12_APPISR   
INTTM13_ISR   		BR		   N:INTTM13_APPISR   
INTFL_ISR     		BR		   N:INTFL_APPISR     
BRK_ISR     		BR		   N:BRK_I_APPISR     

#ifdef __DEBUG_BUILD_ASM
;************************************************************************
;
; Actual Vector Table - necessary to be inserted only with Debug build
; of the application
;
;************************************************************************

		COMMON   MAININTVEC
		DW       INTWDTI_ISR   		; (0x0004)		
		DW       INTLVI_ISR    		; (0x0006)
		DW       INTP0_ISR     		; (0x0008)
		DW       INTP1_ISR     		; (0x000A)
		DW       INTP2_ISR     		; (0x000C)
		DW       INTP3_ISR     		; (0x000E)
		DW       INTSPM_ISR    		; (0x0010)
		DW       INTP5_ISR     		; (0x0012)
		DW       INTCLM_ISR    		; (0x0014)
		DW       INTST0_ISR    		; (0x0016)
		DW       INTSR0_ISR    		; (0x0018)
		DW       INTTRD0_ISR   		; (0x001A)
		DW       INTTRD1_ISR   		; (0x001C)
		DW       INTTRJ0_ISR   		; (0x001E)
		DW       INTRAM_ISR    		; (0x0020)
		DW       INTLIN0TRM_ISR		; (0x0022)
		DW       INTLIN0RVC_ISR		; (0x0024)
		DW       INTLIN0_ISR   		; (0x0026)
		DW       INTIICA0_ISR  		; (0x0028)
		DW       INTP8_ISR     		; (0x002A)
		DW       INTTM00_ISR   		; (0x002C)
		DW       INTTM01_ISR   		; (0x002E)
		DW       INTTM02_ISR   		; (0x0030)
        	DW       INTTM03_ISR   		; (0x0032)
		DW       INTAD_ISR     		; (0x0034)
		DW       INTP6_ISR     		; (0x0036)
		DW       INTP7_ISR     		; (0x0038)
		DW       INTP9_ISR     		; (0x003A)
		DW       INTP10_ISR    		; (0x003C)
		DW       INTST1_ISR    		; (0x003E)
		DW       INTSR1_ISR    		; (0x0040)
		DW       INTTM04_ISR   		; (0x0042)
		DW       INTTM05_ISR   		; (0x0044)
		DW       INTTM06_ISR   		; (0x0046)
		DW       INTTM07_ISR   		; (0x0048)
		DW       INTLIN0WUP_ISR		; (0x004A)
		DW       INTKR_ISR     		; (0x004C)
		DW       INTCAN0ERR_ISR		; (0x004E)
		DW       INTCAN0WUP_ISR		; (0x0050)
		DW       INTCAN0CFR_ISR		; (0x0052)
		DW	 INTCAN0TRM_ISR		; (0x0054)
		DW	 INTCANGRFR_ISR		; (0x0056)
		DW	 INTCANGERR_ISR		; (0x0058)
		DW	 INTTM10_ISR   		; (0x005A)
		DW	 INTTM11_ISR   		; (0x005C)
		DW	 INTTM12_ISR   		; (0x005E)
		DW	 INTTM13_ISR   		; (0x0060)
		DW	 INTFL_ISR		; (0x0062)
		
		COMMON   INTVEC5
		DW       BRK_ISR	    	; (0x007E)
#endif
 
#else
;************************************************************************
;
; include isr's here
;
;************************************************************************
INTWDTI_APPISR     EQU   Default_isr   		;INTWDTI_vect
INTLVI_APPISR	   EQU   Default_isr 		;INTLVI_vect  
INTP0_APPISR	   EQU   Default_isr 		;INTP0_vect   
INTP1_APPISR	   EQU   Default_isr 		;INTP1_vect   
INTP2_APPISR	   EQU   Default_isr 		;INTP2_vect   
INTP3_APPISR	   EQU   Default_isr	     	;INTP3_vect   
INTP4_APPISR	   EQU   Default_isr	     	;INTP4_vect   
INTP5_APPISR	   EQU   Default_isr	     	;INTP5_vect   
INTCSI20_APPISR	   EQU   Default_isr	     	;INTCSI20_vect
INTSR2_APPISR	   EQU   Default_isr	     	;INTSR2_vect  
INTSRE2_APPISR	   EQU   Default_isr	     	;INTSRE2_vect 
INTDMA0_APPISR	   EQU   Default_isr	     	;INTDMA0_vect 
INTDMA1_APPISR	   EQU   Default_isr	     	;INTDMA1_vect 
INTCSI00_APPISR	   EQU   Default_isr	     	;INTCSI00_vect
INTSR0_APPISR	   EQU   Default_isr	     	;INTSR0_vect  
INTSRE0_APPISR	   EQU   Default_isr	     	;INTSRE0_vect 
INTST1_APPISR	   EQU   Default_isr	     	;INTST1_vect  
INTCSI11_APPISR	   EQU   Default_isr	     	;INTCSI11_vect
INTSRE1_APPISR	   EQU   Default_isr	     	;INTSRE1_vect 
INTIICA0_APPISR	   EQU   Default_isr	     	;INTIICA0_vect
INTTM00_APPISR	   EQU   Default_isr	     	;INTTM00_vect 
INTTM01_APPISR	   EQU   Default_isr	     	;INTTM01_vect 
INTTM02_APPISR	   EQU   Default_isr	     	;INTTM02_vect 
INTTM03_APPISR	   EQU   Default_isr	     	;INTTM03_vect 
INTAD_APPISR	   EQU   Default_isr 		;INTAD_vect   
INTRTC_APPISR	   EQU   Default_isr	     	;INTRTC_vect  
INTIT_APPISR	   EQU   Default_isr	     	;INTIT_vect   
INTCSIS0_APPISR	   EQU   Default_isr     	;INTCSIS0_vect
INTSRS0_APPISR	   EQU   Default_isr	     	;INTSRS0_vect 
INTWUTM_APPISR	   EQU   Default_isr	     	;INTWUTM_vect 
INTTM04_APPISR	   EQU   Default_isr	     	;INTTM04_vect 
INTTM05_APPISR	   EQU   Default_isr	     	;INTTM05_vect 
INTTM06_APPISR	   EQU   Default_isr	     	;INTTM06_vect 
INTTM07_APPISR	   EQU   Default_isr	     	;INTTM07_vect 
INTLT_APPISR	   EQU   Default_isr	     	;INTLT_vect   
INTLR_APPISR	   EQU   Default_isr	     	;INTLR_vect   
INTLS_APPISR	   EQU   Default_isr	     	;INTLS_vect   
INTSRES0_APPISR	   EQU   Default_isr	     	;INTSRES0_vect
INTMD_APPISR	   EQU   Default_isr	     	;INTMD_vect   
INTFL_APPISR	   EQU   Default_isr	     	;INTFL_vect   
BRK_APPISR	   EQU   Default_isr	     	;BRK_I_vect

;************************************************************************
;
; Branch Vector Table
;
;************************************************************************

		           RSEG  BRINTVEC
INTWDTI_ISR 	   	BR	 N:INTWDTI_APPISR 
INTLVI_ISR         	BR	 N:INTLVI_APPISR
INTP0_ISR	       	BR	 N:INTP0_APPISR	
INTP1_ISR	       	BR	 N:INTP1_APPISR	
INTP2_ISR	       	BR	 N:INTP2_APPISR	
INTP3_ISR	       	BR	 N:INTP3_APPISR	
INTP4_ISR	       	BR	 N:INTP4_APPISR	
INTP5_ISR	       	BR	 N:INTP5_APPISR	
INTCSI20_ISR	   	BR	 N:INTCSI20_APPISR
INTSR2_ISR	       	BR	 N:INTSR2_APPISR	
INTSRE2_ISR	       	BR	 N:INTSRE2_APPISR	
INTDMA0_ISR	       	BR	 N:INTDMA0_APPISR	
INTDMA1_ISR	       	BR	 N:INTDMA1_APPISR	
INTCSI00_ISR	   	BR	 N:INTCSI00_APPISR	
INTSR0_ISR	       	BR	 N:INTSR0_APPISR	
INTSRE0_ISR	       	BR	 N:INTSRE0_APPISR	
INTST1_ISR	       	BR	 N:INTST1_APPISR	
INTCSI11_ISR	   	BR	 N:INTCSI11_APPISR	
INTSRE1_ISR	       	BR	 N:INTSRE1_APPISR	
INTIICA0_ISR	   	BR	 N:INTIICA0_APPISR	
INTTM00_ISR	       	BR	 N:INTTM00_APPISR	
INTTM01_ISR	       	BR	 N:INTTM01_APPISR	
INTTM02_ISR	       	BR	 N:INTTM02_APPISR	
INTTM03_ISR	       	BR	 N:INTTM03_APPISR	
INTAD_ISR	       	BR	 N:INTAD_APPISR	
INTRTC_ISR	       	BR	 N:INTRTC_APPISR	
INTIT_ISR	       	BR	 N:INTIT_APPISR	
INTCSIS0_ISR	   	BR	 N:INTCSIS0_APPISR	
INTSRS0_ISR	       	BR	 N:INTSRS0_APPISR	
INTWUTM_ISR	       	BR	 N:INTWUTM_APPISR	
INTTM04_ISR	       	BR	 N:INTTM04_APPISR	
INTTM05_ISR	       	BR	 N:INTTM05_APPISR	
INTTM06_ISR	       	BR	 N:INTTM06_APPISR	
INTTM07_ISR	       	BR	 N:INTTM07_APPISR	
INTLT_ISR	       	BR	 N:INTLT_APPISR	
INTLR_ISR	       	BR	 N:INTLR_APPISR	
INTLS_ISR	       	BR	 N:INTLS_APPISR	
INTSRES0_ISR	   	BR	 N:INTSRES0_APPISR	
INTMD_ISR	       	BR	 N:INTMD_APPISR	
INTFL_ISR	       	BR	 N:INTFL_APPISR	
BRK_ISR	           	BR	 N:BRK_APPISR	

#ifdef __DEBUG_BUILD_ASM
;************************************************************************
;
; Actual Vector Table - necessary to be inserted only with Debug build
; of the application
;
;************************************************************************

		COMMON   MAININTVEC
		DW       INTWDTI_ISR 		; (0x0004)		
		DW       INTLVI_ISR  		; (0x0006)
		DW       INTP0_ISR		; (0x0008)
		DW       INTP1_ISR		; (0x000A)
		DW       INTP2_ISR		; (0x000C)
		DW       INTP3_ISR		; (0x000E)
		DW       INTP4_ISR		; (0x0010)
		DW       INTP5_ISR		; (0x0012)
		DW       INTCSI20_ISR		; (0x0014)
		DW       INTSR2_ISR		; (0x0016)
		DW       INTSRE2_ISR		; (0x0018)
		DW       INTDMA0_ISR		; (0x001A)
		DW       INTDMA1_ISR		; (0x001C)
		DW       INTCSI00_ISR		; (0x001E)
		DW       INTSR0_ISR		; (0x0020)
		DW       INTSRE0_ISR		; (0x0022)
		DW       INTST1_ISR		; (0x0024)
		DW       INTCSI11_ISR		; (0x0026)
		DW       INTSRE1_ISR		; (0x0028)
		DW       INTIICA0_ISR		; (0x002A)
		DW       INTTM00_ISR		; (0x002C)
		DW       INTTM01_ISR		; (0x002E)
		DW       INTTM02_ISR		; (0x0030)
        	DW       INTTM03_ISR		; (0x0032)
		DW       INTAD_ISR		; (0x0034)
		DW       INTRTC_ISR		; (0x0036)
		DW       INTIT_ISR		; (0x0038)
		
		COMMON   INTVEC1
		DW       INTCSIS0_ISR		; (0x003C)
		DW       INTSRS0_ISR		; (0x003E)
		DW       INTWUTM_ISR		; (0x0040)
		DW       INTTM04_ISR		; (0x0042)
		DW       INTTM05_ISR		; (0x0044)
		DW       INTTM06_ISR		; (0x0046)
		DW       INTTM07_ISR		; (0x0048)
		
		COMMON   INTVEC2
		DW       INTLT_ISR		; (0x004C)
		DW       INTLR_ISR		; (0x004E)
		DW       INTLS_ISR		; (0x0050)
		DW       INTSRES0_ISR		; (0x0052)
		
		COMMON   INTVEC3
		DW       INTMD_ISR		; (0x005E)
		
		COMMON   INTVEC4
		DW       INTFL_ISR		; (0x0062)
		
		COMMON   INTVEC5
		DW       BRK_ISR	    	; (0x007E)
				
#endif
#endif
	END