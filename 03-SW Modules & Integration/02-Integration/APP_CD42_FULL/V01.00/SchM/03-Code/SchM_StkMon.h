/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : SchM_StkMon.h
** Module name  :
** -------------------------------------------------------------------------------------------------
** Description : Include file of component SchM_StkMon.c
**
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-11ST001-12130.02 (SW LLD SchM).doc
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 22/08/2013
** - First release
**
**
***************************************************************************************************/
/*To avoid multi-inclusions */
#ifndef SCHMSTKMON_H
#define SCHMSTKMON_H

/**************************************** Inclusion files *****************************************/
#include "Std_Types.h"

/************************** Declaration of global symbol and constants ****************************/

/********************************* Declaration of global macros ***********************************/

/********************************* Declaration of global types ************************************/

/****************************** External links of global variables ********************************/

/****************************** External links of global constants ********************************/

/***************************************************************************************************
**                                            FUNCTIONS                                           **
***************************************************************************************************/
#define SCHM_START_SEC_CODE
#include "MemMap.h"
extern  FUNC(void, SCHM_CODE)   SchM_StkMonInit(void);
#define SCHM_STOP_SEC_CODE
#include "MemMap.h"

#define SCHM_START_SEC_CODE
#include "MemMap.h"
extern  FUNC(void, SCHM_CODE)   SchM_StkMon(void);
extern  FUNC(void, SCHM_CODE)   SchM_ECUReset(void);
#define SCHM_STOP_SEC_CODE
#include "MemMap.h"

#define SCHM_START_SEC_ISR_CODE
#include "MemMap.h"
extern __interrupt void SchM_StkMonIsr(void);
#define SCHM_STOP_SEC_ISR_CODE
#include "MemMap.h"


#endif /* SCHMSTKMON_H */
