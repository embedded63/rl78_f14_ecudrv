/***************************************************************************************************
** Copyright (c) 2012 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : SchM.h
** Module name  : Schedule Manager
** -------------------------------------------------------------------------------------------------
** Description  : This module Schedules tasks periodically
**
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-13ST0002-12004.01 (SW HLD CD42 Folding).doc
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 22/08/2012
** - First release
**
***************************************************************************************************/
/* To avoid multi-inclusions */
#ifndef SCHM_H
#define SCHM_H
/**************************************** Inclusion files *****************************************/
#include "Std_Types.h"

/********************************* Declaration of global types ************************************/

/************************** Declaration of global symbol and constants ****************************/

/********************************* Declaration of global macros ***********************************/

/****************************** External links of global variables ********************************/
/* SchM_i1msTrig is externed only to avoid MISRA Warning. MISRA expects volatile variables to be
externed */
#define SCHM_START_SEC_VAR_NOINIT_BOOLEAN
#include "MemMap.h"
extern VAR(boolean, SCHM_VAR) SchM_OverLd;
#define SCHM_STOP_SEC_VAR_NOINIT_BOOLEAN
#include "MemMap.h"

/****************************** External links of global constants ********************************/

/***************************************************************************************************
**                                            FUNCTIONS                                           **
***************************************************************************************************/
#define SCHM_START_SEC_CODE
#include "MemMap.h"
extern FUNC(void, SCHM_CODE) SchM_Main(void);
#define SCHM_STOP_SEC_CODE
#include "MemMap.h"


#define SCHM_START_SEC_INIT_CODE
#include "MemMap.h"
extern FUNC(void, SCHM_CODE) SchM_Init(void);
#define SCHM_STOP_SEC_INIT_CODE
#include "MemMap.h"
#endif /* SCHM_H */
