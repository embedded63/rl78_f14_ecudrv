/***************************************************************************************************
** Copyright (c) 2012 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : SchM.c
** Module name  : Schedule Manager
** -------------------------------------------------------------------------------------------------
** Description  : This module Schedules tasks periodically
**
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-13ST0002-12004.01 (SW HLD 15-40 Magic Folding).doc
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 22/08/2013
** - First release
**
***************************************************************************************************/

/**************************************** Inclusion files *****************************************/
#include "Gpt_Drv.h"
#include "Dio_Hal.h"
#include "Adc_Drv.h"
#include "Cdd_Drv.h"
#include "Pwm_Hal.h"
#include "SchM.h"
#include "main.h"
#include "Wdg_Drv.h"
#include "Adc_Hal.h"

/************************* Declaration of local symbol and constants ******************************/
#if defined (FULL_VERSION)
#define SCHM_TASKBLK5MSSIZE     (uint16)0x02u
#else
#define SCHM_TASKBLK5MSSIZE     (uint16)0x01u
#endif
#define SCHM_TASKBLK10MSSIZE    (uint16)0x05u
#define SCHM_TASKBLK50MSSIZE    (uint16)0x01u
#define SCHM_TASKBLK100MSSIZE   (uint16)0x01u

/********************************* Declaration of local macros ************************************/

/********************************* Declaration of local types *************************************/
typedef struct
{
    VAR(uint16,TYPEDEF) delay;
    P2FUNC(void, SCH_CODE, fptr) (void);
}SchM_SchBlkType;

/******************************* Declaration of local variables ***********************************/

/**************************** Internal functions declarations *************************************/
STATIC FUNC(void, SCHM_CODE) Dummy_50msTask(void);
STATIC FUNC(void, SCHM_CODE) Dummy_100msTask(void);
/******************************* Declaration of local constants ***********************************/
/* MISRA RULE 8.7 VIOLATION: SchM_icTaskBlk5ms is made static so that its value is retained.*/
#define SCHM_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h"
STATIC CONST(SchM_SchBlkType,SCHM_CODE) SchM_icTaskBlk5ms[SCHM_TASKBLK5MSSIZE] =
/*static const SchM_SchBlkType SchM_icTaskBlk5ms[SCHM_TASKBLK5MSSIZE] =*/
{
#if defined (FULL_VERSION)
    {(uint16)0,     &Cdd_Main},
    {(uint16)1,     &Pwm_Hal}
#else
    {(uint16)0,     &Pwm_Hal}
#endif
};
/* MISRA RULE 8.7 VIOLATION: SchM_icTaskBlk10ms is made static so that its value is retained.*/
STATIC CONST(SchM_SchBlkType,SCHM_CODE) SchM_icTaskBlk10ms[SCHM_TASKBLK10MSSIZE] =
{
    {(uint16)0,     &Dio_HalIn},
    {(uint16)1,     &Dio_HalOut},
    {(uint16)2,     &Adc_Main},
    {(uint16)3,     &Adc_Hal},
    {(uint16)4,     &WDGTT_Refresh}

};
/* MISRA RULE 8.7 VIOLATION: SchM_icTaskBlk50ms is made static so that its value is retained.*/
STATIC CONST(SchM_SchBlkType,SCHM_CODE) SchM_icTaskBlk50ms[SCHM_TASKBLK50MSSIZE] =
{
    {(uint16)0,     &Dummy_50msTask}

};
/* MISRA RULE 8.7 VIOLATION: SchM_icTaskBlk100ms is made static so that its value is retained.*/
STATIC CONST(SchM_SchBlkType,SCHM_CODE) SchM_icTaskBlk100ms[SCHM_TASKBLK100MSSIZE] =
{
    {(uint16)5,     &Dummy_100msTask}
};

#define SCHM_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h"

/****************************** Declaration of exported variables *********************************/
#define SCHM_START_SEC_VAR_NOINIT_BOOLEAN
#include "MemMap.h"
/* overload flag */
/* MISRA RULE 8.10 VIOLATION: SchM_OverLd is used in diff func. and hence declared globally */
VAR(boolean, SCHM_VAR) SchM_OverLd;
#define SCHM_STOP_SEC_VAR_NOINIT_BOOLEAN
#include "MemMap.h"

/****************************** Declaration of exported constants *********************************/

/***************************************************************************************************
**                                      FUNCTIONS                                                 **
***************************************************************************************************/

/******************************** Function definitions ********************************************/
#define SCHM_START_SEC_INIT_CODE
#include "MemMap.h"
/***************************************************************************************************
** Function                 : SchMInit

** Description              : Initializes module parameters

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
FUNC(void, SCHM_CODE) SchM_Init(void)
{
    /* reset overload flag*/
    SchM_OverLd = FALSE;
}
#define SCHM_STOP_SEC_INIT_CODE
#include "MemMap.h"

#define SCHM_START_SEC_CODE
#include "MemMap.h"
 /***************************************************************************************************
** Function                 : SchM_Main

** Description              : Schedules tasks

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
FUNC(void, SCHM_CODE) SchM_Main(void)
{
    VAR(uint16, AUTOMATIC)  currDly;
    VAR(uint16, AUTOMATIC)  idxTaskBlk;
    VAR(uint32, AUTOMATIC)  timer1;
    VAR(uint32, AUTOMATIC)  timer2;
    VAR(boolean, AUTOMATIC) schm1msTrig;

    while (1)
    {
        schm1msTrig = Gpt_Get1msTrig();

        if(schm1msTrig == TRUE) /* every 1ms*/
        {
            /* get the current time */
            timer1 = M_GPT_GETSYSTEMTIME;

            /* reset trigger flag */
            Gpt_Reset1msTrig();

            /* invoke 5ms task */
            /* current delay for 5ms period */
            currDly = (uint16)(timer1 % (uint16)5);

            for(idxTaskBlk = (uint16)0; idxTaskBlk < SCHM_TASKBLK5MSSIZE; idxTaskBlk++)
            {
                if(currDly == SchM_icTaskBlk5ms[idxTaskBlk].delay)
                {
                    (*(SchM_icTaskBlk5ms[idxTaskBlk].fptr))();
                }
            }

            /* invoke 10ms task */
            /* current delay for 10ms period */
            currDly = (uint16)(timer1 % (uint16)10);

            for(idxTaskBlk = (uint16)0; idxTaskBlk < SCHM_TASKBLK10MSSIZE; idxTaskBlk++)
            {
                if(currDly == SchM_icTaskBlk10ms[idxTaskBlk].delay)
                {
                    (*(SchM_icTaskBlk10ms[idxTaskBlk].fptr))();
                }
            }

            /* invoke 30ms task */
            /* current delay for 30ms period */
            currDly = (uint16)(timer1 % (uint16)50);

            for(idxTaskBlk = (uint16)0; idxTaskBlk < SCHM_TASKBLK50MSSIZE; idxTaskBlk++)
            {
                if(currDly == SchM_icTaskBlk50ms[idxTaskBlk].delay)
                {
                    (*(SchM_icTaskBlk50ms[idxTaskBlk].fptr))();
                }
            }

            /* invoke 100ms task */
            /* current delay for 100ms period */
            currDly = (uint16)(timer1 % (uint16)100);

            for(idxTaskBlk = (uint16)0; idxTaskBlk < SCHM_TASKBLK100MSSIZE; idxTaskBlk++)
            {
                if(currDly == SchM_icTaskBlk100ms[idxTaskBlk].delay)
                {
                    (*(SchM_icTaskBlk100ms[idxTaskBlk].fptr))();
                }
            }

            /* MISRA does not permit direct read of volatile variable */
            timer2 = M_GPT_GETSYSTEMTIME;

            /* if execution time is more than 1ms */
            if ((timer2 - timer1) > (uint32)1)
            {
                SchM_OverLd = TRUE;
            }
        }
    }
}


STATIC FUNC(void, SCHM_CODE) Dummy_50msTask(void)
{

}

STATIC FUNC(void, SCHM_CODE) Dummy_100msTask(void)
{

}
#define SCHM_STOP_SEC_CODE
#include "MemMap.h"

/**************************** Internal Function definitions ***************************************/
