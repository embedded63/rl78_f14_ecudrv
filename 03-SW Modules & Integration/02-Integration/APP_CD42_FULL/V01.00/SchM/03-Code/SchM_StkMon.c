/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : SchM_StkMon.c
** Module name  :
** -------------------------------------------------------------------------------------------------
** Description  : The Stack Monitoring module is checks for the Stack overflow and Stack underflow
**                If there is stack overflows or underflows then ECU will be RESET.
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-11ST001-12130.02 (SW LLD SchM).doc
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 22/08/2013
** - First release
**
***************************************************************************************************/

/**************************************** Inclusion files *****************************************/
#include "SchM_StkMon.h"
#include "SchM_StkMon_Cfg.h"
#include "Isr.h"
/********************** Declaration of local symbol and constants *********************************/
/********************************* Declaration of local macros ************************************/

/********************************* Declaration of local types *************************************/

/******************************* Declaration of local variables ***********************************/

/******************************* Declaration of local constants ***********************************/

/****************************** Declaration of exported variables *********************************/

/****************************** Declaration of exported constants *********************************/

/***************************************************************************************************
**                                      FUNCTIONS                                                 **
***************************************************************************************************/

/**************************** Internal functions declarations *************************************/
/*root __no_init uint8 *StackByte*/
/******************************** Function definitions ********************************************/
#define SCHM_START_SEC_INIT_CODE
#include "MemMap.h"
/***************************************************************************************************
** Function                 : SchM_StkMonInit

** Description              : Initialize Stack pointer Monitor function


** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
FUNC(void, SCHM_CODE) SchM_StkMonInit(void)
{

#if defined (LIGHT_VERSION)
/* MISRA Rule violation 11.3: casting is done between a pointer type and integer type
  Needed as address is pre defined in the variable */
  P2VAR(uint16, AUTOMATIC, STACK_APPL_DATA) stkPtr;
  stkPtr= (uint16 *)SP_OVERFLOW_ADDR;
  *stkPtr= SCHM_STKFILLPATTERN;
  stkPtr = (uint16 *)(SP_OVERFLOW_ADDR + STACK_SIZE);
  *stkPtr= SCHM_STKFILLPATTERN;
#endif

#if defined (FULL_VERSION)
  Isr_SetPriority(ISR_SCHM_PRIOR,ISR_PRIOR_1);
  SPMMK  = DISABLE_INTERRUPT;  /*Enable SP overflow/underflow interrupt */
  SPOFR  = SP_OVERFLOW_ADDR;  /* Stack pointer Overflow address*/
  SPUFR  = SP_UNDERRFLOW_ADDR;/* Stack pointer Underflow address*/
  SPMCTRL= DISABLE_STKMON;     /* Enable Stack pointer monitoring*/
#endif
}
#define SCHM_STOP_SEC_INIT_CODE
#include "MemMap.h"

#define SCHM_START_SEC_CODE
#include "MemMap.h"
/***************************************************************************************************
** Function                 : SchM_StkMon

** Description              : Monitor the Satck for overflow and underflow

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
FUNC(void, SCHM_CODE) SchM_StkMon(void)
{
    P2VAR(uint16, AUTOMATIC, STACK_APPL_DATA) stkPtr;

    /* MISRA RULE 3.1 VIOLATION :Cast between a pointer to object and an integral type
       MISRA RULE 11.3 VIOLATION :Cast between a pointer to object and an integral type
       stkptr is being pointed to stack end address */
    stkPtr = (uint16*)SP_OVERFLOW_ADDR;
    /* To check stack overflow */
    if((*stkPtr != (uint16)SCHM_STKFILLPATTERN))
    {
        /* Call ECU Reset Function */
        SchM_ECUReset();
    }

    /* MISRA RULE 3.1 VIOLATION :Cast between a pointer to object and an integral type
       MISRA RULE 11.3 VIOLATION :Cast between a pointer to object and an integral type
       stkptr is being pointed to stack end address */
    stkPtr = (uint16*)(SP_OVERFLOW_ADDR + STACK_SIZE);
    /* To check stack underflow */
    if((*stkPtr != (uint16)SCHM_STKFILLPATTERN))
    {
        /* Call ECU Reset Function */
        SchM_ECUReset();
    }
}

/***************************************************************************************************
** Function                 : SchM_ECUReset

** Description              : Reset the ECU

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
FUNC(void, SCHM_CODE) SchM_ECUReset(void)
{
  /* software RESET */
  WDTE = 0x9Au;     /* Reset using watchdog timer */

}
#define SCHM_STOP_SEC_CODE
#include "MemMap.h"


#define SCHM_START_SEC_ISR_CODE
#include "MemMap.h"
/***************************************************************************************************
** Function         : SchM_StkMonIsr

** Description      : executes when stack overflow/underflow occurs

** Parameter        : None

** Return value     : None

** Remarks          : None
***************************************************************************************************/
#if defined (FULL_VERSION)
__interrupt FUNC(void, SCHM_CODE) SchM_StkMonIsr(void)
{
  WDTE = 0x9Au;     /* Reset using watchdog timer */
}
#endif

#define SCHM_STOP_SEC_ISR_CODE
#include "MemMap.h"




/**************************** Internal Function definitions ***************************************/
