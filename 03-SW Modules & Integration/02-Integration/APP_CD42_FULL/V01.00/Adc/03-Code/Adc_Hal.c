/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Adc_Hal.c
** Module name  : ADC HAL
** -------------------------------------------------------------------------------------------------
** Description : Harware Abstraction Layer
**
** -------------------------------------------------------------------------------------------------
**
** Documentation reference :
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 20/08/2013
** - First release
***************************************************************************************************/

/**************************************** Inclusion files *****************************************/
#include "Adc_Hal.h"
#include "Adc_Drv.h"
#include "Adc_Cfg.h"

/********************** Declaration of local symbol and constants *********************************/

/********************************* Declaration of local macros ************************************/

/********************************* Declaration of local types *************************************/

/******************************* Declaration of local variables ***********************************/
static VAR(uint16, AUTOMATIC) Adc_SwtInThrMin;
static VAR(uint16, AUTOMATIC) Adc_SwtInThrMax;
static VAR(uint16, AUTOMATIC) Adc_SwtEosThrMin;
static VAR(uint16, AUTOMATIC) Adc_SwtEosThrMax;
static VAR(uint16, AUTOMATIC) Adc_SwtLockThrMin;
static VAR(uint16, AUTOMATIC) Adc_SwtLockThrMax;
static VAR(uint16, AUTOMATIC) Adc_SwtBtn67Thr1;
static VAR(uint16, AUTOMATIC) Adc_SwtBtn67Thr2;
static VAR(uint16, AUTOMATIC) Adc_SwtBtn67Thr3;


/******************************* Declaration of local constants ***********************************/

/****************************** Declaration of exported variables *********************************/
VAR(uint8,  AUTOMATIC)  Adc_HalSwt6Status;
VAR(uint8,  AUTOMATIC)  Adc_HalSwt7Status;
VAR(uint8,  AUTOMATIC)  Adc_HalSwt67Error;
VAR(uint16,  AUTOMATIC) Adc_BatteryVoltHAL;
VAR(uint16,  AUTOMATIC) Adc_Pwm1RawCurrHAL;
VAR(uint16,  AUTOMATIC) Adc_Pwm2RawCurrHAL;
VAR(uint16,  AUTOMATIC) Adc_Pwm3RawCurrHAL;
VAR(uint16,  AUTOMATIC) Adc_Pwm4RawCurrHAL;
VAR(uint16,  AUTOMATIC) Adc_Pwm5RawCurrHAL;
VAR(uint16,  AUTOMATIC) Adc_Pwm6RawCurrHAL;
VAR(uint16,  AUTOMATIC) Adc_Pwm7RawCurrHAL;
VAR(uint16,  AUTOMATIC) Adc_MtrShuntRawCurrHAL;
VAR(uint16,  AUTOMATIC) Adc_HesCurrHAL;
/****************************** Declaration of exported constants *********************************/

/***************************************************************************************************
**                                      FUNCTIONS                                                 **
***************************************************************************************************/

/**************************** Internal functions declarations *************************************/
FUNC(void, ADC_CODE) Adc_GetHalSwt67Status(void);
FUNC(void, ADC_CODE) Adc_GetHalBattVolt(void);
FUNC(void, ADC_CODE) Adc_GetHalPwmCurrents(void);
FUNC(void, ADC_CODE) Adc_GetHalMtrShuntRawCurr(void);
FUNC(void, ADC_CODE) Adc_GetHalHesCurrent(void);
/******************************** Function definitions ********************************************/
#define ADC_START_SEC_CODE
#include "MemMap.h"

/***************************************************************************************************
** Function                 : Adc_Hal

** Description              : Periodic function for ADC-HAL

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
FUNC(void, ADC_CODE) Adc_Hal(void)
{
    uint32 tempU32;

    /* calculate and update the Battery voltage */
    Adc_GetHalBattVolt();

    /******************  START OF THRESHOLD CALCULATION  ******************************/

    /**********************************************************************************/
    /* PART 1: Calculation of threshold value to detect HIGH or LOW on button signals */
    /* These threshold values will be used in the function Adc_HalGetSwtStatus()      */
    /**********************************************************************************/

    /* minimum threshold calculation */
    tempU32 = ((uint32)(ADC_SWTRATIOMIN) * (uint32)Adc_RawVal[Adc_BatteryVoltIdx])/(uint32)100u;
    Adc_SwtInThrMin = (uint16)tempU32;

    /* maximum threshold calculation */
    tempU32 = ((uint32)(ADC_SWTRATIOMAX) * (uint32)Adc_RawVal[Adc_BatteryVoltIdx])/(uint32)100u;
    Adc_SwtInThrMin = (uint16)tempU32;

    /**********************************************************************************/
    /* PART 2: Calculation of threshold value to detect HIGH or LOW on EOS signals    */
    /* These threshold values will be used in the function Adc_HalGetEOSStatus()      */
    /**********************************************************************************/

    /* minimum threshold calculation */
    tempU32 = ((uint32)(ADC_EOSRATIOMIN) * (uint32)Adc_RawVal[Adc_BatteryVoltIdx])/(uint32)100u;
    Adc_SwtEosThrMin = (uint16)tempU32;

    /* maximum threshold calculation */
    tempU32 = ((uint32)(ADC_EOSRATIOMIN) * (uint32)Adc_RawVal[Adc_BatteryVoltIdx])/(uint32)100u;
    Adc_SwtEosThrMax = (uint16)tempU32;

    /**********************************************************************************/
    /* PART 3: Calculation of threshold value to detect HIGH or LOW on LOCK signals   */
    /* These threshold values will be used in the function Adc_HalGetLockStatus()     */
    /**********************************************************************************/

    /* minimum threshold calculation */
    tempU32 = ((uint32)(ADC_LOCKRATIOMIN) * (uint32)Adc_RawVal[Adc_BatteryVoltIdx])/(uint32)100u;
    Adc_SwtLockThrMin = (uint16)tempU32;

    /* maximum threshold calculation */
    tempU32 = ((uint32)(ADC_LOCKRATIOMAX) * (uint32)Adc_RawVal[Adc_BatteryVoltIdx])/(uint32)100u;
    Adc_SwtLockThrMax = (uint16)tempU32;

    /**********************************************************************************/
    /* PART 4: Calculation of threshold value to detect HIGH or LOW on R3L and R3R    */
    /* These threshold values will be used in the function Adc_GetHalSwt67Status()    */
    /**********************************************************************************/

    /* threshold 1 calculation */
    tempU32 = ((uint32)(ADC_SWT67RATIO1) * (uint32)Adc_RawVal[Adc_BatteryVoltIdx])/(uint32)100u;
    Adc_SwtBtn67Thr1 = (uint16)tempU32;

    /* threshold 2 calculation */
    tempU32 = ((uint32)(ADC_SWT67RATIO2) * (uint32)Adc_RawVal[Adc_BatteryVoltIdx])/(uint32)100u;
    Adc_SwtBtn67Thr2 = (uint16)tempU32;

    /* threshold 3 calculation */
    tempU32 = ((uint32)(ADC_SWT67RATIO3) * (uint32)Adc_RawVal[Adc_BatteryVoltIdx])/(uint32)100u;
    Adc_SwtBtn67Thr3 = (uint16)tempU32;

    /******************  END OF THRESHOLD CALCULATION  ********************************/


    /* calculate the voltage levels for switch 6 and 7 */
    Adc_GetHalSwt67Status();

    /* calculate and update motor shunt current */
    Adc_GetHalMtrShuntRawCurr();

    /* calculate and update hes current */
    Adc_GetHalHesCurrent();
}

/***************************************************************************************************
** Function                 : Adc_HalGet

** Description              : convert 10 bit data to physical value

** Parameter                : ADC index

** Return value             : Scaled ADC value

** Remarks                  : None
***************************************************************************************************/
FUNC(uint16, ADC_CODE) Adc_HalGet(VAR(uint8, AUTOMATIC) idxADC)
{
    VAR(uint16, AUTOMATIC) adcval;
    VAR(uint32, AUTOMATIC) temp;

    /* Intialise to zero */
    adcval = (uint16)0;

    /* Check if the index is valid */
    if(idxADC <= ADC_CHNLMAX)
    {
        /* Conversion od 10-bit data to 16-bit data */
        temp = (uint32)((uint32)(Adc_RawVal[idxADC])*((uint32)5000));
        adcval = (uint16)((uint32)temp/(uint32)1023);
    }

    /* Return the scaled ADC value */
    return(adcval);
}

/***************************************************************************************************
** Function                 : Adc_HalClearRawVal

** Description              : resets the raw adc value

** Parameter                : ADC index

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
FUNC(void, ADC_CODE) Adc_HalClearRawVal(VAR(uint8, AUTOMATIC) idxADC)
{
    /* Check if the index is valid */
    if(idxADC <= ADC_CHNLMAX)
    {
        /* Clear the raw value for the requested index */
        Adc_RawVal[idxADC] = (uint16)0;
    }
}

/***************************************************************************************************
** Function                 : Adc_HalGetSwtStatus

** Description              : Returns TRUE if button is pressed

** Parameter                : ADC index

** Return value             : Switch status

** Remarks                  : None
***************************************************************************************************/
FUNC(uint8, ADC_CODE) Adc_HalGetSwtStatus(VAR(uint8, AUTOMATIC) idxADC)
{
    VAR(uint16, AUTOMATIC) adcval;
    VAR(uint8, AUTOMATIC) retVal;

    /* Check if the index is valid */
    if(idxADC <= ADC_CHNLMAX)
    {
        /* check if the value is non zero */
        adcval = Adc_RawVal[idxADC];

        if(adcval <= (uint16)Adc_SwtInThrMin)
        {
            retVal = FALSE;
        }
        else if(adcval >= (uint16)Adc_SwtInThrMax)
        {
            retVal = TRUE;
        }
        else
        {
            retVal = FALSE;
        }
    }
    else
    {
        /* invalid index, ideally should not be reached */
        retVal = FALSE;
    }

    /* Return the switch status */
    return(retVal);
}

/***************************************************************************************************
** Function                 : Adc_HalGetEOSStatus

** Description              : Returns TRUE if EOS is detetected as active, else FALSE

** Parameter                : ADC index

** Return value             : EOS status

** Remarks                  : None
***************************************************************************************************/
FUNC(uint8, ADC_CODE) Adc_HalGetEOSStatus(VAR(uint8, AUTOMATIC) idxADC)
{
    VAR(uint16, AUTOMATIC) adcval;
    VAR(uint8, AUTOMATIC) retVal;

    /* Check if the index is valid */
    if(idxADC <= ADC_CHNLMAX)
    {
        /* check if the value is non zero */
        adcval = Adc_RawVal[idxADC];

        if(adcval <= (uint16)Adc_SwtEosThrMin)
        {
            retVal = FALSE;
        }
        else if(adcval >= (uint16)Adc_SwtEosThrMax)
        {
            retVal = TRUE;
        }
        else
        {
            retVal = FALSE;
        }
    }
    else
    {
        /* invalid index, ideally should not be reached */
        retVal = FALSE;
    }

    /* Return the switch status */
    return(retVal);
}

/***************************************************************************************************
** Function                 : Adc_HalGetLockStatus

** Description              : Returns TRUE if LOCK sensor is detetected as active, else FALSE

** Parameter                : ADC index

** Return value             : EOS status

** Remarks                  : None
***************************************************************************************************/
FUNC(uint8, ADC_CODE) Adc_HalGetLockStatus(VAR(uint8, AUTOMATIC) idxADC)
{
    VAR(uint16, AUTOMATIC) adcval;
    VAR(uint8, AUTOMATIC) retVal;

    /* Check if the index is valid */
    if(idxADC <= ADC_CHNLMAX)
    {
        /* check if the value is non zero */
        adcval = Adc_RawVal[idxADC];

        if(adcval <= (uint16)Adc_SwtLockThrMin)
        {
            retVal = FALSE;
        }
        else if(adcval >= (uint16)Adc_SwtLockThrMax)
        {
            retVal = TRUE;
        }
        else
        {
            retVal = FALSE;
        }
    }
    else
    {
        /* invalid index, ideally should not be reached */
        retVal = FALSE;
    }

    /* Return the switch status */
    return(retVal);
}

/***************************************************************************************************
** Function                 : Adc_GetHalSwt67Status

** Description              : Returns the button status of Button 6 and 7

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
FUNC(void, ADC_CODE) Adc_GetHalSwt67Status(void)
{
    VAR(uint16, AUTOMATIC) adcval;

    /* obtain the voltage value for Swt 6 and 7*/
    adcval = Adc_RawVal[Adc_ButtonSwt67Idx];

    if(adcval < (uint16)Adc_SwtBtn67Thr1)
    {
        /* NO button is pressed */
    }
    else if((adcval >= (uint16)Adc_SwtBtn67Thr1) && (adcval < (uint16)Adc_SwtBtn67Thr2))
    {
        /* button 6 is pressed and button 7 is not pressed */
        Adc_HalSwt6Status = TRUE;
        Adc_HalSwt7Status = FALSE;
    }
    else if((adcval >= (uint16)Adc_SwtBtn67Thr2) && (adcval < (uint16)Adc_SwtBtn67Thr3))
    {
        /* button 7 is pressed and button 6 is not pressed */
        Adc_HalSwt6Status = FALSE;
        Adc_HalSwt7Status = TRUE;
    }
    else if(adcval < (uint16)Adc_SwtBtn67Thr3)
    {
        /* both buttons are pressed */
        Adc_HalSwt6Status = TRUE;
        Adc_HalSwt7Status = TRUE;
    }
    else
    {
        /* none of the buttons pressed */
        Adc_HalSwt6Status = FALSE;
        Adc_HalSwt7Status = FALSE;
    }
}
/***************************************************************************************************
** Function                 : Adc_GetHalBattVolt

** Description              : calculates the Battery voltage value

** Parameter                : None

** Return value             : uint16

** Remarks                  : None
***************************************************************************************************/
FUNC(void, ADC_CODE) Adc_GetHalBattVolt(void)
{
    VAR(uint16, AUTOMATIC) adcRawVal;

    /* obtain the 16 bit adc value */
    adcRawVal = Adc_HalGet(Adc_BatteryVoltIdx);

    /* multiply it with gain Factor to ge the actual value */
    Adc_BatteryVoltHAL = adcRawVal * ADC_GAINBATTERYVOLTAGE;
}

/***************************************************************************************************
** Function                 : Adc_GetHalMtrShuntRawCurr

** Description              : calculates the Motor shunt current

** Parameter                : None

** Return value             : uint16

** Remarks                  : None
***************************************************************************************************/
FUNC(void, ADC_CODE) Adc_GetHalMtrShuntRawCurr(void)
{
    VAR(uint16, AUTOMATIC) adcRawVal;

    if(TRUE)
    {
        /* conditions OK for shunt current detction */

        /* switch ADC to single mode */
        adcRawVal = Adc_ReadSingleChnl(Adc_MtrShuntCurrIdx);

        /* factor it with gain Factor to get the actual value */
        Adc_MtrShuntRawCurrHAL = adcRawVal * ADC_GAINSHUNTCURR;

        /* switch back ADC to normal mode */
        Adc_SwitchToContinuous();
    }
    else
    {
        /* retain old value in */
    }
}
/***************************************************************************************************
** Function                 : Adc_GetHalHesCurrent

** Description              : calculates the HES Current

** Parameter                : None

** Return value             : uint16

** Remarks                  : None
***************************************************************************************************/
FUNC(void, ADC_CODE) Adc_GetHalHesCurrent(void)
{
    VAR(uint16, AUTOMATIC) adcRawVal;

    if(TRUE)
    {
        /* conditions OK for shunt current detction */

        /* switch ADC to single mode */
        adcRawVal = Adc_ReadSingleChnl(Adc_HesCurrIdx);

        /* factor it with gain Factor to get the actual value */
        Adc_HesCurrHAL = adcRawVal * ADC_GAINHESCURR;

        /* switch back ADC to normal mode */
        Adc_SwitchToContinuous();
    }
    else
    {
        /* retain old value in */
    }
}

#define ADC_STOP_SEC_CODE
#include "MemMap.h"

/**************************** Internal Function definitions ***************************************/
