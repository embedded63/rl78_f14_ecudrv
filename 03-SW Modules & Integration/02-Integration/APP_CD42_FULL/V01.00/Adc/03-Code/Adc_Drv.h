/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Adc_Drv.h
** Module name  : ADC
** -------------------------------------------------------------------------------------------------
** Description : Include file of component Adc_Drv.c
**
** -------------------------------------------------------------------------------------------------
**
** Documentation reference :
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 19/08/2013
** - First release
***************************************************************************************************/

/*To avoid multi-inclusions */
#ifndef ADCDRV_H
#define ADCDRV_H
/**************************************** Inclusion files *****************************************/
#include "Std_Types.h"
#include "Adc_Cfg.h"

/************************** Declaration of global symbol and constants ****************************/
#define ADC_CONTINUOUSMODE    ((uint8)1)
#define ADC_SINGLEMODE        ((uint8)2)

/* Maximum number of channels for continuous mode */
#define ADC_IDXMAXCONTMODE      (uint8)(ADC_CHNLMAX - ADC_IDXMAXSINGLEMODE)

/********************************* Declaration of global macros ***********************************/

/********************************* Declaration of global types ************************************/

/****************************** External links of global variables ********************************/
#define ADC_START_SEC_VAR_NOINIT_16BITS
#include "MemMap.h"
/* Array to hold the converted values according to the sequence of channels */
extern  VAR(uint16, ADC_VAR) Adc_RawVal[ADC_CHNLMAX];
#define ADC_STOP_SEC_VAR_NOINIT_16BITS
#include "MemMap.h"

#define ADC_START_SEC_VAR_NOINIT_8BITS
#include "MemMap.h"
/* Channel Index */
extern VAR(uint8, ADC_VAR) Adc_ChnlIndx;
#define ADC_STOP_SEC_VAR_NOINIT_8BITS
#include "MemMap.h"

/****************************** External links of global constants ********************************/

/***************************************************************************************************
**                                            FUNCTIONS                                           **
***************************************************************************************************/
#define ADC_START_SEC_CODE
#include "MemMap.h"
extern FUNC(void, ADC_CODE) Adc_Init    (void);
extern inline  FUNC(void, ADC_CODE)    Adc_SwitchToSingle       (void);
extern inline  FUNC(void, ADC_CODE)    Adc_SwitchToContinuous   (void);
extern  FUNC(void, ADC_CODE)    Adc_Main            (void);
extern  FUNC(uint16, ADC_CODE)  Adc_ReadSingleChnl  (VAR(uint8, AUTOMATIC) idxADC);
extern  __interrupt void Adc_Isr(void);
#define ADC_STOP_SEC_CODE
#include "MemMap.h"

#endif /* ADCDRV_H */
