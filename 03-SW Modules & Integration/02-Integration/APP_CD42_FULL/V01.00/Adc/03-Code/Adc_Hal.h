/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Adc_Hal.h
** Module name  : ADC HAL
** -------------------------------------------------------------------------------------------------
** Description : Include file of component Adc_Hal.c
**
** -------------------------------------------------------------------------------------------------
**
** Documentation reference :
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 20/08/2013
** - First release
***************************************************************************************************/

/* To avoid multi-inclusions */
#ifndef ADCHAL_H
#define ADCHAL_H

/**************************************** Inclusion files *****************************************/
#include "Std_Types.h"
#include "Pwm_Cfg.h"

/************************** Declaration of global symbol and constants ****************************/
#define Adc_GetButtonSwt1()       Adc_HalGetSwtStatus(Adc_ButtonSwt1Idx)
#define Adc_GetButtonSwt2()       Adc_HalGetSwtStatus(Adc_ButtonSwt2Idx)
#define Adc_GetButtonSwt3()       Adc_HalGetSwtStatus(Adc_ButtonSwt3Idx)
#define Adc_GetButtonSwt4()       Adc_HalGetSwtStatus(Adc_ButtonSwt4Idx)
#define Adc_GetLockSnsr1()        Adc_HalGetLockStatus(Adc_LockSnsr1Idx)
#define Adc_GetLockSnsr2()        Adc_HalGetLockStatus(Adc_LockSnsr2Idx)
#define Adc_GetEosMot1Snsr()      Adc_HalGetEOSStatus(Adc_EosMot1Snsr1Idx)
#define Adc_GetEosMot2Snsr()      Adc_HalGetEOSStatus(Adc_EosMot2Snsr1Idx)

#if defined (FULL_VERSION)
#define Adc_GetButtonSwt5()       Adc_HalGetSwtStatus(Adc_ButtonSwt5Idx)
#define Adc_GetButtonSwt6()       Adc_HalSwt6Status
#define Adc_GetButtonSwt7()       Adc_HalSwt7Status
#endif

/*** Defines for project specific threshold or gain calculations ***/
#define ADC_SWTRATIOMIN             (10u)
#define ADC_SWTRATIOMAX             (170u)

#define ADC_EOSRATIOMIN             (10u)
#define ADC_EOSRATIOMAX             (170u)

#define ADC_LOCKRATIOMIN            (10u)
#define ADC_LOCKRATIOMAX            (170u)

#define ADC_SWT67RATIO1             (40u)
#define ADC_SWT67RATIO2             (100u)
#define ADC_SWT67RATIO3             (150u)

#define ADC_GAINBATTERYVOLTAGE      (20u)
#define ADC_GAINSHUNTCURR           (20u)
#define ADC_GAINHESCURR             (20u)

/********************************* Declaration of global macros ***********************************/

/********************************* Declaration of global types ************************************/

/****************************** External links of global variables ********************************/
extern VAR(uint8,  AUTOMATIC)  Adc_HalSwt6Status;
extern VAR(uint8,  AUTOMATIC)  Adc_HalSwt7Status;
extern VAR(uint16,  AUTOMATIC) Adc_BatteryVoltHAL;
extern VAR(uint16,  AUTOMATIC) Adc_MtrShuntRawCurrHAL;
extern VAR(uint16,  AUTOMATIC) Adc_HesCurrHAL;
/****************************** External links of global constants ********************************/

/***************************************************************************************************
**                                            FUNCTIONS                                           **
***************************************************************************************************/
#define ADC_START_SEC_CODE
#include "MemMap.h"
extern  FUNC(void, ADC_CODE)    Adc_Hal             (void);
extern  FUNC(uint16, ADC_CODE)  Adc_HalGet          (VAR(uint8, AUTOMATIC) idxADC);
extern  FUNC(void, ADC_CODE)    Adc_HalClearRawVal  (VAR(uint8, AUTOMATIC) idxADC);
extern  FUNC(uint8, ADC_CODE)   Adc_HalGetSwtStatus (VAR(uint8, AUTOMATIC) idxADC);
extern  FUNC(uint8, ADC_CODE)   Adc_HalGetEOSStatus (VAR(uint8, AUTOMATIC) idxADC);
extern  FUNC(uint8, ADC_CODE)   Adc_HalGetLockStatus(VAR(uint8, AUTOMATIC) idxADC);
#define ADC_STOP_SEC_CODE
#include "MemMap.h"

#endif /* ADCHAL_H */
