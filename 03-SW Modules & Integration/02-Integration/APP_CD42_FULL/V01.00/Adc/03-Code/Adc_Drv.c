/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Adc_Drv.c
** Module name  : ADC
** -------------------------------------------------------------------------------------------------
** Description : The A/D converter converts analog input voltages into digital values. A/D converter
**               performs conversion on consecutive channels starting from the start channel
**               (ADS) and ending with the end channel (ADC_CHNLMAX). The required sequence of
**               channels is updated in the Adc_iChnlSeq array of type Adc_iChnlSeqType
**
** -------------------------------------------------------------------------------------------------
**
** Documentation reference :
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 19/08/2013
** - First release
***************************************************************************************************/

/**************************************** Inclusion files *****************************************/
#include "Adc_Drv.h"
#include "Pwm_Drv.h"
#include "Isr.h"

/********************** Declaration of local symbol and constants *********************************/

/********************************* Declaration of local macros ************************************/

/********************************* Declaration of local types *************************************/

/******************************* Declaration of local variables ***********************************/
#define ADC_START_SEC_VAR_NOINIT_8BITS
#include "MemMap.h"
/* ADC driver status */
static VAR(uint8, ADC_VAR) Adc_iSt;
#define ADC_STOP_SEC_VAR_NOINIT_8BITS
#include "MemMap.h"

/******************************* Declaration of local constants ***********************************/
#define ADC_START_SEC_CONST_8BITS
#include "MemMap.h"
/* Array to hold the required sequence of channels */
static VAR(uint8, ADC_APPL_DATA) Adc_iChnlSeq[ADC_CHNLMAX] = ADC_CHANNEL_SEQ;
#define ADC_STOP_SEC_CONST_8BITS
#include "MemMap.h"

/****************************** Declaration of exported variables *********************************/
#define ADC_START_SEC_VAR_NOINIT_16BITS
#include "MemMap.h"
/* Array to hold the converted values according to the sequence of channels */
VAR(uint16, ADC_VAR) Adc_RawVal[ADC_CHNLMAX];
#define ADC_STOP_SEC_VAR_NOINIT_16BITS
#include "MemMap.h"

#define ADC_START_SEC_VAR_NOINIT_8BITS
#include "MemMap.h"
/* Channel Index */
VAR(uint8, ADC_VAR) Adc_ChnlIndx;
/* ADC driver conversion (busy) status */
volatile VAR(uint8, ADC_VAR) Adc_iBusySt;
#define ADC_STOP_SEC_VAR_NOINIT_8BITS
#include "MemMap.h"

/****************************** Declaration of exported constants *********************************/

/***************************************************************************************************
**                                      FUNCTIONS                                                 **
***************************************************************************************************/

/**************************** Internal functions declarations *************************************/

/******************************** Function definitions ********************************************/
#define ADC_START_SEC_CODE
#include "MemMap.h"
/***************************************************************************************************
** Function                 : Adc_Init

** Description              : Initialization of ADC

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
FUNC(void, ADC_CODE) Adc_Init(void)
{
    VAR(uint8, AUTOMATIC) idx;

    /* Supply AD clock */
    PER0 |= ADC_PER0_ADCEN_ENABLE_MASK;

    /* ClearADCS bit of ADM0 register to disable AD conversion */
    ADM0 &= ADC_ADM0_ADCS_DISABLE_CONVER;

    /* Disable INTAD interrupt */
    ADMK = 1U;

    /* Clear INTAD interrupt flag */
    ADIF = 0U;

    /* Set INTAD priority at level 1 */
    Isr_SetPriority(ISR_ADC_PRIOR,ISR_PRIOR_1);

    /* Configure all necessary pins as ADC inputs */
    /* This configuration has to match the Adc_iChnlSeq[] array */
#if defined (FULL_VERSION)
    PM3 = ADC_PM3_CONF;
    PM8 = ADC_PM8_CONF;
    PM9 = ADC_PM9_CONF;
    PM12 = ADC_PM12_CONF;
#else
    PM0 = ADC_PM0_CONF;
    PM2 = ADC_PM2_CONF;
    PM12 = ADC_PM12_CONF;
    PM14 = ADC_PM14_CONF;
#endif

    /* Select AD conversion clock (i.e. fCLK/32) */
    /* Set AD conversion operation mode (i.e. select operation mode) */
    /* Enable AD comparator */
    ADM0 = ADC_ADM0_REG_VAL;

    /* Set AD trigger mode selection (i.e. software trigger mode) */
    /* Set AD convertion mode (i.e. oneshotconvertion mode) */
    ADM1 = ADC_ADM1_REG_VAL;

    /* Set AD conversion result upper/lower bound value to generates INTAD */
    /* Set AD resolution (i.e. 10 bits) & Vdd & Vss as Analog Reference */
    ADM2 = ADC_ADM2_REG_VAL;

    /* Set AD conversion result upper/lower bound value */
    ADUL = ADC_ADUL_UPPER_LIMIT; /* AD ADUL VALUE */
    ADLL = ADC_ADUL_LOWER_LIMIT; /* AD ADLL VALUE */

    Adc_ChnlIndx = (uint8)0x00;    /* Clear the channel index at init */

    /* Enable INTAD interrupt */
    ADMK = 0U;

    /* set default values */
    for (idx = (uint8)0; idx < ADC_CHNLMAX; idx++)
    {
        Adc_RawVal[idx] = (uint16)0;
    }

    /* Initialise ADC default state*/
    Adc_iSt = ADC_CONTINUOUSMODE;

    /* Initialise ADC conversion status as not busy */
    Adc_iBusySt = FALSE;
}


/***************************************************************************************************
** Function                 : Adc_Main

** Description              : Start ADC conversion by updating the start and end channel sequence

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
FUNC(void, ADC_CODE) Adc_Main(void)
{
    /* ADC current state */
    switch (Adc_iSt)
    {
        /* ADC in CONTINUOUSMODE */
        case ADC_CONTINUOUSMODE:
        {
            if(Adc_ChnlIndx >= ADC_IDXMAXCONTMODE)
            {
                /* Reset the channel index value */
                Adc_ChnlIndx = (uint8)0x00;
            }

            /* Update the channel index */
            ADS = Adc_iChnlSeq[Adc_ChnlIndx];

            /* Clear INTAD interrupt flag */
            ADIF = 0U;

            /* Set ADC conversion status as busy */
            Adc_iBusySt = TRUE;

            /* Enable AD comparator */
            ADM0 |= ADC_ADM0_ADCS_ENABLE_CONVER;

            break;
        }
        /* ADC in SINGLEMODE */
        case ADC_SINGLEMODE:
        {
            /*if (MF_ECUSt != MF_ECU_NRMOP)*/
            {
                /* reset to continuous mode, when ECU is in Non-operation mode */
                /*Adc_iSt = ADC_CONTINUOUSMODE*/
            }
            break;
        }
        default:
        {
            /* Do Nothing */
            break;
        }
    }
}

/***************************************************************************************************
** Function                 : Adc_SwitchToSingle

** Description              : Waits for current conversion to complete and change ADC to Single mode

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
INLINE FUNC(void, ADC_CODE) Adc_SwitchToSingle(void)
{
    VAR(uint16, AUTOMATIC) exitcounter = (uint16)0;

    /* Check if ADC state is already in single mode */
    if(Adc_iSt != ADC_SINGLEMODE)
    {
        /* Check if ADC is busy */
        if(ADCS == (uint8)0x01)
        {
            /* enable the interrupts */
            __enable_interrupt();
            
            /* Wait till conversion is finished */
            while((Adc_iBusySt == TRUE) && (exitcounter < (uint16) 0xFFFE))
            {
                exitcounter++;
            }

            /* Change ADC to SINGLEMODE */
            Adc_iSt = ADC_SINGLEMODE;
        }
        else
        {
            /* Change ADC to SINGLEMODE */
            Adc_iSt = ADC_SINGLEMODE;
        }
    }
}

/***************************************************************************************************
** Function                 : Adc_ReadSingleChnl

** Description              : Read the current ADC value and change ADC to Single mode and read
                              single channel

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
FUNC(uint16, ADC_CODE) Adc_ReadSingleChnl(VAR(uint8, AUTOMATIC) idxADC)
{
    VAR(uint16, AUTOMATIC) exitcounter = (uint16)0;
    static VAR(uint16, AUTOMATIC) Temp_AdcRawVal = (uint16)0;

    /* Check if the requested index is valid */
    if(idxADC < ADC_CHNLMAX)
    {
        /* Call function to Wait for current conversion to be completed */
        Adc_SwitchToSingle();

        /* Update the channel index */
        ADS = Adc_iChnlSeq[idxADC];

        /* Clear INTAD interrupt flag */
        ADIF = 0U;

         /* Set ADC conversion status as busy */
         Adc_iBusySt = TRUE;

        /* Enable AD comparator */
        ADM0 |= ADC_ADM0_ADCS_ENABLE_CONVER;

        /* enable the interrupts */
        __enable_interrupt();

        /* Wait till conversion is finished */
        while((Adc_iBusySt == TRUE) && (exitcounter < (uint16) 0xFFFE))
        {
            exitcounter++;
        }

        /* Collect 10bit data to the respective array */
        Adc_RawVal[idxADC] = ((uint16)(ADCR >> 6U));

        /* 10bit data */
        Temp_AdcRawVal = Adc_RawVal[idxADC];
    }
    else
    {
        /* Do nothing */
    }

    /* Return the raw ADC value for the requested channel */
    return Temp_AdcRawVal;
}

/***************************************************************************************************
** Function                 : Adc_SwitchToContinuous

** Description              : Reset the ADC mode to continuous mode

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
INLINE FUNC(void, ADC_CODE) Adc_SwitchToContinuous(void)
{
   if(Adc_iSt == ADC_SINGLEMODE)
   {
      /* Change ADC to CONTINUOUSMODE */
      Adc_iSt = ADC_CONTINUOUSMODE;
   }
   else
   {
      /* Do nothing */
   }
}

/***************************************************************************************************
** Function                 : Adc_Isr

** Description              : This function is to perform interrupt service routine tasks.

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
__interrupt void Adc_Isr(void)
{
    /* Check if conversion is completed */
    if(ADCS == (uint8)0x00u)
    {
        switch(Adc_iSt)
        {
            case ADC_CONTINUOUSMODE:
            {
                /* Collect 10bit data to the respective array */
                Adc_RawVal[Adc_ChnlIndx] = ((uint16)(ADCR >> 6U));

                /* Increment the index */
                Adc_ChnlIndx++;

                /* Set ADC conversion status as not busy */
                Adc_iBusySt = FALSE;
            }
            break;
            case ADC_SINGLEMODE:
            {
                /* Set ADC conversion status as not busy */
                Adc_iBusySt = FALSE;
            }
            break;
            default:
            {
                /* Do nothing */
            }
            break;
        }
    }

    /* Clear INTAD interrupt flag */
    ADIF = 0U;
}
#define ADC_STOP_SEC_CODE
#include "MemMap.h"

