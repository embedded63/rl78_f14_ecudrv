/*******************************************************************************
** Copyright (c) 2012 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -----------------------------------------------------------------------------
** File Name : Adc_cfg.h
** Module Name : ADC
** -----------------------------------------------------------------------------
**
** Description : Configuration file of component Adc_Drv.c
** This file must exclusively contain informations needed to
** use this component.
**
** -----------------------------------------------------------------------------
**
** Documentation reference :
**
********************************************************************************
** R E V I S I O N H I S T O R Y
********************************************************************************
** V01.00 19/08/2013
** - First release
*******************************************************************************/
/* To avoid multi-inclusions */
#ifndef ADC_CFG_H
#define ADC_CFG_H
/*************************** Inclusion files **********************************/
#include "Std_Types.h"

/********************** component configuration *******************************/
/* PER0 */
/* ADCEN bit mask of PER0 : Bit 5 -  Enable = 1; Disable = 0 */
#define ADC_PER0_ADCEN_ENABLE_MASK      0x20u
#define ADC_PER0_ADCEN_DISABLE_MASK     0xDFu

/* ADMD */
/* ADMD bit mask of ADM0 : Bit 6 - Select mode = 0; Scan Mode = 1 */
#define ADC_ADM0_ADMD_SELECT_MODE       0x00u
#define ADC_ADM0_ADMD_SCAN_MODE         0x40u

/* ADCS bit mask of ADM0 : Bit 7 - Stops conversion operation = 0;
                                   Enables conversion operation = 1 */
#define ADC_ADM0_ADCS_DISABLE_CONVER    0x00u
#define ADC_ADM0_ADCS_ENABLE_CONVER     0x80u

/* FR0 to FR2 bit mask of ADMO for Conversion Clock */
#define ADC_ADM0_FR64_CONV_CLK          0x00u
#define ADC_ADM0_FR32_CONV_CLK          0x08u
#define ADC_ADM0_FR16_CONV_CLK          0x10u
#define ADC_ADM0_FR08_CONV_CLK          0x18u

/* ADCE bit mask of ADM0 : Bit 0 - Stops voltage comparator operation = 0;
                                   Enables voltage comparator operation = 1 */
#define ADC_ADM0_ADCE_DISABLE_COMP      0x00u
#define ADC_ADM0_ADCE_ENABLE_COMP       0x01u

/* ADM1 */
/* ADTMD 1 and 0 bit mask of ADM1 : Bit 7 and 6 - 0 x = Software Trigger Mode */
#define ADC_ADM1_ADTMD_SWTRIG_MODE      0x00u

/* ADSCM bit mask of ADM1 : Bit 5 - (conversion Mode) Sequential = 0, One-Shot  = 1 */
#define ADC_ADM1_ADSCM_SEQ_MODE         0x00u
#define ADC_ADM1_ADSCM_ONESHOT_MODE     0x20u

/* ADM2 */
/* ADRCK bit mask of ADM2 : Bit 3 - (conversion values) Area1 = 0, Area2and 3  = 1 */
#define ADC_ADM2_ADRCK_AREA_1           0x00u
#define ADC_ADM2_ADRCK_AREA_2_3         0x08u

/* ADTYP bit mask of ADM2 : Bit 0 - (conversion resolution) 10 bit = 0, 8 bit  = 1 */
#define ADC_ADM2_ADTYP_10BIT            0x00u
#define ADC_ADM2_ADTYP_8BIT             0x01u

/* ADUL Upper limit value */
#define ADC_ADUL_UPPER_LIMIT            0xFFu
/* ADUL Lower limit value */
#define ADC_ADUL_LOWER_LIMIT            0x00u

/* ADM0 register final value */
#define ADC_ADM0_REG_VAL                 (ADC_ADM0_ADMD_SELECT_MODE | \
                                         ADC_ADM0_ADCE_ENABLE_COMP | \
                                         ADC_ADM0_FR16_CONV_CLK)
/* ADM1 register final value */
#define ADC_ADM1_REG_VAL                 (ADC_ADM1_ADTMD_SWTRIG_MODE | \
                                          ADC_ADM1_ADSCM_ONESHOT_MODE)
/* ADM21 register final value */
#define ADC_ADM2_REG_VAL                 (ADC_ADM2_ADRCK_AREA_1 | \
                                          ADC_ADM2_ADTYP_10BIT)

#define ADC_AN0_PM          ((uint8)1)      /* set to 1 to enable AN0 as analog input pin         */
#define ADC_AN1_PM          ((uint8)1)      /* set to 1 to enable AN1 as analog input pin         */
#define ADC_AN2_PM          ((uint8)1)      /* set to 1 to enable AN2 as analog input pin         */
#define ADC_AN3_PM          ((uint8)1)      /* set to 1 to enable AN3 as analog input pin         */

#if defined (FULL_VERSION)
#define ADC_AN4_PM          ((uint8)1)      /* set to 1 to enable AN4 as analog input pin         */
#define ADC_AN5_PM          ((uint8)1)      /* set to 1 to enable AN5 as analog input pin         */
#define ADC_AN6_PM          ((uint8)1)      /* set to 1 to enable AN6 as analog input pin         */
#define ADC_AN7_PM          ((uint8)1)      /* set to 1 to enable AN7 as analog input pin         */

#define ADC_AN8_PM          ((uint8)1)      /* set to 1 to enable AN8  as analog input pin        */
#define ADC_AN9_PM          ((uint8)1)      /* set to 1 to enable AN9  as analog input pin        */
#define ADC_AN10_PM         ((uint8)1)      /* set to 1 to enable AN10 as analog input pin        */
#define ADC_AN11_PM         ((uint8)1)      /* set to 1 to enable AN11 as analog input pin        */
#define ADC_AN12_PM         ((uint8)1)      /* set to 1 to enable AN12 as analog input pin        */
#define ADC_AN13_PM         ((uint8)1)      /* set to 1 to enable AN13 as analog input pin        */
#define ADC_AN14_PM         ((uint8)1)      /* set to 1 to enable AN14 as analog input pin        */
#define ADC_AN15_PM         ((uint8)1)      /* set to 1 to enable AN15 as analog input pin        */

#define ADC_AN24_PM         ((uint8)0)      /* set to 1 to enable AN24 as analog input pin        */
#define ADC_AN25_PM         ((uint8)0)      /* set to 1 to enable AN25 as analog input pin        */
#define ADC_AN26_PM         ((uint8)1)      /* set to 1 to enable AN26 as analog input pin        */

#else
#define ADC_AN16_PM         ((uint8)0)      /* set to 1 to enable AN16 as analog input pin        */
#define ADC_AN17_PM         ((uint8)0)      /* set to 1 to enable AN17 as analog input pin        */
#define ADC_AN18_PM         ((uint8)1)      /* set to 1 to enable AN18 as analog input pin        */
#define ADC_AN19_PM         ((uint8)1)      /* set to 1 to enable AN19 as analog input pin        */

#endif

#if defined (FULL_VERSION)
#define ADC_PM3_CONF        ((uint8)((uint8)(ADC_AN1_PM << 4) | (uint8)(ADC_AN0_PM << 3)))

#define ADC_PM8_CONF        ((uint8)((uint8)(ADC_AN9_PM << 7) | (uint8)(ADC_AN8_PM << 6) | \
                                     (uint8)(ADC_AN7_PM << 5) | (uint8)(ADC_AN6_PM << 4) | \
                                     (uint8)(ADC_AN5_PM << 3) | (uint8)(ADC_AN4_PM << 2) | \
                                     (uint8)(ADC_AN3_PM << 1) | (uint8)ADC_AN2_PM))

#define ADC_PM9_CONF        ((uint8)((uint8)(ADC_AN26_PM << 6) | (uint8)(ADC_AN15_PM << 5) | \
                                     (uint8)(ADC_AN14_PM << 4) | (uint8)(ADC_AN13_PM << 3) | \
                                     (uint8)(ADC_AN12_PM << 2) | (uint8)(ADC_AN11_PM << 1) | \
                                     (uint8)ADC_AN10_PM))

#define ADC_PM12_CONF       ((uint8)((uint8)(ADC_AN24_PM << 5) | (uint8)ADC_AN25_PM))

#else
#define ADC_PM0_CONF        ((uint8)((uint8)(ADC_AN16_PM << 1) | (uint8)ADC_AN17_PM))

#define ADC_PM2_CONF        ((uint8)((uint8)(ADC_AN3_PM << 3) | (uint8)(ADC_AN2_PM << 2) | \
                             (uint8)(ADC_AN1_PM << 1) | (uint8)ADC_AN0_PM))

#define ADC_PM12_CONF       ((uint8)ADC_AN19_PM)
#define ADC_PM14_CONF       ((uint8)(ADC_AN18_PM << 7))
#endif

#if defined (FULL_VERSION)
/* Below defines are the ADC channel numbers. They are used to init Adc_iChnlSeq[].
   During normal operation, the below channel number will be used to set the
   ADC register to start conversion of that respective ADC channel
 */
#define ADC_AN0               ((uint8)0)
#define ADC_AN1               ((uint8)1)
#define ADC_AN2               ((uint8)2)
#define ADC_AN3               ((uint8)3)
#define ADC_AN4               ((uint8)4)
#define ADC_AN5               ((uint8)5)
#define ADC_AN6               ((uint8)6)
#define ADC_AN7               ((uint8)7)
#define ADC_AN8               ((uint8)8)
#define ADC_AN9               ((uint8)9)
#define ADC_AN10              ((uint8)10)
#define ADC_AN11              ((uint8)11)
#define ADC_AN12              ((uint8)12)
#define ADC_AN13              ((uint8)13)
#define ADC_AN14              ((uint8)14)
#define ADC_AN15              ((uint8)15)
#define ADC_AN26              ((uint8)26)
#endif

#if defined (FULL_VERSION)
/* Below defines where the converted RAW ADC value of an ADC channel is found in the
   output array Adc_RawVal[], this should match the sequence in array Adc_iChnlSeq[]
 */
#define ADC_AN0IDX            ((uint8)0)
#define ADC_AN15IDX           ((uint8)1)
#define ADC_AN7IDX            ((uint8)2)
#define ADC_AN8IDX            ((uint8)3)
#define ADC_AN9IDX            ((uint8)4)
#define ADC_AN6IDX            ((uint8)5)
#define ADC_AN10IDX           ((uint8)6)
#define ADC_AN11IDX           ((uint8)7)
#define ADC_AN12IDX           ((uint8)8)
#define ADC_AN13IDX           ((uint8)9)
#define ADC_AN14IDX           ((uint8)10)
#define ADC_AN5IDX            ((uint8)11)
#define ADC_AN26IDX           ((uint8)12)
#define ADC_AN1IDX_1          ((uint8)13)
#define ADC_AN1IDX_5          ((uint8)14)
#define ADC_AN2IDX            ((uint8)15)
#define ADC_AN3IDX_3          ((uint8)16)
#define ADC_AN3IDX_6          ((uint8)17)
#define ADC_AN4IDX_7          ((uint8)18)
#define ADC_AN4IDX_4          ((uint8)19)

/* Configure the sequence of channels */
#define ADC_CHANNEL_SEQ       {(uint8)ADC_AN0, (uint8)ADC_AN15, (uint8)ADC_AN7, (uint8)ADC_AN8, \
                               (uint8)ADC_AN9, (uint8)ADC_AN6, (uint8)ADC_AN10,(uint8)ADC_AN11, \
                               (uint8)ADC_AN12,(uint8)ADC_AN13,(uint8)ADC_AN14,(uint8)ADC_AN5,  \
                               (uint8)ADC_AN26,(uint8)ADC_AN1, (uint8)ADC_AN1, (uint8)ADC_AN2,  \
                               (uint8)ADC_AN3, (uint8)ADC_AN3, (uint8)ADC_AN4, (uint8)ADC_AN4}

/* Maximum number of ADC channels in Full Version */
#define ADC_CHNLMAX           ((uint8)20)

/* Maximum number of channels for single mode - counting last in Adc_iChnlSeq[] */
#define ADC_IDXMAXSINGLEMODE  ((uint8)9)

#endif

/* Configure the array index for respective ADC channel */
#define Adc_ButtonSwt1Idx    ADC_AN7IDX
#define Adc_ButtonSwt2Idx    ADC_AN8IDX
#define Adc_ButtonSwt3Idx    ADC_AN9IDX
#define Adc_ButtonSwt4Idx    ADC_AN6IDX
#define Adc_ButtonSwt5Idx    ADC_AN10IDX
#define Adc_ButtonSwt67Idx   ADC_AN11IDX
#define Adc_BatteryVoltIdx   ADC_AN0IDX
#define Adc_LockSnsr1Idx     ADC_AN13IDX
#define Adc_LockSnsr2Idx     ADC_AN15IDX
#define Adc_EosMot1Snsr1Idx  ADC_AN12IDX
#define Adc_EosMot2Snsr1Idx  ADC_AN14IDX
#define Adc_MtrShuntCurrIdx  ADC_AN5IDX
#define Adc_HesCurrIdx       ADC_AN26IDX

#endif /* ADC_CFG_H */
