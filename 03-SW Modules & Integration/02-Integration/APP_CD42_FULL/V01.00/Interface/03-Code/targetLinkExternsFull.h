

#ifndef TARGETLINKTEXTERNSFULL_H
#define VBATT_H

#ifdef ECU_FULL_CARD


#include	"Dio_Hal.h"
#include	"Adc_Hal.h"
#include	"Cdd_Hal.h"
#include	"Gpt_Drv.h"


#define				Dio_Wakeup_bl									Dio_MCUWakeup								
#define				Adc_BatteryVoltage_u16							Adc_BatteryVoltHAL							
#define				Adc_LockSnsr1_bl								Adc_GetLockSnsr1()							
#define				Adc_LockSnsr2_bl								Adc_GetLockSnsr2()							
#define				Adc_EosMot1Snsr_bl								Adc_GetEosMot1Snsr()						
#define				Adc_EosMot2Snsr_bl								Adc_GetEosMot2Snsr()						
#define				Dio_EcuCfg_u8									Dio_GetEcuCfg()								
#define				Adc_Pwm1RawCurrent_u16							Adc_Pwm3RawCurrHAL							
#define				Adc_Pwm2RawCurrent_u16							Adc_Pwm2RawCurrHAL							
#define				Adc_Pwm3RawCurrent_u16							Adc_Pwm1RawCurrHAL							
#define				Adc_Pwm4RawCurrent_u16							Adc_Pwm4RawCurrHAL							
#define				Adc_Pwm5RawCurrent_u16							Adc_Pwm5RawCurrHAL							
#define				Adc_Pwm6RawCurrent_u16							Adc_Pwm7RawCurrHAL							
#define				Adc_Pwm7RawCurrent_u16							Adc_Pwm6RawCurrHAL							
#define				Adc_MtrShuntRawCurrent_u16						Adc_MtrShuntRawCurrHAL						
#define				Adc_HesCurrent_u16								Adc_HesCurrHAL								
#define				Adc_ButtonSwitch1_bl							Adc_GetButtonSwt1()							
#define				Adc_ButtonSwitch2_bl							Adc_GetButtonSwt2()							
#define				Adc_ButtonSwitch3_bl							Adc_GetButtonSwt3()							
#define				Adc_ButtonSwitch4_bl							Adc_GetButtonSwt4()							
#define				Adc_ButtonSwitch5_bl							Adc_GetButtonSwt5()							
#define				Adc_ButtonSwitch6_bl							Adc_GetButtonSwt6()							
#define				Adc_ButtonSwitch7_bl							Adc_GetButtonSwt7()							
#define				DIAG_Select_u8									D_MVSELECT_USER_U8							
#define				Cdd_Hes1LastCallTime_u32						Cdd_HesR2LLastCallTime_u32					
#define				Cdd_Hes2LastCallTime_u32						Cdd_HesR2RLastCallTime_u32					
#define				Cdd_Hes1PulseCount_u16							Cdd_HesR2LPulseCount_u16					
#define				Cdd_Hes2PulseCount_u16							Cdd_HesR2RPulseCount_u16					
#define				LIB_SystemTime_u32								Gpt_GetSysTime()							
#define				DIAG_Move1_u8									D_MV_NONE_U8								
#define				DIAG_Move2_u8									D_MV_NONE_U8								
#define				DIAG_Move3_u8									D_MV_NONE_U8								
#define				DIAG_Move4_u8									D_MV_NONE_U8								
#define				DIAG_Move5_u8									D_MV_NONE_U8								
#define				DIAG_Move6_u8									D_MV_NONE_U8								
#define				DIAG_Move7_u8									D_MV_NONE_U8								
#define				NVM_Status_u8									D_NVM_IDLE_U8								
#define				NVM_Mtr1CoursePulse_u16							260U										
#define				NVM_Mtr2CoursePulse_u16							260U										


extern uint16* NVM_SysMdCloseFlagTypeN_pu16[1];
#endif		/*ECU_FULL_CARD */
#endif		/*TARGETLINKTEXTERNSFULL_H */