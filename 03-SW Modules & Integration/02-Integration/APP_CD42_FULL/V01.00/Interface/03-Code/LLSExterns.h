/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : LLSExternsFull.h
** Module name  :
** -------------------------------------------------------------------------------------------------
** Description : File which provides the link between Low level Software and FORD FULL configuration
** -------------------------------------------------------------------------------------------------
**
** Documentation reference  :
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 01/10/2013
** - First release
**
***************************************************************************************************/
/*To avoid multi-inclusions */
#ifndef LLSEXTERNSFULL_H
#define LLSEXTERNSFULL_H
/**************************************** Inclusion files *****************************************/
/* #include "MTRCMD.h" */
/* #include "SYSMD.h"  */
/* #include "PWMCMD.h" */
#include "main.h"
/********************** Declaration of global symbol and constants ********************************/

/********************************* Declaration of global macros ***********************************/
/* Mapping between LLS and targetlink for DIO Module */
#define             DIO_MCUKEEPALIVE                         SYSMD_KeepAlive_bl
#define             DIO_RELAYR3RP                            MTRCMD_MtrRlyR3R_P_bl
#define             DIO_RELAYR3COM                           MTRCMD_MtrRlyCom_bl
#define             DIO_RELAYR3LP                            MTRCMD_MtrRlyR3L_P_bl
#define             DIO_GLOBALCMD                            MTRCMD_GlobalMtrRly_bl
#define             DIO_HESFECENABLE                         MTRCMD_HesFetEnable_bl
/* Mapping between LLS and targetlink for PWM Module */
#define             PWM_DUTYCYCLE3                           PWMCMD_DutyCycle1_u8
#define             PWM_DUTYCYCLE2                           PWMCMD_DutyCycle2_u8
#define             PWM_DUTYCYCLE1                           PWMCMD_DutyCycle3_u8
#define             PWM_DUTYCYCLE4                           PWMCMD_DutyCycle4_u8
#define             PWM_DUTYCYCLE5                           PWMCMD_DutyCycle5_u8
#define             PWM_DUTYCYCLE7                           PWMCMD_DutyCycle6_u8
#define             PWM_DUTYCYCLE6                           PWMCMD_DutyCycle7_u8
/* Mapping between LLS and targetlink CDD Module */
#define             CDD_HESR2RRESET                          MTRCMD_ResetHes1_bl
#define             CDD_HESR2LRESET                          MTRCMD_ResetHes2_bl
/********************************* Declaration of global types ************************************/

/****************************** External links of global variables ********************************/

/****************************** External links of global constants ********************************/

/***************************************************************************************************
**                                            FUNCTIONS                                           **
***************************************************************************************************/

#endif /* LLSEXTERNSFULL_H */


