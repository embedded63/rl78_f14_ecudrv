/*******************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -----------------------------------------------------------------------------
** File Name : Compiler.h
** -----------------------------------------------------------------------------
**
** Description : IAR compiler abstraction
**
** -----------------------------------------------------------------------------
**
** Documentation reference : AUTOSAR_SWS_CompilerAbstraction.pdf
**
********************************************************************************
** R E V I S I O N H I S T O R Y
********************************************************************************
** V01.00 P.Bonnet 13/10/2011
** - Baseline Created
** -----------------------------------------------------------------------------
*******************************************************************************/
/* To avoid multi-inclusions */
#ifndef COMPILER_H
#define COMPILER_H
/*************************** Inclusion files **********************************/
#include "Compiler_Cfg.h"
/**************** Declaration of global symbol and constants ******************/
#define AUTOMATIC
#define TYPEDEF
#define STATIC static
#define NULL_PTR ((void *)0)
#define INLINE inline
#define CODETYPE
#define VARTYPE
#define CONSTTYPE
#define PTR2VARTYPE
#define PTRTYPE
/******************** Declaration of global macros ****************************/
/* MISRA Rule violation 19.10: Each instance of parameter in macro is not enclosed 
as they  represent type */
#define FUNC(rettype, CODETYPE) rettype
#define VAR(vartype, VARTYPE) vartype
#define CONST(consttype, CONSTTYPE) const consttype
#define P2VAR(ptrtype, PTR2VARTYPE, PTRTYPE) ptrtype *
#define P2CONST(ptrtype, memclass, ptrclass) const ptrtype *
#define CONSTP2VAR(ptrtype, memclass, ptrclass) ptrtype * const
#define CONSTP2CONST(ptrtype, memclass, ptrclass) const ptrtype * const
#define P2FUNC(rettype, ptrclass, fctname) rettype (* fctname)
#define CONSTP2FUNC(rettype, ptrclass, fctname) rettype (*const fctname)
/********************* Declaration of global types ****************************/
#if 0 /* 25Apr2012 */
typedef uint8 Std_ReturnType;
typedef struct
{
VAR(uint16, TYPEDEF) vendorID;
VAR(uint16, TYPEDEF) moduleID;
VAR(uint8, TYPEDEF) instanceID;
VAR(uint8, TYPEDEF) sw_major_version;
VAR(uint8, TYPEDEF) sw_minor_version;
VAR(uint8, TYPEDEF) sw_patch_version;
} Std_VersionInfoType;
#endif /*#if 0  25Apr2012 */
#endif /* COMPILER_H */
