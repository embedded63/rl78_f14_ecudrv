/*******************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -----------------------------------------------------------------------------
** File Name : Compiler_Cfg.h
** -----------------------------------------------------------------------------
**
** Description : compiler configuration abstraction
**
** -----------------------------------------------------------------------------
**
** Documentation reference : AUTOSAR_SWS_CompilerAbstraction.pdf
**
********************************************************************************
** R E V I S I O N H I S T O R Y
********************************************************************************
** V01.00 P.Bonnet 13/10/2011
** - Baseline Created

*******************************************************************************/
/* To avoid multi-inclusions */
#ifndef COMPILER_CFG_H
#define COMPILER_CFG_H



/**************** Declaration of global symbol and constants ******************/
#define NEAR
#define FAR
/******************************************************************************/
/* RTSCU */
/******************************************************************************/
#define RTSCU_CODE FAR
#define RTSCU_VAR_NOINIT FAR
#define RTSCU_VAR_POWER_ON_INIT FAR
#define RTSCU_VAR_FAST FAR
#define RTSCU_VAR FAR
#define RTSCU_CONST FAR
#define RTSCU_APPL_DATA FAR
#define RTSCU_APPL_CONST FAR
#define RTSCU_APPL_CODE FAR
#define RTSCU_CALLOUT_CODE FAR

#endif /* COMPILER_CFG_H */
