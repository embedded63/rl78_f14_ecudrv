/*******************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -----------------------------------------------------------------------------
** File Name : Std_Types.h
** -----------------------------------------------------------------------------
**
** Description : Standard Types
**
** -----------------------------------------------------------------------------
**
** Documentation reference : AUTOSAR_SWS_StandardTypes.pdf
**
********************************************************************************
** R E V I S I O N H I S T O R Y
********************************************************************************
** V01.00 P.Bonnet 13/10/2011
** - Baseline Created
*******************************************************************************/
/* To avoid multi-inclusions */
#ifndef STD_TYPES_H
#define STD_TYPES_H
/*************************** Inclusion files **********************************/

#include "Platform_Types.h"
#include "Compiler.h"

/**************** Declaration of global symbol and constants ******************/

/********************* Declaration of global types ****************************/
typedef uint8 Std_ReturnType;
typedef struct
{
  VAR(uint16, TYPEDEF) vendorID;
  VAR(uint16, TYPEDEF) moduleID;
  VAR(uint8, TYPEDEF) instanceID;
  VAR(uint8, TYPEDEF) sw_major_version;
  VAR(uint8, TYPEDEF) sw_minor_version;
  VAR(uint8, TYPEDEF) sw_patch_version;
} Std_VersionInfoType;

#endif /* STD_TYPES_H */
