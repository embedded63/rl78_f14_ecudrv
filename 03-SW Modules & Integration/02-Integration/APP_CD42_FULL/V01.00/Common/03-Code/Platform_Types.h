/*******************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -----------------------------------------------------------------------------
** File Name : Platform_Types.h
** -----------------------------------------------------------------------------
**
** Description : Types associated to Renesas and IAR.
**
** -----------------------------------------------------------------------------
**
** Documentation reference : AUTOSAR_SWS_PlatformTypes.pdf
**
********************************************************************************
** R E V I S I O N H I S T O R Y
********************************************************************************
** V01.00  27/09/2013
** - First Version
**
*******************************************************************************/
/* To avoid multi-inclusions */
#ifndef PLATFORM_TYPES_H
#define PLATFORM_TYPES_H

/* IAR Systems Includes  */
#include "intrinsics.h"
#if defined(LIGHT_VERSION)
#include "ior5f109be.h"
#include "ior5f109be_ext.h"
#else
#include "ior5f10plf.h"
#include "ior5f10plf_ext.h"
#endif

/**************** Declaration of global symbol and constants ******************/
#define FALSE 0u
#define TRUE  1u
#define CPU_TYPE CPU_TYPE_32
#define CPU_BIT_ORDER LSB_FIRST
#define CPU_BYTE_ORDER HIGH_BYTE_FIRST
/********************* Declaration of global types ****************************/
typedef unsigned char boolean;
typedef signed char sint8;
typedef unsigned char uint8;
typedef signed short sint16;
typedef unsigned short uint16;
typedef signed long sint32;
typedef unsigned long uint32;
typedef float float32;
typedef double float64;

#endif /* PLATFORM_TYPES_H */
