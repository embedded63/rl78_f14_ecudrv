/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : MemMap.h
** -------------------------------------------------------------------------------------------------
**
** Description : Memory mapping
**
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : AUTOSAR_SWS_MemoryMapping.pdf
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 13/09/2013
** - First release
***************************************************************************************************/

/************************** Declaration of global symbol and constants ****************************/
/* MISRA Rule violation 19.15: Header file protection is not included intentionally as this file is needed 
multiple times */
#define MEMMAP_ERROR

/**************************************************************************************************/
/*                                       DIO                                                      */
/**************************************************************************************************/
#ifdef DIO_START_SEC_INIT_CODE
    #undef DIO_START_SEC_INIT_CODE
    #define START_SECTION_COMMON_INIT_CODE
#endif
#ifdef DIO_STOP_SEC_INIT_CODE
    #undef DIO_STOP_SEC_INIT_CODE
    #define STOP_SECTION_COMMON_INIT_CODE
#endif

#ifdef DIO_START_SEC_CODE
    #undef DIO_START_SEC_CODE
    #define START_SECTION_COMMON_CODE
#endif
#ifdef DIO_STOP_SEC_CODE
    #undef DIO_STOP_SEC_CODE
    #define STOP_SECTION_COMMON_CODE
#endif

#ifdef DIO_START_SEC_ISR_CODE
    #undef DIO_START_SEC_ISR_CODE
    #define START_SECTION_COMMON_ISR_CODE
#endif
#ifdef DIO_STOP_SEC_ISR_CODE
    #undef DIO_STOP_SEC_ISR_CODE
    #define STOP_SECTION_COMMON_ISR_CODE
#endif

#ifdef DIO_START_SEC_VAR_BOOLEAN
    #undef DIO_START_SEC_VAR_BOOLEAN
    #define START_SECTION_COMMON_VAR_BOOLEAN
#endif
#ifdef DIO_STOP_SEC_VAR_BOOLEAN
    #undef DIO_STOP_SEC_VAR_BOOLEAN
    #define STOP_SECTION_COMMON_VAR_BOOLEAN
#endif

#ifdef DIO_START_SEC_VAR_8BITS
    #undef DIO_START_SEC_VAR_8BITS
    #define START_SECTION_COMMON_VAR_8BITS
#endif
#ifdef DIO_STOP_SEC_VAR_8BITS
    #undef DIO_STOP_SEC_VAR_8BITS
    #define STOP_SECTION_COMMON_VAR_8BITS
#endif

#ifdef DIO_START_SEC_VAR_16BITS
    #undef DIO_START_SEC_VAR_16BITS
    #define START_SECTION_COMMON_VAR_16BITS
#endif
#ifdef DIO_STOP_SEC_VAR_16BITS
    #undef DIO_STOP_SEC_VAR_16BITS
    #define STOP_SECTION_COMMON_VAR_16BITS
#endif

#ifdef DIO_START_SEC_VAR_32BITS
    #undef DIO_START_SEC_VAR_32BITS
    #define START_SECTION_COMMON_VAR_32BITS
#endif
#ifdef DIO_STOP_SEC_VAR_32BITS
    #undef DIO_STOP_SEC_VAR_32BITS
    #define STOP_SECTION_COMMON_VAR_32BITS
#endif

#ifdef DIO_START_SEC_VAR_NOINIT_BOOLEAN
    #undef DIO_START_SEC_VAR_NOINIT_BOOLEAN
    #define START_SECTION_COMMON_VAR_NOINIT_BOOLEAN
#endif
#ifdef DIO_STOP_SEC_VAR_NOINIT_BOOLEAN
    #undef DIO_STOP_SEC_VAR_NOINIT_BOOLEAN
    #define STOP_SECTION_COMMON_VAR_NOINIT_BOOLEAN
#endif

#ifdef DIO_START_SEC_VAR_NOINIT_8BITS
    #undef DIO_START_SEC_VAR_NOINIT_8BITS
    #define START_SECTION_COMMON_VAR_NOINIT_8BITS
#endif
#ifdef DIO_STOP_SEC_VAR_NOINIT_8BITS
    #undef DIO_STOP_SEC_VAR_NOINIT_8BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_8BITS
#endif

#ifdef DIO_START_SEC_VAR_NOINIT_16BITS
    #undef DIO_START_SEC_VAR_NOINIT_16BITS
    #define START_SECTION_COMMON_VAR_NOINIT_16BITS
#endif
#ifdef DIO_STOP_SEC_VAR_NOINIT_16BITS
    #undef DIO_STOP_SEC_VAR_NOINIT_16BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_16BITS
#endif

#ifdef DIO_START_SEC_VAR_NOINIT_32BITS
    #undef DIO_START_SEC_VAR_NOINIT_32BITS
    #define START_SECTION_COMMON_VAR_NOINIT_32BITS
#endif
#ifdef DIO_STOP_SEC_VAR_NOINIT_32BITS
    #undef DIO_STOP_SEC_VAR_NOINIT_32BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_32BITS
#endif

#ifdef DIO_START_SEC_VAR_NOINIT_UNSPECIFIED
    #undef DIO_START_SEC_VAR_NOINIT_UNSPECIFIED
    #define START_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
#endif
#ifdef DIO_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #undef DIO_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #define STOP_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
#endif

#ifdef DIO_START_SEC_CONST_8BITS
    #undef DIO_START_SEC_CONST_8BITS
    #define START_SECTION_COMMON_CONST_8BITS
#endif
#ifdef DIO_STOP_SEC_CONST_8BITS
    #undef DIO_STOP_SEC_CONST_8BITS
    #define STOP_SECTION_COMMON_CONST_8BITS
#endif

#ifdef DIO_START_SEC_CONST_16BITS
    #undef DIO_START_SEC_CONST_16BITS
    #define START_SECTION_COMMON_CONST_16BITS
#endif
#ifdef DIO_STOP_SEC_CONST_16BITS
    #undef DIO_STOP_SEC_CONST_16BITS
    #define STOP_SECTION_COMMON_CONST_16BITS
#endif

#ifdef DIO_START_SEC_CONST_32BITS
    #undef DIO_START_SEC_CONST_32BITS
    #define START_SECTION_COMMON_CONST_32BITS
#endif
#ifdef DIO_STOP_SEC_CONST_32BITS
    #undef DIO_STOP_SEC_CONST_32BITS
    #define STOP_SECTION_COMMON_CONST_32BITS
#endif

#ifdef DIO_START_SEC_CONST_UNSPECIFIED
    #undef DIO_START_SEC_CONST_UNSPECIFIED
    #define START_SECTION_COMMON_CONST_UNSPECIFIED
#endif
#ifdef DIO_STOP_SEC_CONST_UNSPECIFIED
    #undef DIO_STOP_SEC_CONST_UNSPECIFIED
    #define STOP_SECTION_COMMON_CONST_UNSPECIFIED
#endif

/**************************************************************************************************/
/*                                       ADC                                                      */
/**************************************************************************************************/
#ifdef ADC_START_SEC_INIT_CODE
    #undef ADC_START_SEC_INIT_CODE
    #define START_SECTION_COMMON_INIT_CODE
#endif
#ifdef ADC_STOP_SEC_INIT_CODE
    #undef ADC_STOP_SEC_INIT_CODE
    #define STOP_SECTION_COMMON_INIT_CODE
#endif

#ifdef ADC_START_SEC_CODE
    #undef ADC_START_SEC_CODE
    #define START_SECTION_COMMON_CODE
#endif
#ifdef ADC_STOP_SEC_CODE
    #undef ADC_STOP_SEC_CODE
    #define STOP_SECTION_COMMON_CODE
#endif

#ifdef ADC_START_SEC_ISR_CODE
    #undef ADC_START_SEC_ISR_CODE
    #define START_SECTION_COMMON_ISR_CODE
#endif
#ifdef ADC_STOP_SEC_ISR_CODE
    #undef ADC_STOP_SEC_ISR_CODE
    #define STOP_SECTION_COMMON_ISR_CODE
#endif

#ifdef ADC_START_SEC_VAR_BOOLEAN
    #undef ADC_START_SEC_VAR_BOOLEAN
    #define START_SECTION_COMMON_VAR_BOOLEAN
#endif
#ifdef ADC_STOP_SEC_VAR_BOOLEAN
    #undef ADC_STOP_SEC_VAR_BOOLEAN
    #define STOP_SECTION_COMMON_VAR_BOOLEAN
#endif

#ifdef ADC_START_SEC_VAR_8BITS
    #undef ADC_START_SEC_VAR_8BITS
    #define START_SECTION_COMMON_VAR_8BITS
#endif
#ifdef ADC_STOP_SEC_VAR_8BITS
    #undef ADC_STOP_SEC_VAR_8BITS
    #define STOP_SECTION_COMMON_VAR_8BITS
#endif

#ifdef ADC_START_SEC_VAR_16BITS
    #undef ADC_START_SEC_VAR_16BITS
    #define START_SECTION_COMMON_VAR_16BITS
#endif
#ifdef ADC_STOP_SEC_VAR_16BITS
    #undef ADC_STOP_SEC_VAR_16BITS
    #define STOP_SECTION_COMMON_VAR_16BITS
#endif

#ifdef ADC_START_SEC_VAR_32BITS
    #undef ADC_START_SEC_VAR_32BITS
    #define START_SECTION_COMMON_VAR_32BITS
#endif
#ifdef ADC_STOP_SEC_VAR_32BITS
    #undef ADC_STOP_SEC_VAR_32BITS
    #define STOP_SECTION_COMMON_VAR_32BITS
#endif

#ifdef ADC_START_SEC_VAR_NOINIT_8BITS
    #undef ADC_START_SEC_VAR_NOINIT_8BITS
    #define START_SECTION_COMMON_VAR_NOINIT_8BITS
#endif
#ifdef ADC_STOP_SEC_VAR_NOINIT_8BITS
    #undef ADC_STOP_SEC_VAR_NOINIT_8BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_8BITS
#endif

#ifdef ADC_START_SEC_VAR_NOINIT_16BITS
    #undef ADC_START_SEC_VAR_NOINIT_16BITS
    #define START_SECTION_COMMON_VAR_NOINIT_16BITS
#endif
#ifdef ADC_STOP_SEC_VAR_NOINIT_16BITS
    #undef ADC_STOP_SEC_VAR_NOINIT_16BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_16BITS
#endif

#ifdef ADC_START_SEC_VAR_NOINIT_32BITS
    #undef ADC_START_SEC_VAR_NOINIT_32BITS
    #define START_SECTION_COMMON_VAR_NOINIT_32BITS
#endif
#ifdef ADC_STOP_SEC_VAR_NOINIT_32BITS
    #undef ADC_STOP_SEC_VAR_NOINIT_32BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_32BITS
#endif

#ifdef ADC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #undef ADC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #define START_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
#endif
#ifdef ADC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #undef ADC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #define STOP_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
#endif

#ifdef ADC_START_SEC_CONST_8BITS
    #undef ADC_START_SEC_CONST_8BITS
    #define START_SECTION_COMMON_CONST_8BITS
#endif
#ifdef ADC_STOP_SEC_CONST_8BITS
    #undef ADC_STOP_SEC_CONST_8BITS
    #define STOP_SECTION_COMMON_CONST_8BITS
#endif

#ifdef ADC_START_SEC_CONST_16BITS
    #undef ADC_START_SEC_CONST_16BITS
    #define START_SECTION_COMMON_CONST_16BITS
#endif
#ifdef ADC_STOP_SEC_CONST_16BITS
    #undef ADC_STOP_SEC_CONST_16BITS
    #define STOP_SECTION_COMMON_CONST_16BITS
#endif

#ifdef ADC_START_SEC_CONST_32BITS
    #undef ADC_START_SEC_CONST_32BITS
    #define START_SECTION_COMMON_CONST_32BITS
#endif
#ifdef ADC_STOP_SEC_CONST_32BITS
    #undef ADC_STOP_SEC_CONST_32BITS
    #define STOP_SECTION_COMMON_CONST_32BITS
#endif

#ifdef ADC_START_SEC_CONST_UNSPECIFIED
    #undef ADC_START_SEC_CONST_UNSPECIFIED
    #define START_SECTION_COMMON_CONST_UNSPECIFIED
#endif
#ifdef ADC_STOP_SEC_CONST_UNSPECIFIED
    #undef ADC_STOP_SEC_CONST_UNSPECIFIED
    #define STOP_SECTION_COMMON_CONST_UNSPECIFIED
#endif

/**************************************************************************************************/
/*                                       GPT                                                     */
/**************************************************************************************************/
#ifdef GPT_START_SEC_INIT_CODE
    #undef GPT_START_SEC_INIT_CODE
    #define START_SECTION_COMMON_INIT_CODE
#endif
#ifdef GPT_STOP_SEC_INIT_CODE
    #undef GPT_STOP_SEC_INIT_CODE
    #define STOP_SECTION_COMMON_INIT_CODE
#endif

#ifdef GPT_START_SEC_CODE
    #undef GPT_START_SEC_CODE
    #define START_SECTION_COMMON_CODE
#endif
#ifdef GPT_STOP_SEC_CODE
    #undef GPT_STOP_SEC_CODE
    #define STOP_SECTION_COMMON_CODE
#endif

#ifdef GPT_START_SEC_ISR_CODE
    #undef GPT_START_SEC_ISR_CODE
    #define START_SECTION_COMMON_ISR_CODE
#endif
#ifdef GPT_STOP_SEC_ISR_CODE
    #undef GPT_STOP_SEC_ISR_CODE
    #define STOP_SECTION_COMMON_ISR_CODE
#endif

#ifdef GPT_START_SEC_VAR_BOOLEAN
    #undef GPT_START_SEC_VAR_BOOLEAN
    #define START_SECTION_COMMON_VAR_BOOLEAN
#endif
#ifdef GPT_STOP_SEC_VAR_BOOLEAN
    #undef GPT_STOP_SEC_VAR_BOOLEAN
    #define STOP_SECTION_COMMON_VAR_BOOLEAN
#endif

#ifdef GPT_START_SEC_VAR_8BITS
    #undef GPT_START_SEC_VAR_8BITS
    #define START_SECTION_COMMON_VAR_8BITS
#endif
#ifdef GPT_STOP_SEC_VAR_8BITS
    #undef GPT_STOP_SEC_VAR_8BITS
    #define STOP_SECTION_COMMON_VAR_8BITS
#endif

#ifdef GPT_START_SEC_VAR_16BITS
    #undef GPT_START_SEC_VAR_16BITS
    #define START_SECTION_COMMON_VAR_16BITS
#endif
#ifdef GPT_STOP_SEC_VAR_16BITS
    #undef GPT_STOP_SEC_VAR_16BITS
    #define STOP_SECTION_COMMON_VAR_16BITS
#endif

#ifdef GPT_START_SEC_VAR_32BITS
    #undef GPT_START_SEC_VAR_32BITS
    #define START_SECTION_COMMON_VAR_32BITS
#endif
#ifdef GPT_STOP_SEC_VAR_32BITS
    #undef GPT_STOP_SEC_VAR_32BITS
    #define STOP_SECTION_COMMON_VAR_32BITS
#endif

#ifdef GPT_START_SEC_VAR_NOINIT_BOOLEAN
    #undef GPT_START_SEC_VAR_NOINIT_BOOLEAN
    #define START_SECTION_COMMON_VAR_NOINIT_BOOLEAN
#endif
#ifdef GPT_STOP_SEC_VAR_NOINIT_BOOLEAN
    #undef GPT_STOP_SEC_VAR_NOINIT_BOOLEAN
    #define STOP_SECTION_COMMON_VAR_NOINIT_BOOLEAN
#endif

#ifdef GPT_START_SEC_VAR_NOINIT_8BITS
    #undef GPT_START_SEC_VAR_NOINIT_8BITS
    #define START_SECTION_COMMON_VAR_NOINIT_8BITS
#endif
#ifdef GPT_STOP_SEC_VAR_NOINIT_8BITS
    #undef GPT_STOP_SEC_VAR_NOINIT_8BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_8BITS
#endif

#ifdef GPT_START_SEC_VAR_NOINIT_16BITS
    #undef GPT_START_SEC_VAR_NOINIT_16BITS
    #define START_SECTION_COMMON_VAR_NOINIT_16BITS
#endif
#ifdef GPT_STOP_SEC_VAR_NOINIT_16BITS
    #undef GPT_STOP_SEC_VAR_NOINIT_16BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_16BITS
#endif

#ifdef GPT_START_SEC_VAR_NOINIT_32BITS
    #undef GPT_START_SEC_VAR_NOINIT_32BITS
    #define START_SECTION_COMMON_VAR_NOINIT_32BITS
#endif
#ifdef GPT_STOP_SEC_VAR_NOINIT_32BITS
    #undef GPT_STOP_SEC_VAR_NOINIT_32BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_32BITS
#endif

#ifdef GPT_START_SEC_VAR_NOINIT_UNSPECIFIED
    #undef GPT_START_SEC_VAR_NOINIT_UNSPECIFIED
    #define START_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
#endif
#ifdef GPT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #undef GPT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #define STOP_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
#endif

#ifdef GPT_START_SEC_CONST_8BITS
    #undef GPT_START_SEC_CONST_8BITS
    #define START_SECTION_COMMON_CONST_8BITS
#endif
#ifdef GPT_STOP_SEC_CONST_8BITS
    #undef GPT_STOP_SEC_CONST_8BITS
    #define STOP_SECTION_COMMON_CONST_8BITS
#endif

#ifdef GPT_START_SEC_CONST_16BITS
    #undef GPT_START_SEC_CONST_16BITS
    #define START_SECTION_COMMON_CONST_16BITS
#endif
#ifdef GPT_STOP_SEC_CONST_16BITS
    #undef GPT_STOP_SEC_CONST_16BITS
    #define STOP_SECTION_COMMON_CONST_16BITS
#endif

#ifdef GPT_START_SEC_CONST_32BITS
    #undef GPT_START_SEC_CONST_32BITS
    #define START_SECTION_COMMON_CONST_32BITS
#endif
#ifdef GPT_STOP_SEC_CONST_32BITS
    #undef GPT_STOP_SEC_CONST_32BITS
    #define STOP_SECTION_COMMON_CONST_32BITS
#endif

#ifdef GPT_START_SEC_CONST_UNSPECIFIED
    #undef GPT_START_SEC_CONST_UNSPECIFIED
    #define START_SECTION_COMMON_CONST_UNSPECIFIED
#endif
#ifdef GPT_STOP_SEC_CONST_UNSPECIFIED
    #undef GPT_STOP_SEC_CONST_UNSPECIFIED
    #define STOP_SECTION_COMMON_CONST_UNSPECIFIED
#endif

/**************************************************************************************************/
/*                                       SCHM                                                      */
/**************************************************************************************************/
#ifdef SCHM_START_SEC_INIT_CODE
    #undef SCHM_START_SEC_INIT_CODE
    #define START_SECTION_COMMON_INIT_CODE
#endif
#ifdef SCHM_STOP_SEC_INIT_CODE
    #undef SCHM_STOP_SEC_INIT_CODE
    #define STOP_SECTION_COMMON_INIT_CODE
#endif

#ifdef SCHM_START_SEC_CODE
    #undef SCHM_START_SEC_CODE
    #define START_SECTION_COMMON_CODE
#endif
#ifdef SCHM_STOP_SEC_CODE
    #undef SCHM_STOP_SEC_CODE
    #define STOP_SECTION_COMMON_CODE
#endif

#ifdef SCHM_START_SEC_ISR_CODE
    #undef SCHM_START_SEC_ISR_CODE
    #define START_SECTION_COMMON_ISR_CODE
#endif
#ifdef SCHM_STOP_SEC_ISR_CODE
    #undef SCHM_STOP_SEC_ISR_CODE
    #define STOP_SECTION_COMMON_ISR_CODE
#endif

#ifdef SCHM_START_SEC_VAR_BOOLEAN
    #undef SCHM_START_SEC_VAR_BOOLEAN
    #define START_SECTION_COMMON_VAR_BOOLEAN
#endif
#ifdef SCHM_STOP_SEC_VAR_BOOLEAN
    #undef SCHM_STOP_SEC_VAR_BOOLEAN
    #define STOP_SECTION_COMMON_VAR_BOOLEAN
#endif

#ifdef SCHM_START_SEC_VAR_8BITS
    #undef SCHM_START_SEC_VAR_8BITS
    #define START_SECTION_COMMON_VAR_8BITS
#endif
#ifdef SCHM_STOP_SEC_VAR_8BITS
    #undef SCHM_STOP_SEC_VAR_8BITS
    #define STOP_SECTION_COMMON_VAR_8BITS
#endif

#ifdef SCHM_START_SEC_VAR_16BITS
    #undef SCHM_START_SEC_VAR_16BITS
    #define START_SECTION_COMMON_VAR_16BITS
#endif
#ifdef SCHM_STOP_SEC_VAR_16BITS
    #undef SCHM_STOP_SEC_VAR_16BITS
    #define STOP_SECTION_COMMON_VAR_16BITS
#endif

#ifdef SCHM_START_SEC_VAR_32BITS
    #undef SCHM_START_SEC_VAR_32BITS
    #define START_SECTION_COMMON_VAR_32BITS
#endif
#ifdef SCHM_STOP_SEC_VAR_32BITS
    #undef SCHM_STOP_SEC_VAR_32BITS
    #define STOP_SECTION_COMMON_VAR_32BITS
#endif

#ifdef SCHM_START_SEC_VAR_NOINIT_BOOLEAN
    #undef SCHM_START_SEC_VAR_NOINIT_BOOLEAN
    #define START_SECTION_COMMON_VAR_NOINIT_BOOLEAN
#endif
#ifdef SCHM_STOP_SEC_VAR_NOINIT_BOOLEAN
    #undef SCHM_STOP_SEC_VAR_NOINIT_BOOLEAN
    #define STOP_SECTION_COMMON_VAR_NOINIT_BOOLEAN
#endif

#ifdef SCHM_START_SEC_VAR_NOINIT_8BITS
    #undef SCHM_START_SEC_VAR_NOINIT_8BITS
    #define START_SECTION_COMMON_VAR_NOINIT_8BITS
#endif
#ifdef SCHM_STOP_SEC_VAR_NOINIT_8BITS
    #undef SCHM_STOP_SEC_VAR_NOINIT_8BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_8BITS
#endif

#ifdef SCHM_START_SEC_VAR_NOINIT_16BITS
    #undef SCHM_START_SEC_VAR_NOINIT_16BITS
    #define START_SECTION_COMMON_VAR_NOINIT_16BITS
#endif
#ifdef SCHM_STOP_SEC_VAR_NOINIT_16BITS
    #undef SCHM_STOP_SEC_VAR_NOINIT_16BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_16BITS
#endif

#ifdef SCHM_START_SEC_VAR_NOINIT_32BITS
    #undef SCHM_START_SEC_VAR_NOINIT_32BITS
    #define START_SECTION_COMMON_VAR_NOINIT_32BITS
#endif
#ifdef SCHM_STOP_SEC_VAR_NOINIT_32BITS
    #undef SCHM_STOP_SEC_VAR_NOINIT_32BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_32BITS
#endif

#ifdef SCHM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #undef SCHM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #define START_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
#endif
#ifdef SCHM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #undef SCHM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #define STOP_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
#endif

#ifdef SCHM_START_SEC_CONST_8BITS
    #undef SCHM_START_SEC_CONST_8BITS
    #define START_SECTION_COMMON_CONST_8BITS
#endif
#ifdef SCHM_STOP_SEC_CONST_8BITS
    #undef SCHM_STOP_SEC_CONST_8BITS
    #define STOP_SECTION_COMMON_CONST_8BITS
#endif

#ifdef SCHM_START_SEC_CONST_16BITS
    #undef SCHM_START_SEC_CONST_16BITS
    #define START_SECTION_COMMON_CONST_16BITS
#endif
#ifdef SCHM_STOP_SEC_CONST_16BITS
    #undef SCHM_STOP_SEC_CONST_16BITS
    #define STOP_SECTION_COMMON_CONST_16BITS
#endif

#ifdef SCHM_START_SEC_CONST_32BITS
    #undef SCHM_START_SEC_CONST_32BITS
    #define START_SECTION_COMMON_CONST_32BITS
#endif
#ifdef SCHM_STOP_SEC_CONST_32BITS
    #undef SCHM_STOP_SEC_CONST_32BITS
    #define STOP_SECTION_COMMON_CONST_32BITS
#endif

#ifdef SCHM_START_SEC_CONST_UNSPECIFIED
    #undef SCHM_START_SEC_CONST_UNSPECIFIED
    #define START_SECTION_COMMON_CONST_UNSPECIFIED
#endif
#ifdef SCHM_STOP_SEC_CONST_UNSPECIFIED
    #undef SCHM_STOP_SEC_CONST_UNSPECIFIED
    #define STOP_SECTION_COMMON_CONST_UNSPECIFIED
#endif

/**************************************************************************************************/
/*                                       ISR                                                     */
/**************************************************************************************************/
#ifdef ISR_START_SEC_INIT_CODE
    #undef ISR_START_SEC_INIT_CODE
    #define START_SECTION_COMMON_INIT_CODE
#endif
#ifdef ISR_STOP_SEC_INIT_CODE
    #undef ISR_STOP_SEC_INIT_CODE
    #define STOP_SECTION_COMMON_INIT_CODE
#endif

#ifdef ISR_START_SEC_CODE
    #undef ISR_START_SEC_CODE
    #define START_SECTION_COMMON_CODE
#endif
#ifdef ISR_STOP_SEC_CODE
    #undef ISR_STOP_SEC_CODE
    #define STOP_SECTION_COMMON_CODE
#endif

#ifdef ISR_START_SEC_ISR_CODE
    #undef ISR_START_SEC_ISR_CODE
    #define START_SECTION_COMMON_ISR_CODE
#endif
#ifdef ISR_STOP_SEC_ISR_CODE
    #undef ISR_STOP_SEC_ISR_CODE
    #define STOP_SECTION_COMMON_ISR_CODE
#endif

#ifdef ISR_START_SEC_VAR_BOOLEAN
    #undef ISR_START_SEC_VAR_BOOLEAN
    #define START_SECTION_COMMON_VAR_BOOLEAN
#endif
#ifdef ISR_STOP_SEC_VAR_BOOLEAN
    #undef ISR_STOP_SEC_VAR_BOOLEAN
    #define STOP_SECTION_COMMON_VAR_BOOLEAN
#endif

#ifdef ISR_START_SEC_VAR_8BITS
    #undef ISR_START_SEC_VAR_8BITS
    #define START_SECTION_COMMON_VAR_8BITS
#endif
#ifdef ISR_STOP_SEC_VAR_8BITS
    #undef ISR_STOP_SEC_VAR_8BITS
    #define STOP_SECTION_COMMON_VAR_8BITS
#endif

#ifdef ISR_START_SEC_VAR_16BITS
    #undef ISR_START_SEC_VAR_16BITS
    #define START_SECTION_COMMON_VAR_16BITS
#endif
#ifdef ISR_STOP_SEC_VAR_16BITS
    #undef ISR_STOP_SEC_VAR_16BITS
    #define STOP_SECTION_COMMON_VAR_16BITS
#endif

#ifdef ISR_START_SEC_VAR_32BITS
    #undef ISR_START_SEC_VAR_32BITS
    #define START_SECTION_COMMON_VAR_32BITS
#endif
#ifdef ISR_STOP_SEC_VAR_32BITS
    #undef ISR_STOP_SEC_VAR_32BITS
    #define STOP_SECTION_COMMON_VAR_32BITS
#endif

#ifdef ISR_START_SEC_VAR_NOINIT_8BITS
    #undef ISR_START_SEC_VAR_NOINIT_8BITS
    #define START_SECTION_COMMON_VAR_NOINIT_8BITS
#endif
#ifdef ISR_STOP_SEC_VAR_NOINIT_8BITS
    #undef ISR_STOP_SEC_VAR_NOINIT_8BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_8BITS
#endif

#ifdef ISR_START_SEC_VAR_NOINIT_16BITS
    #undef ISR_START_SEC_VAR_NOINIT_16BITS
    #define START_SECTION_COMMON_VAR_NOINIT_16BITS
#endif
#ifdef ISR_STOP_SEC_VAR_NOINIT_16BITS
    #undef ISR_STOP_SEC_VAR_NOINIT_16BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_16BITS
#endif

#ifdef ISR_START_SEC_VAR_NOINIT_32BITS
    #undef ISR_START_SEC_VAR_NOINIT_32BITS
    #define START_SECTION_COMMON_VAR_NOINIT_32BITS
#endif
#ifdef ISR_STOP_SEC_VAR_NOINIT_32BITS
    #undef ISR_STOP_SEC_VAR_NOINIT_32BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_32BITS
#endif

#ifdef ISR_START_SEC_VAR_NOINIT_UNSPECIFIED
    #undef ISR_START_SEC_VAR_NOINIT_UNSPECIFIED
    #define START_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
#endif
#ifdef ISR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #undef ISR_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #define STOP_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
#endif

#ifdef ISR_START_SEC_CONST_8BITS
    #undef ISR_START_SEC_CONST_8BITS
    #define START_SECTION_COMMON_CONST_8BITS
#endif
#ifdef ISR_STOP_SEC_CONST_8BITS
    #undef ISR_STOP_SEC_CONST_8BITS
    #define STOP_SECTION_COMMON_CONST_8BITS
#endif

#ifdef ISR_START_SEC_CONST_16BITS
    #undef ISR_START_SEC_CONST_16BITS
    #define START_SECTION_COMMON_CONST_16BITS
#endif
#ifdef ISR_STOP_SEC_CONST_16BITS
    #undef ISR_STOP_SEC_CONST_16BITS
    #define STOP_SECTION_COMMON_CONST_16BITS
#endif

#ifdef ISR_START_SEC_CONST_32BITS
    #undef ISR_START_SEC_CONST_32BITS
    #define START_SECTION_COMMON_CONST_32BITS
#endif
#ifdef ISR_STOP_SEC_CONST_32BITS
    #undef ISR_STOP_SEC_CONST_32BITS
    #define STOP_SECTION_COMMON_CONST_32BITS
#endif

#ifdef ISR_START_SEC_CONST_UNSPECIFIED
    #undef ISR_START_SEC_CONST_UNSPECIFIED
    #define START_SECTION_COMMON_CONST_UNSPECIFIED
#endif
#ifdef ISR_STOP_SEC_CONST_UNSPECIFIED
    #undef ISR_STOP_SEC_CONST_UNSPECIFIED
    #define STOP_SECTION_COMMON_CONST_UNSPECIFIED
#endif

/**************************************************************************************************/
/*                                       CDD                                                     */
/**************************************************************************************************/
#ifdef CDD_START_SEC_INIT_CODE
    #undef CDD_START_SEC_INIT_CODE
    #define START_SECTION_COMMON_INIT_CODE
#endif
#ifdef CDD_STOP_SEC_INIT_CODE
    #undef CDD_STOP_SEC_INIT_CODE
    #define STOP_SECTION_COMMON_INIT_CODE
#endif

#ifdef CDD_START_SEC_CODE
    #undef CDD_START_SEC_CODE
    #define START_SECTION_COMMON_CODE
#endif
#ifdef CDD_STOP_SEC_CODE
    #undef CDD_STOP_SEC_CODE
    #define STOP_SECTION_COMMON_CODE
#endif

#ifdef CDD_START_SEC_ISR_CODE
    #undef CDD_START_SEC_ISR_CODE
    #define START_SECTION_COMMON_ISR_CODE
#endif
#ifdef CDD_STOP_SEC_ISR_CODE
    #undef CDD_STOP_SEC_ISR_CODE
    #define STOP_SECTION_COMMON_ISR_CODE
#endif

#ifdef CDD_START_SEC_VAR_BOOLEAN
    #undef CDD_START_SEC_VAR_BOOLEAN
    #define START_SECTION_COMMON_VAR_BOOLEAN
#endif
#ifdef CDD_STOP_SEC_VAR_BOOLEAN
    #undef CDD_STOP_SEC_VAR_BOOLEAN
    #define STOP_SECTION_COMMON_VAR_BOOLEAN
#endif

#ifdef CDD_START_SEC_VAR_8BITS
    #undef CDD_START_SEC_VAR_8BITS
    #define START_SECTION_COMMON_VAR_8BITS
#endif
#ifdef CDD_STOP_SEC_VAR_8BITS
    #undef CDD_STOP_SEC_VAR_8BITS
    #define STOP_SECTION_COMMON_VAR_8BITS
#endif

#ifdef CDD_START_SEC_VAR_16BITS
    #undef CDD_START_SEC_VAR_16BITS
    #define START_SECTION_COMMON_VAR_16BITS
#endif
#ifdef CDD_STOP_SEC_VAR_16BITS
    #undef CDD_STOP_SEC_VAR_16BITS
    #define STOP_SECTION_COMMON_VAR_16BITS
#endif

#ifdef CDD_START_SEC_VAR_32BITS
    #undef CDD_START_SEC_VAR_32BITS
    #define START_SECTION_COMMON_VAR_32BITS
#endif
#ifdef CDD_STOP_SEC_VAR_32BITS
    #undef CDD_STOP_SEC_VAR_32BITS
    #define STOP_SECTION_COMMON_VAR_32BITS
#endif

#ifdef CDD_START_SEC_VAR_NOINIT_BOOLEAN
    #undef CDD_START_SEC_VAR_NOINIT_BOOLEAN
    #define START_SECTION_COMMON_VAR_NOINIT_BOOLEAN
#endif
#ifdef CDD_STOP_SEC_VAR_NOINIT_BOOLEAN
    #undef CDD_STOP_SEC_VAR_NOINIT_BOOLEAN
    #define STOP_SECTION_COMMON_VAR_NOINIT_BOOLEAN
#endif

#ifdef CDD_START_SEC_VAR_NOINIT_8BITS
    #undef CDD_START_SEC_VAR_NOINIT_8BITS
    #define START_SECTION_COMMON_VAR_NOINIT_8BITS
#endif
#ifdef CDD_STOP_SEC_VAR_NOINIT_8BITS
    #undef CDD_STOP_SEC_VAR_NOINIT_8BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_8BITS
#endif

#ifdef CDD_START_SEC_VAR_NOINIT_16BITS
    #undef CDD_START_SEC_VAR_NOINIT_16BITS
    #define START_SECTION_COMMON_VAR_NOINIT_16BITS
#endif
#ifdef CDD_STOP_SEC_VAR_NOINIT_16BITS
    #undef CDD_STOP_SEC_VAR_NOINIT_16BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_16BITS
#endif

#ifdef CDD_START_SEC_VAR_NOINIT_32BITS
    #undef CDD_START_SEC_VAR_NOINIT_32BITS
    #define START_SECTION_COMMON_VAR_NOINIT_32BITS
#endif
#ifdef CDD_STOP_SEC_VAR_NOINIT_32BITS
    #undef CDD_STOP_SEC_VAR_NOINIT_32BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_32BITS
#endif

#ifdef CDD_START_SEC_VAR_NOINIT_UNSPECIFIED
    #undef CDD_START_SEC_VAR_NOINIT_UNSPECIFIED
    #define START_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
#endif
#ifdef CDD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #undef CDD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #define STOP_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
#endif

#ifdef CDD_START_SEC_CONST_8BITS
    #undef CDD_START_SEC_CONST_8BITS
    #define START_SECTION_COMMON_CONST_8BITS
#endif
#ifdef CDD_STOP_SEC_CONST_8BITS
    #undef CDD_STOP_SEC_CONST_8BITS
    #define STOP_SECTION_COMMON_CONST_8BITS
#endif

#ifdef CDD_START_SEC_CONST_16BITS
    #undef CDD_START_SEC_CONST_16BITS
    #define START_SECTION_COMMON_CONST_16BITS
#endif
#ifdef CDD_STOP_SEC_CONST_16BITS
    #undef CDD_STOP_SEC_CONST_16BITS
    #define STOP_SECTION_COMMON_CONST_16BITS
#endif

#ifdef CDD_START_SEC_CONST_32BITS
    #undef CDD_START_SEC_CONST_32BITS
    #define START_SECTION_COMMON_CONST_32BITS
#endif
#ifdef CDD_STOP_SEC_CONST_32BITS
    #undef CDD_STOP_SEC_CONST_32BITS
    #define STOP_SECTION_COMMON_CONST_32BITS
#endif

#ifdef CDD_START_SEC_CONST_UNSPECIFIED
    #undef CDD_START_SEC_CONST_UNSPECIFIED
    #define START_SECTION_COMMON_CONST_UNSPECIFIED
#endif
#ifdef CDD_STOP_SEC_CONST_UNSPECIFIED
    #undef CDD_STOP_SEC_CONST_UNSPECIFIED
    #define STOP_SECTION_COMMON_CONST_UNSPECIFIED
#endif
/**************************************************************************************************/
/*                                       PWM                                                     */
/**************************************************************************************************/
#ifdef PWM_START_SEC_INIT_CODE
    #undef PWM_START_SEC_INIT_CODE
    #define START_SECTION_COMMON_INIT_CODE
#endif
#ifdef PWM_STOP_SEC_INIT_CODE
    #undef PWM_STOP_SEC_INIT_CODE
    #define STOP_SECTION_COMMON_INIT_CODE
#endif

#ifdef PWM_START_SEC_CODE
    #undef PWM_START_SEC_CODE
    #define START_SECTION_COMMON_CODE
#endif
#ifdef PWM_STOP_SEC_CODE
    #undef PWM_STOP_SEC_CODE
    #define STOP_SECTION_COMMON_CODE
#endif

#ifdef PWM_START_SEC_ISR_CODE
    #undef PWM_START_SEC_ISR_CODE
    #define START_SECTION_COMMON_ISR_CODE
#endif
#ifdef PWM_STOP_SEC_ISR_CODE
    #undef PWM_STOP_SEC_ISR_CODE
    #define STOP_SECTION_COMMON_ISR_CODE
#endif

#ifdef PWM_START_SEC_VAR_BOOLEAN
    #undef PWM_START_SEC_VAR_BOOLEAN
    #define START_SECTION_COMMON_VAR_BOOLEAN
#endif
#ifdef PWM_STOP_SEC_VAR_BOOLEAN
    #undef PWM_STOP_SEC_VAR_BOOLEAN
    #define STOP_SECTION_COMMON_VAR_BOOLEAN
#endif

#ifdef PWM_START_SEC_VAR_8BITS
    #undef PWM_START_SEC_VAR_8BITS
    #define START_SECTION_COMMON_VAR_8BITS
#endif
#ifdef PWM_STOP_SEC_VAR_8BITS
    #undef PWM_STOP_SEC_VAR_8BITS
    #define STOP_SECTION_COMMON_VAR_8BITS
#endif

#ifdef PWM_START_SEC_VAR_16BITS
    #undef PWM_START_SEC_VAR_16BITS
    #define START_SECTION_COMMON_VAR_16BITS
#endif
#ifdef PWM_STOP_SEC_VAR_16BITS
    #undef PWM_STOP_SEC_VAR_16BITS
    #define STOP_SECTION_COMMON_VAR_16BITS
#endif

#ifdef PWM_START_SEC_VAR_32BITS
    #undef PWM_START_SEC_VAR_32BITS
    #define START_SECTION_COMMON_VAR_32BITS
#endif
#ifdef PWM_STOP_SEC_VAR_32BITS
    #undef PWM_STOP_SEC_VAR_32BITS
    #define STOP_SECTION_COMMON_VAR_32BITS
#endif

#ifdef PWM_START_SEC_VAR_NOINIT_8BITS
    #undef PWM_START_SEC_VAR_NOINIT_8BITS
    #define START_SECTION_COMMON_VAR_NOINIT_8BITS
#endif
#ifdef PWM_STOP_SEC_VAR_NOINIT_8BITS
    #undef PWM_STOP_SEC_VAR_NOINIT_8BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_8BITS
#endif

#ifdef PWM_START_SEC_VAR_NOINIT_16BITS
    #undef PWM_START_SEC_VAR_NOINIT_16BITS
    #define START_SECTION_COMMON_VAR_NOINIT_16BITS
#endif
#ifdef PWM_STOP_SEC_VAR_NOINIT_16BITS
    #undef PWM_STOP_SEC_VAR_NOINIT_16BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_16BITS
#endif

#ifdef PWM_START_SEC_VAR_NOINIT_32BITS
    #undef PWM_START_SEC_VAR_NOINIT_32BITS
    #define START_SECTION_COMMON_VAR_NOINIT_32BITS
#endif
#ifdef PWM_STOP_SEC_VAR_NOINIT_32BITS
    #undef PWM_STOP_SEC_VAR_NOINIT_32BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_32BITS
#endif

#ifdef PWM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #undef PWM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #define START_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
#endif
#ifdef PWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #undef PWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #define STOP_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
#endif

#ifdef PWM_START_SEC_CONST_8BITS
    #undef PWM_START_SEC_CONST_8BITS
    #define START_SECTION_COMMON_CONST_8BITS
#endif
#ifdef PWM_STOP_SEC_CONST_8BITS
    #undef PWM_STOP_SEC_CONST_8BITS
    #define STOP_SECTION_COMMON_CONST_8BITS
#endif

#ifdef PWM_START_SEC_CONST_16BITS
    #undef PWM_START_SEC_CONST_16BITS
    #define START_SECTION_COMMON_CONST_16BITS
#endif
#ifdef PWM_STOP_SEC_CONST_16BITS
    #undef PWM_STOP_SEC_CONST_16BITS
    #define STOP_SECTION_COMMON_CONST_16BITS
#endif

#ifdef PWM_START_SEC_CONST_32BITS
    #undef PWM_START_SEC_CONST_32BITS
    #define START_SECTION_COMMON_CONST_32BITS
#endif
#ifdef PWM_STOP_SEC_CONST_32BITS
    #undef PWM_STOP_SEC_CONST_32BITS
    #define STOP_SECTION_COMMON_CONST_32BITS
#endif

#ifdef PWM_START_SEC_CONST_UNSPECIFIED
    #undef PWM_START_SEC_CONST_UNSPECIFIED
    #define START_SECTION_COMMON_CONST_UNSPECIFIED
#endif
#ifdef PWM_STOP_SEC_CONST_UNSPECIFIED
    #undef PWM_STOP_SEC_CONST_UNSPECIFIED
    #define STOP_SECTION_COMMON_CONST_UNSPECIFIED
#endif

/**************************************************************************************************/
/*                                       WDG                                                     */
/**************************************************************************************************/
#ifdef WDG_START_SEC_INIT_CODE
    #undef WDG_START_SEC_INIT_CODE
    #define START_SECTION_COMMON_INIT_CODE
#endif
#ifdef WDG_STOP_SEC_INIT_CODE
    #undef WDG_STOP_SEC_INIT_CODE
    #define STOP_SECTION_COMMON_INIT_CODE
#endif

#ifdef WDG_START_SEC_CODE
    #undef WDG_START_SEC_CODE
    #define START_SECTION_COMMON_CODE
#endif
#ifdef WDG_STOP_SEC_CODE
    #undef WDG_STOP_SEC_CODE
    #define STOP_SECTION_COMMON_CODE
#endif

#ifdef WDG_START_SEC_WDG_CODE
    #undef WDG_START_SEC_WDG_CODE
    #define START_SECTION_COMMON_WDG_CODE
#endif
#ifdef WDG_STOP_SEC_WDG_CODE
    #undef WDG_STOP_SEC_WDG_CODE
    #define STOP_SECTION_COMMON_WDG_CODE
#endif

#ifdef WDG_START_SEC_VAR_BOOLEAN
    #undef WDG_START_SEC_VAR_BOOLEAN
    #define START_SECTION_COMMON_VAR_BOOLEAN
#endif
#ifdef WDG_STOP_SEC_VAR_BOOLEAN
    #undef WDG_STOP_SEC_VAR_BOOLEAN
    #define STOP_SECTION_COMMON_VAR_BOOLEAN
#endif

#ifdef WDG_START_SEC_VAR_8BITS
    #undef WDG_START_SEC_VAR_8BITS
    #define START_SECTION_COMMON_VAR_8BITS
#endif
#ifdef WDG_STOP_SEC_VAR_8BITS
    #undef WDG_STOP_SEC_VAR_8BITS
    #define STOP_SECTION_COMMON_VAR_8BITS
#endif

#ifdef WDG_START_SEC_VAR_16BITS
    #undef WDG_START_SEC_VAR_16BITS
    #define START_SECTION_COMMON_VAR_16BITS
#endif
#ifdef WDG_STOP_SEC_VAR_16BITS
    #undef WDG_STOP_SEC_VAR_16BITS
    #define STOP_SECTION_COMMON_VAR_16BITS
#endif

#ifdef WDG_START_SEC_VAR_32BITS
    #undef WDG_START_SEC_VAR_32BITS
    #define START_SECTION_COMMON_VAR_32BITS
#endif
#ifdef WDG_STOP_SEC_VAR_32BITS
    #undef WDG_STOP_SEC_VAR_32BITS
    #define STOP_SECTION_COMMON_VAR_32BITS
#endif

#ifdef WDG_START_SEC_VAR_NOINIT_8BITS
    #undef WDG_START_SEC_VAR_NOINIT_8BITS
    #define START_SECTION_COMMON_VAR_NOINIT_8BITS
#endif
#ifdef WDG_STOP_SEC_VAR_NOINIT_8BITS
    #undef WDG_STOP_SEC_VAR_NOINIT_8BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_8BITS
#endif

#ifdef WDG_START_SEC_VAR_NOINIT_16BITS
    #undef WDG_START_SEC_VAR_NOINIT_16BITS
    #define START_SECTION_COMMON_VAR_NOINIT_16BITS
#endif
#ifdef WDG_STOP_SEC_VAR_NOINIT_16BITS
    #undef WDG_STOP_SEC_VAR_NOINIT_16BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_16BITS
#endif

#ifdef WDG_START_SEC_VAR_NOINIT_32BITS
    #undef WDG_START_SEC_VAR_NOINIT_32BITS
    #define START_SECTION_COMMON_VAR_NOINIT_32BITS
#endif
#ifdef WDG_STOP_SEC_VAR_NOINIT_32BITS
    #undef WDG_STOP_SEC_VAR_NOINIT_32BITS
    #define STOP_SECTION_COMMON_VAR_NOINIT_32BITS
#endif

#ifdef WDG_START_SEC_VAR_NOINIT_UNSPECIFIED
    #undef WDG_START_SEC_VAR_NOINIT_UNSPECIFIED
    #define START_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
#endif
#ifdef WDG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #undef WDG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #define STOP_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
#endif

#ifdef WDG_START_SEC_CONST_8BITS
    #undef WDG_START_SEC_CONST_8BITS
    #define START_SECTION_COMMON_CONST_8BITS
#endif
#ifdef WDG_STOP_SEC_CONST_8BITS
    #undef WDG_STOP_SEC_CONST_8BITS
    #define STOP_SECTION_COMMON_CONST_8BITS
#endif

#ifdef WDG_START_SEC_CONST_16BITS
    #undef WDG_START_SEC_CONST_16BITS
    #define START_SECTION_COMMON_CONST_16BITS
#endif
#ifdef WDG_STOP_SEC_CONST_16BITS
    #undef WDG_STOP_SEC_CONST_16BITS
    #define STOP_SECTION_COMMON_CONST_16BITS
#endif

#ifdef WDG_START_SEC_CONST_32BITS
    #undef WDG_START_SEC_CONST_32BITS
    #define START_SECTION_COMMON_CONST_32BITS
#endif
#ifdef WDG_STOP_SEC_CONST_32BITS
    #undef WDG_STOP_SEC_CONST_32BITS
    #define STOP_SECTION_COMMON_CONST_32BITS
#endif

#ifdef WDG_START_SEC_CONST_UNSPECIFIED
    #undef WDG_START_SEC_CONST_UNSPECIFIED
    #define START_SECTION_COMMON_CONST_UNSPECIFIED
#endif
#ifdef WDG_STOP_SEC_CONST_UNSPECIFIED
    #undef WDG_STOP_SEC_CONST_UNSPECIFIED
    #define STOP_SECTION_COMMON_CONST_UNSPECIFIED
#endif

/**************************************************************************************************/
/*                                      COMMON                                                    */
/**************************************************************************************************/
/*************************************** INIT *****************************************************/
#ifdef STOP_SECTION_COMMON_INIT_CODE
    #ifdef START_SECTION_COMMON_INIT_CODE
        #undef START_SECTION_COMMON_INIT_CODE
        #undef SECTION_STARTED
        /*#pragma*/
        #undef STOP_SECTION_COMMON_INIT_CODE
        #undef MEMMAP_ERROR
    #else
        #error "STOP_SECTION_COMMON_INIT_CODE: This section has not been previously started !"
    #endif
#endif

#ifdef START_SECTION_COMMON_INIT_CODE
    #ifndef SECTION_STARTED
        #define SECTION_STARTED
        /*#pragma*/

        #undef MEMMAP_ERROR
    #else
        #error "START_SECTION_COMMON_INIT_CODE: A section has already been started !"
    #endif
#endif

#ifdef STOP_SECTION_COMMON_INIT_CONST_UNSPECIFIED
    #ifdef START_SECTION_COMMON_INIT_CONST_UNSPECIFIED
        #undef START_SECTION_COMMON_INIT_CONST_UNSPECIFIED
        #undef SECTION_STARTED
        /*#pragma*/
        #undef STOP_SECTION_COMMON_INIT_CONST_UNSPECIFIED
        #undef MEMMAP_ERROR
    #else
        #error "STOP_SECTION_COMMON_INIT_CONST_UNSPECIFIED: This section has not been previously started !"
    #endif
#endif

#ifdef START_SECTION_COMMON_INIT_CONST_UNSPECIFIED
    #ifndef SECTION_STARTED
        #define SECTION_STARTED
        /*#pragma*/
        #undef MEMMAP_ERROR
    #else
        #error "START_SECTION_COMMON_INIT_CONST_UNSPECIFIED: A section has already been started !"
    #endif
#endif

/*************************************** CODE *****************************************************/
#ifdef STOP_SECTION_COMMON_ISR_CODE
    #ifdef START_SECTION_COMMON_ISR_CODE
        #undef START_SECTION_COMMON_ISR_CODE
        #undef SECTION_STARTED
        /*#pragma*/
        #undef STOP_SECTION_COMMON_ISR_CODE
        #undef MEMMAP_ERROR
    #else
        #error "STOP_SECTION_COMMON_ISR_CODE: This section has not been previously started !"
    #endif
#endif

#ifdef START_SECTION_COMMON_ISR_CODE
    #ifndef SECTION_STARTED
        #define SECTION_STARTED
        /*#pragma*/
        #undef MEMMAP_ERROR
    #else
        #error "START_SECTION_COMMON_ISR_CODE: A section has already been started !"
    #endif
#endif

#ifdef STOP_SECTION_COMMON_CODE
    #ifdef START_SECTION_COMMON_CODE
        #undef START_SECTION_COMMON_CODE
        #undef SECTION_STARTED
        #pragma section = "XCODE"

        #undef STOP_SECTION_COMMON_CODE
        #undef MEMMAP_ERROR
    #else
        #error "STOP_SECTION_COMMON_CODE: This section has not been previously started !"
    #endif
#endif

#ifdef START_SECTION_COMMON_CODE
    #ifndef SECTION_STARTED
        #define SECTION_STARTED
        #pragma section = "XCODE"

        #undef MEMMAP_ERROR
    #else
        #error "START_SECTION_COMMON_CODE: A section has already been started !"
    #endif
#endif

/************************************** CONST *****************************************************/
#ifdef STOP_SECTION_COMMON_CONST_BOOLEAN
    #ifdef START_SECTION_COMMON_CONST_BOOLEAN
        #undef START_SECTION_COMMON_CONST_BOOLEAN
        #undef SECTION_STARTED
        /*#pragma*/
        #undef STOP_SECTION_COMMON_CONST_BOOLEAN
        #undef MEMMAP_ERROR
    #else
        #error "STOP_SECTION_COMMON_CONST_BOOLEAN: This section has not been previously started !"
    #endif
#endif

#ifdef START_SECTION_COMMON_CONST_BOOLEAN
    #ifndef SECTION_STARTED
        #define SECTION_STARTED
        /*#pragma*/
        #undef MEMMAP_ERROR
    #else
        #error "START_SECTION_COMMON_CONST_BOOLEAN: A section has already been started !"
    #endif
#endif

#ifdef STOP_SECTION_COMMON_CONST_8BITS
    #ifdef START_SECTION_COMMON_CONST_8BITS
        #undef START_SECTION_COMMON_CONST_8BITS
        #undef SECTION_STARTED
        /*#pragma*/
        #undef STOP_SECTION_COMMON_CONST_8BITS
        #undef MEMMAP_ERROR
    #else
        #error "STOP_SECTION_COMMON_CONST_8BITS: This section has not been previously started !"
    #endif
#endif

#ifdef START_SECTION_COMMON_CONST_8BITS
    #ifndef SECTION_STARTED
        #define SECTION_STARTED
        /*#pragma*/
        #undef MEMMAP_ERROR
    #else
        #error "START_SECTION_COMMON_CONST_8BITS: A section has already been started !"
    #endif
#endif

#ifdef STOP_SECTION_COMMON_CONST_16BITS
    #ifdef START_SECTION_COMMON_CONST_16BITS
        #undef START_SECTION_COMMON_CONST_16BITS
        #undef SECTION_STARTED
        /*#pragma*/
        #undef STOP_SECTION_COMMON_CONST_16BITS
        #undef MEMMAP_ERROR
    #else
        #error "STOP_SECTION_COMMON_CONST_16BITS: This section has not been previously started !"
    #endif
#endif

#ifdef START_SECTION_COMMON_CONST_16BITS
    #ifndef SECTION_STARTED
        #define SECTION_STARTED
        /*#pragma*/
        #undef MEMMAP_ERROR
    #else
        #error "START_SECTION_COMMON_CONST_16BITS: A section has already been started !"
    #endif
#endif

#ifdef STOP_SECTION_COMMON_CONST_32BITS
    #ifdef START_SECTION_COMMON_CONST_32BITS
        #undef START_SECTION_COMMON_CONST_32BITS
        #undef SECTION_STARTED
        /*#pragma*/
        #undef STOP_SECTION_COMMON_CONST_32BITS
        #undef MEMMAP_ERROR
    #else
        #error "STOP_SECTION_COMMON_CONST_32BITS: This section has not been previously started !"
    #endif
#endif

#ifdef START_SECTION_COMMON_CONST_32BITS
    #ifndef SECTION_STARTED
        #define SECTION_STARTED
        /*#pragma*/
        #undef MEMMAP_ERROR
    #else
        #error "START_SECTION_COMMON_CONST_32BITS: A section has already been started !"
    #endif
#endif

#ifdef STOP_SECTION_COMMON_CONST_UNSPECIFIED
    #ifdef START_SECTION_COMMON_CONST_UNSPECIFIED
        #undef START_SECTION_COMMON_CONST_UNSPECIFIED
        #undef SECTION_STARTED
        /*#pragma*/
        #undef STOP_SECTION_COMMON_CONST_UNSPECIFIED
        #undef MEMMAP_ERROR
    #else
        #error "STOP_SECTION_COMMON_CONST_UNSPECIFIED: This section has not been previously started !"
    #endif
#endif

#ifdef START_SECTION_COMMON_CONST_UNSPECIFIED
    #ifndef SECTION_STARTED
        #define SECTION_STARTED
        /*#pragma*/
        #undef MEMMAP_ERROR
    #else
        #error "START_SECTION_COMMON_CONST_UNSPECIFIED: A section has already been started !"
    #endif
#endif

/**************************************** VAR *****************************************************/
#ifdef STOP_SECTION_COMMON_VAR_BOOLEAN
    #ifdef START_SECTION_COMMON_VAR_BOOLEAN
        #undef START_SECTION_COMMON_VAR_BOOLEAN
        #undef SECTION_STARTED
        #undef STOP_SECTION_COMMON_VAR_BOOLEAN
        #undef MEMMAP_ERROR
        #pragma section = "FAR_N"
    #else
        #error "STOP_SECTION_COMMON_VAR_BOOLEAN: This section has not been previously started !"
    #endif
#endif

#ifdef START_SECTION_COMMON_VAR_BOOLEAN
    #ifndef SECTION_STARTED
        #define SECTION_STARTED
        #undef MEMMAP_ERROR
        #pragma section = "FAR_N"
    #else
        #error "START_SECTION_COMMON_VAR_BOOLEAN: A section has already been started !"
    #endif
#endif

#ifdef STOP_SECTION_COMMON_VAR_8BITS
    #ifdef START_SECTION_COMMON_VAR_8BITS
        #undef START_SECTION_COMMON_VAR_8BITS
        #undef SECTION_STARTED
        #undef STOP_SECTION_COMMON_VAR_8BITS
        #undef MEMMAP_ERROR
        /* #pragma */
    #else
        #error "STOP_SECTION_COMMON_VAR_8BITS: This section has not been previously started !"
    #endif
#endif

#ifdef START_SECTION_COMMON_VAR_8BITS
    #ifndef SECTION_STARTED
        #define SECTION_STARTED
        #undef MEMMAP_ERROR
        /* #pragma */
    #else
        #error "START_SECTION_COMMON_VAR_8BITS: A section has already been started !"
    #endif
#endif

#ifdef STOP_SECTION_COMMON_VAR_16BITS
    #ifdef START_SECTION_COMMON_VAR_16BITS
        #undef START_SECTION_COMMON_VAR_16BITS
        #undef SECTION_STARTED
        #undef STOP_SECTION_COMMON_VAR_16BITS
        #undef MEMMAP_ERROR
        /* #pragma */
    #else
        #error "STOP_SECTION_COMMON_VAR_16BITS: This section has not been previously started !"
    #endif
#endif

#ifdef START_SECTION_COMMON_VAR_16BITS
    #ifndef SECTION_STARTED
        #define SECTION_STARTED
        #undef MEMMAP_ERROR
        /* #pragma */
    #else
        #error "START_SECTION_COMMON_VAR_16BITS: A section has already been started !"
    #endif
#endif

#ifdef STOP_SECTION_COMMON_VAR_32BITS
    #ifdef START_SECTION_COMMON_VAR_32BITS
        #undef START_SECTION_COMMON_VAR_32BITS
        #undef SECTION_STARTED
        #undef STOP_SECTION_COMMON_VAR_32BITS
        #undef MEMMAP_ERROR
        /* #pragma */
    #else
        #error "STOP_SECTION_COMMON_VAR_32BITS: This section has not been previously started !"
    #endif
#endif

#ifdef START_SECTION_COMMON_VAR_32BITS
    #ifndef SECTION_STARTED
        #define SECTION_STARTED
        #undef MEMMAP_ERROR
        /* #pragma */
    #else
        #error "START_SECTION_COMMON_VAR_32BITS: A section has already been started !"
    #endif
#endif

#ifdef STOP_SECTION_COMMON_VAR_UNSPECIFIED
    #ifdef START_SECTION_COMMON_VAR_UNSPECIFIED
        #undef START_SECTION_COMMON_VAR_UNSPECIFIED
        #undef SECTION_STARTED
        #undef STOP_SECTION_COMMON_VAR_UNSPECIFIED
        #undef MEMMAP_ERROR
        #pragma section = "FAR_N"
    #else
        #error "STOP_SECTION_COMMON_VAR_UNSPECIFIED: This section has not been previously started !"
    #endif
#endif

#ifdef START_SECTION_COMMON_VAR_UNSPECIFIED
    #ifndef SECTION_STARTED
        #define SECTION_STARTED
        #undef MEMMAP_ERROR
        #pragma section = "FAR_N"
    #else
        #error "START_SECTION_COMMON_VAR_UNSPECIFIED: A section has already been started !"
    #endif
#endif

/*************************************** VAR NOINIT ***********************************************/
#ifdef STOP_SECTION_COMMON_VAR_NOINIT_BOOLEAN
    #ifdef START_SECTION_COMMON_VAR_NOINIT_BOOLEAN
        #undef START_SECTION_COMMON_VAR_NOINIT_BOOLEAN
        #undef SECTION_STARTED
        #undef STOP_SECTION_COMMON_VAR_NOINIT_BOOLEAN
        #undef MEMMAP_ERROR
        /* #pragma */
    #else
        #error "STOP_SECTION_COMMON_VAR_NOINIT_BOOLEAN: This section has not been previously started !"
    #endif
#endif

#ifdef START_SECTION_COMMON_VAR_NOINIT_BOOLEAN
    #ifndef SECTION_STARTED
        #define SECTION_STARTED
        #undef MEMMAP_ERROR
        /* #pragma */
    #else
        #error "START_SECTION_COMMON_VAR_NOINIT_BOOLEAN: A section has already been started !"
    #endif
#endif

#ifdef STOP_SECTION_COMMON_VAR_NOINIT_8BITS
    #ifdef START_SECTION_COMMON_VAR_NOINIT_8BITS
        #undef START_SECTION_COMMON_VAR_NOINIT_8BITS
        #undef SECTION_STARTED
        #undef STOP_SECTION_COMMON_VAR_NOINIT_8BITS
        #undef MEMMAP_ERROR
        #pragma section = "FAR_N"
    #else
        #error "STOP_SECTION_COMMON_VAR_NOINIT_8BITS: This section has not been previously started !"
    #endif
#endif

#ifdef START_SECTION_COMMON_VAR_NOINIT_8BITS
    #ifndef SECTION_STARTED
        #define SECTION_STARTED
        #undef MEMMAP_ERROR
        #pragma section = "FAR_N"
    #else
        #error "START_SECTION_COMMON_VAR_NOINIT_8BITS: A section has already been started !"
    #endif
#endif

#ifdef STOP_SECTION_COMMON_VAR_NOINIT_16BITS
    #ifdef START_SECTION_COMMON_VAR_NOINIT_16BITS
        #undef START_SECTION_COMMON_VAR_NOINIT_16BITS
        #undef SECTION_STARTED
        #undef STOP_SECTION_COMMON_VAR_NOINIT_16BITS
        #undef MEMMAP_ERROR
        #pragma section = "FAR_N"
    #else
        #error "STOP_SECTION_COMMON_VAR_NOINIT_16BITS: This section has not been previously started !"
    #endif
#endif

#ifdef START_SECTION_COMMON_VAR_NOINIT_16BITS
    #ifndef SECTION_STARTED
        #define SECTION_STARTED
        #undef MEMMAP_ERROR
        #pragma section = "FAR_N"
    #else
        #error "START_SECTION_COMMON_VAR_NOINIT_16BITS: A section has already been started !"
    #endif
#endif

#ifdef STOP_SECTION_COMMON_VAR_NOINIT_32BITS
    #ifdef START_SECTION_COMMON_VAR_NOINIT_32BITS
        #undef START_SECTION_COMMON_VAR_NOINIT_32BITS
        #undef SECTION_STARTED
        #undef MEMMAP_ERROR
        #undef STOP_SECTION_COMMON_VAR_NOINIT_32BITS
        #pragma section = "FAR_N"
    #else
        #error "STOP_SECTION_COMMON_VAR_NOINIT_32BITS: This section has not been previously started !"
    #endif
#endif

#ifdef START_SECTION_COMMON_VAR_NOINIT_32BITS
    #ifndef SECTION_STARTED
        #define SECTION_STARTED
        #undef MEMMAP_ERROR
        #pragma section = "FAR_N"
    #else
        #error "START_SECTION_COMMON_VAR_NOINIT_32BITS: A section has already been started !"
    #endif
#endif

#ifdef STOP_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
    #ifdef START_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
        #undef START_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
        #undef SECTION_STARTED
        #undef MEMMAP_ERROR
        #undef STOP_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
        #pragma section = "FAR_N"
    #else
        #error "STOP_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED: This section has not been previously started !"
    #endif
#endif

#ifdef START_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED
    #ifndef SECTION_STARTED
        #define SECTION_STARTED
        #undef MEMMAP_ERROR
        #pragma segment DATA=COMMON_VAR_NOINIT_UNSPECIFIED,attr=DATA
    #else
        #error "START_SECTION_COMMON_VAR_NOINIT_UNSPECIFIED: A section has already been started !"
    #endif
#endif

#ifdef MEMMAP_ERROR
#error "MemMap.h, wrong pragma command"
#endif
