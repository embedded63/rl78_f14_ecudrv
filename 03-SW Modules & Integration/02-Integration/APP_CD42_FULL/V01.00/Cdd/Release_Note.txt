=============================================================================
                                   FAURECIA
                        Proprietary - Copyright (C) 2012
-----------------------------------------------------------------------------
Release note for CDD  software module

-----------------------------------------------------------------------------


CHANGES
-------

V01.00 : 02/09/2013
####################
First release version

This release contains: 
   Source code: Cdd_Drv.c/Cdd_Drv.h/Cdd_Cfg.h (V01.00),
   Sw LLD (EME-13ST002-12115.01),
   Module tests description (EME-13ST002-12116.01)
