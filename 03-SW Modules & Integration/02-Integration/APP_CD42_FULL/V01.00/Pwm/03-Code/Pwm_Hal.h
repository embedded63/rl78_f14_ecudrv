/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Pwm_Hal.h
** Module name  :
** -------------------------------------------------------------------------------------------------
** Description : Include file of component Pwm_Hal.c
**
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-13ST0002-12105.02 (SW LLD PWM)
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 05/08/2013
** - Baseline Created
**
***************************************************************************************************/
/* To avoid multi-inclusions */
#ifndef PWM_HAL_H
#define PWM_HAL_H
/**************************************** Inclusion files *****************************************/
#include "Std_Types.h"

/************************** Declaration of global symbol and constants ****************************/

/********************************* Declaration of global macros ***********************************/
typedef enum
{
    ACT1_CurrentSense = 1
   ,ACT2_CurrentSense
   ,ACT3_CurrentSense
   ,ACT4_CurrentSense
   ,ACT5_CurrentSense
   ,ACT6_CurrentSense
   ,ACT7_CurrentSense
   ,ACT_Max
}Act_CurrentSense;
/********************************* Declaration of global types ************************************/

/****************************** External links of global variables ********************************/
/*
extern VAR(uint8, PWM_VAR) PWMCMD_DutyCycle1_u8;
extern VAR(uint8, PWM_VAR) PWMCMD_DutyCycle2_u8;
extern VAR(uint8, PWM_VAR) PWMCMD_DutyCycle3_u8;
extern VAR(uint8, PWM_VAR) PWMCMD_DutyCycle4_u8;
extern VAR(uint8, PWM_VAR) PWMCMD_DutyCycle5_u8;
extern VAR(uint8, PWM_VAR) PWMCMD_DutyCycle6_u8;
extern VAR(uint8, PWM_VAR) PWMCMD_DutyCycle7_u8;
*/
extern VAR(uint8, PWM_VAR) PWM_DutCyc1Ch;
extern VAR(uint8, PWM_VAR) PWM_DutCyc2Ch;
extern VAR(uint8, PWM_VAR) PWM_CurrSnsCnt;
extern VAR(uint16, PWM_VAR) PWM_DutCyc1;
extern VAR(uint16, PWM_VAR) PWM_DutCyc2;
extern VAR(uint8, PWM_VAR) PWM_Channle4Select;
/****************************** External links of global constants ********************************/

/***************************************************************************************************
**                                            FUNCTIONS                                           **
***************************************************************************************************/
#define PWM_START_SEC_INIT_CODE
#include "MemMap.h"
extern FUNC(void, PWM_CODE) Pwm_HalInit(void);
#define PWM_STOP_SEC_INIT_CODE
#include "MemMap.h"

#define PWM_START_SEC_CODE
#include "MemMap.h"
extern FUNC(void, PWM_CODE) Pwm_Hal(void);
extern INLINE FUNC(void, PWM_CODE) PWM_SelectMuxCurense(VAR(uint8, AUTOMATIC) Pwm_CurrentSense);
#define PWM_STOP_SEC_CODE
#include "MemMap.h"

#endif /* PWMDRV_H */
