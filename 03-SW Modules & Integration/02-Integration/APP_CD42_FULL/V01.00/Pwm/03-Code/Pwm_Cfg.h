/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Pwm_Cfg.h
** Module name  :
** -------------------------------------------------------------------------------------------------
** Description : Include file of component Pwm_Drv.c
**
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-13ST0002-12105.02 (SW LLD PWM)
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 05/08/2013
** - Baseline Created
**
***************************************************************************************************/

/*To avoid multi-inclusions */
#ifndef PWM_CFG_H
#define PWM_CFG_H
/**************************************** Inclusion files *****************************************/
#include "Std_Types.h"
#include "Dio_Cfg.h"
/************************** Declaration of global symbol and constants ****************************/

/********************************* Declaration of global macros ***********************************/

/********************************* Declaration of global types ************************************/

/****************************** External links of global variables ********************************/

/****************************** External links of global constants ********************************/

/***************************************************************************************************
**                                            FUNCTIONS                                           **
***************************************************************************************************/
#define USED      (0)
#define NOT_USED  (1)
#define PWM_CLOCK_FREQUENCY (uint16)8   /* configure CPU/peripheral hardware clock freq in MHz (16/2 i.e 8MHz) */
#define PWM_PULSE_PERIOD    (uint16)6250 /* required pwm period in micro seconds */

#define DUTCYC_ADJ      0.5       /* O.5% duty cycle resolution */

/* Only even channels can be selected as master */
/* Select only one channel as master */
/* Any channel except channel 0 can be set as slave */
/* Slave channel must be lower than the master channel */
#define TAU_CH1 (USED) /* used as slave */
#define TAU_CH2 (USED)
#define TAU_CH3 (USED)
#define TAU_CH4 (USED)
#define TAU_CH5 (USED) /*USED to measure current sense  timer o/p is disabled to pin */
#define TAU_CH6 (USED)
#define TAU_CH7 (USED)


#define TAU_CH5_AS_PWM    (NOT_USED) 
#define TAU_CH5_SLAVEINTR (USED)  /* used to call wait for single mode in adc */
#define TAU_CH6_SLAVEINTR (USED) 
#define TAU_CH47_MUX (USED)

#define M_TAU_CH1_START_TRG_ON  0x0002u
#define M_TAU_CH2_START_TRG_ON  0x0004u
#define M_TAU_CH3_START_TRG_ON  0x0008u
#define M_TAU_CH4_START_TRG_ON  0x0010u
#define M_TAU_CH5_START_TRG_ON  0x0020u
#define M_TAU_CH6_START_TRG_ON  0x0040u
#define M_TAU_CH7_START_TRG_ON  0x0080u

/* configure the timer register for different channels */
#define  M_TMR00_CONFIG   0x0800u
#define  M_TMR01_CONFIG   0x0409u
#define  M_TMR02_CONFIG   0x0409u
#define  M_TMR03_CONFIG   0x0409u
#define  M_TMR04_CONFIG   0x0409u
#if(TAU_CH5 == USED)
#define  M_TMR05_CONFIG   0x0409u
#else
#define  M_TMR05_CONFIG   0x0000u
#endif
#define  M_TMR06_CONFIG   0x0409u
#define  M_TMR07_CONFIG   0x0409u

/* PIOR0   value 0   value 1 */
/*TI02/TO02     P17   P15   */
/*TI03/TO03     P31   P14   */
/*TI04/TO04     -     P13   */
/*TI05/TO05     -     P12   */
/*TI06/TO06     -     P11   */
/*TI07/TO07     P41   P10   */
/* Configure based on pins used for PWM outputs */
#define SELECT_PORT_PINS      (uint8)0x01 /* PIOR0 value */
#define ENABLE_TAU (TRUE) /* Enable TAU peripheral */

#if defined (FULL_VERSION)
#define Pwm1_AdcRawCurrentIdx ADC_AN1IDX_1   /* Configure adc array index(macros from adc module) here for respective channel */
#define Pwm2_AdcRawCurrentIdx ADC_AN2IDX
#define Pwm3_AdcRawCurrentIdx ADC_AN3IDX_3
#define Pwm4_AdcRawCurrentIdx ADC_AN4IDX_4
#define Pwm5_AdcRawCurrentIdx ADC_AN1IDX_5
#define Pwm6_AdcRawCurrentIdx ADC_AN3IDX_6
#define Pwm7_AdcRawCurrentIdx ADC_AN4IDX_7
#else
#define Pwm1_AdcRawCurrentIdx ADC_AN0IDX   /* Configure adc array index(macros from adc module) here for respective channel */
#define Pwm2_AdcRawCurrentIdx ADC_AN1IDX
#define Pwm3_AdcRawCurrentIdx ADC_AN2IDX
#define Pwm4_AdcRawCurrentIdx ADC_AN3IDX
#endif

#define Pwm_SetDutcylCH1(VALUE) Pwm_WrTDR01(VALUE)
#define Pwm_SetDutcylCH2(VALUE) Pwm_WrTDR07(VALUE)
#define Pwm_SetDutcylCH3(VALUE) Pwm_WrTDR02(VALUE)
#define Pwm_SetDutcylCH4(VALUE) Pwm_WrTDR06(VALUE)
#define Pwm_SetDutcylCH5(VALUE) Pwm_WrTDR04(VALUE)
#define Pwm_SetDutcylCH6(VALUE) Pwm_WrTDR03(VALUE)
#define Pwm_SetDutcylCH7(VALUE) Pwm_WrTDR06(VALUE)
#define Pwm_SetTimer(VALUE)     Pwm_WrTDR05(VALUE)

/* for multiplexed pins */
#if TAU_CH47_MUX == USED
#define Pwm_CfgCH4()            (PM1_bit.no4 = 0x00u)
#define Pwm_SetCH4()            (P1_bit.no4 = 0x01u)
#define Pwm_ReSetCH4()          (P1_bit.no4 = 0x00u)

#define Pwm_CfgCH7()            (PM1_bit.no5 = 0x00u)
#define Pwm_SetCH7()            (P1_bit.no5 = 0x01u)
#define Pwm_ReSetCH7()          (P1_bit.no5 = 0x00u)
#endif

/* configuration to ensure set and reset of PWM channels */
#define PWM_SELECTDIOSENSE1   M_DIO_WRITECHANNEL01(FALSE)
#define PWM_SELECTDIOSENSE5   M_DIO_WRITECHANNEL01(TRUE)

#define PWM_SELECTDIOSENSE3   M_DIO_WRITECHANNEL02(FALSE)
#define PWM_SELECTDIOSENSE6   M_DIO_WRITECHANNEL02(TRUE)

#define PWM_SELECTDIOSENSE4   M_DIO_WRITECHANNEL03(TRUE)
#define PWM_SELECTDIOSENSE7   M_DIO_WRITECHANNEL03(FALSE)

#define PWM_CHANNEL4ACTIVE    0x01u
#define PWM_CHANNEL7ACTIVE    0x02u

#endif /* PWMDRV_H */
