/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Pwm_Drv.c
** Module name  : PWM
** -------------------------------------------------------------------------------------------------
** Description : Initializes the PWM outputs at 0% duty cycle,different PWM duty cycles,
**               Set PWM outputs to idle.
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-13ST0002-12105.02 (SW LLD PWM)
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 5/08/2013
** - Baseline Created
**
***************************************************************************************************/

/************************************** Inclusion files *******************************************/
#include "Pwm_Drv.h"
#include "Pwm_Cfg.h"
#include "Pwm_Hal.h"
#include "Adc_Drv.h"
#include "Dio_Drv.h"
#include "Isr.h"
/************************** Declaration of local symbol and constants *****************************/

/********************************* Declaration of local macros ************************************/
#define M_SETBIT(BIT_NO) (uint16)(0x0001u<<(BIT_NO))

#define M_CLEARBIT(BIT_NO) (uint16)(~(0x0001u<<(BIT_NO)))

/********************************* Declaration of local types *************************************/

/******************************* Declaration of local variables ***********************************/

/******************************* Declaration of local constants ***********************************/

/****************************** Declaration of exported variables *********************************/
static volatile VAR(uint8, PWM_VAR) PWM_DutCycCntIsr;
static volatile VAR(uint8, PWM_VAR) PWM_DutCyc1ChIsr;
static volatile VAR(uint8, PWM_VAR) PWM_DutCyc2ChIsr;
/****************************** Declaration of exported constants *********************************/

/***************************************************************************************************
**                                      FUNCTIONS                                                 **
***************************************************************************************************/
/****************************** Internal functions declarations ***********************************/

/*********************************** Function definitions *****************************************/
#define PWM_START_SEC_INIT_CODE
#include "MemMap.h"
/***************************************************************************************************
** Function         : Pwm_Init

** Description      : Initializes TAU channels as PWM outputs.

** Parameter        : None

** Return value     : None

** Remarks          : None
***************************************************************************************************/

FUNC(void, PWM_CODE) Pwm_Init(void)
{
  TAU0EN = ENABLE_TAU;      /* Enable TAU Peripheral*/
  TPS0 = 0x0001u;   /* Selection of (CK00) as Operating clock fmck  i.e fclk/2  (8 MHz) */
#if defined (FULL_VERSION)
  PIOR0 |= SELECT_PORT_PINS;  /* enable required TO02,03,04,05,06,07 pins for TAU */ 
#else
  PIOR |= SELECT_PORT_PINS;  /* enable required TO02,03,04,05,06,07 pins for TAU */  
#endif
  /* Channel 0 as master */
  TMR00 = M_TMR00_CONFIG;   /* As master in interval timer mode */
  TDR00 = MASTER_VALUE;
  TO0 &= M_CLEARBIT(0);  /* TO0.0 Output 0 from TO0 pin */
  TOE0 &= M_CLEARBIT(0); /* disable channel 0 timer output to pin */
  TOL0 &= M_CLEARBIT(0); /* Timer output level to 0 */
  TOM0 &= M_CLEARBIT(0); /* Master channel output mode */
  Isr_SetPriority(ISR_PWMCHANNEL0_PRIOR,ISR_PRIOR_3);
  /* unmask the master interrupt request */
  TMMK00 = 0u;

  
#if(TAU_CH1 == USED)
  /* Channel 1 slave */
  PM1 &= ~0x40u;
  TMR01 = M_TMR01_CONFIG;
  TO0 &= M_CLEARBIT(1);  /* TO0.1 Output 0 from TO1 pin */
  TOE0 |= M_SETBIT(1);   /* enable channel 1 timer output to pin */
  TOL0 &= M_CLEARBIT(1); /* Timer output level to 0 */
  TOM0 |= M_SETBIT(1);   /* slave channel output mode */
  /* set the interrupt priority */
  Isr_SetPriority(ISR_PWMCHANNEL1_PRIOR,ISR_PRIOR_3);  

#endif  

#if(TAU_CH2 == USED)
  /* Channel 2 as slave */
  PM12 &= ~0x01u;
  PMC12 &= 0xDEu;
  TMR02 = M_TMR02_CONFIG;
  TO0 &= M_CLEARBIT(2);  /* TO0.2 Output 0 from TO2 pin */
  TOE0 |= M_SETBIT(2);   /* enable channel 2 timer output to pin */
  TOL0 &= M_CLEARBIT(2); /* Timer output level to 0 */
  TOM0 |= M_SETBIT(2);   /* slave channel output mode */
  /* set the interrupt priority */
  Isr_SetPriority(ISR_PWMCHANNEL2_PRIOR,ISR_PRIOR_3);  

#endif
  
#if(TAU_CH3 == USED)  
  /* Channel 3 slave */
  PM3 &= ~0x01u;
  TMR03 = M_TMR03_CONFIG;
  TO0 &= M_CLEARBIT(3);  /* TO0.3 Output 0 from TO3 pin */
  TOE0 |= M_SETBIT(3);   /* enable channel 3 timer output to pin */
  TOL0 &= M_CLEARBIT(3); /* Timer output level to 0 */
  TOM0 |= M_SETBIT(3);   /* slave channel output mode */
  /* set the interrupt priority */
  Isr_SetPriority(ISR_PWMCHANNEL3_PRIOR,ISR_PRIOR_3);  

#endif
  
#if(TAU_CH4 == USED)  
  /* Channel 4 slave */
  PM1 &= ~0x08u;
  TMR04 = M_TMR04_CONFIG;
  TO0 &= M_CLEARBIT(4);   /* TO0.4 Output 0 from TO4 pin */
  TOE0 |= M_SETBIT(4);    /* enable channel 4 timer output to pin */
  TOL0 &= M_CLEARBIT(4);  /* Timer output level to 0 */
  TOM0 |= M_SETBIT(4);    /* slave channel output mode */
  /* set the interrupt priority */
  Isr_SetPriority(ISR_PWMCHANNEL4_PRIOR,ISR_PRIOR_3);  

#endif

#if(TAU_CH5 == USED)  
  /* Channel 5 slave */
  PM1 &= ~0x04u;
  TMR05 = M_TMR05_CONFIG;
  TO0 &= M_CLEARBIT(5);   /* TO0.5 Output 0 from TO5 pin */
  /*TOE0 |= M_SETBIT(5)     enable channel 5 timer output to pin */
  TOE0 &= M_CLEARBIT(5);  /* disable channel 5 timer output to pin */    
  TOL0 &= M_CLEARBIT(5);  /* Timer output level to 0 */
  TOM0 |= M_SETBIT(5);    /* slave channel output mode */
  /* set the interrupt priority */
  Isr_SetPriority(ISR_PWMCHANNEL5_PRIOR,ISR_PRIOR_3);  

#else
    /* Channel 5 in timer inverval mode  */
  TMR05 = M_TMR05_CONFIG;        /* timer interval mode */
  TO0 &= M_CLEARBIT(5);   /* TO0.5 Output 0 from TO5 pin */
  TOE0&= M_CLEARBIT(5);   /* disable channel 5 timer output to pin */
  TOL0&= M_CLEARBIT(5);   /* Timer output level to 0 */
  TOM0&= M_CLEARBIT(5);   /* master channel output mode */  
#endif  
#if(TAU_CH6 == USED)  
  /* Channel 6 slave */
  PM1 &= ~0x20u;
  TMR06 = M_TMR06_CONFIG;
  TO0 &= M_CLEARBIT(6);   /* TO0.6 Output 0 from TO6 pin */
  TOE0 |= M_SETBIT(6);    /* enable channel 6 timer output to pin */
  TOL0 &= M_CLEARBIT(6);  /* Timer output level to 0 */
  TOM0 |= M_SETBIT(6);    /* slave channel output mode */
  /* set the interrupt priority */
  Isr_SetPriority(ISR_PWMCHANNEL6_PRIOR,ISR_PRIOR_3);   
#endif

  /* Channel 7 slave */  
#if(TAU_CH7 == USED)  
  PM12 &= ~0x20u;
  TMR07 = M_TMR07_CONFIG;
  TO0 &= M_CLEARBIT(7);   /* TO0.7 Output 0 from TO7 pin */
  TOE0 |= M_SETBIT(7);    /* enable channel 7 timer output to pin */
  TOL0 &= M_CLEARBIT(7);  /* Timer output level to 0 */
  TOM0 |= M_SETBIT(7);    /* slave channel output mode */
  /* set the interrupt priority */
  Isr_SetPriority(ISR_PWMCHANNEL7_PRIOR,ISR_PRIOR_3);   
#endif
#if(USED == TAU_CH47_MUX)
  /* configure T05 as GPIO output pin*/  
  /* configure P15 as output pin (not used by TO07) */
  Pwm_CfgCH7();
  
  /* clear the output to RESET vlaue */
  Pwm_ReSetCH7();

  /* configure T06 as GPIO output pin*/  
  /* configure P14 as output pin (not used by TO07) */
  Pwm_CfgCH4();
  
  /* clear the output to RESET vlaue */
  Pwm_ReSetCH4();
  
  /* disable timer output for channel 6 */
  TOE0&= M_CLEARBIT(6);   /* disable channel 6 timer output to pin */   
#endif  
  TS0  |= 0x0001u;   /* start count/timer operation (master)*/
}
#define PWM_STOP_SEC_INIT_CODE
#include "MemMap.h"

#define PWM_START_SEC_CODE
#include "MemMap.h"
/***********************************************************
** Function name: Pwm_WrTDR01
** Description: Write value to TDR01 regiseter and start/reset pwm counter.
** Parameter: uint16 slave counter value
** Return value: None
** Remarks: global variables used, side effects
************************************************************/
FUNC(void, PWM_CODE) Pwm_WrTDR01(VAR(uint16, AUTOMATIC) slavecntval)
{
  /* check for the slave count */
  if(slavecntval != (uint16)0u)
  {
    /* operate the timer */

    /* load the slave channel counter */
    TDR01 = slavecntval;

    /* enable falling edge interrupt if channel1 PWM is either DC1 OR DC2 */
    if((PWM_DutCyc1Ch == M_PWM_CH1) || (PWM_DutCyc2Ch == M_PWM_CH1))
    {   
      /* unmask the interrupt request */
      TMMK01 = 0u;
    }
    else
    {
      /* mask the interrupt request */
      TMMK01 = 1u;        
    }

    /* check if the timer is on the run */    
    if((TE0 & M_TAU_CH1_START_TRG_ON)!= (uint8)0u)
    {
      /* timer is already tirggered */
    }
    else
    {
      /* start the timer */
      TS0 |= M_SETBIT(1);
    }
  }
  else
  {
    /* stop the timer - because this will not cause channel 1 interrupt to occur */
    /*TT0 |= M_SETBIT(1) operation is stoped */
    
    /* clear slave channel counter */
    TDR01 = 0u;
    /* mask the interrupt request */
    TMMK01 = 1u;
  }
}


/***********************************************************
** Function name: Pwm_WrTDR02
** Description: Write value to TDR02 regiseter and start/reset pwm counter.
** Parameter: uint16 slave counter value
** Return value: None
** Remarks: global variables used, side effects
************************************************************/
FUNC(void, PWM_CODE) Pwm_WrTDR02(VAR(uint16, AUTOMATIC) slavecntval)
{
  /* check for the slave count */
  if(slavecntval != (uint16)0u)
  {
    /* operate the timer */

    /* load the slave channel counter */
    TDR02 = slavecntval;

    /* enable falling edge interrupt if channel3 PWM is either DC1 OR DC2 */
    if((PWM_DutCyc1Ch == M_PWM_CH3) || (PWM_DutCyc2Ch == M_PWM_CH3))
    {   
      /* unmask the interrupt request */
      TMMK02 = 0u;
    }
    else
    {
      /* mask the interrupt request */
      TMMK02 = 1u;        
    }

    /* check if the timer is on the run */    
    if((TE0 & M_TAU_CH2_START_TRG_ON)!= (uint8)0u)
    {
      /* timer is already tirggered */
    }
    else
    {
      /* start the timer */
      TS0 |= M_SETBIT(2);
    }
  }
  else
  {
    /* stop the timer - because this will not cause channel 2 interrupt to occur */
    /*TT0 |= M_SETBIT(2) operation is stoped */
    /* clear slave channel counter */
    TDR02 = 0u;
    /* unmask the interrupt request */
    TMMK02 = 1u;
  }
}


/***********************************************************
** Function name: Pwm_WrTDR03
** Description: Write value to TDR03 regiseter and start/reset pwm counter.
** Parameter: uint16 slave counter value
** Return value: None
** Remarks: global variables used, side effects
************************************************************/
FUNC(void, PWM_CODE) Pwm_WrTDR03(VAR(uint16, AUTOMATIC) slavecntval)
{
  /* check for the slave count */
  if(slavecntval != (uint16)0u)
  {
    /* operate the timer */

    /* load the slave channel counter */
    TDR03 = slavecntval;

    /* enable falling edge interrupt if channel3 PWM is either DC1 OR DC2 */
    if((PWM_DutCyc1Ch == M_PWM_CH6) || (PWM_DutCyc2Ch == M_PWM_CH6))
    {   
      /* unmask the interrupt request */
      TMMK03 = 0u;
    }
    else
    {
      /* mask the interrupt request */
      TMMK03 = 1u;        
    }
    
    /* check if the timer is on the run */    
    if((TE0 & M_TAU_CH3_START_TRG_ON)!= (uint8)0u)
    {
      /* timer is already tirggered */
    }
    else
    {
      /* start the timer */
      TS0 |= M_SETBIT(3);
    }
  }
  else
  {
    /* mask the interrupt request */
    TMMK03 = 1u;
    /* clear slave channel counter */
    TDR03 = 0u;
  }
}


/***********************************************************
** Function name: Pwm_WrTDR04
** Description: Write value to TDR04 regiseter and start/reset pwm counter.
** Parameter: uint16 slave counter value
** Return value: None
** Remarks: global variables used, side effects
************************************************************/
FUNC(void, PWM_CODE) Pwm_WrTDR04(VAR(uint16, AUTOMATIC) slavecntval)
{
  /* check for the slave count */
  if(slavecntval != (uint16)0u)
  {
    /* operate the timer */

    /* load the slave channel counter */
    TDR04 = slavecntval;

    /* enable falling edge interrupt if channel4 PWM is either DC1 OR DC2 */
    if((PWM_DutCyc1Ch == M_PWM_CH5) || (PWM_DutCyc2Ch == M_PWM_CH5))
    {   
      /* unmask the interrupt request */
      TMMK04 = 0u;
    }
    else
    {
      /* mask the interrupt request */
      TMMK04 = 1u;        
    }

    /* check if the timer is on the run */    
    if((TE0 & M_TAU_CH4_START_TRG_ON)!=(uint8)0u)
    {
      /* timer is already tirggered */
    }
    else
    {
      /* start the timer */
      TS0 |= M_SETBIT(4);
    }
  }
  else
  {
   /* mask the interrupt request */
    TMMK04 = 1u;    
    /* clear slave channel counter */
    TDR04 = 0u;
  }
}


/***********************************************************
** Function name: Pwm_WrTDR05
** Description: Write value to TDR05 regiseter and start/reset pwm counter.
** Parameter: uint16 slave counter value
** Return value: None
** Remarks: global variables used, side effects
************************************************************/
FUNC(void, PWM_CODE) Pwm_WrTDR05(VAR(uint16, AUTOMATIC) slavecntval)
{
#if(TAU_CH5_AS_PWM == USED)
  /* check for the slave count */
  if(slavecntval != (uint16)0u)
  {
    /* operate the timer */

    /* load the slave channel counter */
    TDR05 = slavecntval;

    /* enable falling edge interrupt if channel5 PWM is either DC1 OR DC2 */
    if((PWM_DutCyc1Ch == M_PWM_CH7) || (PWM_DutCyc2Ch == M_PWM_CH7))
    {   
      /* unmask the interrupt request */
      TMMK05 = 0u;
    }
    else
    {
      /* mask the interrupt request */
      TMMK05 = 1u;        
    }

    /* check if the timer is on the run */
    if((TE0 & M_TAU_CH5_START_TRG_ON)!=(uint8)0u)
    {
      /* timer is already tirggered */
    }
    else
    {
      /* start the timer */
      TS0 |= M_SETBIT(5);
    }
  }
  else
  {
    /* mask the interrupt request */
    TMMK05 = 1u;
    /* clear slave channel counter */
    TDR05 = 0u;
  }
#else
  /* check for the slave count */
  if(slavecntval != (uint16)0u)
  {
    /* operate the timer */

    /* load the slave channel counter */
    TDR05 = slavecntval;

    /* check if the timer is on the run */    
    if((TE0 & M_TAU_CH5_START_TRG_ON)!=(uint8)0u)
    {
      /* timer is already tirggered */
    }
    else
    {
      /* start the timer */
      TS0 |= M_SETBIT(5);
      /* unmask the interrupt request */
      TMMK05 = 0u;      
    }
  }
  else
  {
    /* mask the interrupt request */
    TMMK05 = 1u;
    /* clear slave channel counter */
    TDR05 = 0u;
  }
#endif
}

/***********************************************************
** Function name: Pwm_WrTDR06
** Description: Write value to TDR06 regiseter and start/reset pwm counter.
** Parameter: uint16 slave counter value
** Return value: None
** Remarks: global variables used, side effects
************************************************************/
FUNC(void, PWM_CODE) Pwm_WrTDR06(VAR(uint16, AUTOMATIC) slavecntval)
{
  /* check for the slave count */
  if(slavecntval != (uint16)0u)
  {
    /* operate the timer */

    /* load the slave channel counter */
    TDR06 = slavecntval;
       /* operate the timer */      
       /* enable falling edge interrupt if channel6 PWM is either DC1 OR DC2 */
    if( ((PWM_DutCyc1Ch == M_PWM_CH4) || (PWM_DutCyc2Ch == M_PWM_CH4)) ||
        ((PWM_DutCyc1Ch == M_PWM_CH7) || (PWM_DutCyc2Ch == M_PWM_CH7)) )
    {
          /* unmask the interrupt */
          TMMK06 = 0u;
    }
    else
    {
          /* mask the interrupt request */
          TMMK06 = 1u;        
    }

    /* check if the timer is on the run */    
    if((TE0 & M_TAU_CH6_START_TRG_ON)!=(uint8)0u)
    {
       /* timer is already tirggered */
    }
    else
    {
       /* start the timer */
       TS0 |= M_SETBIT(6);
    }
  }
  else
  {
    /* mask the interrupt request */
    TMMK06 = 1u;
    /* clear slave channel counter */
    TDR06 = 0u;
  }
}
/***********************************************************
** Function name: Pwm_WrTDR07
** Description: Write value to TDR07 regiseter and start/reset pwm counter.
** Parameter: uint16 slave counter value
** Return value: None
** Remarks: global variables used, side effects
************************************************************/
FUNC(void, PWM_CODE) Pwm_WrTDR07(VAR(uint16, AUTOMATIC) slavecntval)
{
  /* check for the slave count */
  if(slavecntval != (uint16)0u)
  {
    /* operate the timer */

    /* load the slave channel counter */
    TDR07 = slavecntval;

    /* enable falling edge interrupt if channel7 PWM is either DC1 OR DC2 */
    if((PWM_DutCyc1Ch == M_PWM_CH2) || (PWM_DutCyc2Ch == M_PWM_CH2))
    {   
      /* unmask the interrupt request */
      TMMK07 = 0u;
    }
    else
    {
      /* mask the interrupt request */
      TMMK07 = 1u;        
    }

    /* check if the timer is on the run */    
    if((TE0 & M_TAU_CH7_START_TRG_ON)!=(uint8)0u)
    {
      /* timer is already tirggered */
    }
    else
    {
      /* start the timer */
      TS0 |= M_SETBIT(7);
    }
  }
  else
  {
    /* mask the interrupt request */
    TMMK07 = 1u;
    /* clear slave channel counter */
    TDR07 = 0u;    
  }
}

/***********************************************************
** Function name: PWM_UpdateDutCycCntIsr
** Description: decrement the duty cycle count ISR
** Parameter:  None
** Return value: None 
** canbe done or not
** Remarks: global variables used, side effects
************************************************************/
INLINE FUNC(void, PWM_CODE) PWM_UpdateDutCycCntIsr(void)
{
   if(PWM_DutCycCntIsr > (uint8)0x00u)
   {
      /* Decrement the count by one */
      PWM_DutCycCntIsr--;  
   }

   /* Check if count is 0 */
   if(PWM_DutCycCntIsr == (uint8)0x00u)
   {
      Adc_SwitchToContinuous();        
   }
   else
   {
      /* update the current sense multiplex input */
      PWM_SelectMuxCurense(PWM_DutCyc2ChIsr);
   }
}

#define PWM_STOP_SEC_CODE
#include "MemMap.h"

#define PWM_START_SEC_ISR_CODE
#include "MemMap.h"
/***************************************************************************************************
** Function                 : Pwm_INTTM00

** Description              : Interrupt function when end of timer channel0 (master) occures
                              Interrupt when Rising edge of PWM duty.

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
__interrupt FUNC(void, PWM_CODE) Pwm_INTTM00(void)
{
  /*this interrupt is rising edge trigger for slave channels */
  /* clear the interrupt flag */
  TMIF00 = 0u;
  
  PWM_DutCycCntIsr = PWM_CurrSnsCnt;
  
  /* copy the channel and channel 2 values to ISR variables */
  PWM_DutCyc1ChIsr = PWM_DutCyc1Ch;
  PWM_DutCyc2ChIsr = PWM_DutCyc2Ch;
  
  /* for multiplexed channels */
  if(PWM_CHANNEL7ACTIVE == PWM_Channle4Select)
  {
     Pwm_SetCH7();
  }
  else if(PWM_CHANNEL4ACTIVE == PWM_Channle4Select)
  {
     Pwm_SetCH4();
  }
  else
  {
     /* do nothing */
  } 
}

/***************************************************************************************************
** Function                 : Pwm_INTTM01

** Description              : Interrupt function when end of timer channel1 (slave) occures

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
__interrupt FUNC(void, PWM_CODE) Pwm_INTTM01(void)
{
  /* clear the interrupt flag */
  TMIF01 = 0u;
  Adc_ReadSingleChnl(Pwm1_AdcRawCurrentIdx);   /* Call the function to read ACT1 current */
  PWM_UpdateDutCycCntIsr(); /* reset the ADC mode */  
}

/***************************************************************************************************
** Function                 : Pwm_INTTM02

** Description              : Interrupt function when end of timer channel2 (slave) occures

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
__interrupt FUNC(void, PWM_CODE) Pwm_INTTM02(void)
{
  /* clear the interrupt flag */
  TMIF02 = 0u;
  /* Call the function to read motor current */
  Adc_ReadSingleChnl(Pwm3_AdcRawCurrentIdx);
  PWM_UpdateDutCycCntIsr();  /* reset the ADC mode */
}

/***************************************************************************************************
** Function                 : Pwm_INTTM03

** Description              : Interrupt function when end of timer channel3 (slave) occures

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
__interrupt FUNC(void, PWM_CODE) Pwm_INTTM03(void)
{
  /* clear the interrupt flag */
  TMIF03 = 0u;
  Adc_ReadSingleChnl(Pwm6_AdcRawCurrentIdx);         /* Call the function to read motor current */
  PWM_UpdateDutCycCntIsr();                                /* reset the ADC mode */

}

/***************************************************************************************************
** Function                 : Pwm_INTTM04

** Description              : Interrupt function when end of timer channel4 (slave) occures

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
__interrupt FUNC(void, PWM_CODE) Pwm_INTTM04(void)
{
  /* clear the interrupt flag */
  TMIF04 = 0u;

  Adc_ReadSingleChnl(Pwm5_AdcRawCurrentIdx);         /* Call the function to read motor current */
  PWM_UpdateDutCycCntIsr();                                /* reset the ADC mode */
}

/***************************************************************************************************
** Function                 : Pwm_INTTM05

** Description              : Interrupt function when end of timer channel5 (slave) occures

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
#if(TAU_CH5_SLAVEINTR == USED)
__interrupt FUNC(void, PWM_CODE) Pwm_INTTM05(void)
{
  /* clear the interrupt flag */
  TMIF05 = 0u;

#if(TAU_CH5_AS_PWM == NOT_USED )
  /* 50% of minimum duty cycle is completed */
  if(PWM_DutCycCntIsr > 0u)
  {
     /* one or more PWM channels are present */
     /* update the current sense multiplex input */
     PWM_SelectMuxCurense(PWM_DutCyc1ChIsr);
     /* switch the ADC to single mode */
     Adc_SwitchToSingle();    
  }
  else
  {
     /* do nothing */
  }
#else  
  Adc_ReadSingleChnl(Pwm7_AdcRawCurrentIdx);   /* Call the function to read motor current */
  PWM_UpdateDutCycCntIsr();                          /* reset the ADC mode */
#endif  

}
#endif

/***************************************************************************************************
** Function                 : Pwm_INTTM06

** Description              : Interrupt function when end of timer channel6 (slave) occures

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
#if(TAU_CH6_SLAVEINTR == USED)
__interrupt FUNC(void, PWM_CODE) Pwm_INTTM06(void)
{
  /* clear the interrupt flag */
  TMIF06 = 0u;
  if(PWM_CHANNEL7ACTIVE == PWM_Channle4Select)
  {
    /* reset ACT CH7 pin */
    Pwm_ReSetCH7();
    
    Adc_ReadSingleChnl(Pwm7_AdcRawCurrentIdx);   /* Call the function to read motor current */
    PWM_UpdateDutCycCntIsr();                          /* reset the ADC mode */ 
  }
  else if(PWM_CHANNEL4ACTIVE == PWM_Channle4Select)
  {
    /* reset ACT CH4 pin */
    Pwm_ReSetCH4();
    
    Adc_ReadSingleChnl(Pwm4_AdcRawCurrentIdx);   /* Call the function to read motor current */
    PWM_UpdateDutCycCntIsr();                          /* reset the ADC mode */ 
  }
}
#endif

/***************************************************************************************************
** Function                 : Pwm_INTTM07

** Description              : Interrupt function when end of timer channel7 (slave) occures

** Parameter                : None

** Return value             : None

** Remarks                  : None
***************************************************************************************************/
#if defined (FULL_VERSION)
__interrupt FUNC(void, PWM_CODE) Pwm_INTTM07(void)
{
  /* clear the interrupt flag */
  TMIF07 = 0u;
  Adc_ReadSingleChnl(Pwm2_AdcRawCurrentIdx);         /* Call the function to read motor current */
  PWM_UpdateDutCycCntIsr();                                /* reset the ADC mode */
}
#endif

#define PWM_STOP_SEC_ISR_CODE
#include "MemMap.h"
