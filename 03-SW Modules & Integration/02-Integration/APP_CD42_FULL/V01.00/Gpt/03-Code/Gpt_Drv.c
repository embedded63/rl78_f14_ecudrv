/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Gpt_Drv.c
** Module name  :
** -------------------------------------------------------------------------------------------------
** Description : Implementes a interval timer to generate interrupt every 1ms.
**
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-xxxxxxx-12107.01 (SW LLD GPT)
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 19/08/2013
** - First release
***************************************************************************************************/

/************************************** Inclusion files *******************************************/
#include "Gpt_Drv.h"
#include "Gpt_Cfg.h"
#include "Isr.h"

/************************** Declaration of local symbol and constants *****************************/

/********************************* Declaration of local macros ************************************/
#define M_GPT_CH_OUTPUT_VALUE_0       (0x0001u<<CHANNEL_USED)
#define M_GPT_CH_OUTPUT_DISABLE       (0x0001u<<CHANNEL_USED)
#define M_GPT_CH_OUTPUT_LEVEL_L       (0x0001u<<CHANNEL_USED)
#define M_GPT_CH_OUTPUT_COMBIN        (0x0001u<<CHANNEL_USED)
#define M_GPT_CH_MODE_INTERVAL_TIMER  (0x0000u)
#define M_GPT_CH_START_TRG_ON         (0x0001u<<CHANNEL_USED)
#define M_GPT_CH_ENABLE_INTERRUPT     (0u)
/********************************* Declaration of local types *************************************/

/******************************* Declaration of local variables ***********************************/

/******************************* Declaration of local constants ***********************************/

/****************************** Declaration of exported variables *********************************/
#define GPT_START_SEC_VAR_NOINIT_32BITS
#include "MemMap.h"
volatile VAR(uint32, GPT_VAR) Gpt_Cntr1ms;      /* 1ms timer for normal operations */
#define GPT_STOP_SEC_VAR_NOINIT_32BITS
#include "MemMap.h"


#define GPT_START_SEC_VAR_NOINIT_BOOLEAN
#include "MemMap.h"
/* 1ms trigger */
/* MISRA RULE 8.10 VIOLATION: SchM_1msTrig is used in diff func. and hence declared globally */
volatile VAR(boolean, GPT_VAR) Gpt_1msTrig;
#define GPT_STOP_SEC_VAR_NOINIT_BOOLEAN
#include "MemMap.h"

/****************************** Declaration of exported constants *********************************/

/***************************************************************************************************
**                                      FUNCTIONS                                                 **
***************************************************************************************************/

/****************************** Internal functions declarations ***********************************/

/*********************************** Function definitions *****************************************/
#define GPT_START_SEC_CODE
#include "MemMap.h"
/***************************************************************************************************
** Function         : Gpt_Init

** Description      : General Purpose Timer driver init routine

** Parameter        : None

** Return value     : None

** Remarks          : None
***************************************************************************************************/
FUNC(void, GPT_CODE) Gpt_Init(void)
{
#if (TIMERRJ_USED == FALSE)
    TAU0EN = 1u;      /* Enable TAU Peripheral*/
    TPS0 = 0x0000u;   /* Selection of (CK00) as Operating clock fmck  i.e fclk/2^0 */
    PIOR |= 0x0001u;  /* enable peripherals TO02,03,04,05,06,07*/

    /* PR1 PR0  Priority level selection */
    Isr_SetPriority(ISR_GPT_PRIOR,ISR_PRIOR_0);

    /* Enable Interrupt Service for Timer channel */
    TMMK06 = M_GPT_CH_ENABLE_INTERRUPT;
    TDR06 = (M_GPT_CLOCK_FREQUENCY - 1u);
    TMR06 =  M_GPT_CH_MODE_INTERVAL_TIMER; /*interval timer mode */
    TO0 &=  ~M_GPT_CH_OUTPUT_VALUE_0;      /* TO0.n Output 0 from TOn pin */
    TOE0 &= ~M_GPT_CH_OUTPUT_DISABLE;      /* disable channel 7 timer output to pin */
    TOL0 &= ~M_GPT_CH_OUTPUT_LEVEL_L;      /* Timer output level to 0 */
    TOM0 &= ~M_GPT_CH_OUTPUT_COMBIN;      /* combination operation mode */

    /* Initialize 1ms counter */
    Gpt_Cntr1ms = (uint32)0;

    TS0  |= M_GPT_CH_START_TRG_ON;  /* operation is enabled (start software trigger is generated) */
#else
    TRJ0EN = 1u; /*Enable clock supplyl to TimerRJ Peripheral*/
    TRJ0 = M_GPT_TIMERELOADVAL; /* Timer RJ Reload Register */
    /* set the TSTART to 1 */
    TRJCR0_bit.no0 = 1u;
    TRJMR0 = 0x00u; /* select clock as fCLK and mode as TIMER MODE */
    /* set the priority of the interrupt */
    Isr_SetPriority(ISR_GPT_PRIOR,ISR_PRIOR_0);
    /* Initialize 1ms counter */
    Gpt_Cntr1ms = (uint32)0;
    TRJMK0 = 0u;    /*Enable interrupt */

#endif

}

/***************************************************************************************************
** Function         : Gpt_Isr1ms

** Description      : Timer ISR, executes every 1ms

** Parameter        : None

** Return value     : None

** Remarks          : None
***************************************************************************************************/
/* MISRA RULE 8.1 VIOLATION: Function 'Gpt_Isr1ms' defined without a prototype in scope since it is
   an interrupt function */
__interrupt FUNC(void, GPT_CODE) Gpt_Isr1ms(void)
{
    /* Increment global 1ms counter */
    Gpt_Cntr1ms++;

    /* trigger for scheduler */
    Gpt_1msTrig = TRUE;

    
}
/***************************************************************************************************
** Function               : Gpt_CalcDiff1ms

** Description            : Calculates the time period difference in 1ms, between provided input and the
                            free running timer

** Parameter  gptMarkVal  : Mark timer value to know the time difference.

** Return value           : uint32 - Time difference in 1ms/bit resolution

** Remarks                : None
***************************************************************************************************/
FUNC(uint32, GPT_CODE) Gpt_GetElapsedTime (VAR(uint32, AUTOMATIC) gptMarkVal)
{
    uint32 retDiff;            /* local to caluclate difference time in 1ms */
    uint32 gptCt1msCpy;        /* local to copy running counter value */

    /* make a copy of the free running 1ms couter */
    gptCt1msCpy = Gpt_Cntr1ms;

    /* has the running counter crossed the 32-bit limit at least once */
    if(gptCt1msCpy < gptMarkVal)
    {
        /* YES: then its an overflow, so take care wiht difference     */
        /* however multiple such overflows will be treated just as one */
        retDiff = gptCt1msCpy + ((uint32)0xFFFFFFFFU - gptMarkVal);
    }
    else
    {
        /* NO: no overflow, normal difference gives time elapsed */
        retDiff = gptCt1msCpy - gptMarkVal;
    }

    /* return time elapsed in 1ms resolution */
    return (retDiff);
}

/***************************************************************************************************
** Function               : Gpt_Get1msTrig

** Description            : Returns Gpt_1msTrig

** Parameter              : None

** Return value           : boolean - 1msTrig

** Remarks                : None
***************************************************************************************************/
inline FUNC(boolean, GPT_CODE) Gpt_Get1msTrig(void)
{
    /* return  Gpt_1msTrig */
    return (Gpt_1msTrig);
}

/***************************************************************************************************
** Function               : Gpt_Reset1msTrig

** Description            : Reset Gpt_1msTrig to FALSE

** Parameter              : None

** Return value           : None

** Remarks                : None
***************************************************************************************************/
inline FUNC(void, GPT_CODE) Gpt_Reset1msTrig(void)
{
    Gpt_1msTrig = FALSE;
}
/***************************************************************************************************
** Function               : Gpt_GetSysTime

** Description            : Outputs the System time in 1ms Intrevel

** Parameter              : None

** Return value           : None

** Remarks                : None
***************************************************************************************************/
FUNC(uint32, GPT_CODE) Gpt_GetSysTime(void)
{
    return(Gpt_Cntr1ms);
}
#define GPT_STOP_SEC_CODE
#include "MemMap.h"
