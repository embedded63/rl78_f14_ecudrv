/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Gpt_Cfg.h
** Module name  : GPT
** -------------------------------------------------------------------------------------------------
** Description : Include file of component Gpt_Drv.c
**
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-xxxxxx-12107.01 (SW LLD GPT)
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 19/08/2013
** - First release
***************************************************************************************************/

/*To avoid multi-inclusions */
#ifndef GPTCFG_H
#define GPTCFG_H
/**************************************** Inclusion files *****************************************/
#include "Std_Types.h"
/************************** Declaration of global symbol and constants ****************************/

/********************************* Declaration of global macros ***********************************/

/********************************* Declaration of global types ************************************/

/****************************** External links of global variables ********************************/

/****************************** External links of global constants ********************************/

/***************************************************************************************************
**                                            FUNCTIONS                                           **
***************************************************************************************************/
#if defined (LIGHT_VERSION)
#define CHANNEL_USED 6
#define TIMERRJ_USED FALSE
#else
#define TIMERRJ_USED TRUE
#endif
#define M_GPT_CLOCK_FREQUENCY  (uint16)20000 /* in KHz i.e x1 freq */
#define M_GPT_TIMERELOADVAL    (uint16)0x3E80 /* Timer reload value corresponding to 16MHz Frequency */

#endif /* GPTCFG_H */
