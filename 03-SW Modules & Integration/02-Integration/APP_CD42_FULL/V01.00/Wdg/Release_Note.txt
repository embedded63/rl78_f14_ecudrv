=============================================================================
                                   FAURECIA
                        Proprietary - Copyright (C) 2012
-----------------------------------------------------------------------------
Release note for WDG driver software module

-----------------------------------------------------------------------------


CHANGES
-------
V01.00 : 19/08/2013
####################
First release version

This release contains: 
   Source code: Wdg_Drv.c/Wdg_Drv.h (V01.00),
   SW LLD (EME-13ST002-12125.01),
   Module tests description (EME-13ST002-12126.01)
