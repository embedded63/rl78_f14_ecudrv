/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Dio_Cfg.h
** Module name  : Digital Input Output
** -------------------------------------------------------------------------------------------------
** Description : Configuration file where Each Port and Pin is configured as input or output.
**               Provides Interface to Driver to read a specific pin or to assert a value on
**               specific pin.Configures the register values for available ports based on
**               requirement
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-11ST001-12101 (SW LLD DIO)
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 03/07/2013
** - First release
**
***************************************************************************************************/

/* To avoid multi-inclusions */
#ifndef DIOCFG_H
#define DIOCFG_H
/**************************************** Inclusion files *****************************************/
#include "Std_Types.h"

/************************** Declaration of global symbol and constants ****************************/
/* configure if a 64 pin is used or a 32 pin is used */
#define PORT_CFG_NOTUSED     (0x00)
#define PORT_CFG_USED        (0x01)
#define HW_CONFIG_LIGHTVER   (0x00)
#define HW_CONFIG_FULLVER    (0x01)
#if defined (FULL_VERSION)
#define HW_CONFIG    HW_CONFIG_FULLVER
#else
#define HW_CONFIG    HW_CONFIG_LIGHTVER
#endif

/* List out all the ports present in the board */
/* Configure port as
   PORT_CFG_NOTUSED if it is not used either as input or output
   PORT_CFG_USED    if it is used as input or output */
#define PORT00_CFG  (PORT_CFG_NOTUSED)
#define PORT01_CFG  (PORT_CFG_USED)
#if(HW_CONFIG == HW_CONFIG_LIGHTVER)
#define PORT02_CFG  (PORT_CFG_NOTUSED)
#endif
#define PORT03_CFG  (PORT_CFG_USED)
#define PORT04_CFG  (PORT_CFG_USED)
#if defined (FULL_VERSION)
#define PORT05_CFG  (PORT_CFG_USED)
#else
#define PORT05_CFG  (PORT_CFG_USED)
#endif
#define PORT06_CFG  (PORT_CFG_USED)
#define PORT07_CFG  (PORT_CFG_USED)
#if(HW_CONFIG == HW_CONFIG_FULLVER)
#define PORT08_CFG  (PORT_CFG_NOTUSED)
#define PORT09_CFG  (PORT_CFG_NOTUSED)
#define PORT10_CFG  (PORT_CFG_NOTUSED)
#define PORT15_CFG  (PORT_CFG_NOTUSED)
#endif
#define PORT12_CFG  (PORT_CFG_NOTUSED)
#define PORT13_CFG  (PORT_CFG_NOTUSED)
#define PORT14_CFG  (PORT_CFG_NOTUSED)


/* Configure the Mode of pins for each used port */
/* Configure if the pins are used as input or output and this will be copied to PMx register */
#define M_PORT01_MODE      (PORT_OUTPUT(1)|PORT_OUTPUT(2))
#define M_PORT03_MODE      (PORT_OUTPUT(2)|PORT_INPUT(1))
#define M_PORT04_MODE      (PORT_INPUT(1))
#define M_PORT05_MODE      (PORT_OUTPUT(0)|PORT_OUTPUT(1)|PORT_OUTPUT(2)|PORT_OUTPUT(3))
#define M_PORT06_MODE      (PORT_INPUT(0)|PORT_OUTPUT(3))
#define M_PORT07_MODE      (PORT_OUTPUT(3)|PORT_OUTPUT(4)|PORT_INPUT(2))

/* Configure the onchip pull up resistor */
#define M_PU0_CONFIG        (uint8)(0x00)
#define M_PU1_CONFIG        (uint8)(0x00)
#if(HW_CONFIG == HW_CONFIG_LIGHTVER)
#define M_PU2_CONFIG        (uint8)(0x00)
#endif
#define M_PU3_CONFIG        (uint8)(0x00)
#define M_PU4_CONFIG        (uint8)(0x01)
#define M_PU5_CONFIG        (uint8)(0x00)
#define M_PU6_CONFIG        (uint8)(0x00)
#define M_PU7_CONFIG        (uint8)(0x00)
#if(HW_CONFIG == HW_CONFIG_FULLVER)
#define M_PU8_CONFIG        (uint8)(0x00)
#define M_PU9_CONFIG        (uint8)(0x00)
#define M_PU10_CONFIG        (uint8)(0x00)
#define M_PU15_CONFIG        (uint8)(0x00)
#endif
#define M_PU12_CONFIG       (uint8)(0x00)
#define M_PU14_CONFIG       (uint8)(0x00)

/* Configure the PMC registers */
/* These registers set the P00, P01, P120, and P147 digital I/O/analog input in 1-bit units.*/
/* 0 for digital i/0 1 for analog. default is analog */
/* If pin is not used as dig i/o, leave it as 0x01 */

#if (PORT00_CFG == PORT_CFG_USED)
#if(HW_CONFIG == HW_CONFIG_LIGHTVER)
#define M_PMC00_CONFIG      0x00
#define M_PMC01_CONFIG      0x01
#endif
#endif

#if(PORT12_CFG == PORT_CFG_USED)
#define M_PMC120_CONFIG      0x01
#endif

#if(PORT14_CFG == PORT_CFG_USED)
#define M_PMC147_CONFIG      0x01
#endif

/* Configure the values for PIMO register */
/* Input mode can be selected as either normal or TTL */
/* TTL input buffer can be selected for serial communication, etc with an external device of the
   different potential.*/
/* Configure input mode register if needed for PORTS 0,1 and 5 */
/* TRUE for TTL input, FALSE for Normal input */

#if(PORT00_CFG == PORT_CFG_USED)
#if(HW_CONFIG == HW_CONFIG_LIGHTVER)
/* for 48 pin F12, PIMO is configurable only for pin 1 */
#define M_PIM0_CONFIG     DIO_PIM(1,FALSE)
#endif
#endif

#if(PORT01_CFG == PORT_CFG_USED)
#if(HW_CONFIG == HW_CONFIG_LIGHTVER)
/* only Pins 3,4,5,6 and 7 are configurable for 48 pin F12 */
#define M_PIM1_CONFIG     DIO_PIM(3,FALSE)+DIO_PIM(4,FALSE)+DIO_PIM(5,FALSE)+ \
                          DIO_PIM(6,FALSE)+DIO_PIM(7,FALSE)
#else
/* for F13, 64 pin 3,4,6,7 are configurable */
#define M_PIM1_CONFIG     DIO_PIM(3,FALSE)+DIO_PIM(4,FALSE)+ \
                          DIO_PIM(6,FALSE)+DIO_PIM(7,FALSE)
#endif
#endif

#if(PORT03_CFG == PORT_CFG_USED)
#if(HW_CONFIG != HW_CONFIG_LIGHTVER)
/* only Pin 0 is configurable  for 64pin F13*/
#define M_PIM3_CONFIG     DIO_PIM(0,FALSE)
#endif
#endif

#if(PORT12_CFG == PORT_CFG_USED)
#if(HW_CONFIG != HW_CONFIG_LIGHTVER)
/* only Pins 5 is configurable for 64pin F13 */
#define M_PIM12_CONFIG     DIO_PIM(5,FALSE)
#endif
#endif

/* Configure the output mode register for ports0,1 5 and 7 */
/* TRUE for N-ch open drain output, FALSE for normal output */
#if(PORT00_CFG == PORT_CFG_USED)
#if(HW_CONFIG == HW_CONFIG_LIGHTVER)
/* for 48 pin, PIMO is configurable only for pin 0 */
#define M_POM0_CONFIG     DIO_POM(0,FALSE)
#endif
#endif

#if(PORT01_CFG == PORT_CFG_USED)
/* only Pins 3,4,5,6 and 7 are configurable */
#define M_POM1_CONFIG     DIO_POM(3,FALSE)+DIO_POM(4,FALSE)+DIO_POM(5,FALSE)+ \
                          DIO_POM(6,FALSE)+DIO_POM(7,FALSE)
#endif
#if(PORT05_CFG == PORT_CFG_USED)
#if(HW_CONFIG == HW_CONFIG_LIGHTVER)
/* only Pin 0 is configurable */
#define M_POM5_CONFIG     DIO_POM(0,FALSE)
#endif
#endif
#if(PORT06_CFG == PORT_CFG_USED)
#if(HW_CONFIG != HW_CONFIG_LIGHTVER)
/* POM6 is configurable only for pins 0,1,2,3,  */
#define M_POM6_CONFIG    DIO_POM(0,FALSE)+DIO_POM(1,FALSE)+DIO_POM(2,FALSE)+ \
                         DIO_POM(3,FALSE)
#endif
#endif
#if(PORT07_CFG == PORT_CFG_USED)
#if(HW_CONFIG == HW_CONFIG_LIGHTVER)
/* POM7 is configurable only for pin 4 */
#define M_POM7_CONFIG     DIO_POM(4,FALSE)
#endif
#endif

#if(PORT12_CFG == PORT_CFG_USED)
#if(HW_CONFIG == HW_CONFIG_LIGHTVER)
/* POM6 is configurable only for pin 0  */
#define M_POM12_CONFIG   DIO_POM(0,FALSE)
#endif
#endif

/* Configure the pull-up resistors */
/* Project Specific inputs */
/* Give PORT and PIN Values in the function DIO_READ_CHANNEL(PORT,PIN) */
#define M_DIO_READCHANNEL01   DIO_READ_CHANNEL(7,2)
#define M_DIO_READCHANNEL02   DIO_READ_CHANNEL(3,1)
#define M_DIO_READCHANNEL03   DIO_READ_CHANNEL(4,1)
#define M_DIO_READCHANNEL04   DIO_READ_CHANNEL(6,0)
/* Configure project specific outputs */
/* Give PORT and PIN Values in the function DIO_WRITE_CHANNEL(PORT,PIN,VALUE) */
#define M_DIO_WRITECHANNEL01(VALUE)   DIO_WRITE_CHANNEL(1,2,(VALUE))
#define M_DIO_WRITECHANNEL02(VALUE)   DIO_WRITE_CHANNEL(1,1,(VALUE))
#define M_DIO_WRITECHANNEL03(VALUE)   DIO_WRITE_CHANNEL(6,3,(VALUE))
#define M_DIO_WRITECHANNEL04(VALUE)   DIO_WRITE_CHANNEL(3,2,(VALUE))
#define M_DIO_WRITECHANNEL05(VALUE)   DIO_WRITE_CHANNEL(5,3,(VALUE))
#define M_DIO_WRITECHANNEL06(VALUE)   DIO_WRITE_CHANNEL(7,4,(VALUE))
#define M_DIO_WRITECHANNEL07(VALUE)   DIO_WRITE_CHANNEL(7,3,(VALUE))
#define M_DIO_WRITECHANNEL08(VALUE)   DIO_WRITE_CHANNEL(5,1,(VALUE))
#define M_DIO_WRITECHANNEL09(VALUE)   DIO_WRITE_CHANNEL(5,2,(VALUE))
#define M_DIO_WRITECHANNEL10(VALUE)   DIO_WRITE_CHANNEL(5,0,(VALUE))

/********************************* Declaration of global macros ***********************************/

/********************************* Declaration of global types ************************************/

/****************************** External links of global variables ********************************/

/****************************** External links of global constants ********************************/

/***************************************************************************************************
**                                            FUNCTIONS                                           **
***************************************************************************************************/

#endif/*DIOCFG_H*/

