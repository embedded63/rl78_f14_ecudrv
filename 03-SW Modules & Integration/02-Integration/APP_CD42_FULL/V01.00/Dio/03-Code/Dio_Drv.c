/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Dio_Drv.c
** Module name  : Digital Input Output
** -------------------------------------------------------------------------------------------------
** Description : Configures uC pins as either output or input based on data configured in Dio_Cfg.h
**               file.
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-11ST001-12101 (SW LLD DIO)
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 03/07/2013
** - First release
**
***************************************************************************************************/

/************************************** Inclusion files *******************************************/

#include "Dio_Drv.h"

/************************** Declaration of local symbol and constants *****************************/

/********************************* Declaration of local macros ************************************/

/********************************* Declaration of local types *************************************/

/******************************* Declaration of local variables ***********************************/

/******************************* Declaration of local constants ***********************************/

/****************************** Declaration of exported variables *********************************/

/****************************** Declaration of exported constants *********************************/

/***************************************************************************************************
**                                      FUNCTIONS                                                 **
***************************************************************************************************/

/****************************** Internal functions declarations ***********************************/
/*********************************** Function definitions *****************************************/
#define DIO_START_SEC_CODE
#include "MemMap.h"
/***************************************************************************************************
** Function         : Dio_Init

** Description      : Configures digital inputs and outputs

** Parameter        : None

** Return value     : None

** Remarks          : None
***************************************************************************************************/
FUNC(void, DIO_CODE) Dio_Init(void)
{
    /* Configure digital outputs and inputs */
#if(PORT00_CFG != PORT_CFG_NOTUSED)
    M_DIO_PORT00_DIRECTION;
    M_DIO_PU0;
#if(HW_CONFIG == HW_CONFIG_LIGHTVER)
/* for 48 pin F12, PIMO is configurable only for pin 1 */
    PIM0 = M_PIM0_CONFIG;
    POM0 = M_POM0_CONFIG;
    PMC0_bit.no0 = M_PMC00_CONFIG;
    PMC0_bit.no1 = M_PMC01_CONFIG;
#endif
#endif

#if(PORT01_CFG != PORT_CFG_NOTUSED)
    M_DIO_PORT01_DIRECTION;
    M_DIO_PU1;
    PIM1 = M_PIM1_CONFIG;
    POM1 = M_POM1_CONFIG;
#endif
#if(HW_CONFIG == HW_CONFIG_LIGHTVER)
#if(PORT02_CFG != PORT_CFG_NOTUSED)
    M_DIO_PORT02_DIRECTION;
    M_DIO_PU2;
#endif
#endif

#if(PORT03_CFG != PORT_CFG_NOTUSED)
    M_DIO_PORT03_DIRECTION;
    M_DIO_PU3;
#if(HW_CONFIG != HW_CONFIG_LIGHTVER)
    PIM3 = M_PIM3_CONFIG;
#endif
#endif

#if(PORT04_CFG != PORT_CFG_NOTUSED)
    M_DIO_PORT04_DIRECTION;
    M_DIO_PU4;
#endif

#if(PORT05_CFG != PORT_CFG_NOTUSED)
    M_DIO_PORT05_DIRECTION;
    /*M_DIO_PU5;*/

#if(HW_CONFIG == HW_CONFIG_LIGHTVER)
    /* Auchtung!! */
    POM5 = M_POM5_CONFIG;    
    /*PIM5 M_PIM5_CONFIG;*/ /* NOT DEFINED */
#endif
#endif

#if(PORT06_CFG != PORT_CFG_NOTUSED)
    M_DIO_PORT06_DIRECTION;
    M_DIO_PU6;
#if(HW_CONFIG != HW_CONFIG_LIGHTVER)
    POM6 = M_POM6_CONFIG;
#endif
#endif

#if(PORT07_CFG != PORT_CFG_NOTUSED)
    M_DIO_PORT07_DIRECTION;
    M_DIO_PU7;
#if(HW_CONFIG == HW_CONFIG_LIGHTVER)    
    POM7 = M_POM7_CONFIG;
#endif
#endif

#if(PORT12_CFG != PORT_CFG_NOTUSED)
    M_DIO_PORT12_DIRECTION;
    M_DIO_PU12;
    PMC12_bit.no0 = M_PMC120_CONFIG;
#endif

#if(PORT14_CFG != PORT_CFG_NOTUSED)
    M_DIO_PORT14_DIRECTION;
    M_DIO_PU14;
    PMC14_bit.no7 = M_PMC147_CONFIG;
#endif

}
#define DIO_STOP_SEC_CODE
#include "MemMap.h"

