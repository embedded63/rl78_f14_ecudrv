/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Dio_Hal.h
** Module name  : Digital Input Output
** -------------------------------------------------------------------------------------------------
** Description : Include file of component Dio_Hal.c
**
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-11ST001-12101 (SW LLD DIO)
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 03/07/2013
** - First release
**
***************************************************************************************************/
/*To avoid multi-inclusions */
#ifndef DIOHAL_H
#define DIOHAL_H
/**************************************** Inclusion files *****************************************/

#include "Std_Types.h"
/************************** Declaration of global symbol and constants ****************************/

/********************************* Declaration of global macros ***********************************/

/********************************* Declaration of global types ************************************/

/****************************** External links of global variables ********************************/
#define DIO_START_SEC_VAR_BOOLEAN
#include "MemMap.h"
extern VAR(boolean, DIO_VAR) Dio_VehCfgBit0;
extern VAR(boolean, DIO_VAR) Dio_VehCfgBit1;
extern VAR(boolean, DIO_VAR) Dio_VehCfgBit2;
extern VAR(boolean, DIO_VAR) Dio_MCUWakeup;
extern VAR(boolean, DIO_VAR) Dio_McuADCSupply;
#define DIO_STOP_SEC_VAR_BOOLEAN
#include "MemMap.h"
/****************************** External links of global constants ********************************/

/***************************************************************************************************
**                                            FUNCTIONS                                           **
***************************************************************************************************/
#define DIO_START_SEC_CODE
#include "MemMap.h"
extern FUNC(void, DIO_CODE) Dio_InitHal(void);
extern FUNC(void, DIO_CODE) Dio_HalIn  (void);
extern FUNC(void, DIO_CODE) Dio_HalOut (void);
extern FUNC(uint8, DIO_CODE) Dio_GetEcuCfg(void);
#define DIO_STOP_SEC_CODE
#include "MemMap.h"

#endif /* DIOHAL_H */
