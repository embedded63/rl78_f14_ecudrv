/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Dio_Drv.h
** Module name  : Digital Input Output
** -------------------------------------------------------------------------------------------------
** Description : Include file of component Dio_Drv.c
**
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-11ST001-12101 (SW LLD DIO)
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 03/07/2013
** - First release
**
***************************************************************************************************/
/*To avoid multi-inclusions */
#ifndef DIODRV_H
#define DIODRV_H
/**************************************** Inclusion files *****************************************/

#include "Std_Types.h"

#include "Dio_Cfg.h"
/************************** Declaration of global symbol and constants ****************************/
/* Status of the ports used/not used */
/* configure if a 64 pin is used or a 32 pin is used */

/* MACROS for configure Digital inputs or outputs and Input enable register */

#if (PORT00_CFG != PORT_CFG_NOTUSED)
#if (HW_CONFIG == HW_CONFIG_LIGHTVER)
/* for 48 pin only PM00 and PM01 are available, remaining bits should always be one */
#define M_DIO_PORT00_DIRECTION       (PM0  =   (uint8)((0xFC) + (M_PORT00_MODE)) )
#else
/* for 64 pin F13 PM00 is only available */
#define M_DIO_PORT00_DIRECTION       (PM0  =   (uint8)((0xFE) + (M_PORT00_MODE)) )
#endif
#define M_DIO_PU0                    (PU0  =   (uint8)(M_PU0_CONFIG) )
#endif

#if(PORT01_CFG != PORT_CFG_NOTUSED)
#define M_DIO_PORT01_DIRECTION       (PM1  =   (uint8)(M_PORT01_MODE))
#define M_DIO_PU1                    (PU1  =   (uint8)(M_PU1_CONFIG) )
#endif

#if(HW_CONFIG == HW_CONFIG_LIGHTVER)
#if(PORT02_CFG != PORT_CFG_NOTUSED)
#define M_DIO_PORT02_DIRECTION       (PM2  =   (uint8)(M_PORT02_MODE))
#define M_DIO_PU2                    (PU2  =   (uint8)(M_PU2_CONFIG) )
#endif
#endif

#if(PORT03_CFG != PORT_CFG_NOTUSED)
#if(HW_CONFIG == HW_CONFIG_LIGHTVER)
/* for PORT03, only P30,P31 are available, remaining bits should always be one */
#define M_DIO_PORT03_DIRECTION       (PM3  =   (uint8)((uint8)(0xFC) + ((uint8)M_PORT03_MODE))  )
#else
/* For F13 64 pin, P30,31,32,33,34 are avaialble, remaining should always be 1*/
#define M_DIO_PORT03_DIRECTION       (PM3  =   (uint8)((uint8)(0xE0) + ((uint8)M_PORT03_MODE))  )
#endif
#define M_DIO_PU3                    (PU3  =   (uint8)(M_PU3_CONFIG) )
#endif

#if(PORT04_CFG != PORT_CFG_NOTUSED)
#if(HW_CONFIG == HW_CONFIG_LIGHTVER)
/* for PORT04,for 48 pin, only P40,P41 are available, remaining bits should always be one */
#define M_DIO_PORT04_DIRECTION       (PM4  =   (uint8)((uint8)(0xFC) + (uint8)(M_PORT04_MODE))  )
#else
/* for PORT04,for 64 pin, only P40,P41,P42,P43 are available, remaining bits should always be one */
#define M_DIO_PORT04_DIRECTION       (PM4  =   (uint8)((uint8)(0xF0) + (uint8)(M_PORT04_MODE))  )
#endif
#define M_DIO_PU4                    (PU4  =   (uint8)(M_PU4_CONFIG) )
#endif

#if(PORT05_CFG != PORT_CFG_NOTUSED)
#if(HW_CONFIG == HW_CONFIG_LIGHTVER)
/* for 48 pin, in PORT05, only P50,P51 are available, remaining bits should always be one */
#define M_DIO_PORT05_DIRECTION       (PM5  =   (uint8)((uint8)(0xFC) + (uint8)(M_PORT05_MODE))  )
#else
/* for F13 64 pin, in PORT05, only P50-P53 are available, remaining bits should always be one */
#define M_DIO_PORT05_DIRECTION       (PM5  =   (uint8)((uint8)(0xF0) + (uint8)(M_PORT05_MODE))  )
#endif
#define M_DIO_PU5                    (PU5  =   (uint8)(M_PU5_CONFIG) )
#endif

#if(PORT06_CFG != PORT_CFG_NOTUSED)
/* for PORT06, only P60-P63 are available, remaining bits should always be one */
#define M_DIO_PORT06_DIRECTION       (PM6  =   (uint8)((uint8)(0xF0) + (uint8)(M_PORT06_MODE))  )
#define M_DIO_PU6                    (PU6  =   (uint8)(M_PU6_CONFIG) )
#endif

#if(PORT07_CFG != PORT_CFG_NOTUSED)
#if(HW_CONFIG == HW_CONFIG_LIGHTVER)
/* for 48 pin, in PORT07, only P70-P75 are available, remaining bits should always be one */
#define M_DIO_PORT07_DIRECTION       (PM7  =   (uint8)((uint8)(0xC0) + (uint8)(M_PORT07_MODE))  )
#else
/* for 64 pin, in PORT07, All 7 pins are avaialble */
#define M_DIO_PORT07_DIRECTION       (PM7  =   (uint8)(M_PORT07_MODE) )
#endif
#define M_DIO_PU7                    (PU7  =   (uint8)(M_PU7_CONFIG) )
#endif

#if(HW_CONFIG != HW_CONFIG_LIGHTVER)
/* Ports 8,9 are only available for 64 pin F13 */
#if(PORT08_CFG != PORT_CFG_NOTUSED)
#define M_DIO_PORT8_DIRECTION       (PM8  =  (uint8)(M_PORT8_MODE) )
#endif
#if(PORT09_CFG != PORT_CFG_NOTUSED)
#define M_DIO_PORT9_DIRECTION       (PM9  =  (uint8)(M_PORT9_MODE) )
#define M_DIO_PU9                   (PU9  =  (uint8)(M_PU9_CONFIG) )
#endif
#endif

#if(PORT12_CFG != PORT_CFG_NOTUSED)
/* for PORT12, only P120 abd P125 are available, remaining bits should always be one */
#define M_DIO_PORT12_DIRECTION       (PM12  =  (uint8)((uint8)(0xDE) + (uint8)(M_PORT12_MODE))  )
#define M_DIO_PU12                   (PU12  =  (uint8)(M_PU12_CONFIG) )
#endif

#if(PORT14_CFG != PORT_CFG_NOTUSED)
#if(HW_CONFIG == HW_CONFIG_LIGHTVER)
/* for 48pin, in PORT14, only P140,P146 and P147 are available, remaining bits should always be one */
#define M_DIO_PORT14_DIRECTION       (PM14  =  (uint8)((uint8)(0x3E) + (uint8)(M_PORT14_MODE))  )
#else
/* for 64pin, in PORT14, only P140 is available, remaining bits should always be one */
#define M_DIO_PORT14_DIRECTION       (PM14  =  (uint8)((uint8)(0xFE) + (uint8)(M_PORT14_MODE))  )
#endif
#define M_DIO_PU14                   (PU14  =  (uint8)(M_PU14_CONFIG) )
#endif

/********************************* Declaration of global macros ***********************************/
/* Bit mask for setting the port number */
#define PORT_OUTPUT(CHANNEL)          ((uint8)(((uint8)0) << (CHANNEL)))
#define PORT_INPUT(CHANNEL)           ((uint8)(((uint8)1) << (CHANNEL)))
#define DIO_PIM(PIN,VALUE)            (uint8)( ((VALUE) << (PIN)))
#define DIO_POM(PIN,VALUE)            (uint8)( ((VALUE) << (PIN)))

/* MISRA Rule violation 19.12 & 19.13: ## pre processor is used to optimize the code and is used in 
more than one instance */
#define DIO_READ_CHANNEL(PORT,PIN)     (uint8)((uint8)((P##PORT) >> (uint8)(PIN)) &(uint8)0x01)
/* Macro to set or clear a paticular pin in a port */
#define DIO_WRITE_CHANNEL(PORT,PIN,VALUE)    ((VALUE) ? (P##PORT |= (uint8)(((uint8)1) << (PIN))) : \
                                                        (P##PORT &=   (uint8)(~(uint8)( ((uint8)1) << (PIN))))  )


 


/********************************* Declaration of global types ************************************/

/****************************** External links of global variables ********************************/

/****************************** External links of global constants ********************************/

/***************************************************************************************************
**                                            FUNCTIONS                                           **
***************************************************************************************************/
#define DIO_START_SEC_CODE
#include "MemMap.h"
extern FUNC(void, DIO_CODE)    Dio_Init           (void);
#define DIO_STOP_SEC_CODE
#include "MemMap.h"
#endif /* DIODRV_H */
