/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Dio_Hal.c
** Module name  : Digital Input Output (HAL)
** -------------------------------------------------------------------------------------------------
** Description : Hardware Absraction Layer for DIO module
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-11ST001-12101 (SW LLD DIO)
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 03/07/2013
** - First release
**
***************************************************************************************************/

/************************************** Inclusion files *******************************************/
#include "Dio_Cfg.h"
#include "Dio_Hal.h"
#include "Dio_Drv.h"
#include "LLSExterns.h"

/************************** Declaration of local symbol and constants *****************************/

/********************************* Declaration of local macros ************************************/

/********************************* Declaration of local types *************************************/

/******************************* Declaration of local variables ***********************************/

/******************************* Declaration of local constants ***********************************/

/****************************** Declaration of exported variables *********************************/

#define DIO_START_SEC_VAR_BOOLEAN
#include "MemMap.h"
VAR(boolean, DIO_VAR) Dio_VehCfgBit0;
VAR(boolean, DIO_VAR) Dio_VehCfgBit1;
VAR(boolean, DIO_VAR) Dio_VehCfgBit2;
VAR(boolean, DIO_VAR) Dio_MCUWakeup;
VAR(boolean, DIO_VAR) Dio_McuADCSupply;
#define DIO_STOP_SEC_VAR_BOOLEAN
#include "MemMap.h"

/****************************** Declaration of exported constants *********************************/

/***************************************************************************************************
**                                      FUNCTIONS                                                 **
***************************************************************************************************/

/****************************** Internal functions declarations ***********************************/

/*********************************** Function definitions *****************************************/
#define DIO_START_SEC_CODE
#include "MemMap.h"
/***************************************************************************************************
** Function         : Dio_InitHal

** Description      : Configures digital inputs and outputs

** Parameter        : None

** Return value     : None

** Remarks          : None
***************************************************************************************************/
FUNC(void, DIO_CODE) Dio_InitHal(void)
{
   /* Initialize the port values. Configure them as input, and output */
   Dio_Init();

   /* Initialize vehicle config bit0 */
   Dio_VehCfgBit0 = FALSE;

   /* Initialize vehicle config bit1 */
   Dio_VehCfgBit1 = FALSE;

   /* Initialize vehicle config bit2 */
   Dio_VehCfgBit2 = FALSE;

   /* Initialize wakeup */
   Dio_MCUWakeup = FALSE;

   /* Initialize all the output pins to 0*/
   /* set the ADC Supply to 1 during initialization */
   Dio_McuADCSupply   = TRUE;

}

/***************************************************************************************************
** Function         : Dio_HalIn

** Description      : Input interfaces to Application

** Parameter        : None

** Return value     : None

** Remarks          : None
***************************************************************************************************/
FUNC(void, DIO_CODE) Dio_HalIn(void)
{
   /* Update Vehicle config bit0 to upper layers */
   if(M_DIO_READCHANNEL01)
   {
      Dio_VehCfgBit0 = TRUE;
   }
   else
   {
      Dio_VehCfgBit0 = FALSE;
   }
   /* Update Vehicle config bit1 to upper layers */
   if(M_DIO_READCHANNEL02)
   {
      Dio_VehCfgBit1 = TRUE;
   }
   else
   {
      Dio_VehCfgBit1 = FALSE;
   }
   /* Update Vehicle config bit2 to upper layers */
   if(M_DIO_READCHANNEL03)
   {
      Dio_VehCfgBit2 = TRUE;
   }
   else
   {
      Dio_VehCfgBit2 = FALSE;
   }
   /* Update Vehicle confic bit0 to upper layers */
   if(M_DIO_READCHANNEL04)
   {
      Dio_MCUWakeup = FALSE;
   }
   else
   {
      Dio_MCUWakeup = TRUE;
   }

}

/***************************************************************************************************
** Function         : Dio_HalOut

** Description      : Output interfaces from Application

** Parameter        : None

** Return value     : None

** Remarks          : None
***************************************************************************************************/
FUNC(void, DIO_CODE) Dio_HalOut(void)
{
   /* ADC Supply */
   /* 0 : Disabled
      1 : Enable */
   M_DIO_WRITECHANNEL04(Dio_McuADCSupply);
   /* Relay power */
   /* 0 : Relay Opened
    1 : Relay Closed */
   M_DIO_WRITECHANNEL05(DIO_GLOBALCMD);
   /* HES sensor command */
   /* 0 : HES diabled
      1 : HES enabled */
   M_DIO_WRITECHANNEL06(DIO_HESFECENABLE);
   /* MCU Keep Alive */
   /* 0 : OFF
    1 :ON */
   M_DIO_WRITECHANNEL07(DIO_MCUKEEPALIVE);
   /* RELAY_R3RP */
   /* 0 : Open
      1 : Close */
   M_DIO_WRITECHANNEL08(DIO_RELAYR3RP);
   /* RELAY_R3COM */
   /* 0 : Open
      1 : Close */
   M_DIO_WRITECHANNEL09(DIO_RELAYR3COM);
   /* RELAY_R3LP */
   /* 0 : Open
      1 : Close */
   M_DIO_WRITECHANNEL10(DIO_RELAYR3LP);
}
/***************************************************************************************************
** Function         : Dio_HalIn

** Description      : ECU Config from individual vehicle config bits

** Parameter        : None

** Return value     : None

** Remarks          : None
***************************************************************************************************/
FUNC(uint8, DIO_CODE) Dio_GetEcuCfg(void)
{
   /* Obtain the ECU Config from individual vehicle config bits */
  VAR(uint8, AUTOMATIC) retval;
  retval = ((uint8)(Dio_VehCfgBit0 << (uint8)2u)+(uint8)(Dio_VehCfgBit1 << (uint8)1u)+(Dio_VehCfgBit2));
  return(retval);
}

#define DIO_STOP_SEC_CODE
#include "MemMap.h"
