/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Isr.c
** Module name  : Interrupt Servcie routine
** -------------------------------------------------------------------------------------------------
** Description : Functions related to Interrupt servcie routine initializationa nd configuration
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-11ST001-12101 (SW LLD ISR)
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 10/09/2013
** - First release
**
***************************************************************************************************/

/************************************** Inclusion files *******************************************/

#include "main.h"
#include "Isr.h"
#include "Cdd_Cfg.h"

/************************** Declaration of local symbol and constants *****************************/

/********************************* Declaration of local macros ************************************/

/********************************* Declaration of local types *************************************/

/******************************* Declaration of local variables ***********************************/

/******************************* Declaration of local constants ***********************************/

/****************************** Declaration of exported variables *********************************/

/****************************** Declaration of exported constants *********************************/

/***************************************************************************************************
**                                      FUNCTIONS                                                 **
***************************************************************************************************/

/****************************** Internal functions declarations ***********************************/
/*********************************** Function definitions *****************************************/
#define ISR_START_SEC_CODE
#include "MemMap.h"
/**************************************************************************************************
** Function         : Isr_SetPriority

** Description      : Sets the priority of the module with the specified value

** Parameter        : Module, Priority

** Return value     : None

** Remarks          : None
**************************************************************************************************/
FUNC(void, ISR_CODE) Isr_SetPriority(VAR(Isr_ModPrior, AUTOMATIC)Module, VAR(Isr_PriorLevel, AUTOMATIC)Prior)
{
   switch(Module)
   {
      case ISR_ADC_PRIOR:
      {
         /* Set priority for SDC interrupts */
         ADPR0 = (uint8)(Prior & (uint8)0x01);
         ADPR1 = (uint8)((uint8)(Prior >> (uint8)0x01) & (uint8)0x01);
      }
      break;
      case ISR_SCHM_PRIOR:
      {
#if defined (FULL_VERSION)
         /* Set priority for Scheduler interrupts */
         SPMPR0 = (uint8)(Prior & (uint8)0x01);
         SPMPR1 = (uint8)((uint8)(Prior >> (uint8)0x01) & (uint8)0x01);
#endif
      }
      break;
#if defined (FULL_VERSION)
      case ISR_CDDHESR3R_PRIOR:
      {
         /* Set priority for CDD Interrupts */
         CDD_PPR0HESR3R = (uint8)(Prior & (uint8)0x01);
         CDD_PPR1HESR3R = (uint8)((uint8)(Prior >> (uint8)0x01) & (uint8)0x01);
      }
      break;
      case ISR_CDDHESR3L_PRIOR:
      {
         /* Set priority for CDD Interrupts */
         CDD_PPR0HESR3L = (uint8)(Prior & (uint8)0x01);
         CDD_PPR1HESR3L = (uint8)((uint8)(Prior >> (uint8)0x01) & (uint8)0x01);
      }
      break;
#endif
      case ISR_GPT_PRIOR:
      {
#if defined (FULL_VERSION)
         /* Set priority for GPT interrupt */
         TRJPR00 = (uint8)(Prior & (uint8)0x01);
         TRJPR10 = (uint8)((uint8)(Prior >> (uint8)0x01) & (uint8)0x01);
#else
         /* Set priority for GPT interrupt */
         TMPR006 = (uint8)(Prior & (uint8)0x01);
         TMPR106 = (uint8)((uint8)(Prior >> (uint8)0x01) & (uint8)0x01);
#endif
      }
      break;
      case ISR_PWMCHANNEL0_PRIOR:
      {
         /* Set priority for Channel 00 interrupt */
         TMPR000 = (uint8)(Prior & (uint8)0x01);
         TMPR100 = (uint8)((uint8)(Prior >> (uint8)0x01) & (uint8)0x01);
      }
      break;
      case ISR_PWMCHANNEL1_PRIOR:
      {
         /* Set priority for Channel 00 interrupt */
         TMPR001 = (uint8)(Prior & (uint8)0x01);
         TMPR101 = (uint8)((uint8)(Prior >> (uint8)0x01) & (uint8)0x01);
      }
      break;
      case ISR_PWMCHANNEL2_PRIOR:
      {
         /* Set priority for Channel 00 interrupt */
         TMPR002 = (uint8)(Prior & (uint8)0x01);
         TMPR102 = (uint8)((uint8)(Prior >> (uint8)0x01) & (uint8)0x01);
      }
      break;
      case ISR_PWMCHANNEL3_PRIOR:
      {
         /* Set priority for Channel 00 interrupt */
         TMPR003 = (uint8)(Prior & (uint8)0x01);
         TMPR103 = (uint8)((uint8)(Prior >> (uint8)0x01) & (uint8)0x01);
      }
      break;
      case ISR_PWMCHANNEL4_PRIOR:
      {
         /* Set priority for Channel 00 interrupt */
         TMPR004 = (uint8)(Prior & (uint8)0x01);
         TMPR104 = (uint8)((uint8)(Prior >> (uint8)0x01) & (uint8)0x01);
      }
      break;
      case ISR_PWMCHANNEL5_PRIOR:
      {
         /* Set priority for Channel 00 interrupt */
         TMPR005 = (uint8)(Prior & (uint8)0x01);
         TMPR105 = (uint8)((uint8)(Prior >> (uint8)0x01) & (uint8)0x01);
      }
      break;
      case ISR_PWMCHANNEL6_PRIOR:
      {
         /* Set priority for Channel 00 interrupt */
         TMPR006 = (uint8)(Prior & (uint8)0x01);
         TMPR106 = (uint8)((uint8)(Prior >> (uint8)0x01) & (uint8)0x01);
      }
      break;
      case ISR_PWMCHANNEL7_PRIOR:
      {
         /* Set priority for Channel 00 interrupt */
         TMPR007 = (uint8)(Prior & (uint8)0x01);
         TMPR107 = (uint8)((uint8)(Prior >> (uint8)0x01) & (uint8)0x01);
      }
      break;
      default:
      {
         /* DO nothing invalid module requested */
      }
      break;
   } /* End of switch */
}
#define ISR_STOP_SEC_CODE
#include "MemMap.h"

