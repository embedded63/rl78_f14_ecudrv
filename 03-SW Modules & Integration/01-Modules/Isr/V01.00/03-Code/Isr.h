/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Isr.h
** Module name  : Interrupt servcie routine
** -------------------------------------------------------------------------------------------------
** Description : Include file of component Isr.c
**
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-11ST001-12101 (SW LLD ISR)
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 10/09/2013
** - First release
**
***************************************************************************************************/
/*To avoid multi-inclusions */
#ifndef ISR_H
#define ISR_H
/**************************************** Inclusion files *****************************************/
#include "Std_Types.h"

/************************** Declaration of global symbol and constants ****************************/
/* Modules which requires to set interrupt priorites */
typedef enum
{
    ISR_ADC_PRIOR = 0u,
    ISR_SCHM_PRIOR,
    ISR_GPT_PRIOR,
    ISR_CDDHESR3L_PRIOR,
    ISR_CDDHESR3R_PRIOR,
    ISR_PWMCHANNEL0_PRIOR,
    ISR_PWMCHANNEL1_PRIOR,
    ISR_PWMCHANNEL2_PRIOR,
    ISR_PWMCHANNEL3_PRIOR,
    ISR_PWMCHANNEL4_PRIOR,
    ISR_PWMCHANNEL5_PRIOR,
    ISR_PWMCHANNEL6_PRIOR,
    ISR_PWMCHANNEL7_PRIOR
}Isr_ModPrior;

/* Interrupt priority levels */
typedef enum
{
    ISR_PRIOR_0 = 0u,
    ISR_PRIOR_1,
    ISR_PRIOR_2,
    ISR_PRIOR_3,
    ISR_PRIOR_4,
    ISR_PRIOR_5
}Isr_PriorLevel;

/********************************* Declaration of global macros ***********************************/

/********************************* Declaration of global types ************************************/

/****************************** External links of global variables ********************************/

/****************************** External links of global constants ********************************/

/***************************************************************************************************
**                                            FUNCTIONS                                           **
***************************************************************************************************/
#define ISR_START_SEC_CODE
#include "MemMap.h"
extern FUNC(void, ISR_CODE) Isr_SetPriority(VAR(Isr_ModPrior, AUTOMATIC) Module,
                                            VAR(Isr_PriorLevel, AUTOMATIC) Prior);
#define ISR_STOP_SEC_CODE
#include "MemMap.h"
#endif /* ISR_H */
