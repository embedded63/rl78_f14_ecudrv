/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Cdd_Cfg.h
** Module name  : Complex device driver
** -------------------------------------------------------------------------------------------------
** Description : configuration file for cdd module
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : (SW LLD CDD)
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 02/09/2013
** - First release
**
***************************************************************************************************/

/* To avoid multi-inclusions */
#ifndef CDDCFG_H
#define CDDCFG_H
/**************************************** Inclusion files *****************************************/
#include "Std_Types.h"

/************************** Declaration of global symbol and constants ****************************/
/* Select the PORT mode bit for CDD Interrupt trigger */
#define CDD_HESR3R_PORTMODEBIT  PM13_bit.no7
#define CDD_HESR3L_PORTMODEBIT  PM7_bit.no1
/* Select port bit */
#define CDD_HESR3R_PORTBIT  P13_bit.no7
#define CDD_HESR3L_PORTBIT  P7_bit.no1

/* select the value for configuration of edge detection */
#define EGP0_CONFIG_RISINGEDGE   0x41u
#define EGN0_CONFIG_RISINGEDGE   0x00u
#define EGP_CLEAR_CONFIG         0x00u
#define EGN_CLEAR_CONFIG         0x00u
#define EGP0_CONFIG_FALLINGEDGE  0x00u
#define EGN0_CONFIG_FALLINGEDGE  0x41u

#define CDD_PPR0HESR3L      PPR06
#define CDD_PPR1HESR3L      PPR16
#define CDD_PPR0HESR3R      PPR01
#define CDD_PPR1HESR3R      PPR11


/********************************* Declaration of global macros ***********************************/

/********************************* Declaration of global types ************************************/

/****************************** External links of global variables ********************************/

/****************************** External links of global constants ********************************/

/***************************************************************************************************
**                                            FUNCTIONS                                           **
***************************************************************************************************/



#endif/*CDDCFG_H*/
