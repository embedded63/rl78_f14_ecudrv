/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Cdd_Drv.c
** Module name  :
** -------------------------------------------------------------------------------------------------
** Description : Implementes the complex device driver software.
**
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-xxxxxxx-12107.01 (SW LLD CDD)
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 28/08/2013
** - First release
***************************************************************************************************/

/************************************** Inclusion files *******************************************/

#include "Cdd_Cfg.h"
#include "Cdd_Drv.h"
#include "Gpt_Drv.h"
#include "LLSExterns.h"
#include "Isr.h"
/************************** Declaration of local symbol and constants *****************************/

/********************************* Declaration of local macros ************************************/

/********************************* Declaration of local types *************************************/

/******************************* Declaration of local variables ***********************************/

/******************************* Declaration of local constants ***********************************/

/****************************** Declaration of exported variables *********************************/
#define CDD_START_SEC_VAR_NOINIT_16BITS
#include "MemMap.h"
volatile VAR(uint16, CDD_VAR) Cdd_HesR2LPulseCount_u16;
volatile VAR(uint16, CDD_VAR) Cdd_HesR2RPulseCount_u16;
#define CDD_STOP_SEC_VAR_NOINIT_16BITS
#include "MemMap.h"

#define CDD_START_SEC_VAR_NOINIT_32BITS
#include "MemMap.h"
volatile VAR(uint32, CDD_VAR) Cdd_HesR2LLastCallTime_u32;
volatile VAR(uint32, CDD_VAR) Cdd_HesR2RLastCallTime_u32;
#define CDD_STOP_SEC_VAR_NOINIT_32BITS
#include "MemMap.h"
/****************************** Declaration of exported constants *********************************/

/***************************************************************************************************
**                                      FUNCTIONS                                                 **
***************************************************************************************************/

/****************************** Internal functions declarations ***********************************/

/*********************************** Function definitions *****************************************/
#define CDD_START_SEC_CODE
#include "MemMap.h"
/***************************************************************************************************
** Function         : Cdd_Init

** Description      : Complex devcie  driver init routine

** Parameter        : None

** Return value     : None

** Remarks          : None
***************************************************************************************************/
FUNC(void, CDD_CODE) Cdd_Init(void)
{
   /* clear the EGPx and EGNx bits */
   EGP0 = EGP_CLEAR_CONFIG;
   EGN0 = EGN_CLEAR_CONFIG;
   /* Configure the ports as input ports */
   /* No need to configure PM13 as it is input by default */
   /* CDD_HESR2L_PORTMODEBIT 0x01u*/
   CDD_HESR3L_PORTMODEBIT = 0x01u;
   /* write a value 0 initially in the port bits */
   CDD_HESR3L_PORTBIT = 0x00u;
   CDD_HESR3R_PORTBIT = 0x00u;
   /* Enable the rising edge for both the interrupts */
   EGP0 = EGP0_CONFIG_RISINGEDGE;
   EGN0 = EGN0_CONFIG_RISINGEDGE;
   /* select the priority of the interrupts */
   Isr_SetPriority(ISR_CDDHESR3L_PRIOR,ISR_PRIOR_2);
   Isr_SetPriority(ISR_CDDHESR3R_PRIOR,ISR_PRIOR_2);
   /* Disable interrupt masks */
   PMK0 = 0x00u;
   PMK6 = 0x00u;
   /* Initialize interrupt flags */
   PIF0 = 0x00u;
   PIF6 = 0x00u;
   /* reset pulse counts */
   Cdd_HesR2LPulseCount_u16 = 0x00u;
   Cdd_HesR2RPulseCount_u16 = 0x00u;
   /* reet snapshot time */
   Cdd_HesR2LLastCallTime_u32 = 0x00u;
   Cdd_HesR2RLastCallTime_u32 = 0x00u;
}
/***************************************************************************************************
** Function         : Cdd_Main

** Description      : Function to reset the position count and snapshot time

** Parameter        : None

** Return value     : None

** Remarks          : None
***************************************************************************************************/
FUNC(void, CDD_CODE) Cdd_Main(void)
{
   /* check if the reset hes1 flag is triggered from upper layers */
  if(CDD_HESR2LRESET == TRUE)
  {
     /* reset the pulse count for HES1 */
     Cdd_HesR2LPulseCount_u16 = 0x00u;
     /* Reset the snapshot time */
     Cdd_HesR2LLastCallTime_u32 = Gpt_Cntr1ms;
  }
  else
  {
     /* No reset is triggered do nothing */

  }

  /* check if the reset hes1 flag is triggered from upper layers */
  if(CDD_HESR2RRESET == TRUE)
  {
     /* reset the pulse count for HES2 */
     Cdd_HesR2RPulseCount_u16 = 0x00u;
     /* Reset the snapshot time */
     Cdd_HesR2RLastCallTime_u32 = Gpt_Cntr1ms;
  }
  else
  {
     /* No reset is triggered do nothing */

  }
}


/***************************************************************************************************
** Function         : Cdd_IsrHesR2L

** Description      : Cdd ISR, executes whenever a rising edge is detected on Port pin

** Parameter        : None

** Return value     : None

** Remarks          : None
***************************************************************************************************/
__interrupt FUNC(void, CDD_CODE) Cdd_IsrHesR3L(void)
{
   VAR(uint32, AUTOMATIC) Cdd_iHesR2LLastCallTime_u32;
   /* copy the value of previous snapshot time to a local variable */
   Cdd_iHesR2LLastCallTime_u32 = Cdd_HesR2LLastCallTime_u32;
   /* Clear the interrupt flag */
   PIF0 = 0x00u;
   /* check if reset is triggered */

   if( CDD_HESR2LRESET == TRUE )
   {
     /* reset the pulse count for HES1 */
     Cdd_HesR2LPulseCount_u16 = 0x00u;
   }
   else
   {
     /* check if previous snapshot time is */
     if(Gpt_Cntr1ms > Cdd_iHesR2LLastCallTime_u32 )
     {
        /* increment pulse count */
        Cdd_HesR2LPulseCount_u16++;
     }
     else
     {
        /* do nothing */
     }
   }
   /* get the snapshot time */
   Cdd_HesR2LLastCallTime_u32 = Gpt_Cntr1ms;
}
/***************************************************************************************************
** Function         : Cdd_IsrHesR2R

** Description      : Cdd ISR, executes whenever a rising edge is detected on Port pin

** Parameter        : None

** Return value     : None

** Remarks          : None
***************************************************************************************************/
__interrupt FUNC(void, CDD_CODE) Cdd_IsrHesR3R(void)
{
   uint32 Cdd_iHesR2RLastCallTime_u32;
   /* copy the value of previous snapshot time to a local variable */
   Cdd_iHesR2RLastCallTime_u32 = Cdd_HesR2RLastCallTime_u32;  
   /* Clear the interrupt flag */
   PIF1 = 0x00u;
   /* check if reset is triggered */

   if( CDD_HESR2RRESET == TRUE )
   {
     /* reset the pulse count for HES1 */
     Cdd_HesR2RPulseCount_u16 = 0x00u;
   }
   else
   {
     if(Gpt_Cntr1ms > Cdd_iHesR2RLastCallTime_u32 )
     {
        /* increment pulse count */
        Cdd_HesR2RPulseCount_u16++;
     }
     else
     {
        /* do nothing */
     }
   }
   /* get the snapshot time */
   Cdd_HesR2RLastCallTime_u32 = Gpt_Cntr1ms;
}
#define CDD_STOP_SEC_CODE
#include "MemMap.h"
