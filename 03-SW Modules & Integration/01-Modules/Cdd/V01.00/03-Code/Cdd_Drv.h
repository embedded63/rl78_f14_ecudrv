/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Cdd_Drv.h
** Module name  : Complex device driver
** -------------------------------------------------------------------------------------------------
** Description : Header file for Cdd_Drv.c
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : (SW LLD CDD)
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 02/09/2013
** - First release
**
***************************************************************************************************/

/* To avoid multi-inclusions */
#ifndef CDDDRV_H
#define CDDDRV_H
/**************************************** Inclusion files *****************************************/
#include "Std_Types.h"

/************************** Declaration of global symbol and constants ****************************/

/********************************* Declaration of global macros ***********************************/

/********************************* Declaration of global types ************************************/

/****************************** External links of global variables ********************************/
#define CDD_START_SEC_VAR_NOINIT_8BITS
#include "MemMap.h"
extern volatile VAR(uint16, CDD_VAR) Cdd_HesR2LPulseCount_u16;
extern volatile VAR(uint16, CDD_VAR) Cdd_HesR2RPulseCount_u16;
#define CDD_STOP_SEC_VAR_NOINIT_8BITS
#include "MemMap.h"

#define CDD_START_SEC_VAR_NOINIT_32BITS
#include "MemMap.h"
extern volatile VAR(uint32, CDD_VAR) Cdd_HesR2LLastCallTime_u32;
extern volatile VAR(uint32, CDD_VAR) Cdd_HesR2RLastCallTime_u32;
#define CDD_STOP_SEC_VAR_NOINIT_32BITS
#include "MemMap.h"
/****************************** External links of global constants ********************************/

/***************************************************************************************************
**                                            FUNCTIONS                                           **
***************************************************************************************************/
#define CDD_START_SEC_CODE
#include "MemMap.h"
extern FUNC(void, CDD_CODE) Cdd_Init(void);
extern FUNC(void, CDD_CODE) Cdd_Main(void);
extern __interrupt FUNC(void, CDD_CODE) Cdd_IsrHesR3L(void);
extern __interrupt FUNC(void, CDD_CODE) Cdd_IsrHesR3R(void);
#define CDD_STOP_SEC_CODE
#include "MemMap.h"


#endif/*CDDDRV_H*/

