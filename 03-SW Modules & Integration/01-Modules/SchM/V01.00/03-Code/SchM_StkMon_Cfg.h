/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : SchM_StkMon_Cfg.h
** Module name  :
** -------------------------------------------------------------------------------------------------
** Description : Include file of component SchM_StkMon.c
**
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-11ST001-12130.02 (SW LLD SchM).doc
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 22/08/2013
** - First release
**
**
***************************************************************************************************/
/*To avoid multi-inclusions */
#ifndef SCHMSTKMON_CFG_H
#define SCHMSTKMON_CFG_H

/**************************************** Inclusion files *****************************************/
#include "Std_Types.h"

/************************** Declaration of global symbol and constants ****************************/


#define STACK_SIZE           (700u)    /* System stack Size */
#define SP_OVERFLOW_ADDR     (0xFE1Eu)   /* System stack overflow address */
#define SCHM_STKFILLPATTERN  (0x55AAu)    /* System stack fill pattern */
#define SP_UNDERRFLOW_ADDR   (SP_OVERFLOW_ADDR - STACK_SIZE)    /* System stack underflow address */


#if defined (FULL_VERSION)
  /* PR1 PR0  Interrutp Priority level selection */
  /*  0   0   Specify level 0 (high priority level) */
  /*  0   1   Specify level 1 */
  /*  1   0   Specify level 2 */
  /*  1   1   Specify level 3 (low priority level) */
#define PR0_VALUE 0u
#define PR1_VALUE 0u
#define ENABLE_INTERRUPT 0u /*0 ENABLE, 1 DISABLE */
#define DISABLE_INTERRUPT 1u /*0 ENABLE, 1 DISABLE */
#define ENABLE_STKMON        (0x80u)    /*80 ENABLE, 00 DISABLE*/
#define DISABLE_STKMON       (0x00u)
#endif
/********************************* Declaration of global macros ***********************************/

/********************************* Declaration of global types ************************************/

/****************************** External links of global variables ********************************/

/****************************** External links of global constants ********************************/

/***************************************************************************************************
**                                            FUNCTIONS                                           **
***************************************************************************************************/


#endif /* SCHMSTKMON_CFG_H */
