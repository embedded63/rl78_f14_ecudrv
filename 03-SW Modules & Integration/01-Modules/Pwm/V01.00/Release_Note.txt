=============================================================================
                                   FAURECIA
                        Proprietary - Copyright (C) 2012
-----------------------------------------------------------------------------
Release note for PWM driver software module

-----------------------------------------------------------------------------


CHANGES
-------
V01.00 : 14/08/2013
####################
First release version

This release contains: 
   Source code: Pwm_Drv.c/Pwm_Drv.h/Pwm_Cfg.h/Pwm_Hal.c/Pwm_Hal.h (V01.10),
   SW LLD (EME-13ST002-12106.01),
   Module tests description (EME-13ST002-12107.01)




