/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Pwm_Hal.c
** Module name  : PWM
** -------------------------------------------------------------------------------------------------
** Description : Hal for PWM module
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-13ST0002-12105.02 (SW LLD PWM)
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 5/08/2013
** - Baseline Created
**
***************************************************************************************************/

/************************************** Inclusion files *******************************************/
#include "Pwm_Hal.h"
#include "Pwm_Drv.h"
#include "Adc_Drv.h"
#include "Dio_Hal.h"
#include "Dio_Drv.h"
#include "LLSExterns.h"
#include "Gpt_Drv.h"

/************************** Declaration of local symbol and constants *****************************/

/********************************* Declaration of local macros ************************************/
#define M_PWM_DUTY2CNT(VALUE) (uint16)((MASTER_VALUE*(uint32)VALUE)/100u)
#define M_PWM_ADJDUTY2CNT(VALUE) (uint16)((MASTER_VALUE*(uint32)VALUE)/100u)
/********************************* Declaration of local types *************************************/

/******************************* Declaration of local variables ***********************************/
#define PWM_START_SEC_VAR_8BITS
#include "MemMap.h"
static VAR(uint8, PWM_VAR) PWM_StCurrSample;
#define PWM_STOP_SEC_VAR_8BITS
#include "MemMap.h"
/******************************* Declaration of local constants ***********************************/
#if defined (FULL_VERSION)
const VAR(uint8, PWM_VAR) PWM_GetAdcIDX[6]= {
                          Pwm1_AdcRawCurrentIdx,Pwm3_AdcRawCurrentIdx,Pwm7_AdcRawCurrentIdx,
                          Pwm5_AdcRawCurrentIdx,Pwm6_AdcRawCurrentIdx,Pwm4_AdcRawCurrentIdx
                          };
#else
const VAR(uint8, PWM_VAR) PWM_GetAdcIDX[3]= {
                          Pwm1_AdcRawCurrentIdx,Pwm3_AdcRawCurrentIdx,Pwm4_AdcRawCurrentIdx
                          };
#endif
/****************************** Declaration of exported variables *********************************/
#define PWM_START_SEC_VAR_8BITS
#include "MemMap.h"
/* The declarations PWMCMD_DutyCycle1..7_u8  should be deleted when integrated with application */
/*
VAR(uint8, PWM_VAR) PWMCMD_DutyCycle1_u8;
VAR(uint8, PWM_VAR) PWMCMD_DutyCycle2_u8;
VAR(uint8, PWM_VAR) PWMCMD_DutyCycle3_u8;
VAR(uint8, PWM_VAR) PWMCMD_DutyCycle4_u8;
VAR(uint8, PWM_VAR) PWMCMD_DutyCycle5_u8;
VAR(uint8, PWM_VAR) PWMCMD_DutyCycle6_u8;
VAR(uint8, PWM_VAR) PWMCMD_DutyCycle7_u8;*/
VAR(uint8, PWM_VAR) PWM_DutCyc1Ch;
VAR(uint8, PWM_VAR) PWM_DutCyc2Ch;
VAR(uint8, PWM_VAR) PWM_CurrSnsCnt;
VAR(uint8, PWM_VAR) PWM_Channle4Select;
#define PWM_STOP_SEC_VAR_8BITS
#include "MemMap.h"


#define PWM_START_SEC_VAR_16BITS
#include "MemMap.h"
VAR(uint16, PWM_VAR) PWM_DutCyc1;
VAR(uint16, PWM_VAR) PWM_DutCyc2;
#define PWM_STOP_SEC_VAR_16BITS
#include "MemMap.h"
/****************************** Declaration of exported constants *********************************/

/***************************************************************************************************
**                                      FUNCTIONS                                                 **
***************************************************************************************************/
/****************************** Internal functions declarations ***********************************/

/*********************************** Function definitions *****************************************/
#define PWM_START_SEC_INIT_CODE
#include "MemMap.h"
/***********************************************************
** Function name: Pwm_HalInit
** Description: Pwm_HalInit
** Parameter index : Description
** Return value: None
** Remarks: global variables used, side effects
************************************************************/
FUNC(void, PWM_CODE) Pwm_HalInit(void)
{
   PWM_StCurrSample = ACT1_CurrentSense;
}
#define PWM_STOP_SEC_INIT_CODE
#include "MemMap.h"

#define PWM_START_SEC_CODE
#include "MemMap.h"
/***********************************************************
** Function name: Pwm_Hal
** Description: SetDutyCycle for all channels
** Parameter: None
** Return value: None
** Remarks: global variables used, side effects
************************************************************/
FUNC(void, PWM_CODE) Pwm_Hal(void)
{
  uint16 tmrreloadval;
  uint16 dutycycles_cnt[7];
  uint8  idx;

  dutycycles_cnt[0] = M_PWM_DUTY2CNT(PWM_DUTYCYCLE1);
  dutycycles_cnt[1] = M_PWM_DUTY2CNT(PWM_DUTYCYCLE2);
  dutycycles_cnt[2] = M_PWM_DUTY2CNT(PWM_DUTYCYCLE3);
  dutycycles_cnt[3] = M_PWM_DUTY2CNT(PWM_DUTYCYCLE4);
  dutycycles_cnt[4] = M_PWM_DUTY2CNT(PWM_DUTYCYCLE5);
  dutycycles_cnt[5] = M_PWM_DUTY2CNT(PWM_DUTYCYCLE6);
  dutycycles_cnt[6] = M_PWM_DUTY2CNT(PWM_DUTYCYCLE7);

  /* PWM_DutCyc1 is the highest duty cycel  and PWM_DutCyc1Ch is respective channel */
  /* PWM_DutCyc2 is the second highest duty cycel  and PWM_DutCyc2Ch is respective channel */
  PWM_DutCyc1 = 0u;
  PWM_DutCyc2 = 0u;
  PWM_DutCyc2Ch = 0u;
  PWM_DutCyc1Ch = 0u;
  PWM_CurrSnsCnt = 0u;

  for (idx = (uint8)0; idx < (uint8)7; idx++)
  {
    if(dutycycles_cnt[idx] > (uint16)0) /* any dutycycle not zero*/
    {
      PWM_CurrSnsCnt++;                 /* increment currrent sense count */
      if(PWM_CurrSnsCnt == (uint8)1)    /* store DC1 and channel */
      {
        PWM_DutCyc1 = dutycycles_cnt[idx];
        PWM_DutCyc1Ch = idx+1u;
      }
      else if(PWM_CurrSnsCnt == (uint8)2) /* store DC2 and channel */
      {
        PWM_DutCyc2 = dutycycles_cnt[idx];
        PWM_DutCyc2Ch = idx+1u;
        break;
      }
    }
  }

  /* both DC1 and DC2 are not zero */
  if(PWM_CurrSnsCnt == 2u)
  {
    if(PWM_DutCyc1 == PWM_DutCyc2) /* DC1 and DC2 are same, modify DC1 to DC1+0.5%,DC2 to DC2 - 0.5%  */
    {
      dutycycles_cnt[PWM_DutCyc1Ch-1u] = PWM_DutCyc1 + M_PWM_ADJDUTY2CNT(DUTCYC_ADJ);
      dutycycles_cnt[PWM_DutCyc2Ch-1u] = PWM_DutCyc2 - M_PWM_ADJDUTY2CNT(DUTCYC_ADJ);
      tmrreloadval = (uint16)dutycycles_cnt[PWM_DutCyc2Ch-1u]>>1u; /* min is DC2 and timer reload value half of DC2 */
    }
    else if(PWM_DutCyc1 > PWM_DutCyc2)
    {
      tmrreloadval = (uint16)PWM_DutCyc2>>1u; /* min is DC2 and timer reload value half of DC2 */
    }
    else
    {
      tmrreloadval = (uint16)PWM_DutCyc1>>1u; /* min is DC1 and timer reload value half of DC2 */
    }
  }
  else if(PWM_CurrSnsCnt == 1u)   /* only DC1 available */
  {
      tmrreloadval = (uint16)PWM_DutCyc1>>1u;
  }
  else
  {
    /* Sample ADC when no PWM is active */

    /* set the mux pin */
    PWM_SelectMuxCurense(PWM_StCurrSample);

    /* sample the ADC */
    Adc_ReadSingleChnl(PWM_GetAdcIDX[PWM_StCurrSample]);
    
    /* switch to continuous mode */
    Adc_SwitchToContinuous();

    /* go to the next channel */
    PWM_StCurrSample++;

    /* reset after last index reached */
    if(PWM_StCurrSample > ACT_Max)
    {
      PWM_StCurrSample = ACT1_CurrentSense;
    }
    tmrreloadval = 0u;
  }

  /* CH4 and CH7 use the same PWM channel, so multiplex them */
  if(dutycycles_cnt[3] != 0u)
  {
     PWM_Channle4Select = PWM_CHANNEL4ACTIVE;
     
     /* set the duty cycle for CH4 */
     Pwm_SetDutcylCH4(dutycycles_cnt[3]);
     
     /* set the pin related for CH7 as logical 0 */
     Pwm_ReSetCH7();
  }
  else if(dutycycles_cnt[6] != 0u)
  {
     PWM_Channle4Select = PWM_CHANNEL7ACTIVE; 

     /* set the duty cycle for CH7 */
     Pwm_SetDutcylCH7(dutycycles_cnt[6]);

     /* set the pin related for CH4 as logical 0 */
     Pwm_ReSetCH4();
  }
  else
  {
     PWM_Channle4Select = 0x00u;
     
     /* set the pin related for CH7 as logical 0 */
     Pwm_ReSetCH4();
     /* set the pin related for CH4 as logical 0 */
     Pwm_ReSetCH7();
  }

  /* set the DIO interface to sample ACT to logical 0 */
  Pwm_SetDutcylCH1(dutycycles_cnt[0]);
  Pwm_SetDutcylCH2(dutycycles_cnt[1]);
  Pwm_SetDutcylCH3(dutycycles_cnt[2]);  
  Pwm_SetDutcylCH5(dutycycles_cnt[4]);
  Pwm_SetDutcylCH6(dutycycles_cnt[5]);

  /* this is needed to load the channel 5 with half of the duty cycle value to get interrupt */
  Pwm_SetTimer(tmrreloadval);
}
/***********************************************************
** Function name: PWM_SelectMuxCurense
** Description: Dio mux to the current sense are updated
** Parameter: uint8 channel 1 current sense
** Return value: None
** Remarks: global variables used, side effects
************************************************************/
INLINE FUNC(void, PWM_CODE) PWM_SelectMuxCurense(VAR(uint8, AUTOMATIC) Pwm_CurrentSense)
{
   switch(Pwm_CurrentSense)
   {
      case ACT1_CurrentSense:
      {
         PWM_SELECTDIOSENSE1;
      }
      break;
      case ACT3_CurrentSense:
      {
         PWM_SELECTDIOSENSE3;
      }
      break;
      case ACT4_CurrentSense:
      {
         PWM_SELECTDIOSENSE4;
      }
      break;

      case ACT5_CurrentSense:
      {
         PWM_SELECTDIOSENSE5;
      }
      break;
      case ACT6_CurrentSense:
      {
         PWM_SELECTDIOSENSE6;
      }
      break;
      case ACT7_CurrentSense:
      {
         PWM_SELECTDIOSENSE7;
      }

      break;
      break;
      default:
      break;
   }
}
#define PWM_STOP_SEC_CODE
#include "MemMap.h"
