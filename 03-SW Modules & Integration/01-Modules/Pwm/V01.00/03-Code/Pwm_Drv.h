/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Pwm_Drv.h
** Module name  :
** -------------------------------------------------------------------------------------------------
** Description : Include file of component Pwm_Drv.c
**
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-13ST0002-12105.02 (SW LLD PWM)
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 05/08/2013
** - Baseline Created
**
***************************************************************************************************/

/*To avoid multi-inclusions */
#ifndef PWMDRV_H
#define PWMDRV_H
/**************************************** Inclusion files *****************************************/
#include "Std_Types.h"
#include "Pwm_Cfg.h"
/************************** Declaration of global symbol and constants ****************************/

/********************************* Declaration of global macros ***********************************/
#define MASTER_VALUE (uint16)((PWM_CLOCK_FREQUENCY*PWM_PULSE_PERIOD)-1u)

#define M_PWM_CH1 (uint8)1
#define M_PWM_CH2 (uint8)2
#define M_PWM_CH3 (uint8)3
#define M_PWM_CH4 (uint8)4
#define M_PWM_CH5 (uint8)5
#define M_PWM_CH6 (uint8)6
#define M_PWM_CH7 (uint8)7


/********************************* Declaration of global types ************************************/

/****************************** External links of global variables ********************************/

/****************************** External links of global constants ********************************/

/***************************************************************************************************
**                                            FUNCTIONS                                           **
***************************************************************************************************/
#define PWM_START_SEC_INIT_CODE
#include "MemMap.h"
extern FUNC(void, PWM_CODE) Pwm_Init(void);
#define PWM_STOP_SEC_INIT_CODE
#include "MemMap.h"

#define PWM_START_SEC_CODE
#include "MemMap.h"
extern INLINE FUNC(void, PWM_CODE) PWM_UpdateDutCycCntIsr(void);
extern FUNC(void, PWM_CODE) Pwm_WrTDR01(VAR(uint16, AUTOMATIC) slavecntval);
extern FUNC(void, PWM_CODE) Pwm_WrTDR02(VAR(uint16, AUTOMATIC) slavecntval);
extern FUNC(void, PWM_CODE) Pwm_WrTDR03(VAR(uint16, AUTOMATIC) slavecntval);
extern FUNC(void, PWM_CODE) Pwm_WrTDR04(VAR(uint16, AUTOMATIC) slavecntval);
extern FUNC(void, PWM_CODE) Pwm_WrTDR05(VAR(uint16, AUTOMATIC) slavecntval);
extern FUNC(void, PWM_CODE) Pwm_WrTDR06(VAR(uint16, AUTOMATIC) slavecntval);
extern FUNC(void, PWM_CODE) Pwm_WrTDR07(VAR(uint16, AUTOMATIC) slavecntval);
#define PWM_STOP_SEC_CODE
#include "MemMap.h"

#define PWM_START_SEC_ISR_CODE
#include "MemMap.h"
extern __interrupt FUNC(void, PWM_CODE) Pwm_INTTM00(void);
extern __interrupt FUNC(void, PWM_CODE) Pwm_INTTM01(void);
extern __interrupt FUNC(void, PWM_CODE) Pwm_INTTM02(void);
extern __interrupt FUNC(void, PWM_CODE) Pwm_INTTM03(void);
extern __interrupt FUNC(void, PWM_CODE) Pwm_INTTM04(void);
extern __interrupt FUNC(void, PWM_CODE) Pwm_INTTM05(void);
extern __interrupt FUNC(void, PWM_CODE) Pwm_INTTM06(void);
extern __interrupt FUNC(void, PWM_CODE) Pwm_INTTM07(void);
#define PWM_STOP_SEC_ISR_CODE
#include "MemMap.h"

#endif /* PWMDRV_H */
