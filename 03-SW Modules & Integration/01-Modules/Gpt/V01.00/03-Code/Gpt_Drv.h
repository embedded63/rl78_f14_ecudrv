/***************************************************************************************************
** Copyright (c) 2011 FAURECIA
**
** This software is the property of Faurecia.
** It can not be used or duplicated without Faurecia authorization.
**
** -------------------------------------------------------------------------------------------------
** File Name    : Gpt_Drv.h
** Module name  :
** -------------------------------------------------------------------------------------------------
** Description : Include file of component Gpt_Drv.c
**
** -------------------------------------------------------------------------------------------------
**
** Documentation reference : EME-xxxxxx-12107.01 (SW LLD GPT)
**
****************************************************************************************************
** R E V I S I O N  H I S T O R Y
****************************************************************************************************
** V01.00 19/08/2013
** - First release
***************************************************************************************************/

/*To avoid multi-inclusions */
#ifndef GPTDRV_H
#define GPTDRV_H
/**************************************** Inclusion files *****************************************/
#include "Std_Types.h"
/************************** Declaration of global symbol and constants ****************************/

/********************************* Declaration of global macros ***********************************/
#define M_GPT_GETSYSTEMTIME            (Gpt_Cntr1ms)             /* returns free running timer value */
/********************************* Declaration of global types ************************************/

/****************************** External links of global variables ********************************/
#define GPT_START_SEC_VAR_NOINIT_32BITS
#include "MemMap.h"
extern volatile VAR(uint32, GPT_VAR) Gpt_Cntr1ms;   /* 1ms timer for normal operations */
#define GPT_STOP_SEC_VAR_NOINIT_32BITS
#include "MemMap.h"
/****************************** External links of global constants ********************************/

/***************************************************************************************************
**                                            FUNCTIONS                                           **
***************************************************************************************************/
#define GPT_START_SEC_CODE
#include "MemMap.h"
extern  FUNC(uint32, GPT_CODE) Gpt_GetElapsedTime (VAR(uint32, AUTOMATIC) gptMarkVal);
extern FUNC(void, GPT_CODE) Gpt_Init(void);
extern inline FUNC(boolean, GPT_CODE) Gpt_Get1msTrig(void);
extern inline FUNC(void, GPT_CODE) Gpt_Reset1msTrig(void);
extern  __interrupt FUNC(void, GPT_CODE) Gpt_Isr1ms (void);
extern FUNC(uint32, GPT_CODE) Gpt_GetSysTime(void);
#define GPT_STOP_SEC_CODE
#include "MemMap.h"
#endif /* GPTDRV_H */
